<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Helpers\Iklan;

Route::get('/','Frontend\PageController@home');

// Auth
Route::get('/login.html', 'Frontend\AuthController@formLogin');
Route::post('/goLogin', 'Frontend\AuthController@login');
Route::get('/register.html', 'Frontend\AuthController@formRegister');
Route::get('/register-success.html', 'Frontend\AuthController@registerSuccess')->middleware('redirectIfUserNotLogin');
Route::get('/email-verification.html', 'Frontend\AuthController@emailVerification')->middleware('redirectIfUserNotLogin');
Route::post('/goRegister', 'Frontend\AuthController@register');
Route::get('/lupa-password.html', 'Frontend\AuthController@formForgot');
Route::post('/goForgot', 'Frontend\AuthController@forgotPassword');

Route::group(['middleware' => 'verifiedOnly'], function() {
    // User Produk
    Route::get('user/edit.html', 'Frontend\UserController@edit');
    Route::get('user-produk.html', 'Frontend\UserProductController@index');
    Route::get('user/tambah-produk.html', 'Frontend\UserProductController@create');
    Route::post('addProductToUser', 'Frontend\UserProductController@addProductToUser');
    Route::get('product/edit/{id}.html', 'Frontend\UserProductController@edit');
    Route::post('edit-produk/{id}','Frontend\UserProductController@update');
    Route::delete('hapus-produk/{id}','Frontend\UserProductController@delete');

    // User Panen
    Route::get('user-panen.html', 'Frontend\UserPanenController@index');
    Route::get('user/tambah-panen.html', 'Frontend\UserPanenController@create');
    Route::post('addPanenToUser', 'Frontend\UserPanenController@store');
    Route::get('edit-panen/{id}.html', 'Frontend\UserPanenController@edit');
    Route::put('updatePanenUser/{id}', 'Frontend\UserPanenController@update');
    Route::delete('deletePanenUser/{id}', 'Frontend\UserPanenController@destroy');

    // User Lahan
    Route::get('user-lahan.html', 'Frontend\UserLahanController@index');
    Route::get('user/tambah-lahan.html', 'Frontend\UserLahanController@create');
    Route::post('addLahanToUser', 'Frontend\UserLahanController@store');
    Route::get('edit-lahan/{id}.html', 'Frontend\UserLahanController@edit');
    Route::put('updateLahanUser/{id}', 'Frontend\UserLahanController@update');
    Route::delete('deleteLahanUser/{id}', 'Frontend\UserLahanController@destroy');

    // User Penjualan
    Route::get('user-penjualan.html', 'Frontend\UserPenjualanController@index');
    Route::get('penjualan/detail/{id}.html', 'Frontend\UserPenjualanController@detail');

    // User Pembelian
    Route::get('user-pembelian.html', 'Frontend\UserPembelianController@index');
    Route::get('pembelian/detail/{id}.html', 'Frontend\UserPembelianController@detail');

    // User Profile
    Route::get('user-profile.html', 'Frontend\UserController@index');
    Route::get('edit-profile.html', 'Frontend\UserController@show');
    Route::post('update-profile-image', 'Frontend\UserController@editImage');
    Route::post('update-profile', 'Frontend\UserController@editProfile');
    Route::get('edit-village', 'Frontend\UserController@showVillage');
    Route::post('update-profile-village', 'Frontend\UserController@editVillage');
    Route::get('edit-password', 'Frontend\UserController@showPassword');
    Route::post('update-profile-password', 'Frontend\UserController@editPassword');

    // User Addresses
    Route::get('create-address', 'Frontend\UserAddressController@create');
    Route::post('store-address', 'Frontend\UserAddressController@store');
    Route::get('show-address/{id}', 'Frontend\UserAddressController@show');
    Route::put('update-address/{id}', 'Frontend\UserAddressController@update');
    Route::delete('delete-address/{id}', 'Frontend\UserAddressController@delete');

    // Cart
    Route::get('cart.html', 'Frontend\OrderController@cart');
    Route::post('addProductToCart', 'Frontend\OrderController@addProductToCart');
    Route::post('updateProductQtyInCart', 'Frontend\OrderController@updateProductQtyInCart');
    Route::delete('deleteProductInCart/{id}', 'Frontend\OrderController@deleteProductInCart');

    // Account
    Route::get('account.html', 'Frontend\OrderController@account');
    Route::get('order/address','Frontend\OrderController@ajaxShowDeliveryAddress');

    // Checkout
    //Route::get('checkout.html', 'Frontend\OrderController@checkoutPage');
    Route::post('addProductToCheckout', 'Frontend\OrderController@addProductToCheckout');
    Route::post('checkout', 'Frontend\OrderController@checkout');

    // Confirm Payment
    Route::get('konfirmasi-pembayaran.html', 'Frontend\OrderController@confirmPaymentPage');
    Route::post('confirmPayment', 'Frontend\OrderController@confirmPayment');
});

// Produk/Store
Route::get('jual-beli.html','Frontend\ProductController@index');
Route::get('jual-beli/{slug}.html','Frontend\ProductController@detail');

// Artikel
Route::get('artikel.html', 'Frontend\ArticleController@index');
Route::get('artikel/{slug}.html', 'Frontend\ArticleController@detail');
Route::get('artikel/kategori/{slug}.html', 'Frontend\ArticleController@articleByCategory');
Route::post('artikel/pencarian-hasil-artikel.html','Frontend\ArticleController@search');
Route::post('postCommentArticle/{id}', 'Frontend\ArticleController@postCommentArticle');

// Panen
Route::get('panen.html','Frontend\HarvestController@index');
Route::get('panen/kategori/{slug}.html', 'Frontend\HarvestController@harvestByCategory');
Route::get('panen/{slug}.html', 'Frontend\HarvestController@detail');
Route::post('panen/pencarian-hasil-panen.html','Frontend\HarvestController@search');
Route::post('postCommentHarvest/{id}', 'Frontend\HarvestController@postCommentHarvest');

// Store/Produk
Route::post('produk/pencarian-hasil-produk.html','Frontend\ProductController@search');
Route::get('jual-beli.html', 'Frontend\ProductController@index');
Route::get('produk.html', 'Frontend\ProductController@product');
Route::get('produk/kategori/{slug}.html', 'Frontend\ProductController@productByCategory');
Route::get('produk/{slug}.html', 'Frontend\ProductController@detail');
Route::post('postCommentProduct/{id}', 'Frontend\ProductController@postCommentProduct');
Route::get('/searchCity', 'Frontend\ProductController@loadData');

Route::get('inquiry','Frontend\PageController@inquiry');

// RAJA ONGKIR
Route::get('/subdistrict-raja-ongkir', 'Frontend\OrderController@loadData');
Route::post('/cost', 'Frontend\OrderController@cost');


// Forum
Route::get('forum.html', 'Frontend\ForumController@index');
Route::get('forum/create.html', 'Frontend\ForumController@create');
Route::get('forum/{slug}.html', 'Frontend\ForumController@detail');
Route::post('forum/pencarian-hasil-forum.html', 'Frontend\ForumController@search');
Route::get('forum/category/{slug}.html', 'Frontend\ForumController@category');
Route::post('forum/store', 'Frontend\ForumController@store');
Route::delete('forum/delete/{slug}', 'Frontend\ForumController@delete');
Route::post('addReply', 'Frontend\ForumController@addReply');

// Harga
Route::get('info-harga.html', 'Frontend\InfoHargaController@index');

// Video
Route::get('video.html','Frontend\VideoController@index');
Route::get('video/{slug}.html','Frontend\VideoController@detail');
Route::post('postCommentVideo/{id}', 'Frontend\VideoController@postCommentVideo');
Route::get('video/kategori/{slug}.html', 'Frontend\VideoController@videoByCategory');
Route::post('video/pencarian-hasil-video.html','Frontend\VideoController@search');

// Desa
Route::get('desa.html','Frontend\VillageController@index');
Route::get('desa/detail.html', 'Frontend\VillageController@detail');
Route::get('desa/{slug}.html', 'Frontend\VillageController@detail');

// Region

// Regency
Route::get('/regency/{id}', 'Frontend\RegionController@regency');

// District
Route::get('/district/{id}', 'Frontend\RegionController@district');

// Village
Route::get('/village/{id}', 'Frontend\RegionController@village');

// Market
Route::get('/market/{id}', 'Frontend\MarketController@market');

// Footer (info-pages)
// Route::get('info-pages/{slug}', 'Frontend\InfoPagesController@index');
Route::get('info-pages/{slug}.html', 'Frontend\InfoPagesController@show');

// Store
Route::get('/store/{slug}.html','Frontend\StoreController@show');
Route::get('/cari', 'Frontend\StoreController@loadData');
Route::get('/store/product-by-etalase/{id}.html','Frontend\StoreController@productByEtalasee');
Route::post('store/public/hasil-pencarian-produk.html','Frontend\StoreController@loadDataProductt');

// Store: Only for Store owners
Route::get('/store.html', 'Frontend\StoreController@index');
Route::get('/store/setting','Frontend\StoreController@setting');
Route::get('/store/setting/info.html','Frontend\StoreController@setting');
Route::get('/store/setting/location.html','Frontend\StoreController@location');
Route::post('/store','Frontend\StoreController@store');
Route::get('/store/location','Frontend\StoreController@showAjax');
Route::put('/store/{idOrSlug}','Frontend\StoreController@update');
Route::put('/store/update-sub-district/{idOrSlug}','Frontend\StoreController@updateSubdistrict');
Route::get('/store/setting/{slug}/products.html','Frontend\StoreController@sellerProducts');
Route::get('/store/setting/create.html','Frontend\StoreController@create');
Route::get('/store/setting/courier.html','Frontend\StoreController@courier');
Route::get('/changeActiveCourier','Frontend\StoreController@changeActiveCourier');
Route::post('/addCourier','Frontend\StoreController@AddCourier');
Route::get('store/seller/etalase/{id}.html', 'Frontend\StoreController@productByEtalase');
Route::post('store/hasil-pencarian-produk.html','Frontend\StoreController@loadDataProduct');

// Etalase
Route::get('/store/{slug}/etalase.html', 'Frontend\EtalaseController@index');
Route::get('/etalase/create.html', 'Frontend\EtalaseController@create');
Route::post('/etalase', 'Frontend\EtalaseController@store');
Route::post('/etalaseAjax', 'Frontend\EtalaseController@storeAjax');
Route::put('/etalase/{id}', 'Frontend\EtalaseController@update');
Route::delete('etalase/{id}', 'Frontend\EtalaseController@destroy');

// Gv Wallets
Route::get('/store/{slug}/bankaccounts.html', 'Frontend\BankAccountsController@index');
Route::get('/store/setting/transactionHistories.html', 'Frontend\BankAccountsController@transactionHistories');
Route::post('/addAccount', 'Frontend\BankAccountsController@addAccount');
Route::get('/bankaccounts/{id}', 'Frontend\BankAccountsController@detailAccount');
Route::put('/bankaccounts/update/{id}', 'Frontend\BankAccountsController@updateAccount');
Route::delete('/bankaccounts/delete/{id}', 'Frontend\BankAccountsController@deleteAccount');

// Activate Account
Route::get('/activate/{code}', 'Frontend\AuthController@activate');

// Footer Info Page
Route::get('/page/{slug}.html', 'Backend\InfoPageController@show');

Auth::routes();
Route::Resource('user','UserController');
Route::Resource('role','RoleController');
Route::Resource('permission','PermissionController');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/sendLocation', 'BackEnd\MarketController@sendLocation');

Route::prefix('admin')->group(function () {
    Route::resource('category','Backend\CategoryController');
    Route::post('article/{id}','Backend\ArticleController@update');
    Route::resource('article','Backend\ArticleController');
    Route::resource('market', 'Backend\MarketController');
    Route::get('price/commodities', 'Backend\PriceController@commodities');
    Route::get('price/regencies', 'Backend\PriceController@regencies');
    Route::resource('price', 'Backend\PriceController');
    Route::resource('commodity', 'Backend\CommodityController');
    Route::resource('video','Backend\VideoController');
    Route::resource('product','Backend\ProductController');
    Route::resource('transaction', 'Backend\TransactionController');
    Route::resource('banner', 'Backend\BannerController');
    Route::resource('infopage', 'Backend\InfoPageController');
    Route::resource('user','Backend\UserController');
    Route::get('harvest/trashed', 'Backend\HarvestController@trash');
    Route::get('harvest/restore/{id}', 'Backend\HarvestController@restore');
    Route::delete('harvest/delPermanent/{id}', 'Backend\HarvestController@delPermanent');
    Route::resource('harvest', 'Backend\HarvestController');
    Route::get('user-admin/trashed', 'Backend\AdminController@trash');
    Route::get('user-admin/restore/{id}', 'Backend\AdminController@restore');
    Route::delete('user-admin/delete-permanent/{id}', 'Backend\AdminController@delPermanent');
    Route::resource('user-admin', 'Backend\AdminController');
    Route::get('iklan/trashed', 'Backend\IklanController@trash');
    Route::get('iklan/restore/{id}', 'Backend\IklanController@restore');
    Route::delete('iklan/delPermanent/{id}', 'Backend\IklanController@delPermanent');
    Route::post('iklan/{id}', 'Backend\IklanController@update');
    Route::resource('iklan', 'Backend\IklanController');
    Route::get('units', 'Backend\UnitsController@index');
    Route::post('units/create', 'Backend\UnitsController@store');
    Route::get('units/{id}', 'Backend\UnitsController@show');
    Route::put('units/{id}', 'Backend\UnitsController@update');
    Route::delete('units/{id}', 'Backend\UnitsController@destroy');
    Route::get('login','Backend\AuthController@formlogin');
    Route::post('login','Backend\AuthController@login');
    Route::post('logout','Backend\AuthController@logout');
});

Route::get('payment','Frontend\orderController@webview');
Route::get('android-back-after-payment',function()
{
    return view('frontend.android-back-after-payment');
});



Route::get('getip',function()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
    {
        $ip_address = $_SERVER['HTTP_CLIENT_IP'];
    }
    //whether ip is from proxy
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
    {
        $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    //whether ip is from remote address
    else
    {
        $ip_address = $_SERVER['REMOTE_ADDR'];
    }
    //return  $ip_address;
    return response()->json(['id'=>$ip_address],200);
});


// contoh pemanggilan data wilayah
Route::get('test','HomeController@test');

Route::post('rajaongkir/cost','Frontend\RajaOngkirController@cost');
