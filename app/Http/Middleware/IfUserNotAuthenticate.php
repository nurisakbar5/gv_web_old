<?php

namespace App\Http\Middleware;

use Closure;

class IfUserNotAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session('id')==null)
        {
            return redirect('/login.html?next=' . urlencode($request->fullUrl()))->with('message','Anda harus login terlebih dahulu.');
        }
        return $next($request);
    }
}
