<?php

namespace App\Http\Middleware;

use Closure;

class VerifiedOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session('id') && !session('verified'))
            return redirect('/email-verification.html');

        return $next($request);
    }
}
