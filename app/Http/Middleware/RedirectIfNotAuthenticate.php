<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfNotAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session('admin_id')==null)
        {
            return redirect('admin/login')->with('message','Anda Harus Login Dulu');
        }
        
        return $next($request);
    }
}
