<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function test(\App\Repositories\WebService\RegionRepository $region)
    {
        $data['province'] = $region->getAllProvince();
        // id province 11 = aceh
        $data['regency']  = $region->getAllRegency(11);

        // id kabupaten aceh tamiang  = 1114 
        $data['district'] = $region->getAllDistrict(1114);

        // id kecamatan banda mulia = 1114061

        $data['villages'] = $region->getAllVillage(1114061);
        dd($data);
        //dd($region->getAllRegency(11));
    }
}
