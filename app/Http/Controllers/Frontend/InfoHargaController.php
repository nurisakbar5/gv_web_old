<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\WebService\RegionRepository;
use App\Repositories\WebService\PriceRepository;
Use App\Repositories\WebService\ServiceRepository;
use App\Repositories\WebService\InfoPageRepository;

class InfoHargaController extends Controller
{
    protected $infoPageRepository;
    public function __construct(InfoPageRepository $infoPageRepository)
    {
        $this->infoPageRepository = $infoPageRepository;
    }

	public function index(Request $request, RegionRepository $regionRepo, PriceRepository $priceRepo, ServiceRepository $serviceRepo)
	{
		// mendapatkan data kabupaten dari web service
		//$reqlocationRegency = $serviceRepo->getLocationRegency();

		//$period = date('Ymd', strtotime($request->period)) ?? date('Ymd');



		if(isset($request->period))
		{
			$period  = date_format(date_create($request->period),"Ymd") ;
		}else
		{
			$period = date('Ymd');
		}

		if(isset($request->regency))
		{
			$regencyId =  $request->regency;
		}else
		{
			//$regencyId 			= $reqlocationRegency->data->id;
			$regencyId	= '3204';
		}

		//$data['regency']					= $regencyRepo->getRegencyById($regencyId);

		$data['provinces']                  = $regionRepo->getAllProvince();
		$data['prices']	                    = $priceRepo->getPriceByRegency($regencyId, $period);
        $data['regencyDetail']				= $regionRepo->getRegencyDetail($regencyId);

		return view('frontend.info-harga.index',$data);
	}
}
