<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\EtalaseRepository;
use App\Repositories\WebService\StoreRepository;

class EtalaseController extends Controller
{

    protected $EtalaseRepo;
    protected $StoreRepo;

    public function __construct(EtalaseRepository $EtalaseRepo, StoreRepository  $StoreRepo)
    {
        $this->middleware('redirectIfUserNotLogin');
        $this->EtalaseRepo = $EtalaseRepo;
        $this->StoreRepo = $StoreRepo;
    }

    public function index(Request $request, $slug)
    {
        $id_user = $request->session()->get('id');
        // $slug = $request->session()->get('store_id');
        // dd($id_user);
        $data['etalases'] = $this->EtalaseRepo->etalase($slug);
        $data['store'] = $this->StoreRepo->getStoreInfo($id_user);
        // dd($data['etalases']);
        return view('frontend.etalase.index', $data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
    		'store_id' => 'required',
    		'name' => 'required'
        ]);

        $response = $this->EtalaseRepo->create($request);
        // dd($response);
        if ($response->success==true) {
            return \Redirect::back()->with('message',$response->message);
        }else{
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    public function storeAjax(Request $request)
    {
        $response = $this->EtalaseRepo->createWithAjax($request);
        return response()->json($response);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
    		'name' => 'required'
        ]);

        $response = $this->EtalaseRepo->update($request, $id);
        // dd($response);
        if ($response->status=="success") {
            return \Redirect::back()->with('message',$response->message);
        }else{
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    public function destroy($id)
    {
        $response = $this->EtalaseRepo->destroy($id);
        if(isset($response->status)=="success")
        {
            return \Redirect::back()->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->with('message', "Delete Data Failed, Something Wrong");
        }
    }
}
