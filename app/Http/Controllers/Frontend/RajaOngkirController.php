<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\CourierRepository;



class RajaOngkirController extends Controller
{
    protected $courRepo;

    function __construct(CourierRepository $courRepo)
    {
        $this->middleware('redirectIfUserNotLogin');
        $this->courRepo = $courRepo;
    }


    // menampilkan service berdasarkan kurir
    function cost(Request $request)
    {
        $data['result'] = $this->courRepo->cost($request);
        $data['store_id'] = $request->store_id;
        return view('frontend.show_courier_service',$data);
    }
}
