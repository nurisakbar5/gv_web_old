<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\UserRepository;
use App\Repositories\WebService\UserPembelianRepository;
use App\Repositories\WebService\OrderRepository;
use App\Repositories\WebService\InfoPageRepository;

class UserPembelianController extends Controller
{
    protected $userRepo;
    protected $userPembelianRepo;
    protected $infoPageRepository;

    public function __construct(UserRepository $userRepo, UserPembelianRepository $userPembelianRepo, InfoPageRepository $infoPageRepository)
    {
        $this->middleware('redirectIfUserNotLogin');
        $this->userRepo = $userRepo;
        $this->userPembelianRepo = $userPembelianRepo;
        $this->infoPageRepository = $infoPageRepository;
    }

    public function index(Request $request)
    {
        $token = $request->session()->get('token');

        $data['profile'] = $this->userRepo->getUserProfileByToken($token);
        $data['pembelians'] = $this->userPembelianRepo->getUserPembelianByToken($token);

        return view('frontend.user.pembelian.pembelian', $data);
    }

    public function detail(Request $request, $id, OrderRepository $orderRepo)
    {
        $token = $request->session()->get('token');

        $data['profile'] = $this->userRepo->getUserProfileByToken($token);
        //$data['detail'] = $this->userPembelianRepo->getUserDetailPembelian($id);
        //dd($data);

        $data['detail'] = $orderRepo->getTransactionDetail($id);

        return view('frontend.user.pembelian.detail', $data);
    }
}
