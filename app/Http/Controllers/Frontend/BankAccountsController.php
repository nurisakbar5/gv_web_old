<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\BankAccountsRepository;
use App\Repositories\WebService\StoreRepository;

class BankAccountsController extends Controller
{
    protected $storeRepo, $bankAccountRepo;

    public function __construct(StoreRepository $storeRepo, BankAccountsRepository $bankAccountRepo)
    {
        $this->storeRepo = $storeRepo;
        $this->bankAccountRepo = $bankAccountRepo;
    }

    public function index(Request $request)
    {
        $id_user = $request->session()->get('id');
        $data['store'] = $this->storeRepo->getStoreInfo($id_user);
        $id_store = $data['store']->data->id;
        $data['accounts'] = $this->bankAccountRepo->getBankAccount($id_store);
        // $id = $data['accounts']->data->id;
        $data['banks'] = $this->bankAccountRepo->allBankAccount();
        // $data['detail'] = $this->bankAccountRepo->detail($id)->data;
        // dd($response);
        // dd( $data['accounts']);
        return view('frontend.store.seller-gv-wallets', $data);
    }

    public function transactionHistories(Request $request)
    {
        $id_user = $request->session()->get('id');
        $data['store'] = $this->storeRepo->getStoreInfo($id_user);
        return view('frontend.store.seller-transaction-histories', $data);
    }

    public function addAccount(Request $request){
        $this->validate($request, [
            'store_id'=>'required',
            'bank_id'=>'required',
            'account_number'=>'required',
            'account_name'=>'required'
        ]);

        $response = $this->bankAccountRepo->create($request);
        if (isset($response->status)==true) {
            return \Redirect::back()->with('message', $response->message);
        }else{
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    public function detailAccount($id)
    {
        $response = $this->bankAccountRepo->detail($id)->data;
        return response()->json([
            'error' => false,
            'detail'=> $response,
        ], 200);
        // return view('frontend.store.seller-gv-wallets', $data);
    }

    public function updateAccount(Request $request, $id){
        $this->validate($request, [
            'bank_id'=>'required',
            'account_number'=>'required',
            'account_name'=>'required'
        ]);

        $response = $this->bankAccountRepo->update($request, $id);
        // if (isset($response->status)==true) {
        //     return \Redirect::back()->with('message', $response->message);
        // }else{
        //     return \Redirect::back()->withErrors($response)->withInput($request->all());
        // }
        return response()->json([
            'error' => false,
            'detail'=> $response,
        ], 200);
    }

    public function deleteAccount($id)
    {
        $response = $this->bankAccountRepo->delete($id);

        // if(isset($response->success)==true)
        // {
        //     return \Redirect::back()->with('message', $response->message);
        // }else
        // {
        //     return \Redirect::back()->with('message', $response->message);
        // }
        return response()->json([
            'error' => false,
            'detail'=> $response,
        ], 200);
    }
}
