<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\UserRepository;
use App\Repositories\WebService\UserPenjualanRepository;
use App\Repositories\WebService\OrderRepository;
use App\Repositories\WebService\InfoPageRepository;

class UserPenjualanController extends Controller
{
	protected $userRepo;
	protected $userPenjualanRepo;
    protected $infoPageRepository;

    public function __construct(UserRepository $userRepo, UserPenjualanRepository $userPenjualanRepo, InfoPageRepository $infoPageRepository)
    {
        $this->middleware('redirectIfUserNotLogin');
        $this->userRepo = $userRepo;
        $this->userPenjualanRepo = $userPenjualanRepo;
        $this->infoPageRepository = $infoPageRepository;
    }

    public function index(Request $request)
    {
        $token = $request->session()->get('token');
        $id = $request->session()->get('id');

        $data['profile'] = $this->userRepo->getUserProfileByToken($token);
        $data['penjualans'] = $this->userPenjualanRepo->getUserPenjualanByToken($token);

        return view('frontend.user.penjualan.penjualan', $data);
    }

    public function detail(Request $request, $id, OrderRepository $orderRepo)
    {
    	$token = $request->session()->get('token');

        $data['profile'] = $this->userRepo->getUserProfileByToken($token);
        // $data['detail'] = $this->userPenjualanRepo->getUserDetailPenjualan($id);

        $data['detail'] = $orderRepo->getTransactionDetail($id);
        // dd($data['detail']);

        return view('frontend.user.penjualan.detail', $data);
    }
}
