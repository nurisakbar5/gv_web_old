<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\PageRepository;
use App\Repositories\WebService\VillageRepository;
use App\Repositories\WebService\IklanRepository;
use App\Repositories\WebService\ArticleRepository;
use App\Repositories\WebService\RegionRepository;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;
use App\Repositories\WebService\InfoPageRepository;

class VillageController extends Controller
{

    protected $infoPageRepository;
    public function __construct(InfoPageRepository $infoPageRepository)
    {
        $this->infoPageRepository = $infoPageRepository;
    }

    public function index(IklanRepository $i, ArticleRepository $a, RegionRepository $r, PageRepository $v)
    {
        /*
        $data['villages'] = $pageRepo->PageVillage();
        */
        $data['iklans']     = $i->getAllIklan();
        $data['populars']   = $a->getArticlePopular(0,4);
        $data['provinces']  = $r->getAllProvince();
        $data['villages']   = $r->getAllVillage('1114061');

        // dd($data['villages']);

        //return $data;
        return view('frontend.village.index', $data);
    }

    public function detail()
    {
        /*
        $reqVillageDetail = $r->getVillageBySlug($slug);

        if ($reqVillageDetail->success==true)
        {
            $data['village'] = $reqVillageDetail->data;

            // SEO Detail Desa
            SEOMeta::setTitle($data['village']->name);
            //SEOMeta::setDescription(str_limit(strip_tags($data['village']->village), 200, '...'));
            //SEOMeta::setKeywords($data['village']->category->name); // Keywords tambah tags lagi

            OpenGraph::setTitle($data['village']->name);
            //OpenGraph::setDescription(str_limit(strip_tags($data['village']->village), 200, '...'));
            OpenGraph::addProperty('locale', 'id_ID');
            OpenGraph::addProperty('locale:alternate', ['id_ID', 'en-us']);
            OpenGraph::addProperty('type', 'website');
            OpenGraph::addProperty('image', $data['village']->photos);
            OpenGraph::addProperty('image:type', 'image/jpeg/jpg/png');
            OpenGraph::addProperty('image:width', '');
            OpenGraph::addProperty('image:height', '');
            OpenGraph::addProperty('image:alt', '');

            TwitterCard::setTitle($data['village']->name);
            TwitterCard::setSite('@globalvillage');

            JsonLd::setTitle($data['village']->name);
            //JsonLd::setDescription(str_limit(strip_tags($data['village']->village), 200, '...'));
            JsonLd::setType('Website');
            JsonLd::addImage($data['village']->photos);

            return view('frontend.village.detail', $data);
        } else
        {
            return view('frontend.404');
        }*/

        return view('frontend.village.detail');
    }
}
