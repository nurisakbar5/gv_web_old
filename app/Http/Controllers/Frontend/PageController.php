<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\PageRepository;
use App\Repositories\WebService\InfoPageRepository;

class PageController extends Controller
{
    protected $pageRepository;
    protected $infoPageRepository;

    function __construct(PageRepository $pageRepository, InfoPageRepository $infoPageRepository)
    {
        $this->pageRepository = $pageRepository;
        $this->infoPageRepository = $infoPageRepository;
    }

    public function home()
    {
        $data['homeData'] =  $this->pageRepository->pageHome();
        $data['infoPages'] = $this->infoPageRepository->getAllInfoPage();
        // dd($data['infoPages']);
        $data['infoPages2'] = $this->infoPageRepository->getAllInfoPage2();
        return view('frontend.home',$data);
    }

    public function inquiry(\App\Repositories\WebService\OrderRepository $orderRepo)
    {

        $invoice = "a";
        if($invoice=='a')
        {
            $string = "0;Success;TX151028132832A001;50000.00;IDR;Payment For Me;28/10/2015 13:28:32";
        }else
        {
            $string = "1;Invalid Order Id;;;;;";
        }

        return $string;
    }
}
