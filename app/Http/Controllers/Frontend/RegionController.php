<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\WebService\RegionRepository;

class RegionController extends Controller
{
	public function regency($id, RegionRepository $regionRepo)
	{	
		return $regionRepo->getAllRegency($id);
	}

	public function district($id, RegionRepository $regionRepo)
	{
		return $regionRepo->getAllDistrict($id);
	}

	public function village($id, RegionRepository $regionRepo)
	{
		return $regionRepo->getAllVillage($id);
	}
}
