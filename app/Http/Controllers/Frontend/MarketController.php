<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\MarketRepository;

class MarketController extends Controller
{
    protected $marketRepo;

    function __construct(MarketRepository $marketRepo)
    {
        $this->marketRepo = $marketRepo;
    }

    public function market($id)
	{
		return $this->marketRepo->getMarketByRegency($id);
	}
}
