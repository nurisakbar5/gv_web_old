<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\UserRepository;
use App\Repositories\WebService\PageRepository;
use App\Repositories\WebService\CategoryRepository;
use App\Repositories\WebService\HarvestRepository;
use App\Repositories\WebService\LandRepository;
use App\Repositories\WebService\InfoPageRepository;

class UserPanenController extends Controller
{
    protected $infoPageRepository;
    public function __construct(HarvestRepository $harvestRepo, UserRepository $userRepository,CategoryRepository $categoryRepo, InfoPageRepository $infoPageRepository)
    {
        $this->middleware('redirectIfUserNotLogin');
        $this->harvestRepo = $harvestRepo;
        $this->userRepository = $userRepository;
        $this->categoryRepo = $categoryRepo;
        $this->infoPageRepository = $infoPageRepository;
    }

    public function index(Request $request)
    {
        $token = $request->session()->get('token');

        $data['profile'] = $this->userRepository->getUserProfileByToken($token);
        $data['panens'] = $this->userRepository->getUserPanenByToken($token);


        return view('frontend.user.panen.index', $data);
    }

    public function create(Request $request)
    {
        $token = $request->session()->get('token');

        $data['categories'] = $this->categoryRepo->getCategoryByEntity('harvest');
        $data['lands'] = $this->userRepository->getUserLahanByToken($token);
        $data['units'] = $this->harvestRepo->getAllUnit();

        return view('frontend.user.panen.create', $data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title'             => 'required',
            'description'       => 'required',
            'token'             => 'required',
            'land_id'           => 'required',
            'category_id'       => 'required',
            'estimated_date'    => 'required',
            'estimated_income'  => 'required',
            'unit_id'           => 'required',
            'image_1'           => 'required',
            'image_2'           => 'required',
        ]);

        $response = $this->harvestRepo->create($request);
        if(isset($response->status)==true)
        {
            return redirect('/user-panen.html')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    public function edit($id, Request $request)
    {
        $token = $request->session()->get('token');

        $data['panen'] = $this->harvestRepo->getHarvestById($id);
        $data['lands'] = $this->userRepository->getUserLahanByToken($token);
        $data['units'] = $this->harvestRepo->getAllUnit();
        $data['categories'] = $this->categoryRepo->getCategoryByEntity('harvest');
        // dd($data['panen']);

        return view('frontend.user.panen.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'             => 'required',
            'description'       => 'required',
            'token'             => 'required',
            'land_id'           => 'required',
            'category_id'       => 'required',
            'estimated_date'    => 'required',
            'estimated_income'  => 'required',
            'unit_id'           => 'required',
        ]);

        $response = $this->harvestRepo->update($request,$id);

        if(isset($response->status)=='success')
        {
            return redirect('/user-panen.html')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    public function destroy($id)
    {
        $response               = $this->harvestRepo->delete($id);

        if(isset($response->status)=='success')
        {
            return redirect('/user-panen.html')->with('message',$response->message);
        }else
        {
            return redirect('/user-panen.html')->with('message',"Gagal menghapus panen.");
        }
    }

}
