<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\ForumRepository;
use App\Repositories\WebService\CategoryRepository;
use App\Repositories\WebService\ArticleRepository;
use App\Repositories\WebService\VideoRepository;
use App\Repositories\WebService\PageRepository;
use App\Repositories\WebService\InfoPageRepository;

class ForumController extends Controller
{
    protected $infoPageRepository;
    public function __construct(ForumRepository $forumRepo, InfoPageRepository $infoPageRepository)
    {
        $this->middleware('redirectIfUserNotLogin', ['only'=>['postCommentForum']]);
        $this->forumRepo = $forumRepo;
        $this->infoPageRepository = $infoPageRepository;
    }

    public function index(PageRepository $pageRepo, VideoRepository $videoRepo, CategoryRepository $categoryRepo, ForumRepository $r, ArticleRepository $articleRepo)
    {
        $data['per_page'] = per_page('forum');
        $data['start'] = (request()->page - 1 ?? 0) * $data['per_page'];
        $data['end'] = $data['per_page'];

        $data['forums'] = $r->getAllForum($data['start'], $data['end']);

        $data['total'] = count($r->getAllForum()->data);

        $data['pages'] = round($data['total'] / $data['per_page']);
        // dd($data);
        $data['categories'] = $categoryRepo->getCategoryByEntity('forum');
        $data['articlePopulars'] = $articleRepo->getArticlePopular(0,4);
        $data['videos'] = $videoRepo->getAllVideo(3,3);


        return view('frontend.forum.index')->with($data);;
    }

    public function create(CategoryRepository $c, ForumRepository $r)
    {
        // Jika belum login, di suruh login dahulu
        if (session()->get('token') == null)
        {
            return redirect('login.html')->with('message', 'Anda harus login untuk bisa berkonsultasi.');
        }

        $data['forumRelated'] = $r->getLatestForum();
        // dd($data['forumRelated']);
        $data['categories'] = $c->getCategoryByEntity('forum');


        return view('frontend.forum.create')->with($data);
    }

    public function detail(ForumRepository $r, CategoryRepository $categoryRepo, $slug)
    {
        $reqForumDetail = $r->getForumBySlug($slug);

        if ($reqForumDetail->success==true)
        {
            $data['forum'] = $reqForumDetail->data;
            $data['replies'] = $r->getForumReplyBySlug($slug);
            $data['categories'] = $categoryRepo->getCategoryByEntity('forum');
            $data['forumRelated'] = $this->forumRepo->getForumRelatedBySlug($slug);
            // dd($data['forumRelated']);

            return view('frontend.forum.detail', $data);
        }else{
            return view('frontend.404');
        }
    }

    public function addReply(Request $req, ForumRepository $r)
    {

        $this->validate($req, [
            "forum_id" => "required",
            "token" => "required",
            "reply" => "required"
        ]);

            $response = $r->addReply($req);
            return \Redirect::back()->with('message',$response->message);
    }

    public function store(Request $request, ForumRepository $f)
    {
    	$this->validate($request, [
    		'title' => 'required',
    		'description' => 'required',
    		'category_id' => 'required'
    	]);

        $forum = $f->create($request);


    	//flash('Pertanyaan berhasil dibuat.')->success();

        //return redirect('/forum/'.$forum->data->slug.'.html');
        return redirect('/forum/'.$forum->data->slug.'.html')->with('message',$forum->message);
    }

    public function search(Request $req, ForumRepository $r, CategoryRepository $categoryRepo, ArticleRepository $articleRepo, VideoRepository $videoRepo)
    {
        $data['forums'] = $r->getForumSearch($req->keyword);
        $data['categories'] = $categoryRepo->getCategoryByEntity('forum');
        $data['articlePopulars'] = $articleRepo->getArticlePopular(0,4);
        $data['videos'] = $videoRepo->getAllVideo(3,3);
        //return $data;


        return view('frontend.forum.search', $data);
    }

    public function category(ForumRepository $r, CategoryRepository $categoryRepo, ArticleRepository $articleRepo, VideoRepository $videoRepo, $slug)
    {
        $data['per_page'] = per_page('forum');
        $data['start'] = (request()->page - 1 ?? 0) * $data['per_page'];
        $data['end'] = $data['per_page'];

        $data['forums'] = $r->getForumByCategory($slug, $data['start'], $data['end']);

        if ($data['forums']->success==true) {
            # code...

        $data['total'] = count($r->getForumByCategory($slug)->data);

        $data['pages'] = round($data['total'] / $data['per_page']);
        // dd($data);

        $data['categories'] = $categoryRepo->getCategoryByEntity('forum');
        $data['articlePopulars'] = $articleRepo->getArticlePopular(0,4);
        $data['videos'] = $videoRepo->getAllVideo(3,3);


        return view('frontend.forum.bycategory', $data);
        }else{
            return view('frontend.404');
        }
    }

    public function delete($slug)
    {
        $response = $this->forumRepo->delete($slug);

        if(isset($response->status)=='success')
        {
            return redirect('forum.html')->with('message',$response->message);
        }else
        {
            return redirect('forum.html')->with('message',"Delete Data Failed, Something Wrong");
        }
    }
}
