<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\InfoPageRepository;

class InfoPagesController extends Controller
{
    protected $infoPageRepository;

    public function __construct(InfoPageRepository $infoPageRepository)
    {
        $this->infoPageRepository = $infoPageRepository;
    }

    public function index()
    {
        $data['infoPages'] = $this->infoPageRepository->getAllInfoPage();
        // dd($data['infoPages']);
        $data['infoPages2'] = $this->infoPageRepository->getAllInfoPage2();
        return view('frontend.layout', $data);
    }

    public function show($slug)
    {
        $data['detail'] = $this->infoPageRepository->getInfoPage($slug);

        $data['infoPages'] = $this->infoPageRepository->getAllInfoPage();
        // dd($data['infoPages']);
        $data['infoPages2'] = $this->infoPageRepository->getAllInfoPage2();

        return view('frontend.infopage.show', $data);
    }

}
