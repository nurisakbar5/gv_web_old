<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\HarvestRepository;
use App\Repositories\WebService\PageRepository;
use Illuminate\Support\Facades\Input;
use App\Repositories\WebService\CategoryRepository;
use App\Repositories\WebService\RegionRepository;

use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;
use App\Repositories\WebService\InfoPageRepository;

class HarvestController extends Controller
{

    protected $harvestRepo;
    protected $infoPageRepository;

    public function __construct(HarvestRepository $harvestRepo, InfoPageRepository $infoPageRepository)
    {
        $this->middleware('redirectIfUserNotLogin', ['only'=>['postCommentHarvest']]);
        $this->harvestRepo = $harvestRepo;
        $this->infoPageRepository = $infoPageRepository;
    }

    public function index(PageRepository $pageRepo, CategoryRepository $categoryRepo)
    {

        if (Input::get('keyword') != null AND Input::get('kategori') == null) {
            $data['categories'] = $this->harvestRepo->getHarvestSearchByKeyword(Input::get('keyword'));
            //return $data;
        } else if (Input::get('kategori') != null AND Input::get('keyword') == null) {
            $data['categories'] = $this->harvestRepo->getHarvestSearchByCategory(Input::get('kategori'));
            //return $data;
        } else {
            $data['categories'] = $pageRepo->pageHarvest();
            //return $data;
        }

        // //return $data['categories'];
        $data['harvestByCategory']  = $pageRepo->pageHarvest();
        $data['categories']         = $categoryRepo->getCategoryByEntity('harvest');
        return view('frontend.harvest.index', $data);
    }

    // menampilkan panen berdasarkan slug kategori
    public function harvestByCategory($slug, CategoryRepository $categoryRepo, RegionRepository $regionRepo)
    {
        // mendapatkan data seluruh provinsi
        $data['provinces']                  = $regionRepo->getAllProvince();

        // mendapatkan data harvest berdasarkan kategory
        $data['per_page'] = per_page('harvest');
        $data['start'] = (request()->page - 1 ?? 0) * $data['per_page'];
        $data['end'] = $data['per_page'];

        $data['harvests'] = $this->harvestRepo->getHarvestByCategory($slug, $data['start'], $data['end']);
        // dd($data['harvests']);
        if ($data['harvests']->success==true) {
            $data['total'] = count($this->harvestRepo->getHarvestByCategory($slug)->data);
            $data['pages'] = round($data['total'] / $data['per_page']);
            // dd($data);

            // // mendapatkan semua data kategori panen
            $data['categories'] = $categoryRepo->getCategoryByEntity('harvest');

            return view('frontend.harvest.harvestByCategory', $data);
        }else{
            return view('frontend.404');
        }

    }

    public function search(Request $request, CategoryRepository $categoryRepo)
    {
        $data['harvestSearch']      = $this->harvestRepo->getHarvestSearchByKeyword($request->keyword);
        $data['categories']         = $categoryRepo->getCategoryByEntity('harvest');
        // dd($data['categories']);

        return view('frontend.harvest.search', $data);
    }

    public function detail(HarvestRepository $r, $slug)
    {
        $reqDetail = $r->getHarvestBySlug($slug);
        if ($reqDetail->success==true) {
            $data['harvest'] = $reqDetail->data;
            // dd($data['harvest']);

            // SEO Detail Harvest
            SEOMeta::setTitle($data['harvest']->title);
            SEOMeta::setDescription($data['harvest']->description);
            //SEOMeta::setKeywords(); // Harvest belum ada yang bisa dijadikan Keywords

            OpenGraph::setTitle($data['harvest']->title);
            OpenGraph::setDescription($data['harvest']->description);
            OpenGraph::addProperty('locale', 'id-ID');
            OpenGraph::addProperty('locale:alternate', ['id_ID', 'en-us']);
            OpenGraph::addProperty('type', 'website');
            foreach ($data['harvest']->front_images as $image)
            {
                OpenGraph::addProperty('image', $image);
            }
            OpenGraph::addProperty('image:type', 'image/jpeg/jpg/png');
            OpenGraph::addProperty('image:width', '');
            OpenGraph::addProperty('image:height', '');
            OpenGraph::addProperty('image:alt', '');

            TwitterCard::setTitle($data['harvest']->title);
            TwitterCard::setSite('@globalvillage');

            JsonLd::setTitle($data['harvest']->title);
            JsonLd::setDescription($data['harvest']->description);
            JsonLd::setType('Website');
            foreach ($data['harvest']->front_images as $image)
            {
                JsonLd::addImage('image', $image);
            }

        $data['comments'] = $r->getHarvestCommentBySlug($slug);
        $data['relateds'] = $r->getHarvestRelatedBySlug($slug);
        // dd ($data['relateds']);

        return view('frontend.harvest.detail', $data);
        }else{
            return view('frontend.404');
        }

    }

    public function postCommentHarvest(Request $req, HarvestRepository $r)
    {

        $this->validate($req, [
            "comment" => "required"
        ]);

        $response = $r->postCommentHarvest($req);
        if (isset($response->statusCode)==201) {
            return \Redirect::back()->with('message',$response->message);
        }else{
            return \Redirect::back()->withErrors($response)->withInput($req->all());
        }
    }
}
