<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\UserRepository;
use App\Repositories\WebService\RegionRepository;
use Illuminate\Support\Facades\Input;
use App\Repositories\WebService\InfoPageRepository;

class UserController extends Controller
{
    protected $infoPageRepository;

    public function __construct(UserRepository $userRepository, RegionRepository $regionRepository, InfoPageRepository $infoPageRepository)
    {
        $this->middleware('redirectIfUserNotLogin');
        $this->userRepository = $userRepository;
        $this->regionRepository = $regionRepository;
        $this->infoPageRepository = $infoPageRepository;
    }

    public function index(Request $request)
    {
        $token = $request->session()->get('token');

        $data['user'] = $this->userRepository->getUserProfileByToken($token);
        $data['addresses'] = $this->userRepository->getUserAddressesByToken($token);
        // dd($data['user']);

    	return view('frontend.user.profile.index', $data);
    }

    public function show(Request $request)
    {
        $token = $request->session()->get('token');
        $id = session('id');
        $data['profile'] = $this->userRepository->getUserProfileById($id);
        $data['provinces'] = $this->regionRepository->getAllProvince();
        $data['villages'] = $this->regionRepository->getAllVillage($id);
        $data['user'] = $this->userRepository->getUserProfileByToken($token);
        // dd($data['profile']);
        return view('frontend.user.profile.edit-profile', $data);
    }

    public function editImage(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
            'image' => 'required'
        ]);

        $response = $this->userRepository->updateProfileImage($request);
        // dd($response);
        if(isset($response->status)==true)
        {
            return \Redirect::back()->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    public function editProfile(Request $request)
    {
        $this->validate($request, [
            'token'=> 'required',
            'name'=>'required',
            'email'=>'required',
            'phone'=>'required',
            'village'=>'required',
            'address'=>'required'
        ]);

        $response = $this->userRepository->updateProfile($request);
        // dd($response);
        if(isset($response->status)==true)
        {
            session(['address'=>$request->address]);
            return redirect('user-profile.html')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    public function showPassword(Request $request)
    {
        $token = $request->session()->get('token');

        $data['user'] = $this->userRepository->getUserProfileByToken($token);

        return view('frontend.user.profile.edit-password', $data);
    }

    public function editPassword(Request $request)
    {
        $this->validate($request, [
            'token'=> 'required',
            'old_password'=>'required',
            'new_password'=>'required'
        ]);

        $response = $this->userRepository->updateProfilePassword($request);
        // dd($response);
        if(isset($response->status)==true)
        {
            session()->forget(['id', 'name']);
            return redirect('login.html')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    public function showVillage(Request $request)
    {
        $token = $request->session()->get('token');

        $data['user'] = $this->userRepository->getUserProfileByToken($token);
        $data['provinces'] = $this->regionRepository->getAllProvince();

        return view('frontend.user.profile.edit-desa', $data);
    }

    public function editVillage(Request $request)
    {
        $this->validate($request, [
            'token'=> 'required',
            'village'=>'required'
        ]);

        $response = $this->userRepository->updateProfileVillage($request);
        // dd($response);
        if(isset($response->status)==true)
        {
            return redirect('user-profile.html')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

}
