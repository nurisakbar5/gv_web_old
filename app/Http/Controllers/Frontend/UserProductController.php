<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\UserRepository;
use App\Repositories\WebService\PageRepository;
use App\Repositories\WebService\CategoryRepository;
use App\Repositories\WebService\UserProductRepository;
use App\Repositories\WebService\InfoPageRepository;
use App\Repositories\WebService\StoreRepository;
use App\Repositories\WebService\EtalaseRepository;

class UserProductController extends Controller
{
    protected $infoPageRepository;
    protected $storeRepo;
    protected $etalaseRepo;
    public function __construct(EtalaseRepository $etalaseRepo,StoreRepository $storeRepo, UserProductRepository $userProductRepo, InfoPageRepository $infoPageRepository)
    {
        $this->middleware('redirectIfUserNotLogin');
        $this->userProductRepo = $userProductRepo;
        $this->infoPageRepository = $infoPageRepository;
        $this->storeRepo = $storeRepo;
        $this->etalaseRepo = $etalaseRepo;
    }

    public function index(UserRepository $u, Request $request)
    {
        $token = $request->session()->get('token');

        $data['profile'] = $u->getUserProfileByToken($token);
        // dd($data['profile']);
        $id_store = $data['profile']->data->store_id;
        // dd($id_store);
        if (!isset($id_store)) {
            return view('frontend.store.alert')->withErrors('message', $request->message);
        }else{
            $data['products'] = $u->getUserProductByIdStore($id_store);
            return view('frontend.user.product.index', $data);
        }
    }

    public function create(UserRepository $u, CategoryRepository $categoryRepo, Request $request)
    {
        $token = $request->session()->get('token');
        $id_user = $request->session()->get('id');
        $data['profile'] = $u->getUserProfileByToken($token);
        // dd($data['profile']);
        $id_store = $data['profile']->data->store_id;
        $request = $this->storeRepo->getStoreInfo($id_user);
        if ($request->success==true) {
            $data['units'] = $this->userProductRepo->getAllUnit();
            $data['categories'] = $categoryRepo->getCategoryByEntity('product');
            $data['etalases'] = $this->etalaseRepo->etalase($id_store);
            // dd($data['profile']);
            return view('frontend.user.product.create', $data);
        } else {
            return view('frontend.store.alert')->withErrors('message', $request->message);
        }
    }

    public function addProductToUser(Request $req)
    {
        $this->validate($req, [
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
            'stock' => 'required',
            'weight' => 'required',
            'unit_id' => 'required',
            'category_id' => 'required',
            'etalase_id' => 'required',
            'token' => 'required',
            'image_1' => 'required',
            'image_2' => 'required',
            'image_3' => 'required'
        ]);

        $response = $this->userProductRepo->addProductToUser($req);
        // dd($response);
        if (isset($response->status)==true)
        {
            return redirect('user-produk.html')->with('message', $response->message);
        } else
        {
            return \Redirect::back()->withErrors($response)->withInput($req->all());
        }
    }

    public function edit(Request $request, $slug, CategoryRepository $categoryRepo, UserRepository $u)
    {
        $token = $request->session()->get('token');
        $data['profile'] = $u->getUserProfileByToken($token);
        // dd($data['profile']);
        $id_store = $data['profile']->data->store_id;
        $data['units'] = $this->userProductRepo->getAllUnit();
        $data['products'] = $this->userProductRepo->getProductBySlug($slug);
        // dd($data['products']);
        $data['categories'] = $categoryRepo->getCategoryByEntity('product');
        $data['etalases'] = $this->etalaseRepo->etalase($id_store);
        // dd($data['etalases']);
        return view('frontend.user.product.edit',$data);
    }

    public function update(Request $request, $slug)
    {

        $response = $this->userProductRepo->update($request,$slug);

        if(isset($response->status)=='success')
        {
            return redirect('user-produk.html')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    public function delete($slug)
    {
        $response = $this->userProductRepo->delete($slug);

        if(isset($response->status)=='success')
        {
            return redirect('user-produk.html')->with('message',$response->message);
        }else
        {
            return redirect('user-produk.html')->with('message',"Delete Data Failed, Something Wrong");
        }
    }
}
