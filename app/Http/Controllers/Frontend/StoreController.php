<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\StoreRepository;
use App\Repositories\WebService\RegionRepository;
use App\Repositories\WebService\EtalaseRepository;
use App\Repositories\WebService\CategoryRepository;
use App\Repositories\WebService\ProductRepository;
use Dotenv\Regex\Success;

class StoreController extends Controller
{
    protected $storeRepo, $categoryRepo, $regionRepo, $etalaseRepo, $productRepo;
    public function __construct(ProductRepository $productRepo, CategoryRepository $categoryRepo, StoreRepository $storeRepo, RegionRepository $regionRepo, EtalaseRepository $etalaseRepo)
    {
        $this->storeRepo = $storeRepo;
        $this->regionRepo = $regionRepo;
        $this->etalaseRepo = $etalaseRepo;
        $this->categoryRepo = $categoryRepo;
        $this->productRepo = $productRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id_user = $request->session()->get('id');
        // dd($id_user);

        // dd($data['store']);
        $request = $this->storeRepo->getStoreInfo($id_user);
        // dd($request);
        if ($request->success==true) {
            $data['store'] = $this->storeRepo->getStoreInfo($id_user);
            return view('frontend.store.seller-dashboard', $data);
        } else {
            return view('frontend.store.alert')->withErrors('message', $request->message);
        }

    }

    public function loadData(Request $request)
    {
        if($request->has('q'))
        {
            $cari = $request->q;
            $data = $this->regionRepo->searchDistrict($cari);
            return response()->json($data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontend.store.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'token'=> 'required',
            'name'=>'required',
            'description'=>'required',
            'subdistrict_id'=>'required'
        ]);

        $response = $this->storeRepo->create($request);
        if (isset($response->status)==true) {
            return redirect('store.html')->with('message', $response->message);
        }else{
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $data['result'] = $this->storeRepo->storeWithProduct($slug);
        // dd($data['result']);
        $data['etalases'] = $this->etalaseRepo->etalase($slug);
        return view('frontend.store.index', $data);
    }

    public function productByEtalasee($id)
    {
        $data['products'] = $this->productRepo->getProductByEtalase($id);
        $slug = $data['products']->store_slug;
        $data['result'] = $this->storeRepo->storeWithProduct($slug);
        $data['etalases'] =$this->etalaseRepo->etalase($slug);
        return view('frontend.store.product-by-etalase', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'description'=>'required',
        ]);

        $response = $this->storeRepo->update($request, $id);
        // dd($response);
        if (isset($response->status)==true) {
            return redirect('store/setting/info.html')->with('message', $response->message);
        }else{
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    public function showAjax(Request $request)
    {
        $id_user = $request->session()->get('id');
        $response = $this->storeRepo->getStoreInfo($id_user)->data;
        return response()->json([
            'error' => false,
            'detail'=> $response,
        ], 200);
    }

    public function updateSubdistrict(Request $request, $id)
    {
        // $this->validate($request, [
        //     'subdistrict_id'=>'required',
        // ]);

        $response = $this->storeRepo->updateSubdistrict($request, $id);
        return response()->json([
            'error' => false,
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function setting(Request $request)
    {
        $id_user = $request->session()->get('id');
        $data['store'] = $this->storeRepo->getStoreInfo($id_user);
        return view('frontend.store.seller-setting', $data);
    }

    public function location(Request $request)
    {
        $id_user = $request->session()->get('id');
        $data['store'] = $this->storeRepo->getStoreInfo($id_user);
        // dd($data['store']);
        return view('frontend.store.seller-location', $data);
    }

    public function sellerProducts(Request $request, $slug)
    {
        $id_user = $request->session()->get('id');
        $data['store'] = $this->storeRepo->getStoreInfo($id_user);
        $data['result'] = $this->storeRepo->storeWithProduct($slug);
        $data['etalases'] = $this->etalaseRepo->etalase($slug);
        $data['categories'] = $this->categoryRepo->getCategoryByEntity('product');
        // $etalase_id = $data['etalases']->data->id;
        // dd($data['etalases']);
        // $data['products'] = $this->productRepo->getProductByEtalase($etalase_id);
        // dd($data['result']);
        return view('frontend.store.seller-product', $data);
    }

    public function productByEtalase(Request $request, $id)
    {
        $id_user = $request->session()->get('id');
        $data['store'] = $this->storeRepo->getStoreInfo($id_user);
        $slug = $data['store']->data->slug;
        $data['products'] = $this->productRepo->getProductByEtalase($id);
        $data['etalases'] = $this->etalaseRepo->etalase($slug);
        // $data['result'] = $this->storeRepo->storeWithProduct($slug);
        // dd($data['products']);
        $data['categories'] = $this->categoryRepo->getCategoryByEntity('product');
        return view('frontend.store.seller-product-by-etalase', $data);
    }

    public function courier(Request $request)
    {
        $id_user = $request->session()->get('id');
        $idOrSlug = $this->storeRepo->getStoreInfo($id_user)->data->id;
        $data['store'] = $this->storeRepo->getStoreInfo($id_user);
        $data['couriers'] = $this->storeRepo->storeCouriers($idOrSlug);
        $data['cours'] = $this->storeRepo->couriers();
        // dd($data['couriers']);
        // dd($data['store']);
        return view('frontend.store.seller-courier', $data);
    }

    public function changeActiveCourier(){


        $id = $_GET['id'];
        $response = $this->storeRepo->changeActiveCourier($id);

        // $this->validate($request, [
        //     'id'=>'required',
        // ]);

        // dd($request->id);
        // foreach($request->id as $idService)
        // {
        //     dd($idService);
        //     $response = $this->storeRepo->changeActiveCourier($idService);
        // }

        // return redirect('store/setting/courier.html')->with('message', 'Berhasil Melakukan Perubahan');


        // if (isset($response->success)==true) {
        //     return redirect('store/setting/courier.html')->with('message', $response->message);
        // }else{
        //     return \Redirect::back()->withErrors($response)->withInput($request->all());
        // }
    }

    public function addCourier(Request $request){
        $this->validate($request, [
            'store_id'=>'required',
            'courier_code'=>'required'
        ]);

        $response = $this->storeRepo->addCourier($request);
        // if (isset($response->status)==true) {
        //     return \Redirect::back()->with('message', $response->message);
        // }else{
        //     return \Redirect::back()->withErrors($response)->withInput($request->all());
        // }
    }

    public function loadDataProduct(Request $request)
    {
        $id_user = $request->session()->get('id');
        $data['store'] = $this->storeRepo->getStoreInfo($id_user);
        $slug = $data['store']->data->slug;
        $data['etalases'] = $this->etalaseRepo->etalase($slug);
        $data['productSearch'] = $this->storeRepo->searcProduct($request->id, $request->keyword);
        // dd($data);
        return view('frontend.store.seller-search-products', $data);
    }

    public function loadDataProductt(Request $request)
    {
        $data['productSearch'] = $this->storeRepo->searcProduct($request->id, $request->keyword);
        $slug = $data['productSearch']->store_slug;
        $data['result'] = $this->storeRepo->storeWithProduct($slug);
        $data['etalases'] =$this->etalaseRepo->etalase($slug);
        // dd($data);
        return view('frontend.store.search-products', $data);
    }
}
