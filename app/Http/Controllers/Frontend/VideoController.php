<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\PageRepository;
use App\Repositories\WebService\VideoRepository;
use App\Repositories\WebService\ArticleRepository;
use App\Repositories\WebService\IklanRepository;
use App\Repositories\WebService\CategoryRepository;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;
use App\Repositories\WebService\InfoPageRepository;

class VideoController extends Controller
{
    protected $infoPageRepository;
    public function __construct(VideoRepository $videoRepo, InfoPageRepository $infoPageRepository)
    {
        $this->middleware('redirectIfUserNotLogin', ['only'=>['postCommentVideo']]);
        $this->videoRepo = $videoRepo;
        $this->infoPageRepository = $infoPageRepository;
    }

    public function index(PageRepository $pageRepo, CategoryRepository $c, ArticleRepository $a, IklanRepository $i)
    {
        $data['videos'] = $pageRepo->PageVideo();

        //return $data;

        $data['video_category'] = $c->getCategoryByEntity('video');
        $data['populars'] = $a->getArticlePopular(0,4);
        $data['iklans'] = $i->getAllIklan();

        return view('frontend.video.index',$data);
    }


    public function videoByCategory(CategoryRepository $c, $slug)
    {
        $data['per_page'] = per_page('video');
        $data['start'] = (request()->page - 1 ?? 0) * $data['per_page'];
        $data['end'] = $data['per_page'];

        $data['videos'] = $this->videoRepo->getVideoByCategory($slug, $data['start'], $data['end']);
        // dd($data['videos']);
        if ($data['videos']->success==true) {
            $data['total'] = count($this->videoRepo->getVideoByCategory($slug)->data);

            $data['pages'] = round($data['total'] / $data['per_page']);
            // dd($data);

            // // mendapatkan semua data kategori produk
            $data['categories'] = $c->getCategoryByEntity('video');
            return view('frontend.video.list')->with($data);
        }else{
            return view('frontend.404');
        }
    }

    public function detail($slug)
    {
        $reqVideoDetail = $this->videoRepo->getVideoBySlug($slug);

        if ($reqVideoDetail->success == true)
        {
            $data['video'] = $reqVideoDetail->data;

            // SEO Detail Video
            SEOMeta::setTitle($data['video']->title);
            SEOMeta::setDescription(str_limit(strip_tags($data['video']->description), 200, '...'));
            //SEOMeta::setKeywords(); // Video belum ada Keywords

            OpenGraph::addVideo($data['video']->video_file, [
                'secure_url' => '',
                'type' => 'application/x-shockwave-flash',
                'width' => 620,
                'height' => 580
            ]);
            OpenGraph::setTitle($data['video']->title)
            ->setDescription(str_limit(strip_tags($data['video']->description), 200, '...'))
            ->setType('video.movie')
            ->setVideoMovie([
                'actor' => 'profile / array',
                'actor:role' => 'string',
                'director' => 'profile /array',
                'writer' => 'profile / array',
                'duration' => 'integer',
                'release_date' => 'datetime',
                'tag' => 'string / array'
            ]);
            OpenGraph::addProperty('locale', 'id_ID');
            OpenGraph::addProperty('locale:alternate', ['id_ID', 'en-us']);

            TwitterCard::setTitle($data['video']->title);
            TwitterCard::setSite('@globalvillage');

            JsonLd::setTitle($data['video']->title);
            JsonLd::setDescription(str_limit(strip_tags($data['video']->description), 200, '...'));
            JsonLd::setType('Video');

            $data['related'] = $this->videoRepo->getRelatedVideo($slug);
            $data['comment'] = $this->videoRepo->getCommendVideoBySlug($slug)->data;
            // dd($data['comment']);

            return view('frontend.video.detail')->with($data);
        } else
        {
            return view('frontend.404');
        }
    }

    public function search(Request $request, CategoryRepository $c, ArticleRepository $a, IklanRepository $i)
    {
        $data['videoSearch']      = $this->videoRepo->getVideoSearchByKeyword($request->keyword);
        $data['video_category'] = $c->getCategoryByEntity('video');
        $data['populars'] = $a->getArticlePopular(0,4);
        $data['iklans'] = $i->getAllIklan();

        return view('frontend.video.search', $data);
    }

    public function postCommentVideo(Request $req)
    {
        $this->validate($req, [
            "comment" => "required"
        ]);

        $response = $this->videoRepo->postCommentVideo($req);

        if (isset($response->status)==true) {
            return \Redirect::back()->with('message',$response->message);
        }else{
            return \Redirect::back()->withErrors($response)->withInput($req->all());
        }
    }
}
