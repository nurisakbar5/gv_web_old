<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\UserRepository;
use App\Repositories\WebService\InfoPageRepository;
/**
 *
 */
class AuthController extends Controller
{
    protected $infoPageRepository;
    protected $url;
    protected $client;
    public function __construct(InfoPageRepository $infoPageRepository)
    {
        $this->url =  env("APP_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
        $this->infoPageRepository = $infoPageRepository;
    }

	public function formLogin()
	{
        if(session('id')==null)
        {
            return view('frontend.login');
        }
        else
        {
            return redirect('/user-profile.html');
        }
	}

	public function formRegister()
	{
		return view('frontend.register');
	}

    public function formForgot()
    {
        return view('frontend.forgot');
    }

	public function login(Request $request, UserRepository $userRepo)
    {
    	$this->validate($request, [
            "email" => "required|email",
            "password" => "required"
        ]);

        $response = $userRepo->login($request);
        if($response->status!=true)
        {
           return redirect('login.html')->with('message',$response->message);
        }else
        {
            $next = $request->next;

            $result     = $response->data;
            session(['id' => $result->id]);
            session(['name' => $result->name]);
            session(['token' => $result->token]);
            session(['photo' => $result->photo]);
            session(['address' => $result->address]);
            session(['verified' => $result->active]);
            // session(['store_id' => $result->store_id]);

            // return redirect('userProduct')->with('message','Selamat Datang '.$result->name);
            // return \Redirect::back()->with('message',$response->message);
            if($next)
                return redirect($next);

            return redirect('user-profile.html')->with(['message','Selamat Datang '.$result->name]);
        }
    }

    public function register(Request $req, UserRepository $userRepo)
    {
    	$this->validate($req, array(
            "name" => "required",
            "email" => "required",
            "password" => "required|min:5",
            "agree" => 'required'
        ));

        $response = $userRepo->register($req);

        if(isset($response->status))
        {
            if($response->status==true)
            {
                $result     = $response->data;
                session(['id' => $result->id]);
                session(['name' => $result->name]);
                session(['token' => $result->token]);
                session(['verified' => 0]);

                // return redirect('user-profile.html')->with('message','Pendaftaran Berhasil!! Selamat Datang '. $result->name);
                return redirect('/register-success.html');
            }else
            {
                return redirect('/register.html')->with('message',$response->message);
            }

        }else
        {
            //return redirect('/register.html')->with('message',$response);
            return \Redirect::back()->withErrors($response)->withInput($req->all());
        }
    }

    public function registerSuccess()
    {
        return view('frontend.account-verification.registration-success');
    }

    public function emailVerification()
    {
        return view('frontend.account-verification.account-not-active');
    }

    public function activate($code,UserRepository $userRepo)
    {
        $response = $userRepo->activate($code);

        if($response->status==false)
        {
            return 'terjadi kesalah';

        }else {
            return view('frontend.user.active');
        }
    }

    public function forgotPassword(Request $r, UserRepository $u)
    {
        $this->validate($r, [
            "email" => "required|email"
        ]);

        $response = $u->forgot($r);

        if($response->status==true)
        {
            return redirect('/login.html')->with('message', $response->message);
        }else
        {
            return redirect('/lupa-password.html')->with('message', $response->message);
        }
    }
}
