<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\ProductRepository;
use App\Repositories\WebService\PageRepository;
use App\Repositories\WebService\CategoryRepository;
use App\Repositories\WebService\RegionRepository;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;
use Illuminate\Support\Facades\Input;
use App\Repositories\WebService\InfoPageRepository;
use App\Repositories\WebService\CourierRepository;

class ProductController extends Controller
{
    protected $infoPageRepository, $productRepo, $regionRepo, $courierRepo;

    public function __construct(CourierRepository $courierRepo, ProductRepository $productRepo, InfoPageRepository $infoPageRepository, RegionRepository $regionRepo)
    {
        $this->middleware('redirectIfUserNotLogin', ['only'=>['postCommentProduct']]);
        $this->productRepo = $productRepo;
        $this->infoPageRepository = $infoPageRepository;
        $this->regionRepo = $regionRepo;
        $this->courierRepo = $courierRepo;
    }


    public function index(PageRepository $pageRepo)
    {
        // if (Input::get('keyword') != null AND Input::get('kategori') == null) {
        //     $data['categories'] = $r->getProductSearchByKeyword(Input::get('keyword'));
        //     //return $data;
        // } else if (Input::get('kategori') != null AND Input::get('keyword') == null) {
        //     $data['categories'] = $r->getproductSearchByCategory(Input::get('kategori'));
        //     //return $data;
        // } else {
        //     $data['categories'] = $pageRepo->pageStore();
        //     //return $data;
        // }
        $data['latest'] = $this->productRepo->getAllProduct(0,4);
        // dd($data['latest']);

        $data['productGroupByCategories']   = $pageRepo->pageStore();


  //       if(isset($request->period))
		// {
		// 	$period  = date_format(date_create($request->period),"Ymd") ;
		// }else
		// {
		// 	$period = date('Ymd');
  //       }

  //       if(isset($request->regency))
		// {
		// 	$regencyId =  $request->regency;
		// }else
		// {
		// 	//$regencyId 			= $reqlocationRegency->data->id;
		// 	$regencyId	= '3204';
  //       }

  //       $data['regency']					= $regencyRepo->getRegencyById($regencyId);
  //       $data['regencyDetail']				= $regencyRepo->getRegencyDetailById($regencyId);
  //       // Untuk Lokasi
  //       $data['provinces']                  = $regionRepo->getAllProvince();
        // $data['latest']	                    = $this->productRepo->getProductByRegency($regencyId);

		return view('frontend.products.index', $data);
    }

    public function search(Request $request, CategoryRepository $categoryRepo, RegionRepository $regionRepo)
    {
        $data['provinces']          = $regionRepo->getAllProvince();
        $data['productSearch']      = $this->productRepo->getProductSearchByKeyword($request->keyword);
        $data['categories']         = $categoryRepo->getCategoryByEntity('product');

        return view('frontend.products.search', $data);

    }

    public function detail(ProductRepository $s, Request $req, $slug)
    {
        $reqProductDetail = $s->getProductBySlug($slug);

        session(['slug' => $slug]);
        // dd($reqProductDetail);

        if ($reqProductDetail->success==true)
        {
            $data['detail'] = $reqProductDetail->data;
            //dd($data['detail']);

            // SEO Detail Produk
            SEOMeta::setTitle($data['detail']->name);
            SEOMeta::setDescription($data['detail']->description);
            SEOMeta::setKeywords($data['detail']->category->name); // Bisa ditambah lagi

            OpenGraph::setTitle($data['detail']->name);
            OpenGraph::setDescription($data['detail']->description);
            OpenGraph::addProperty('locale', 'id_ID');
            OpenGraph::addProperty('locale:alternate', ['id_ID', 'en-us']);
            OpenGraph::addProperty('type', 'website');

            foreach ($data['detail']->front_images as $image)
            {
                OpenGraph::addProperty('image', $image);
            }
            OpenGraph::addProperty('image:type', 'image/jpeg/jpg/png');
            OpenGraph::addProperty('image:width', '');
            OpenGraph::addProperty('image:height', '');
            OpenGraph::addProperty('image:alt', '');

            TwitterCard::setTitle($data['detail']->name);
            TwitterCard::setSite('@globalvillage');

            JsonLd::setTitle($data['detail']->name);
            JsonLd::setDescription($data['detail']->description);
            JsonLd::setType('Website');
            foreach ($data['detail']->front_images as $image)
            {
                JsonLd::addImage('image', $image);
            }

            $data['commend'] = $s->getCommendProduct($slug);
            $data['related'] = $s->getRelatedProduct($slug);
            //dd($data);

            return view('frontend.products.detail')->with($data);
        } else
        {

            return view('frontend.404');
        }
    }

    public function product(Request $request, PageRepository $pageRepo, RegionRepository $regionRepo, CategoryRepository $categoryRepo)
    {
        // mendapatkan list produk group by kategori khusus untuk halaman produk
        //$data['productGroupByCategories']   = $pageRepo->pageProduct();

        // mendapatkan data semua kategori produk
        $data['categories']                 = $categoryRepo->getCategoryByEntity('product');

        if(isset($request->regency))
		{
            $regencyId =  $request->regency;
            $data['productsLocation']           = $this->productRepo->getProductByRegency($regencyId);
            $data['regency']					= $regionRepo->getAllRegency($regencyId);
            $data['regencyDetail']				= $regionRepo->getRegencyDetail($regencyId);
		}else
		{
			//$regencyId 			= $reqlocationRegency->data->id;
            $data['products']                   = $pageRepo->pageProduct();
            // dd($data['products']);
        }

        $data['provinces']                  = $regionRepo->getAllProvince();

        return view('frontend.products.product', $data);
    }

    public function loadData(Request $request)
    {
        if($request->has('q'))
        {
            $cari = $request->q;
            $data = $this->courierRepo->searchCity($cari);
            return response()->json($data);
        }
    }

    // menampilkan produk berdasarkan slug kategori
    public function productByCategory($slug, CategoryRepository $categoryRepo, RegionRepository $regionRepo)
    {
        // mendapatkan data seluruh provinsi
        $data['provinces']                  = $regionRepo->getAllProvince();

        // mendapatkan data product berdasarkan kategory
        $data['per_page'] = per_page('product');
        $data['start'] = (request()->page - 1 ?? 0) * $data['per_page'];
        $data['end'] = $data['per_page'];

        $data['products'] = $this->productRepo->getProductByCategory($slug, $data['start'], $data['end']);
        // dd($data['products']);
        if ($data['products']->success==true) {
            $data['total'] = count($this->productRepo->getProductByCategory($slug)->data);
            $data['pages'] = round($data['total'] / $data['per_page']);
            // dd($data);

            // // mendapatkan semua data kategori produk
            $data['categories'] = $categoryRepo->getCategoryByEntity('product');


            return view('frontend.products.productByCategory', $data);
        }else{

            return view('frontend.404');
        }

    }

    public function postCommentProduct(Request $req, ProductRepository $r)
    {

        $this->validate($req, [
            "comment" => "required"
        ]);

        $response = $r->postCommentProduct($req);

        if (isset($response->status)==true) {
            return \Redirect::back()->with('message',$response->message);
        }else{
            return \Redirect::back()->withErrors($response)->withInput($req->all());
        }
    }
}
