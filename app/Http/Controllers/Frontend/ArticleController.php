<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\ArticleRepository;
use App\Repositories\WebService\PageRepository;
use App\Repositories\WebService\ProductRepository;
use App\Repositories\WebService\CategoryRepository;
use App\Repositories\WebService\VideoRepository;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;
use App\Repositories\WebService\InfoPageRepository;

class ArticleController extends Controller
{
    protected $infoPageRepository;
    public function __construct(ArticleRepository $articleRepo, InfoPageRepository $infoPageRepository)
    {
        $this->middleware('redirectIfUserNotLogin', ['only'=>['postCommentArticle']]);
        $this->articleRepo = $articleRepo;
        $this->infoPageRepository = $infoPageRepository;
    }

    public function index(PageRepository $pageRepo)
    {
        $data['populars'] = $this->articleRepo->getArticlePopular(0,4);
        $data['latests']    = $this->articleRepo->getAllArticle(0,3);
        $data['paling'] = $this->articleRepo->getAllArticle(2,6);

        return view('frontend.article.index')->with($data);
    }

    public function articleByCategory($slug, ArticleRepository $r, CategoryRepository $c)
    {
        $data['per_page'] = per_page('article');
        $data['start'] = (request()->page - 1 ?? 0) * $data['per_page'];
        $data['end'] = $data['per_page'];

        $data['articles'] = $r->getArticleByCategory($slug, $data['start'], $data['end']);
        // dd($data['articles']);
        if ($data['articles']->success==true) {

            $data['categories'] = $c->getCategoryByEntity('article');

            $data['total'] = count($r->getArticleByCategory($slug)->data);

            $data['pages'] = round($data['total'] / $data['per_page']);
            // dd($data);


            return view('frontend.article.list')->with($data);
        }else{
            return view('frontend.404');
        }

    }

    public function detail(ArticleRepository $r, $slug, ProductRepository $p, CategoryRepository $c, VideoRepository $v)
    {
        $reqArticleDetail = $r->getArticleBySlug($slug);

        if ($reqArticleDetail->success==true)
        {
            $data['article'] = $reqArticleDetail->data;

            // SEO Detail Artikel
            SEOMeta::setTitle($data['article']->title);
            SEOMeta::setDescription(str_limit(strip_tags($data['article']->article), 200, '...'));
            SEOMeta::addMeta('article:published_time', $data['article']->publish_date, 'property');
            SEOMeta::addMeta('article:section', $data['article']->category->name, 'property');
            SEOMeta::setKeywords($data['article']->category->name); // Keywords tambah tags lagi

            OpenGraph::setTitle($data['article']->title)
            ->setDescription(str_limit(strip_tags($data['article']->article), 200, '...'))
            ->setType('article')
            ->setArticle([
                'published_time' => $data['article']->publish_date,
                'modified_time' => 'datetime',
                'expiration_time' => 'datetime',
                'author' => 'profile / array',
                'section' => $data['article']->category->name,
                'tag' => 'string / array'
            ]);
            OpenGraph::addProperty('locale', 'id_ID');
            OpenGraph::addProperty('locale:alternate', ['id_ID', 'en-us']);
            OpenGraph::addProperty('type', 'article');
            OpenGraph::addProperty('image', $data['article']->image_url);
            OpenGraph::addProperty('image:type', 'image/jpeg/jpg/png');
            OpenGraph::addProperty('image:width', '');
            OpenGraph::addProperty('image:height', '');
            OpenGraph::addProperty('image:alt', '');

            TwitterCard::setTitle($data['article']->title);
            TwitterCard::setSite('@globalvillage');

            JsonLd::setTitle($data['article']->title);
            JsonLd::setDescription(str_limit(strip_tags($data['article']->article), 200, '...'));
            JsonLd::setType('Article');
            JsonLd::addImage($data['article']->image_url);

            $data['comments'] = $r->getArticleCommentBySlug($slug);
            $data['relateds'] = $r->getArticleRelatedBySlug($slug, 3);
            $data['populars'] = $r->getArticlePopular(0, 3);
            $data['products'] = $p->getAllProduct(0, 5);
            $data['categories'] = $c->getCategoryByEntity('article');
            $data['videos'] = $v->getAllVideo(0, 3);
            // dd($data['products']);
            return view('frontend.article.detail')->with($data);
        } else
        {

            return view('frontend.404');
        }
    }

    public function search(Request $req)
    {
        $data['articleSearch'] = $this->articleRepo->getArticleSearchByKeyword($req->keyword)->data;
        $data['populars'] = $this->articleRepo->getArticlePopular(0,4);
        $data['latests'] = $this->articleRepo->getAllArticle(0,2);
        $data['paling'] = $this->articleRepo->getAllArticle(2,6);



        return view('frontend.article.search', $data);
    }

    public function postCommentArticle(Request $req, ArticleRepository $r)
    {

        $this->validate($req, [
            "comment" => "required"
        ]);

        $response = $r->postCommentArticle($req);
            // dd($response);
        if (isset($response->status)==true) {
            return \Redirect::back()->with('message',$response->message);
        }else{
            return \Redirect::back()->withErrors($response)->withInput($req->all());
        }

    }
}
