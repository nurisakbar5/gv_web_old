<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\UserAddressRepository;
use App\Repositories\WebService\RegionRepository;
use Illuminate\Support\Facades\Input;
use App\Repositories\WebService\InfoPageRepository;

class UserAddressController extends Controller
{
    protected $infoPageRepository;

    public function __construct(UserAddressRepository $userAddressRepository, RegionRepository $regionRepository, InfoPageRepository $infoPageRepository)
    {
        $this->middleware('redirectIfUserNotLogin');
        $this->userAddressRepository = $userAddressRepository;
        $this->regionRepository = $regionRepository;
        $this->infoPageRepository = $infoPageRepository;
    }

    public function create()
    {
        $data['provinces'] = $this->regionRepository->getAllProvince();

        return view('frontend.user.address.create', $data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'token'=> 'required',
            'village_id'=>'required',
            'street'=>'required',
            'phone'=>'required',
            'name'=>'required',
            'receiver_name'=>'required'
        ]);

        $response = $this->userAddressRepository->create($request);
        // dd($response);
        if(isset($response->success)==true)
        {
            return redirect('user-profile.html')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    public function show($id)
    {
        $data['address'] = $this->userAddressRepository->show($id);
        $data['provinces'] = $this->regionRepository->getAllProvince();
        // dd($data['address']);

        return view('frontend.user.address.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'village_id'=>'required',
            'street'=>'required',
            'phone'=>'required',
            'name'=>'required',
            'receiver_name'=>'required'
        ]);

        $response = $this->userAddressRepository->update($request, $id);
        // dd($response);
        if(isset($response->success)==true)
        {
            return redirect('user-profile.html')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    public function delete($id)
    {

        $response = $this->userAddressRepository->delete($id);
        // dd($response);
        if(isset($response->success)==true)
        {
            return redirect('user-profile.html')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->with('message', "Delete Data Failed, Something Wrong");
        }
    }

}
