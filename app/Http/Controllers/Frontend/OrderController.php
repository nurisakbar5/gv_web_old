<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\OrderRepository;
use App\Repositories\WebService\ProductRepository;
use App\Repositories\WebService\UserRepository;
use App\Repositories\WebService\InfoPageRepository;
use App\Repositories\WebService\CourierRepository;

class OrderController extends Controller
{
    public $orderRepo;
    protected $infoPageRepository;
    protected $userRepo;
    protected $courRepo;

    function __construct(CourierRepository $courRepo, OrderRepository $orderRepo, InfoPageRepository $infoPageRepository, UserRepository $userRepo)
    {
        $this->middleware('redirectIfUserNotLogin');
        $this->orderRepo = $orderRepo;
        $this->infoPageRepository = $infoPageRepository;
        $this->userRepo = $userRepo;
        $this->courRepo = $courRepo;
    }

    public function cart(Request $req, ProductRepository $p)
    {
        $slug = $req->session()->get('slug');
        $token = $req->session()->get('token');

        if ($token) {
            $data['getItems'] = $this->orderRepo->getAllItem($token);
        }

        $data['detail'] = $p->getProductBySlug($slug);
        $data['user'] = $this->userRepo->getUserProfileByToken($token);
        // dd($data['getItems']);

        return view('frontend.cart', $data);
    }

    public function loadData(Request $request)
    {
        if($request->has('q'))
        {
            $cari = $request->q;
            $data = $this->courRepo->searchSubDistrict($cari);
            return response()->json($data);
        }
    }

    public function cost(Request $request)
    {
        $hasil= $this->courRepo->cost($request)->value;
        return response()->json($hasil);
    }

    function addProductToCart(Request $req)
    {
        $this->validate($req, [
            "token" => "required",
            "product_id" => "required",
            "qty" => "required"
        ]);

        $response = $this->orderRepo->addProductToCart($req);

        return redirect('cart.html')->with('message', $response->message);
    }

    function updateProductQtyInCart(Request $req)
    {
         $this->validate($req, [
            "id" => "required",
            "qty" => "required",
            "token" => "required"
        ]);

        $response = $this->orderRepo->updateProductQtyInCart($req->all());
    }

    function deleteProductInCart($id)
    {
        $response = $this->orderRepo->deleteProductInCart($id);

        return \Redirect::back()->with('message', $response->message);
    }

    function account(Request $req, UserRepository $u,\App\Repositories\WebService\AddressRepository $uddt,\App\Repositories\WebService\EspayRepository $espay)
    {
        $token = $req->session()->get('token');

        if ($token != null) {
            $data['user'] = $u->getUserProfileByToken($token);
        }

        $data['getItems']                       = $this->orderRepo->getAllItem($token);
        $data['userDeliveryDestantionList']     = $uddt->getAllData($token);
        $data['paymentChannels']                = $espay->getChannelPayment();

        //dd($data);
        return view('frontend.account', $data);
    }

    function ajaxShowDeliveryAddress(\App\Repositories\WebService\AddressRepository $uddt)
    {
        $reqDeliveryDestination             = $uddt->detail($_GET['id']);
        $data['userDeliveryDestination']    = $reqDeliveryDestination->data;
         return view('frontend.order.ajaxShowDeliveryAddress',$data);
    }

    function confirmPayment(Request $req, UserRepository $u,\App\Repositories\WebService\AddressRepository $uddt,\App\Repositories\WebService\EspayRepository $espay)
    {
        $this->validate($req, [
            'no_invoice'    => 'required',
            'image_confirm' =>  'required',
            'user_id'         =>  'required'
        ]);

        $response = $this->orderRepo->confirmPayment($req->all());

        return redirect('frontend.order.confirmPayment')->with('message', $response->message);
    }

    function confirmPaymentPage()
    {

        return view('frontend.order.confirmPayment');
    }

    function checkout(Request $req)
    {
        $this->validate($req, [
            "address"           =>  "required",
            "store"             =>  "required",
            "courier"           =>  "required",
            "token"             =>  "required",
            "payment_type_id"   =>  "required",
            "courier_service"   =>  "required"

        ]);

        $payment = explode('-',$req->payment_type_id);
        $input = $req->all();
        $input['payment_type_id'] = $payment[1];
        $response  = $this->orderRepo->addProductToCheckout($input);

        //dd($response);

        if($response->success===true)
        {
            $orderData = $response->data;
            $url        = "https://globalvillage.id/user-pembelian.html";

            // ========================= DEVELOPMENT =============================
            $urlEspay   = "https://sandbox-kit.espay.id";
            $key        = "666b963e4d33936ade282b02e69ea37a";
            $signature  = "7a8nwsb94eqmvh82";
            $commCode   = "SGWAPLIKASIGLOBAL";
            $bankCode   = '013';
            $productCode= "PERMATAATM"; 

            // ========================= PRODUCTION ===============================
            // $urlEspay   = "https://kit.espay.id";
            // $key        = "e6e99a4226d810710be0fd05dfed309c";
            // $signature  = "0q0ksdt1mkk7a6pp";
            // $commCode   = "SGWGLOBALVILLAGE";
            // $bankCode   = $payment[0];
            // $productCode= $payment[1];

            $paymentId  = $orderData->payment_reference;
            $amount     = $orderData->total;
            $url = $urlEspay."/index/order/?url=$url&key=$key&paymentId=$paymentId&paymentAmount=$amount&signature=$signature&commCode=$commCode&bankCode=$bankCode&productCode=$productCode";
            //return $url;
            return \Redirect::to($url);

        }else
        {
            return redirect('cart.html')->with($response->message);
        }

    }


    function espayPagePayment($paymentId,$amount,$bankCode,$productCode)
    {
        $url        = "http://globalvillage.co.id/";
        $key        = "666b963e4d33936ade282b02e69ea37a";
        $paymentId  = "INV0012023";
        $amount     = 100000;
        $signature  = "7a8nwsb94eqmvh82";
        $bankCode   = "013";
        $productCode= "PERMATAATM";
        $commCode   ="SGWAPLIKASIGLOBAL";
        $url = "https://sandbox-kit.espay.id/index/order/?url=$url&key=$key&paymentId=$paymentId&paymentAmount=$amount&signature=$signature&commCode=$commCode&bankCode=$bankCode&productCode=$productCode";
        return \Redirect::to($url);
    }
}
