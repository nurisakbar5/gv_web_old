<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\UserRepository;
use App\Repositories\WebService\PageRepository;
use App\Repositories\WebService\CategoryRepository;
use App\Repositories\WebService\LandRepository;
use App\Repositories\WebService\HarvestRepository;
use App\Repositories\WebService\RegionRepository;
use App\Repositories\WebService\InfoPageRepository;

class UserLahanController extends Controller
{
    protected $infoPageRepository;

    public function __construct(LandRepository $landRepo, InfoPageRepository $infoPageRepository)
    {
        $this->middleware('redirectIfUserNotLogin');
        $this->landRepo = $landRepo;
        $this->infoPageRepository = $infoPageRepository;
    }

    public function index(UserRepository $u, Request $request)
    {
        $token = $request->session()->get('token');

        $data['profile'] = $u->getUserProfileByToken($token);
        $data['lahans'] = $u->getUserLahanByToken($token);


        return view('frontend.user.lahan.index', $data);
    }

    public function create(HarvestRepository $harvestRepo, RegionRepository $regionRepo)
    {
        $data['units'] = $harvestRepo->getAllUnit();
        $data['provinces'] = $regionRepo->getAllProvince();

        return view('frontend.user.lahan.create', $data);
    }

    public function edit($id, HarvestRepository $harvestRepo, RegionRepository $regionRepo)
    {
        $data['units'] = $harvestRepo->getAllUnit();
        $data['provinces'] = $regionRepo->getAllProvince();
        $data['land'] = $this->landRepo->getLandById($id);

        return view('frontend.user.lahan.edit', $data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'              => 'required',
            'large'             => 'required',
            'token'             => 'required',
            'description'       => 'required',
            'unit_area'         => 'required',
            'street'            => 'required',
            'village_id'        => 'required',
            'image_1'           => 'required',
            'image_2'           => 'required',
            'image_3'           => 'required',
            'image_4'           => 'required',
        ]);

        //return dd($request->all());

        $response = $this->landRepo->create($request);
        if(isset($response->status)==true)
        {
            return redirect('/user-lahan.html')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'              => 'required',
            'large'             => 'required',
            'token'             => 'required',
            'description'       => 'required',
            'unit_area'         => 'required',
            'street'            => 'required',
            'village_id'        => 'required'
        ]);

        $response = $this->landRepo->update($request,$id);

        if(isset($response->status)=='success')
        {
            return redirect('/user-lahan.html')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    public function destroy($id)
    {
        $response               = $this->landRepo->delete($id);

        if(isset($response->status)=='success')
        {
            return redirect('/user-lahan.html')->with('message',$response->message);
        }else
        {
            return redirect('/user-lahan.html')->with('message',"Delete Data Failed, Something Wrong");
        }
    }

}
