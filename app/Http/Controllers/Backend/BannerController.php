<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\BannerRepository;

class BannerController extends Controller
{

    protected $bannerRepo;
    
    function __construct(BannerRepository $bannerRepo)
    {
        $this->middleware('redirectIfNotLogin');
        $this->bannerRepo = $bannerRepo;       
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['banners']       = $this->bannerRepo->getAllBanner();
        return view('backend.banner.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.banner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'        =>  'required',
            'description' =>  'required',
            'image_web'   =>  'required',
            'image_mobile'=> 'required'
            ]);

        $response = $this->bannerRepo->create($request);

        if(isset($response->status)=='success')
        {
            return redirect('/admin/banner')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $request        = $this->bannerRepo->getBanner($id);
        $data['banner'] = $request->data;
        
        return view('backend.banner.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = $this->bannerRepo->update($request,$id);
        
        if(isset($response->status)=='success')
        {
            return redirect('/admin/banner')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = $this->bannerRepo->delete($id);
        
        if(isset($response->status)=='success')
        {
            return redirect('/admin/banner')->with('message',$response->message);
        }else
        {
            return redirect('/admin/banner')->with('message',"Delete Data Failed, Something Wrong");
        }
    }
}
