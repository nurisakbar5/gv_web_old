<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\CategoryRepository;

class CategoryController extends Controller
{
    protected $categoryRepo;

    function __construct(CategoryRepository $categoryRepo)
    {
        $this->middleware('redirectIfNotLogin');
        $this->categoryRepo = $categoryRepo;
        $this->entity = ['article'=>'article','video'=>'video','product'=>'product','farm'=>'farm','harvest'=>'harvest','land'=>'land','komoditas'=>'komoditas','forum'=>'Forum'];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['categories']     = $this->categoryRepo->getAllCategory();
        return view('backend.category.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['entity'] = $this->entity;
        return view('backend.category.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $response = $this->categoryRepo->create($request);

        if(isset($response->status)=='success')
        {
            return redirect('/admin/category')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response               = $this->categoryRepo->getCategoryById($id);
        $data['category']       = $response->data;
        $data['entity'] = $this->entity;
        return view('backend.category.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        

        $response = $this->categoryRepo->update($request,$id);
        
        if(isset($response->status)=='success')
        {
            return redirect('/admin/category')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = $this->categoryRepo->delete($id);
        
        if(isset($response->status)=='success')
        {
            return redirect('/admin/category')->with('message',$response->message);
        }else
        {
            return redirect('/admin/category')->with('message',"Delete Data Failed, Something Wrong");
        }
    }
}
