<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\IklanRepository;

class IklanController extends Controller
{
    protected $iklanRepo;
    
    function __construct(IklanRepository $iklanRepo)
    {
        $this->middleware('redirectIfNotLogin');
        $this->iklanRepo = $iklanRepo;       
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['iklans']       = $this->iklanRepo->getAllIklan();
        return view('backend.iklan.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.iklan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'        => 'required',
            'description' => 'required',
            'link'        => 'required',
            'image'       => 'required'
            ]);

        $response = $this->iklanRepo->create($request);

        if(isset($response->status)=='success')
        {
            return redirect('/admin/iklan')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $request        = $this->iklanRepo->getIklan($id);
        $data['iklan'] = $request->data;
        
        return view('backend.iklan.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = $this->iklanRepo->update($request,$id);
        
        if(isset($response->status)=='success')
        {
            return redirect('/admin/iklan')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = $this->iklanRepo->delete($id);
        
        if(isset($response->status)=='success')
        {
            return redirect('/admin/iklan')->with('message',$response->message);
        }else
        {
            return redirect('/admin/iklan')->with('message',"Delete Data Failed, Something Wrong");
        }
    }

    public function trash()
    {
        $request                = $this->iklanRepo->getTrashIklan();
        $data['trashIklan']     = $request->data;
        return view('backend.iklan.trash_iklan',$data);
    }

    public function restore($id)
    {
        $request                = $this->iklanRepo->getRestoreIklan($id);

        if(isset($response->status)=='success')
        {
            return redirect('admin/iklan/trashed')->with('message', "Iklan Successfully Restored");
        }else
        {
            return redirect('/admin/iklan/trashed')->with('message',"Iklan Successfully Restored");
            
        }
    }

    public function delPermanent($id)
    {
        $response = $this->iklanRepo->deletePermanent($id);
        
        if(isset($response->status)=='success')
        {
            return redirect('/admin/iklan/trashed')->with('message',$response->message);
        }else
        {
            return redirect('/admin/iklan/trashed')->with('message',"Delete Data Failed, Something Wrong");
        }
    }
}
