<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\UserRepository;

class UserController extends Controller
{
    protected $userRepo;

    function __construct(UserRepository $userRepo)
    {
        $this->middleware('redirectIfNotLogin');
        $this->userRepo = $userRepo;
    }


    public function index()
    {
        $request                = $this->userRepo->getAlluser();
        $data['users']          = $request->data;
        return view('backend.user.index',$data);
    }

    public function show($id)
    {
        $request                = $this->userRepo->getUserById($id);
        $data['farmer']         = $request->data;
        //dd($data['user']);
        return view('backend.user.show',$data);
    }
}
