<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\ProductRepository;
use App\Repositories\WebService\CategoryRepository;

class ProductController extends Controller
{
    protected $productRepo;

    function __construct(ProductRepository $productRepo)
    {
        $this->middleware('redirectIfNotLogin');
        $this->productRepo = $productRepo;
    }

    public function index()
    {
        $data['products']       = $this->productRepo->getAllProduct();
        return view('backend.product.index',$data);
    }

    public function create(CategoryRepository $categoryRepo)
    {
        $data['categories']     = $categoryRepo->getAllCategory();
        return view('backend.product.create',$data);
    }

    public function edit($id,CategoryRepository $categoryRepo)
    {
        $data['categories']     = $categoryRepo->getAllCategory();
        $data['product']        = $this->productRepo->getProductById($id);
        return view('backend.product.edit',$data);
    }

    public function show($slug)
    {
        $request                = $this->productRepo->getProductBySlug($slug);
        $data['product']         = $request->data;
        //dd($data['product']);
        return view('backend.product.show',$data);
    }

    public function destroy($id)
    {
        $response               = $this->productRepo->delete($id);
        
        if(isset($response->status)=='success')
        {
            return redirect('/admin/product')->with('message',$response->message);
        }else
        {
            return redirect('/admin/product')->with('message',"Delete Data Failed, Something Wrong");
        }
    }
}
