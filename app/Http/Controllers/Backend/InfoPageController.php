<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\InfoPageRepository;

class InfoPageController extends Controller
{

    protected $infoPageRepo;

    function __construct(InfoPageRepository $infoPageRepo)
    {
        $this->middleware('redirectIfNotLogin', ['except'=>['show']]);
        $this->infoPageRepo = $infoPageRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['infopages']       = $this->infoPageRepo->getAllInfoPagee();
        return view('backend.infopage.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.infopage.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'        =>  'required',
            'description' =>  'required',
            ]);

        $response = $this->infoPageRepo->create($request);

        if(isset($response->status)=='success')
        {
            return redirect('/admin/infopage')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['detail'] = $this->infoPageRepo->getInfoPage($id);

        $data['pages'] = $this->infoPageRepo->getAllInfoPagee();
        $data['current'] = $id;

        return view('frontend.infopage.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $request        = $this->infoPageRepo->getInfoPage($id);
        $data['infopage'] = $request->data;

        return view('backend.infopage.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = $this->infoPageRepo->update($request,$id);

        if(isset($response->status)=='success')
        {
            return redirect('/admin/infopage')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = $this->infoPageRepo->delete($id);

        if(isset($response->status)=='success')
        {
            return redirect('/admin/infopage')->with('message',$response->message);
        }else
        {
            return redirect('/admin/infopage')->with('message',"Delete Data Failed, Something Wrong");
        }
    }
}
