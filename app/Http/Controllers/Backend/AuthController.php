<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\AdminRepository;

class AuthController extends Controller
{
    function formlogin()
    {
        return view('backend.formlogin');
    }

    function login(Request $request,AdminRepository $adminRepo)
    {
        $response = $adminRepo->login($request);
        if($response->success!=true)
        {
            return redirect('/admin/login')->with('message',$response->message);
        }else
        {
            $result     = $response->data;
            session(['admin_id' => $result->id]);
            session(['admin_name' => $result->name]);
            return redirect('admin/article')->with('message','Selamat Datang '.$result->name);
        }
    }

    public function logout()
    {
        session()->forget(['admin_id', 'admin_name']);
        return redirect('/admin/login')->with('message','Anda Berhasil Logout');
    }
}
