<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\PriceRepository;
use App\Repositories\WebService\MarketRepository;
use App\Repositories\WebService\CommodityRepository;
use App\Repositories\WebService\RegionRepository;

class PriceController extends Controller
{
    protected $priceRepo;
    protected $regionRepo;
    protected $marketRepo;
    protected $commodityRepo;

    function __construct(PriceRepository $priceRepo, RegionRepository $regionRepo, MarketRepository $marketRepo, CommodityRepository $commodityRepo)
    {
        $this->middleware('redirectIfNotLogin');
        $this->priceRepo = $priceRepo;
        $this->regionRepo = $regionRepo;
        $this->marketRepo = $marketRepo;
        $this->commodityRepo = $commodityRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(isset($request->period))
		{
			$period = date_format(date_create($request->period),"Y-m-d") ;
		}else
		{
			$period = date('Y-m-d');
		}

		if(isset($request->markets))
		{
			$marketId = $request->markets;
		}else
		{
			$marketId = '1';
		}

        $data['provinces'] = $this->regionRepo->getAllProvince();
        // dd($data['provinces']);
        $data['prices']	= $this->priceRepo->getPriceByMarket($marketId, $period);

        return view('backend.price.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function create()
    {
        $data['markets'] = $this->marketRepo->getAllMarket();
        $data['commodities'] = $this->commodityRepo->getAllCommodity();
        return view('backend.price.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
        $this->validate($request, [
            'market_id' => 'required',
            'commodity_id' => 'required',
            'price' => 'required',
            ]);

        $response = $this->priceRepo->create($request);

        if(isset($response->status)=='success')
        {
            return redirect('/admin/price')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['price'] = $this->priceRepo->getPriceById($id);
        $data['markets'] = $this->marketRepo->getAllMarket();
        $data['commodities'] = $this->commodityRepo->getAllCommodity();
        //return $data;
        return view('backend.price.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'market_id' => 'required',
            'commodity_id' => 'required',
            'price' => 'required',
            ]);

        $response = $this->priceRepo->update($request,$id);

        if(isset($response->status)=='success')
        {
            return redirect('/admin/price')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response               = $this->priceRepo->delete($id);

        if(isset($response->status)=='success')
        {
            return redirect('/admin/price')->with('message',$response->message);
        }else
        {
            return redirect('/admin/price')->with('message',"Delete Data Failed, Something Wrong");
        }
    }

    public function commodities(Request $request)
    {
        if(isset($request->period))
		{
			$period = date_format(date_create($request->period),"Ymd") ;
		}else
		{
			$period = date('Ymd');
		}

		if(isset($request->commodities))
		{
			$commodityId = $request->commodities;
		}else
		{
			$commodityId = '1';
		}

        $data['commodities'] = $this->commodityRepo->getAllCommodity();
        // dd($data['commodities']);
        $data['prices'] = $this->priceRepo->getPriceByCommodities($commodityId, $period);

        return view('backend.price.commodities',$data);
    }

    public function regencies(Request $request)
    {
        if(isset($request->period))
		{
			$period = date_format(date_create($request->period),"Ymd") ;
		}else
		{
			$period = date('Ymd');
		}

		if(isset($request->regencies))
		{
			$regencyId = $request->regencies;
		}else
		{
			$regencyId = '6472';
		}

        $data['provinces'] = $this->regionRepo->getAllProvince();
        // dd($data['regencies']);
        $data['prices'] = $this->priceRepo->getPriceByRegency($regencyId, $period);

        return view('backend.price.regencies',$data);
    }
}
