<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\VideoRepository;
use App\Repositories\WebService\CategoryRepository;
class VideoController extends Controller
{

    protected $videoRepo;

    function __construct(VideoRepository $videoRepo)
    {
        $this->middleware('redirectIfNotLogin');
        $this->videoRepo = $videoRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response               = $this->videoRepo->getAllVideo();
        $data['videos']         = $response->data;
        return view('backend.video.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CategoryRepository $repoCategory)
    {
        $data['categories']     = $repoCategory->getCategoryByEntity('video');
        return view('backend.video.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {

        $request->validate([
            'title'         => 'required',
            'category_id'   => 'required',
            'description'   => 'required',
            'tags'          => 'required',
            'video'         => 'required|mimetypes:video/mp4'
        ]);

        $response = $this->videoRepo->create($request);

        if(isset($response->status)=='success')
        {
            return redirect('/admin/video')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,CategoryRepository $repoCategory)
    {
        $data['video']          = $this->videoRepo->getVideoById($id);
        $data['categories']     = $repoCategory->getCategoryByEntity('video');
        return view('backend.video.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title'         => 'required',
            'category_id'   => 'required',
            'description'   => 'required',
            'tags'          => 'required'
        ]);


        $response = $this->videoRepo->update($request,$id);

        if(isset($response->status)=='success')
        {
            return redirect('/admin/video')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response               = $this->videoRepo->delete($id);

        if(isset($response->status)=='success')
        {
            return redirect('/admin/video')->with('message',$response->message);
        }else
        {
            return redirect('/admin/video')->with('message',"Delete Data Failed, Something Wrong");
        }
    }
}
