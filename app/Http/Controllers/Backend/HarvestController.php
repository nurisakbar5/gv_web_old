<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\HarvestRepository;

class HarvestController extends Controller
{
    protected $harvestRepo;

    function __construct(HarvestRepository $harvestRepo)
    {
        $this->middleware('redirectIfNotLogin');
        $this->harvestRepo = $harvestRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $request          = $this->harvestRepo->getHarvest();
        $data['harvests'] = $request;
        return view('backend.harvest.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $request         = $this->harvestRepo->getHarvestBySlug($slug);
        $data['harvest'] = $request;
        // dd($data['harvest']);
        return view('backend.harvest.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = $this->harvestRepo->delete($id);
        
        if(isset($response->status)=='success')
        {
            return redirect('/admin/harvest')->with('message',$response->message);
        }else
        {
            return redirect('/admin/harvest')->with('message',"Delete Data Failed, Something Wrong");
        }
    }

    public function trash()
    {
        $request                = $this->harvestRepo->getTrashHarvest();
        $data['trashHarvest']     = $request->data;
        return view('backend.harvest.trash',$data);
    }

    public function restore($id)
    {
        $request                = $this->harvestRepo->getRestoreHarvest($id);

        if(isset($request->status)=='success')
        {
            return redirect('/admin/harvest/trashed')->with('message',$request->message);
        }else
        {
            return redirect('/admin/harvest/trashed')->with('message',"Restore Data Failed, Something Wrong");
            
        }
    }

    public function delPermanent($id)
    {
        $response = $this->harvestRepo->deletePermanent($id);
        
        if(isset($response->status)=='success')
        {
            return redirect('/admin/harvest/trashed')->with('message',$response->message);
        }else
        {
            return redirect('/admin/harvest/trashed')->with('message',"Delete Data Failed, Something Wrong");
        }
    }
}
