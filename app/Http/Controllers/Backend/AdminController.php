<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\AdminRepository;

class AdminController extends Controller
{

    protected $adminRepo;

    function __construct(AdminRepository $adminRepo)
    {
        $this->middleware('redirectIfNotLogin');
        $this->adminRepo = $adminRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $request                = $this->adminRepo->getAllAdmin();
        $data['users']          = $request->data;
        return view('backend.admin.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'                  => 'required',
            'email'                 => 'required|unique:admins',
            'password'              => 'required',
            'password_confirmation' => 'required|same:password'
            ]);

        $response = $this->adminRepo->create($request);
        if(isset($response->status)==true)
        {
            return redirect('/admin/user-admin')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['article']        = $this->adminRepo->getArticleById($id);
        return view('backend.article.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = $this->adminRepo->delete($id);

        if(isset($response->status)=='success')
        {
            return redirect('/admin/user-admin')->with('message',$response->message);
        }else
        {
            return redirect('/admin/user-admin')->with('message',"Delete Data Failed, Something Wrong");
        }
    }

    public function trash()
    {
        $request                = $this->adminRepo->getTrashAdmin();
        $data['trashAdmin']     = $request->data;
        return view('backend.admin.trash_admin',$data);
    }

    public function restore($id)
    {
        $response                = $this->adminRepo->getRestoreAdmin($id);

        if(isset($response->status)=='success')
        {
            return redirect('admin/user-admin/trashed')->with('message', $response->message);
        }else
        {
            return redirect('/admin/user-admin/trashed')->with('message',"Restore Data Failed, Something Wrong");

        }
    }

    public function delPermanent($id)
    {
        $response = $this->adminRepo->deletePermanent($id);

        if(isset($response->status)=='success')
        {
            return redirect('/admin/user-admin')->with('message',$response->message);
        }else
        {
            return redirect('/admin/user-admin')->with('message',"Delete Data Failed, Something Wrong");
        }
    }
}
