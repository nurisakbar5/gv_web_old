<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\MarketRepository;
use App\Repositories\WebService\RegionRepository;

class MarketController extends Controller
{
    protected $marketRepo;
    protected $regionRepo;

    function __construct(MarketRepository $marketRepo, RegionRepository $regionRepo)
    {
        $this->middleware('redirectIfNotLogin');
        $this->marketRepo = $marketRepo;
        $this->regionRepo = $regionRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['markets']       = $this->marketRepo->getAllMarket();
        //return $data;
        return view('backend.market.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function create(Request $request)
    {
        // if(isset($request->regency))
		// {
		// 	$province_id =  $request->regency;
        // }
        $data['provinces']  = $this->regionRepo->getAllProvince();
        // $data['regencies']  = $this->regionRepo->getAllRegency($province_id);
        return view('backend.market.create', $data);
    }

    public function sendLocation()
    {
        return \Location::getLocation();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
        $this->validate($request, [
            'name'         =>  'required',
            'regency_id'       =>  'required'
            ]);

        $response = $this->marketRepo->create($request);

        if(isset($response->status)=='success')
        {
            return redirect('/admin/market')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, RegionRepository $regionRepo)
    {
        $data['market']        = $this->marketRepo->getMarketById($id);
        $data['regencies']     = $regionRepo->getAllRegency();
        return view('backend.market.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'         =>  'required',
            'regency_id'       =>  'required'
            ]);

        $response = $this->marketRepo->update($request,$id);

        if(isset($response->status)=='success')
        {
            return redirect('/admin/market')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response               = $this->marketRepo->delete($id);

        if(isset($response->status)=='success')
        {
            return redirect('/admin/market')->with('message',$response->message);
        }else
        {
            return redirect('/admin/market')->with('message',"Delete Data Failed, Something Wrong");
        }
    }
}
