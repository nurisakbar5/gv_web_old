<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\ArticleRepository;
use App\Repositories\WebService\CategoryRepository;
class ArticleController extends Controller
{
    protected $articleRepo;
    
    function __construct(ArticleRepository $articleRepo)
    {
        $this->middleware('redirectIfNotLogin');
        $this->articleRepo = $articleRepo;       
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['articles']       = $this->articleRepo->getAllArticle();
        return view('backend.article.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function create(CategoryRepository $categoryRepo)
    {
        $data['categories']     = $categoryRepo->getAllCategory();
        return view('backend.article.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
        $this->validate($request, [
            'title'         =>  'required',
            'article'       =>  'required',
            'category_id'   =>  'required',
            'image'         => 'required'
            ]);

        $response = $this->articleRepo->create($request);

        if(isset($response->status)=='success')
        {
            return redirect('/admin/article')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,CategoryRepository $categoryRepo)
    {
        $data['categories']     = $categoryRepo->getAllCategory();
        $data['article']        = $this->articleRepo->getArticleById($id);
        return view('backend.article.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'                 =>  'required',
            'article'               =>  'required',
            'category_id'           =>  'required',
            ]);

        $response = $this->articleRepo->update($request,$id);

        if(isset($response->status)=='success')
        {
            return redirect('/admin/article')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response               = $this->articleRepo->delete($id);
        
        if(isset($response->status)=='success')
        {
            return redirect('/admin/article')->with('message',$response->message);
        }else
        {
            return redirect('/admin/article')->with('message',"Delete Data Failed, Something Wrong");
        }
    }
}
