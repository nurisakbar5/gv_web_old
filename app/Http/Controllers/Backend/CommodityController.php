<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\CommodityRepository;
use App\Repositories\WebService\CategoryRepository;
class CommodityController extends Controller
{
    protected $commodityRepo;
    
    function __construct(CommodityRepository $commodityRepo)
    {
        $this->middleware('redirectIfNotLogin');
        $this->commodityRepo = $commodityRepo;       
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['commodities']       = $this->commodityRepo->getAllCommodity();
        return view('backend.commodity.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function create(CategoryRepository $categoryRepo)
    {
        //$data['categories']     = $categoryRepo->getAllCategory();
        $data['pluckCategories'] = $categoryRepo->getCategoryByEntity('komoditas');
        //return $data;
        return view('backend.commodity.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
        $this->validate($request, [
            'name'         =>  'required',
            'category_id'   =>  'required',
            'image'         => 'required'
            ]);

        $response = $this->commodityRepo->create($request);

        if(isset($response->status)=='success')
        {
            return redirect('/admin/commodity')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,CategoryRepository $categoryRepo)
    {
        //$data['categories']     = $categoryRepo->getAllCategory();
        $data['pluckCategories'] = $categoryRepo->getCategoryByEntity('komoditas');
        //return $data;
        $data['commodity']        = $this->commodityRepo->getCommodityById($id);
        return view('backend.commodity.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'         =>  'required',
            'category_id'   =>  'required',
            ]);

        $response = $this->commodityRepo->update($request,$id);

        if(isset($response->status)=='success')
        {
            return redirect('/admin/commodity')->with('message',$response->message);
        }else
        {
            return \Redirect::back()->withErrors($response)->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response               = $this->commodityRepo->delete($id);
        
        if(isset($response->status)=='success')
        {
            return redirect('/admin/commodity')->with('message',$response->message);
        }else
        {
            return redirect('/admin/commodity')->with('message',"Delete Data Failed, Something Wrong");
        }
    }
}
