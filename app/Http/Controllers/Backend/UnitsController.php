<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WebService\UnitsRepository;

class UnitsController extends Controller
{
    protected $unitsRepo;

    function __construct(UnitsRepository $unitsRepo)
    {
        $this->middleware('redirectIfNotLogin');
        $this->unitsRepo = $unitsRepo;
    }

    public function index()
    {
        $data['units'] = $this->unitsRepo->getAllUnits();
        // dd($data['units']);
        return view('backend.units.index', $data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required',
            'weight_in_gram'=>'required'
        ]);

        $response = $this->unitsRepo->create($request);
        return response()->json([
            'error'   => true,
            'message' => $response,
        ], 200);
    }

    public function show($id)
    {
        $response = $this->unitsRepo->getUnits($id)->data;
        return response()->json([
            'error' => false,
            'detail'=> $response,
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'=>'required',
            'weight_in_gram'=>'required'
        ]);

        $response = $this->unitsRepo->update($request, $id);
        return response()->json([
            'error'   => true,
            'message' => $response,
        ], 200);
    }

    public function destroy($id)
    {
        $response = $this->unitsRepo->delete($id);
        return response()->json([
            'error'  => false,
            'message'=> $response
        ], 200);
    }
}
