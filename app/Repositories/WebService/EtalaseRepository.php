<?php

namespace App\Repositories\WebService;


class EtalaseRepository
{
    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function etalase($slug)
    {
        $request                = $this->client->get($this->url.'stores/'.$slug.'/etalases');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function create($request)
    {
        $request = $this->client->post($this->url.'etalases',[
            'multipart' => [
                [
                    'name'      => 'store_id',
                    'contents'  => $request->store_id
                ],
                [
                    'name'      => 'name',
                    'contents'  => $request->name
                ]
            ]
        ]);
        $response = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function update($request, $id)
    {
        $request = $this->client->put($this->url.'etalases/'.$id,[
            'form_params' => [
                'name' => $request['name'],
            ]
        ]);
        $response = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function destroy($id)
    {
        $request                = $this->client->delete($this->url.'etalases/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }
}
