<?php

namespace App\Repositories\WebService;


class MarketRepository
{
    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function getAllMarket($start=null,$limit=null)
    {

        if($start===null)
        {
            $request                = $this->client->get($this->url.'markets');
        }else
        {
            $request                = $this->client->get($this->url.'markets?limit='.$limit.'&start='.$start);
        }
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getMarketById($id)
    {
        $request                = $this->client->get($this->url.'markets/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getMarketByRegency($id)
    {
        $request                = $this->client->get($this->url.'markets/regency/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function delete($id)
    {
        $request                = $this->client->delete($this->url.'markets/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function create($request)
    {

        $req = $this->client->post($this->url.'markets', [
            'multipart' => [
                [
                    'name'     => 'name',
                    'contents' => $request->name,
                ],
                [
                    'name'     => 'regency_id',
                    'contents' => $request->regency_id
                ]

            ]
        ]);

        $response = json_decode($req->getBody()->getContents());
        return $response;
    }

    public function update($request,$id)
    {
        $req = $this->client->post($this->url.'markets/'.$id, [
            'multipart' => [
                [
                    'name'     => 'name',
                    'contents' => $request->name,
                ],
                [
                    'name'     => 'regency_id',
                    'contents' => $request->regency_id
                ]
            ]
        ]);

        $response               = json_decode($req->getBody()->getContents());
        return $response;
    }
}
