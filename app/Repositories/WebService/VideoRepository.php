<?php

namespace App\Repositories\WebService;


class VideoRepository
{
    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function getAllVideo($start=null,$limit=null)
    {
        if($start==null)
        {
            $request                = $this->client->get($this->url.'videos');
        }else
        {
            $request                = $this->client->get($this->url.'videos?limit='.$limit.'&start='.$start);
        }

        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getVideoById($id)
    {
        $request                = $this->client->get($this->url.'videos/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getVideoRelatedBySlug($slug, $limit=null)
    {
        $request                = $this->client->get($this->url.'videos/'.$slug.'/related?limit='.$limit);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    // mendapatkan data video berdasarkan slug kategory
    public function getVideoByCategory($categorySlug,$start=null,$limit=null)
    {
        if($start===null)
        {
            $request                = $this->client->get($this->url."videos/category/$categorySlug");
        }else
        {
            $request                = $this->client->get($this->url."videos/category/$categorySlug".'?offset='.$start.'&limit='.$limit);
        }
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getVideoBySlug($slug)
    {
        $request                = $this->client->get($this->url.'videos/'.$slug);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }


    public function getVideoCategory()
    {
        $request                = $this->client->get($this->url.'video/category/7/0/3');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getRelatedVideo($slug, $limit=null)
    {
        $request                = $this->client->get($this->url.'videos/'.$slug.'/related?limit='.$limit);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getCommendVideoBySlug($slug, $limit=null)
    {
        $request                = $this->client->get($this->url.'videos/'.$slug.'/comments');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function postCommentVideo($request)
    {
        $request = $this->client->post($this->url.'videos/comments', [
            'multipart' => [
                [
                    'name'     => 'token',
                    'contents' => $request->token
                ],
                [
                    'name'     => 'comment',
                    'contents' => $request->comment
                ],
                [
                    'name'     => 'id',
                    'contents' => $request->id
                ]
            ]
        ]);

        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function create($request)
    {
        ini_set('max_execution_time', 2180);

        $video_file       =   $request->file('video');
        $video_file_path  =   $video_file->getPathName();
        $video_fileName   =   $video_file->getClientOriginalName();

        $req = $this->client->post($this->url.'videos', [
            'multipart' => [
                [
                    'name'     => 'video',
                    'contents' => fopen($video_file_path, 'r'),
                    'filename'=> $video_fileName
                ],
                [
                    'name'     => 'title',
                    'contents' => $request->title
                ],
                [
                    'name'     => 'tags',
                    'contents' => $request->tags
                ],
                [
                    'name'     => 'description',
                    'contents' => $request->description
                ],
                [
                    'name'     => 'category_id',
                    'contents' => $request->category_id
                ]
            ]
        ]);

        $response               = json_decode($req->getBody()->getContents());
        return $response;
    }


    public function update($request,$id)
    {
        ini_set('max_execution_time', 2180);

        if ($request->hasFile('video')) {

            $video_file       =   $request->file('video');
            $video_file_path  =   $video_file->getPathName();
            $video_fileName   =   $video_file->getClientOriginalName();

            // upload dengan video baru
            $req = $this->client->post($this->url.'videos/'.$id, [
                'multipart' => [
                    [
                        'name'     => 'video',
                        'contents' => fopen($video_file_path, 'r'),
                        'filename'=> $video_fileName
                    ],
                    [
                        'name'     => 'title',
                        'contents' => $request->title
                    ],
                    [
                        'name'     => 'tags',
                        'contents' => $request->tags
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ]
                ]
            ]);
        }else
        {
            $req = $this->client->post($this->url.'videos/'.$id, [
                'multipart' => [
                    [
                        'name'     => 'title',
                        'contents' => $request->title
                    ],
                    [
                        'name'     => 'tags',
                        'contents' => $request->tags
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ]
                ]
            ]);
        }

        $response               = json_decode($req->getBody()->getContents());
        return $response;
    }

    public function delete($id)
    {
        $request    = $this->client->delete($this->url.'videos/'.$id);
        $response   = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getVideoSearchByKeyword($keyword)
    {
        $request                = $this->client->get($this->url.'videos?keyword='.$keyword.'&limit=6');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }
}
