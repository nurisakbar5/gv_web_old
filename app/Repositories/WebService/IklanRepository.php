<?php 

namespace App\Repositories\WebService;

class IklanRepository
{
	protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function getAllIklan()
    {
        $request                = $this->client->get($this->url.'iklan/');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getIklan($id)
    {
        $request                = $this->client->get($this->url.'iklan/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function create($request)
    {
        ini_set('max_execution_time', 2180);
        
        $file       =   $request->file('image'); 
        $file_path  =   $file->getPathName();
        $fileName   =   $file->getClientOriginalName();

        $req = $this->client->post($this->url.'iklan/add', [
            'multipart' => [
                [
                    'name'     => 'image',
                    'contents' => fopen($file_path, 'r'),
                    'filename'=> $fileName
                ],
                [
                    'name'     => 'title',
                    'contents' => $request->title
                ],
                [
                    'name'     => 'description',
                    'contents' => $request->description
                ],
                [
                	'name'	   => 'link',
                	'contents' => $request->link
                ]
            ]
        ]);

        $response = json_decode($req->getBody()->getContents());
        return $response;
    }

    public function update($request,$id)
    {
        if ($request->hasFile('image')) {
            // update with image
            $file       =   $request->file('image'); 
            $file_path  =   $file->getPathName();
            $fileName   =   $file->getClientOriginalName();

            $req = $this->client->post($this->url.'iklan/'.$id, [
                'multipart' => [
                    [
	                    'name'     => 'image',
	                    'contents' => fopen($file_path, 'r'),
		                'filename' => $fileName
	                ],
	                [
	                    'name'     => 'title',
	                    'contents' => $request->title
	                ],
	                [
	                    'name'     => 'description',
	                    'contents' => $request->description
	                ],
	                [
	                	'name'	   => 'link',
	                	'contents' => $request->link
	                ]
                ]
            ]);
        }else{
            $req = $this->client->post($this->url.'iklan/'.$id, [
                'multipart' => [
                    [
	                    'name'     => 'title',
	                    'contents' => $request->title
	                ],
	                [
	                    'name'     => 'description',
	                    'contents' => $request->description
	                ],
	                [
	                	'name'	   => 'link',
	                	'contents' => $request->link
	                ]
	            ]
            ]);
        }

        $response               = json_decode($req->getBody()->getContents());
        return $response;
    }

    public function delete($id)
    {
        $request                = $this->client->delete($this->url.'iklan/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function deletePermanent($id)
    {
        $request                = $this->client->delete($this->url.'iklan/trash/delpermanent/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getTrashIklan()
    {
        $request                = $this->client->get($this->url.'iklan/trash');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getRestoreIklan($id)
    {
        $request                = $this->client->get($this->url.'iklan/trash/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }
}