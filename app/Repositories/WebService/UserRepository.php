<?php

namespace App\Repositories\WebService;

class UserRepository
{

    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function getAllUser()
    {
        $request                = $this->client->get($this->url.'users');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getUserById($id)
    {
        $request                = $this->client->get($this->url.'users/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getUserProfileByToken($token)
    {
        $request                = $this->client->get($this->url.'users/profile/'.$token);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getUserProfileById($id)
    {
        $request                = $this->client->get($this->url.'users/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getUserAddressesByToken($token)
    {
        $request                = $this->client->get($this->url.'users/'.$token.'/addresses');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getUserProductByIdStore($id_store)
    {
        $request                = $this->client->get($this->url.'stores/'.$id_store.'/products');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getUserPanenByToken($token)
    {
        $request                = $this->client->get($this->url.'users/'.$token.'/harvests');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getUserPenjualanByToken($token)
    {
        $request                = $this->client->get($this->url.'order/selling/'.$token);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getUserPembelianByToken($token)
    {
        $request                = $this->client->get($this->url.'order/purchase/'.$token);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getUserLahanByToken($token)
    {
        $request                = $this->client->get($this->url.'users/'.$token.'/lands');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function activate($code)
    {
        $request                = $this->client->get($this->url.'users/activate/'.$code);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function update($id,$request)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        if($request->password!=null)
        {
            $user->password = $request->password;
        }
        $user->update();
    }

    public function login($request)
    {
        $request = $this->client->post($this->url.'users/login', [
            'multipart' => [
                [
                    'name'     => 'email',
                    'contents' => $request->email
                ],
                [
                    'name'     => 'password',
                    'contents' => $request->password
                ]
            ]
        ]);

        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function register($request)
    {
        $request = $this->client->post($this->url.'users/register', [
            'multipart' => [
                [
                    'name'     => 'name',
                    'contents' => $request->name
                ],
                [
                    'name'     => 'email',
                    'contents' => $request->email
                ],
                [
                    'name'     => 'password',
                    'contents' => $request->password
                ]
            ]
        ]);

        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function forgot($request)
    {
        $request = $this->client->post($this->url.'users/forgetpassword', [
            'multipart' => [
                [
                    'name'     => 'email',
                    'contents' => $request->email
                ]
            ]
        ]);

        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function updateProfileImage($request)
    {
        ini_set('max_execution_time', 2180);

        $file       =   $request->file('image');
        $file_path  =   $file->getPathName();
        $fileName   =   $file->getClientOriginalName();

        $req = $this->client->post($this->url.'users/update-profile-image', [
            'multipart' => [
                [
                    'name'     => 'image',
                    'contents' => fopen($file_path, 'r'),
                    'filename'=> $fileName
                ],
                [
                	'name'	   => 'token',
                	'contents' => $request->token
                ]
            ]
        ]);

        $response = json_decode($req->getBody()->getContents());
        return $response;
    }

    public function updateProfile($request)
    {
        $request = $this->client->post($this->url.'users/update-profilee', [
            'multipart' => [
                [
                    'name'     => 'token',
                    'contents' => $request->token
                ],
                [
                    'name'     => 'name',
                    'contents' => $request->name
                ],
                [
                    'name'     => 'email',
                    'contents' => $request->email
                ],
                [
                    'name'     => 'phone',
                    'contents' => $request->phone
                ],
                [
                    'name'     => 'village',
                    'contents' => $request->village
                ],
                [
                    'name'     => 'address',
                    'contents' => $request->address
                ]
            ]
        ]);

        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function updateProfilePassword($request)
    {
        $request = $this->client->post($this->url.'users/change-password', [
            'multipart' => [
                [
                    'name'     => 'token',
                    'contents' => $request->token
                ],
                [
                    'name'     => 'old_password',
                    'contents' => $request->old_password
                ],
                [
                    'name'     => 'new_password',
                    'contents' => $request->new_password
                ]
            ]
        ]);

        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function updateProfileVillage($request)
    {
        $request = $this->client->post($this->url.'users/update-profilee-village', [
            'multipart' => [
                [
                    'name'     => 'token',
                    'contents' => $request->token
                ],
                [
                    'name'     => 'village',
                    'contents' => $request->village
                ]
            ]
        ]);

        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }
}
