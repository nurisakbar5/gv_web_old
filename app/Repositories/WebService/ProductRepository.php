<?php

namespace App\Repositories\WebService;


class ProductRepository
{
    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function getAllProduct($start=null,$limit=null)
    {

        if($start===null)
        {
            $request                = $this->client->get($this->url.'products');
        }else
        {
            $request                = $this->client->get($this->url.'products?limit='.$limit.'&start='.$start);
        }
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getProductByCategory($categoryId,$start=null,$limit=null)
    {
        if($start===null)
        {
            $request                = $this->client->get($this->url."products/category/$categoryId");
        }else
        {
            $request                = $this->client->get($this->url."products/category/$categoryId".'?offset='.$start.'&limit='.$limit);
        }
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getProductByEtalase($etalaseId,$start=null,$limit=null)
    {
        if($start===null)
        {
            $request                = $this->client->get($this->url."products/etalase/$etalaseId");
        }else
        {
            $request                = $this->client->get($this->url."products/etalase/$etalaseId".'?offset='.$start.'&limit='.$limit);
        }
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getProductByRegency($regencyId)
    {
        $request                = $this->client->get($this->url."products/region/".$regencyId);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getProductById($id)
    {
        $request                = $this->client->get($this->url.'products/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getCommendProduct($slug)
    {
        $request                = $this->client->get($this->url.'products/'.$slug.'/comments');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function postCommentProduct($request)
    {
        $request = $this->client->post($this->url.'products/comments', [
            'multipart' => [
                [
                    'name'     => 'token',
                    'contents' => $request->token
                ],
                [
                    'name'     => 'comment',
                    'contents' => $request->comment
                ],
                [
                    'name'     => 'id',
                    'contents' => $request->id
                ]
            ]
        ]);

        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getRelatedProduct($slug)
    {
        $request                = $this->client->get($this->url.'products/'.$slug.'/related');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getProductBySlug($slug)
    {
        $request                = $this->client->get($this->url.'products/'.$slug);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function delete($id)
    {
        $request                = $this->client->delete($this->url.'products/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function update($request,$id)
    {
        if ($request->hasFile('image')) {
            // update with image
            $file       =   $request->file('image');
            $file_path  =   $file->getPathName();
            $fileName   =   $file->getClientOriginalName();

            $req = $this->client->post($this->url.'products/'.$id, [
                'multipart' => [
                    [
                        'name'     => 'image',
                        'contents' => fopen($file_path, 'r'),
                        'filename'=> $fileName
                    ],
                    [
                        'name'     => 'title',
                        'contents' => $request->title
                    ],
                    [
                        'name'     => 'product',
                        'contents' => $request->product
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ]
                    ]
            ]);
        }else{
            $req = $this->client->post($this->url.'products/'.$id, [
                'multipart' => [
                    [
                        'name'     => 'title',
                        'contents' => $request->title
                    ],
                    [
                        'name'     => 'product',
                        'contents' => $request->product
                    ],
                    [
                        'name'     => 'tags',
                        'contents' => $request->tags
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ]
                    ]
            ]);
        }

        $response               = json_decode($req->getBody()->getContents());
        return $response;
    }

    public function getProductSearchByKeyword($keyword)
    {
        $request                = $this->client->get($this->url.'products?keyword='.$keyword.'&offset=0&limit=9');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getproductSearchByCategory($id)
    {
        $request                = $this->client->get($this->url.'products/category/'.$id.'/0/3/');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }
}
