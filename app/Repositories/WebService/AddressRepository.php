<?php

namespace App\Repositories\WebService;


class AddressRepository
{
    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function getAllData($token)
    {
        $request                = $this->client->get($this->url.'users/'.$token.'/addresses');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function detail($id)
    {
        $request                = $this->client->get($this->url.'addresses/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

}
