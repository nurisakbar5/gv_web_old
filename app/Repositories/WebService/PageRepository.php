<?php

namespace App\Repositories\WebService;


use Kurt\Repoist\Repositories\Eloquent\AbstractRepository;

class PageRepository extends AbstractRepository
{
    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function PageHome()
    {
        $request                = $this->client->get($this->url.'page/home');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function PageStore()
    {
        $request                = $this->client->get($this->url.'page/store');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function PageArticle()
    {
        $request                = $this->client->get($this->url.'article');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function PageProduct()
    {
        $request                = $this->client->get($this->url.'products?limit=9&start=0');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function PageHarvest()
    {
        $request                = $this->client->get($this->url.'page/harvest');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function PageVideo()
    {
        $request                = $this->client->get($this->url.'videos?limit=12');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function PageForum()
    {
        $request                = $this->client->get($this->url.'forum');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function PageVillage()
    {
        $request                = $this->client->get($this->url.'village');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }
}
