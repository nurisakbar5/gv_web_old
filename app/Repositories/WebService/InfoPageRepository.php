<?php

namespace App\Repositories\WebService;

class InfoPageRepository
{
	protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function getAllInfoPagee()
    {
        $request                = $this->client->get($this->url.'infopages');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getAllInfoPage()
    {
        $request                = $this->client->get($this->url.'infopages?offset=0&limit=5');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getAllInfoPage2()
    {
        $request                = $this->client->get($this->url.'infopages?offset=5&limit=5');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getInfoPage($slug)
    {
        $request                = $this->client->get($this->url.'infopages/'.$slug);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function create($request)
    {

        $req = $this->client->post($this->url.'infopages', [
            'multipart' => [
                [
                    'name'     => 'title',
                    'contents' => $request->title
                ],
                [
                    'name'     => 'description',
                    'contents' => $request->description
                ],
                [
                    'name'     => 'publish',
                    'contents' => $request->publish
                ]
            ]
        ]);
        $response = json_decode($req->getBody()->getContents());
        return $response;
    }

    public function update($request,$id)
    {
        $req = $this->client->post($this->url.'infopages/edit/'.$id, [
            'multipart' => [
                [
                    'name'     => 'title',
                    'contents' => $request->title
                ],
                [
                    'name'     => 'description',
                    'contents' => $request->description
                ],
                [
                    'name'     => 'publish',
                    'contents' => $request->publish
                ]
            ]
        ]);


        $response = json_decode($req->getBody()->getContents());
        return $response;
    }

    public function delete($id)
    {
        $request                = $this->client->delete($this->url.'infopages/delete/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }
}
