<?php

namespace App\Repositories\WebService;


class AdminRepository
{
    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function getAllAdmin()
    {
        $request                = $this->client->get($this->url.'admins');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function login($request)
    {
        //$request                = $this->client->post($this->url.'admin/login');
        $request = $this->client->post($this->url.'admins/login', [
            'multipart' => [
                [
                    'name'     => 'email',
                    'contents' => $request->email
                ],
                [
                    'name'     => 'password',
                    'contents' => $request->password
                ]
            ]
        ]);

        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function register()
    {
        $request                = $this->client->post($this->url.'admins');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function delete($id)
    {
        $request                = $this->client->delete($this->url.'admins/delete/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function deletePermanent($id)
    {
        $request                = $this->client->delete($this->url.'admins/trash/delpermanent/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getTrashAdmin()
    {
        $request                = $this->client->get($this->url.'admins/trash');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getRestoreAdmin($id)
    {
        $request                = $this->client->get($this->url.'admins/trash/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function create($request)
    {
        $request = $this->client->post($this->url.'admins', [
            'multipart' => [
                [
                    'name'     => 'name',
                    'contents' => $request->name
                ],
                [
                    'name'     => 'email',
                    'contents' => $request->email
                ],
                [
                    'name'     => 'password',
                    'contents' => $request->password
                ],
                [
                    'name'     => 'password_confirmation',
                    'contents' => $request->password_confirmation
                ]
            ]
        ]);

        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }
}
