<?php

namespace App\Repositories\WebService;


class LandRepository
{
    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function getAllLand()
    {
        $request                = $this->client->get($this->url.'lands');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getLandById($id)
    {
        $request                = $this->client->get($this->url.'lands/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

   public function create($request)
    {
        ini_set('max_execution_time', 2180);

        $file       =   $request->file('image_1');
        $file_path  =   $file->getPathName();
        $fileName   =   $file->getClientOriginalName();

        $file2       =   $request->file('image_2');
        $file_path2  =   $file2->getPathName();
        $fileName2   =   $file2->getClientOriginalName();

        $file3       =   $request->file('image_3');
        $file_path3  =   $file3->getPathName();
        $fileName3   =   $file3->getClientOriginalName();

        $file4       =   $request->file('image_4');
        $file_path4  =   $file4->getPathName();
        $fileName4   =   $file4->getClientOriginalName();

        $req = $this->client->post($this->url.'lands', [
            'multipart' => [
                [
                    'name'     => 'image_1',
                    'contents' => fopen($file_path, 'r') ,
                    'filename'=> $fileName
                ],
                [
                    'name'     => 'image_2',
                    'contents' => fopen($file_path2, 'r') ,
                    'filename'=> $fileName2
                ],
                [
                    'name'     => 'image_3',
                    'contents' => fopen($file_path3, 'r') ,
                    'filename'=> $fileName3
                ],
                [
                    'name'     => 'image_4',
                    'contents' => fopen($file_path4, 'r') ,
                    'filename'=> $fileName4
                ],
                [
                    'name'     => 'name',
                    'contents' => $request->name
                ],
                [
                    'name'     => 'description',
                    'contents' => $request->description
                ],
                [
                    'name'     => 'token',
                    'contents' => $request->token
                ],
                [
                    'name'     => 'large',
                    'contents' => $request->large
                ],
                [
                    'name'     => 'unit_area',
                    'contents' => $request->unit_area
                ],
                [
                    'name'     => 'street',
                    'contents' => $request->street
                ],
                [
                    'name'     => 'address',
                    'contents' => $request->street
                ],
                [
                    'name'     => 'village_id',
                    'contents' => $request->village_id
                ]
            ]
        ]);

        $response = json_decode($req->getBody()->getContents());
        return $response;
    }

    public function update($request,$id)
    {
        if ($request->hasFile('image_1') && $request->hasFile('image_2') && $request->hasFile('image_3') && $request->hasFile('image_4')) {
            // update with image_1
            $file       =   $request->file('image_1');
            $file_path  =   $file->getPathName();
            $fileName   =   $file->getClientOriginalName();

            $file2       =   $request->file('image_2');
            $file_path2  =   $file2->getPathName();
            $fileName2   =   $file2->getClientOriginalName();

            $file3       =   $request->file('image_3');
            $file_path3  =   $file3->getPathName();
            $fileName3   =   $file3->getClientOriginalName();

            $file4       =   $request->file('image_4');
            $file_path4  =   $file4->getPathName();
            $fileName4   =   $file4->getClientOriginalName();

            $req = $this->client->post($this->url.'lands/'.$id, [
                'multipart' => [
                    [
                        'name'     => 'image_1',
                        'contents' => fopen($file_path, 'r') ,
                        'filename'=> $fileName
                    ],
                    [
                        'name'     => 'image_2',
                        'contents' => fopen($file_path2, 'r') ,
                        'filename'=> $fileName2
                    ],
                    [
                        'name'     => 'image_3',
                        'contents' => fopen($file_path3, 'r') ,
                        'filename'=> $fileName3
                    ],
                    [
                        'name'     => 'image_4',
                        'contents' => fopen($file_path4, 'r') ,
                        'filename'=> $fileName4
                    ],
                    [
                        'name'     => 'name',
                        'contents' => $request->name
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ],
                    [
                        'name'     => 'large',
                        'contents' => $request->large
                    ],
                    [
                        'name'     => 'unit_area',
                        'contents' => $request->unit_area
                    ],
                    [
                        'name'     => 'street',
                        'contents' => $request->street
                    ],
                    [
                        'name'     => 'address',
                        'contents' => $request->street
                    ],
                    [
                        'name'     => 'village_id',
                        'contents' => $request->village_id
                    ]
                ]

            ]);
        } else {
            $req = $this->client->post($this->url.'lands/'.$id, [
                'multipart' => [
                    [
                        'name'     => 'name',
                        'contents' => $request->name
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ],
                    [
                        'name'     => 'large',
                        'contents' => $request->large
                    ],
                    [
                        'name'     => 'unit_area',
                        'contents' => $request->unit_area
                    ],
                    [
                        'name'     => 'street',
                        'contents' => $request->street
                    ],
                    [
                        'name'     => 'address',
                        'contents' => $request->street
                    ],
                    [
                        'name'     => 'village_id',
                        'contents' => $request->village_id
                    ]
                ]

            ]);
        }

        if ($request->hasFile('image_1')) {
            $file_1       =   $request->file('image_1');
            $file_path_1  =   $file_1->getPathName();
            $fileName_1   =   $file_1->getClientOriginalName();

            $req = $this->client->post($this->url.'lands/'.$id, [
                'multipart' => [
                    [
                        'name'     => 'image_1',
                        'contents' => fopen($file_path_1, 'r') ,
                        'filename'=> $fileName_1
                    ],
                    [
                        'name'     => 'name',
                        'contents' => $request->name
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ],
                    [
                        'name'     => 'large',
                        'contents' => $request->large
                    ],
                    [
                        'name'     => 'unit_area',
                        'contents' => $request->unit_area
                    ],
                    [
                        'name'     => 'street',
                        'contents' => $request->street
                    ],
                    [
                        'name'     => 'address',
                        'contents' => $request->street
                    ],
                    [
                        'name'     => 'village_id',
                        'contents' => $request->village_id
                    ]
                ]

            ]);
        }

        if ($request->hasFile('image_2')) {
            $file_2       =   $request->file('image_2');
            $file_path_2  =   $file_2->getPathName();
            $fileName_2   =   $file_2->getClientOriginalName();

            $req = $this->client->post($this->url.'lands/'.$id, [
                'multipart' => [
                    [
                        'name'     => 'image_2',
                        'contents' => fopen($file_path_2, 'r') ,
                        'filename'=> $fileName_2
                    ],
                    [
                        'name'     => 'name',
                        'contents' => $request->name
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ],
                    [
                        'name'     => 'large',
                        'contents' => $request->large
                    ],
                    [
                        'name'     => 'unit_area',
                        'contents' => $request->unit_area
                    ],
                    [
                        'name'     => 'street',
                        'contents' => $request->street
                    ],
                    [
                        'name'     => 'address',
                        'contents' => $request->street
                    ],
                    [
                        'name'     => 'village_id',
                        'contents' => $request->village_id
                    ]
                ]

            ]);
        }

        if ($request->hasFile('image_3')) {
            $file_3       =   $request->file('image_3');
            $file_path_3  =   $file_3->getPathName();
            $fileName_3   =   $file_3->getClientOriginalName();

            $req = $this->client->post($this->url.'lands/'.$id, [
                'multipart' => [
                    [
                        'name'     => 'image_3',
                        'contents' => fopen($file_path_3, 'r') ,
                        'filename'=> $fileName_3
                    ],
                    [
                        'name'     => 'name',
                        'contents' => $request->name
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ],
                    [
                        'name'     => 'large',
                        'contents' => $request->large
                    ],
                    [
                        'name'     => 'unit_area',
                        'contents' => $request->unit_area
                    ],
                    [
                        'name'     => 'street',
                        'contents' => $request->street
                    ],
                    [
                        'name'     => 'address',
                        'contents' => $request->street
                    ],
                    [
                        'name'     => 'village_id',
                        'contents' => $request->village_id
                    ]
                ]

            ]);
        }

        if ($request->hasFile('image_4')) {
            $file_4       =   $request->file('image_4');
            $file_path_4  =   $file_4->getPathName();
            $fileName_4   =   $file_4->getClientOriginalName();

            $req = $this->client->post($this->url.'lands/'.$id, [
                'multipart' => [
                    [
                        'name'     => 'image_4',
                        'contents' => fopen($file_path_4, 'r') ,
                        'filename'=> $fileName_4
                    ],
                    [
                        'name'     => 'name',
                        'contents' => $request->name
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ],
                    [
                        'name'     => 'large',
                        'contents' => $request->large
                    ],
                    [
                        'name'     => 'unit_area',
                        'contents' => $request->unit_area
                    ],
                    [
                        'name'     => 'street',
                        'contents' => $request->street
                    ],
                    [
                        'name'     => 'address',
                        'contents' => $request->street
                    ],
                    [
                        'name'     => 'village_id',
                        'contents' => $request->village_id
                    ]
                ]

            ]);
        }

        $response               = json_decode($req->getBody()->getContents());
        return $response;
    }

    public function delete($id)
    {
        $request                = $this->client->delete($this->url.'lands/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }
}
