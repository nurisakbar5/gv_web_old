<?php

namespace App\Repositories\WebService;


class RegionRepository
{
    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    // mendapatkan semua data provinsi
    public function getAllProvince()
    {
        $request                = $this->client->get($this->url.'regions/province');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    // mendapatkan data detail provinsi
    public function getProvinceDetail($provinceId)
    {
        $request                = $this->client->get($this->url.'regions/province/'.$provinceId);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }


    // mendapatkan data kabupaten pada sebuah provinsi
    public function getAllRegency($provinceId)
    {
        $request                = $this->client->get($this->url.'regions/province/'.$provinceId.'/regency');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    // mendapatkan data detail kabupaten
    public function getRegencyDetail($id)
    {
        $request                = $this->client->get($this->url.'regions/regency/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    // mendapatkan data kecamatan dalam sebuah kabupaten
    public function getAllDistrict($regencyid)
    {
        $request                = $this->client->get($this->url.'regions/regency/'.$regencyid.'/district');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    // mendapatkan detail kecamatan
    public function getDistrictDetail($districtId)
    {
        $request                = $this->client->get($this->url.'regions/district/'.$districtId);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }


    // mendapatkan data desa pada sebuah kecamatan
    public function getAllVillage($districtId)
    {
        $request                = $this->client->get($this->url.'regions/district/'.$districtId.'/villages');
        $response               = json_decode($request->getBody()->getContents());

        return $response->data;
    }


    public function getVillageDetail($villageId)
    {
        $request                = $this->client->get($this->url.'regions/village/'.$villageId);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function searchVillage($keyword)
    {
        $request                = $this->client->get($this->url.'regions/village/search/'.$keyword);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function searchDistrict($keyword)
    {
        $request                = $this->client->get($this->url.'regions/district/search/'.$keyword);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    // public function delete($id)
    // {
    //     $request                = $this->client->delete($this->url.'region/regency/32/'.$id);
    //     $response               = json_decode($request->getBody()->getContents());
    //     return $response;
    // }

    // public function create($request)
    // {
    //     $req = $this->client->post($this->url.'region/regency/32/add', [
    //         'multipart' => [
    //             [
    //                 'name'     => 'name',
    //                 'contents' => $request->name,
    //             ]

    //         ]
    //     ]);

    //     $response = json_decode($req->getBody()->getContents());
    //     return $response;
    // }

    // public function update($request,$id)
    // {
    //     $req = $this->client->put($this->url.'region/regency/32/'.$id, [
    //         'multipart' => [
    //             [
    //                 'name'     => 'name',
    //                 'contents' => $request->name,
    //             ]
    //         ]
    //     ]);

    //     $response               = json_decode($req->getBody()->getContents());
    //     return $response;
    // }
}
