<?php

namespace App\Repositories\WebService;

class UnitsRepository
{
	protected $url;
	protected $client;

	public function __construct()
	{
		$this->url =  env("API_URL");
		$this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
	}

	public function getAllUnits()
	{
		$request                = $this->client->get($this->url.'units/');
		$response               = json_decode($request->getBody()->getContents());
		return $response;
	}

	public function getUnits($id)
	{
		$request                = $this->client->get($this->url.'units/'.$id);
		$response               = json_decode($request->getBody()->getContents());
		return $response;
	}

	public function create($request)
	{
		ini_set('max_execution_time', 2180);

		$req = $this->client->post($this->url.'units', [
			'multipart' => [
				[
					'name'     => 'name',
					'contents' => $request->name
				],
				[
					'name'	   => 'weight_in_gram',
					'contents' => $request->weight_in_gram
				]
			]
		]);

		$response = json_decode($req->getBody()->getContents());
		return $response;
	}

	public function update($request,$id)
	{
        $request = $this->client->put($this->url.'units/'.$id,[
            'form_params' => [
                'name' => $request['name'],
                'weight_in_gram' => $request['weight_in_gram'],
            ]
        ]);
		$response               = json_decode($request->getBody()->getContents());
		return $response;
	}

	public function delete($id)
	{
		$request                = $this->client->delete($this->url.'units/'.$id);
		$response               = json_decode($request->getBody()->getContents());
		return $response;
	}
}
