<?php

namespace App\Repositories\WebService;

class EspayRepository
{
    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function getChannelPayment()
    {
        $request                = $this->client->get($this->url.'espay/channelpayment');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }
}
