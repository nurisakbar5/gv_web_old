<?php

namespace App\Repositories\WebService;

class CategoryRepository
{
    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function getAllCategory($start=null,$limit=null)
    {
        $request                = $this->client->get($this->url.'categories');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function create($request)
    {
        ini_set('max_execution_time', 2180);

        $image_web                  =   $request->file('image_web');
        $file_path_image_web        =   $image_web->getPathName();
        $fileName_image_web         =   $image_web->getClientOriginalName();

        $image_mobile              =   $request->file('image_mobile');
        $file_path_image_mobile    =   $image_mobile->getPathName();
        $fileName_image_mobile     =   $image_mobile->getClientOriginalName();

        $req = $this->client->post($this->url.'categories', [
            'multipart' => [
                [
                    'name'     => 'image_web',
                    'contents' => fopen($file_path_image_web, 'r'),
                    'filename'=> $fileName_image_web
                ],
                [
                    'name'     => 'image_mobile',
                    'contents' => fopen($file_path_image_mobile, 'r'),
                    'filename'=> $fileName_image_mobile
                ],
                [
                    'name'     => 'name',
                    'contents' => $request->name
                ],
                [
                    'name'     => 'entity',
                    'contents' => $request->entity
                ],
                [
                    'name'     => 'publish',
                    'contents' => $request->publish
                ]
            ]
        ]);
        $response = json_decode($req->getBody()->getContents());
        return $response;
    }

    public function update($request,$id)
    {
        $content =[];

        if ($request->hasFile('image_web')) {
            $image_web              =   $request->file('image_web');
            $file_path_image_web    =   $image_web->getPathName();
            $fileName_image_web     =   $image_web->getClientOriginalName();

            array_push($content,['name'=>'image_web','contents'=>fopen($file_path_image_web, 'r'),'filename'=>$fileName_image_web]);
        }

        if ($request->hasFile('image_mobile')) {
            $image_mobile              =   $request->file('image_mobile');
            $file_path_image_mobile    =   $image_mobile->getPathName();
            $fileName_image_mobile     =   $image_mobile->getClientOriginalName();

            array_push($content,['name'=>'image_mobile','contents'=>fopen($file_path_image_mobile, 'r'),'filename'=>$fileName_image_mobile]);
        }

        array_push($content,   [
            'name'     => 'name',
            'contents' => $request->name
        ],
        [
            'name'     => 'entity',
            'contents' => $request->entity
        ],
        [
            'name'     => 'publish',
            'contents' => $request->publish
        ]);

        //dd($content);


        $req = $this->client->post($this->url.'categories/'.$id, [
            'multipart' => $content
        ]);

        $response = json_decode($req->getBody()->getContents());
        return $response;
    }

    public function delete($id)
    {
        $request                = $this->client->delete($this->url.'categories/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getCategoryByEntity($entity)
    {
        $request                = $this->client->get($this->url.'categories/pluck/'.$entity);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getCategoryById($id)
    {
        $request                = $this->client->get($this->url.'categories/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getAllPluckCategory()
    {
        $request                = $this->client->get($this->url.'categories/pluck/category');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }
}
