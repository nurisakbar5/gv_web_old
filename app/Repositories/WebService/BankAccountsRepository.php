<?php

namespace App\Repositories\WebService;


class BankAccountsRepository
{
    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function allBankAccount()
    {
        $request                = $this->client->get($this->url.'banks');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getBankAccount($id_store)
    {
        $request                = $this->client->get($this->url.'stores/'.$id_store.'/bankaccounts');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function create($request)
    {
        $request = $this->client->post($this->url.'bankaccounts',[
            'multipart' => [
                [
                    'name'      => 'store_id',
                    'contents'  => $request->store_id
                ],
                [
                    'name'      => 'bank_id',
                    'contents'  => $request->bank_id
                ],
                [
                    'name'      => 'account_number',
                    'contents'  => $request->account_number
                ],
                [
                    'name'      => 'account_name',
                    'contents'  => $request->account_name
                ],
            ]
        ]);
        $response = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function detail($id)
    {
        $request                = $this->client->get($this->url.'bankaccounts/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function update($request, $id)
    {
        $request = $this->client->put($this->url.'bankaccounts/'.$id,[
            'form_params' => [
                'bank_id' => $request['bank_id'],
                'account_number' => $request['account_number'],
                'account_name' => $request['account_name'],
            ]
        ]);
        $response = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function delete($id)
    {
        $request                = $this->client->delete($this->url.'bankaccounts/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

}
