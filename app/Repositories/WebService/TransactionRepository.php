<?php

namespace App\Repositories\WebService;

class TransactionRepository
{
	protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function getAllTransactionPurchase()
    {
        $request                = $this->client->get($this->url.'transactions');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getTransactionDetail($id)
    {
        $request                = $this->client->get($this->url.'transactions/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }
}
