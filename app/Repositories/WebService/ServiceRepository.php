<?php

namespace App\Repositories\WebService;


class ServiceRepository
{
    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function getLocationRegency()
    {
        $request                = $this->client->get($this->url.'getlocation');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }
}
