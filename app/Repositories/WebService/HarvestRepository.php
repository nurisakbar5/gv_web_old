<?php

namespace App\Repositories\WebService;

use Kurt\Repoist\Repositories\Eloquent\AbstractRepository;

class HarvestRepository extends AbstractRepository
{
    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function getAllHarvest($start=null,$limit=null)
    {
        $request                = $this->client->get($this->url.'page/harvest');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getAllUnit()
    {
        $request                = $this->client->get($this->url.'units');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getHarvest($start=null,$limit=null)
    {
        $request                = $this->client->get($this->url.'harvests');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getHarvestByCategory($categorySlug,$offset=null,$limit=null)
    {
        if($offset===null)
        {
            $request = $this->client->get($this->url."harvests/category/$categorySlug");
        }else
        {
            $request = $this->client->get($this->url."harvests/category/$categorySlug".'?offset='.$offset.'&limit='.$limit);
        }
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getHarvestById($id)
    {
        $request                = $this->client->get($this->url.'harvests/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getHarvestBySlug($slug)
    {
        $request                = $this->client->get($this->url.'harvests/'.$slug);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getHarvestCommentBySlug($slug)
    {
        $request                = $this->client->get($this->url.'harvests/'.$slug.'/comments');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getHarvestRelatedBySlug($slug)
    {
        $request                = $this->client->get($this->url.'harvests/'.$slug.'/related?limit=4');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getHarvestSearchByKeyword($keyword)
    {
        $request                = $this->client->get($this->url.'harvests?keyword='.$keyword.'&limit=6');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getHarvestSearchByCategory($id)
    {
        $request                = $this->client->get($this->url."harvests/category/$id".'?offset=1&limit=6');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function postCommentHarvest($request)
    {
        $request = $this->client->post($this->url.'harvests/comments', [
            'multipart' => [
                [
                    'name'     => 'token',
                    'contents' => $request->token
                ],
                [
                    'name'     => 'comment',
                    'contents' => $request->comment
                ],
                [
                    'name'     => 'id',
                    'contents' => $request->id
                ]
            ]
        ]);

        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function deletePermanent($id)
    {
        $request                = $this->client->delete($this->url.'harvests/trash/delete-permanent/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getTrashHarvest()
    {
        $request                = $this->client->get($this->url.'harvests/trash');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getRestoreHarvest($id)
    {
        $request                = $this->client->get($this->url.'harvests/trash/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function create($request)
    {
        if ($request->hasFile('image_1') && $request->hasFile('image_2') && $request->hasFile('image_3') && $request->hasFile('image_4'))
        {
            ini_set('max_execution_time', 2180);

            $file       =   $request->file('image_1');
            $file_path  =   $file->getPathName();
            $fileName   =   $file->getClientOriginalName();

            $file2       =   $request->file('image_2');
            $file_path2  =   $file2->getPathName();
            $fileName2   =   $file2->getClientOriginalName();

            $file3       =   $request->file('image_3');
            $file_path3  =   $file3->getPathName();
            $fileName3   =   $file3->getClientOriginalName();

            $file4       =   $request->file('image_4');
            $file_path4  =   $file4->getPathName();
            $fileName4   =   $file4->getClientOriginalName();

            $req = $this->client->post($this->url.'harvests', [
                'multipart' => [
                    [
                        'name'     => 'image_1',
                        'contents' => fopen($file_path, 'r') ,
                        'filename'=> $fileName
                    ],
                    [
                        'name'     => 'image_2',
                        'contents' => fopen($file_path2, 'r') ,
                        'filename'=> $fileName2
                    ],
                    [
                        'name'     => 'image_3',
                        'contents' => fopen($file_path3, 'r') ,
                        'filename'=> $fileName3
                    ],
                    [
                        'name'     => 'image_4',
                        'contents' => fopen($file_path4, 'r') ,
                        'filename'=> $fileName4
                    ],
                    [
                        'name'     => 'title',
                        'contents' => $request->title
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ],
                    [
                        'name'     => 'land_id',
                        'contents' => $request->land_id
                    ],
                    [
                        'name'     => 'estimated_date',
                        'contents' => $request->estimated_date
                    ],
                    [
                        'name'     => 'estimated_income',
                        'contents' => $request->estimated_income
                    ],
                    [
                        'name'     => 'unit_id',
                        'contents' => $request->unit_id
                    ]
                ]
            ]);
        }
        if ($request->hasFile('image_1') && $request->hasFile('image_2'))
        {
            ini_set('max_execution_time', 2180);

            $file       =   $request->file('image_1');
            $file_path  =   $file->getPathName();
            $fileName   =   $file->getClientOriginalName();

            $file2       =   $request->file('image_2');
            $file_path2  =   $file2->getPathName();
            $fileName2   =   $file2->getClientOriginalName();

            $req = $this->client->post($this->url.'harvests', [
                'multipart' => [
                    [
                        'name'     => 'image_1',
                        'contents' => fopen($file_path, 'r') ,
                        'filename'=> $fileName
                    ],
                    [
                        'name'     => 'image_2',
                        'contents' => fopen($file_path2, 'r') ,
                        'filename'=> $fileName2
                    ],
                    [
                        'name'     => 'title',
                        'contents' => $request->title
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ],
                    [
                        'name'     => 'land_id',
                        'contents' => $request->land_id
                    ],
                    [
                        'name'     => 'estimated_date',
                        'contents' => $request->estimated_date
                    ],
                    [
                        'name'     => 'estimated_income',
                        'contents' => $request->estimated_income
                    ],
                    [
                        'name'     => 'unit_id',
                        'contents' => $request->unit_id
                    ]
                ]
            ]);
        }
        if ($request->hasFile('image_1') && $request->hasFile('image_2') && $request->hasFile('image_3'))
        {
            ini_set('max_execution_time', 2180);

            $file       =   $request->file('image_1');
            $file_path  =   $file->getPathName();
            $fileName   =   $file->getClientOriginalName();

            $file2       =   $request->file('image_2');
            $file_path2  =   $file2->getPathName();
            $fileName2   =   $file2->getClientOriginalName();

            $file3       =   $request->file('image_3');
            $file_path3  =   $file3->getPathName();
            $fileName3   =   $file3->getClientOriginalName();

            $req = $this->client->post($this->url.'harvests', [
                'multipart' => [
                    [
                        'name'     => 'image_1',
                        'contents' => fopen($file_path, 'r') ,
                        'filename'=> $fileName
                    ],
                    [
                        'name'     => 'image_2',
                        'contents' => fopen($file_path2, 'r') ,
                        'filename'=> $fileName2
                    ],
                    [
                        'name'     => 'image_3',
                        'contents' => fopen($file_path3, 'r') ,
                        'filename'=> $fileName3
                    ],
                    [
                        'name'     => 'title',
                        'contents' => $request->title
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ],
                    [
                        'name'     => 'land_id',
                        'contents' => $request->land_id
                    ],
                    [
                        'name'     => 'estimated_date',
                        'contents' => $request->estimated_date
                    ],
                    [
                        'name'     => 'estimated_income',
                        'contents' => $request->estimated_income
                    ],
                    [
                        'name'     => 'unit_id',
                        'contents' => $request->unit_id
                    ]
                ]
            ]);
        }
        if ($request->hasFile('image_1') && $request->hasFile('image_2') && $request->hasFile('image_4'))
        {
            ini_set('max_execution_time', 2180);

            $file       =   $request->file('image_1');
            $file_path  =   $file->getPathName();
            $fileName   =   $file->getClientOriginalName();

            $file2       =   $request->file('image_2');
            $file_path2  =   $file2->getPathName();
            $fileName2   =   $file2->getClientOriginalName();

            $file4       =   $request->file('image_4');
            $file_path4  =   $file4->getPathName();
            $fileName4   =   $file4->getClientOriginalName();

            $req = $this->client->post($this->url.'harvests', [
                'multipart' => [
                    [
                        'name'     => 'image_1',
                        'contents' => fopen($file_path, 'r') ,
                        'filename'=> $fileName
                    ],
                    [
                        'name'     => 'image_2',
                        'contents' => fopen($file_path2, 'r') ,
                        'filename'=> $fileName2
                    ],
                    [
                        'name'     => 'image_4',
                        'contents' => fopen($file_path4, 'r') ,
                        'filename'=> $fileName4
                    ],
                    [
                        'name'     => 'title',
                        'contents' => $request->title
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ],
                    [
                        'name'     => 'land_id',
                        'contents' => $request->land_id
                    ],
                    [
                        'name'     => 'estimated_date',
                        'contents' => $request->estimated_date
                    ],
                    [
                        'name'     => 'estimated_income',
                        'contents' => $request->estimated_income
                    ],
                    [
                        'name'     => 'unit_id',
                        'contents' => $request->unit_id
                    ]
                ]
            ]);
        }
        $response = json_decode($req->getBody()->getContents());
        return $response;
    }

    public function update($request,$id)
    {

        if ($request->hasFile('image_1') && $request->hasFile('image_2') && $request->hasFile('image_3') && $request->hasFile('image_4')) {
            // update with image_1
            $file       =   $request->file('image_1');
            $file_path  =   $file->getPathName();
            $fileName   =   $file->getClientOriginalName();

            $file2       =   $request->file('image_2');
            $file_path2  =   $file2->getPathName();
            $fileName2   =   $file2->getClientOriginalName();

            $file3       =   $request->file('image_3');
            $file_path3  =   $file3->getPathName();
            $fileName3   =   $file3->getClientOriginalName();

            $file4       =   $request->file('image_4');
            $file_path4  =   $file4->getPathName();
            $fileName4   =   $file4->getClientOriginalName();

            $req = $this->client->post($this->url.'harvests/'.$id, [
                'multipart' => [
                    [
                        'name'     => 'image_1',
                        'contents' => fopen($file_path, 'r') ,
                        'filename'=> $fileName
                    ],
                    [
                        'name'     => 'image_2',
                        'contents' => fopen($file_path2, 'r') ,
                        'filename'=> $fileName2
                    ],
                    [
                        'name'     => 'image_3',
                        'contents' => fopen($file_path3, 'r') ,
                        'filename'=> $fileName3
                    ],
                    [
                        'name'     => 'image_4',
                        'contents' => fopen($file_path4, 'r') ,
                        'filename'=> $fileName4
                    ],
                    [
                        'name'     => 'title',
                        'contents' => $request->title
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ],
                    [
                        'name'     => 'land_id',
                        'contents' => $request->land_id
                    ],
                    [
                        'name'     => 'estimated_date',
                        'contents' => $request->estimated_date
                    ],
                    [
                        'name'     => 'estimated_income',
                        'contents' => $request->estimated_income
                    ],
                    [
                        'name'     => 'unit_id',
                        'contents' => $request->unit_id
                    ]
                ]

            ]);
        } else {
            $req = $this->client->post($this->url.'harvests/'.$id, [
                'multipart' => [
                    [
                        'name'     => 'title',
                        'contents' => $request->title
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ],
                    [
                        'name'     => 'land_id',
                        'contents' => $request->land_id
                    ],
                    [
                        'name'     => 'estimated_date',
                        'contents' => $request->estimated_date
                    ],
                    [
                        'name'     => 'estimated_income',
                        'contents' => $request->estimated_income
                    ],
                    [
                        'name'     => 'unit_id',
                        'contents' => $request->unit_id
                    ]
                ]

            ]);
        }

        if ($request->hasFile('image_1')) {
            // update with image_1
            $file       =   $request->file('image_1');
            $file_path  =   $file->getPathName();
            $fileName   =   $file->getClientOriginalName();

            $req = $this->client->post($this->url.'harvests/'.$id, [
                'multipart' => [
                    [
                        'name'     => 'image_1',
                        'contents' => fopen($file_path, 'r') ,
                        'filename'=> $fileName
                    ],
                    [
                        'name'     => 'title',
                        'contents' => $request->title
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ],
                    [
                        'name'     => 'land_id',
                        'contents' => $request->land_id
                    ],
                    [
                        'name'     => 'estimated_date',
                        'contents' => $request->estimated_date
                    ],
                    [
                        'name'     => 'estimated_income',
                        'contents' => $request->estimated_income
                    ],
                    [
                        'name'     => 'unit_id',
                        'contents' => $request->unit_id
                    ]
                ]

            ]);
        }

        if ($request->hasFile('image_2')) {
            // update with image_1

            $file2       =   $request->file('image_2');
            $file_path2  =   $file2->getPathName();
            $fileName2   =   $file2->getClientOriginalName();

            $req = $this->client->post($this->url.'harvests/'.$id, [
                'multipart' => [
                    [
                        'name'     => 'image_2',
                        'contents' => fopen($file_path2, 'r') ,
                        'filename'=> $fileName2
                    ],
                    [
                        'name'     => 'title',
                        'contents' => $request->title
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ],
                    [
                        'name'     => 'land_id',
                        'contents' => $request->land_id
                    ],
                    [
                        'name'     => 'estimated_date',
                        'contents' => $request->estimated_date
                    ],
                    [
                        'name'     => 'estimated_income',
                        'contents' => $request->estimated_income
                    ],
                    [
                        'name'     => 'unit_id',
                        'contents' => $request->unit_id
                    ]
                ]

            ]);
        }

        if ($request->hasFile('image_3')) {
            $file3       =   $request->file('image_3');
            $file_path3  =   $file3->getPathName();
            $fileName3   =   $file3->getClientOriginalName();

            $req = $this->client->post($this->url.'harvests/'.$id, [
                'multipart' => [
                    [
                        'name'     => 'image_3',
                        'contents' => fopen($file_path3, 'r') ,
                        'filename'=> $fileName3
                    ],
                    [
                        'name'     => 'title',
                        'contents' => $request->title
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ],
                    [
                        'name'     => 'land_id',
                        'contents' => $request->land_id
                    ],
                    [
                        'name'     => 'estimated_date',
                        'contents' => $request->estimated_date
                    ],
                    [
                        'name'     => 'estimated_income',
                        'contents' => $request->estimated_income
                    ],
                    [
                        'name'     => 'unit_id',
                        'contents' => $request->unit_id
                    ]
                ]

            ]);
        }

        if ($request->hasFile('image_4')) {
            $file4       =   $request->file('image_4');
            $file_path4  =   $file4->getPathName();
            $fileName4   =   $file4->getClientOriginalName();

            $req = $this->client->post($this->url.'harvests/'.$id, [
                'multipart' => [
                    [
                        'name'     => 'image_4',
                        'contents' => fopen($file_path4, 'r') ,
                        'filename'=> $fileName4
                    ],
                    [
                        'name'     => 'title',
                        'contents' => $request->title
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ],
                    [
                        'name'     => 'land_id',
                        'contents' => $request->land_id
                    ],
                    [
                        'name'     => 'estimated_date',
                        'contents' => $request->estimated_date
                    ],
                    [
                        'name'     => 'estimated_income',
                        'contents' => $request->estimated_income
                    ],
                    [
                        'name'     => 'unit_id',
                        'contents' => $request->unit_id
                    ]
                ]

            ]);
        }

        if ($request->hasFile('image_1') && $request->hasFile('image_2')) {
            // update with image_1
            $file       =   $request->file('image_1');
            $file_path  =   $file->getPathName();
            $fileName   =   $file->getClientOriginalName();

            $file2       =   $request->file('image_2');
            $file_path2  =   $file2->getPathName();
            $fileName2   =   $file2->getClientOriginalName();

            $req = $this->client->post($this->url.'harvests/'.$id, [
                'multipart' => [
                    [
                        'name'     => 'image_1',
                        'contents' => fopen($file_path, 'r') ,
                        'filename'=> $fileName
                    ],
                    [
                        'name'     => 'image_2',
                        'contents' => fopen($file_path2, 'r') ,
                        'filename'=> $fileName2
                    ],
                    [
                        'name'     => 'title',
                        'contents' => $request->title
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ],
                    [
                        'name'     => 'land_id',
                        'contents' => $request->land_id
                    ],
                    [
                        'name'     => 'estimated_date',
                        'contents' => $request->estimated_date
                    ],
                    [
                        'name'     => 'estimated_income',
                        'contents' => $request->estimated_income
                    ],
                    [
                        'name'     => 'unit_id',
                        'contents' => $request->unit_id
                    ]
                ]

            ]);
        }

        $response               = json_decode($req->getBody()->getContents());
        return $response;
    }

    public function delete($id)
    {
        $request                = $this->client->delete($this->url.'harvests/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

}
