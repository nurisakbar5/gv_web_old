<?php

namespace App\Repositories\WebService;


class PriceRepository
{
    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function getAllPrice($start=null,$limit=null)
    {

        if($start===null)
        {
            $request                = $this->client->get($this->url.'prices');
        }else
        {
            $request                = $this->client->get($this->url.'prices?limit='.$limit.'&start='.$start);
        }
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getPriceByRegency($regencyId,$periode)
    {
        $request                = $this->client->get($this->url.'prices/'.$regencyId.'/'.$periode);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getPriceById($id)
    {
        $request                = $this->client->get($this->url.'prices/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getPriceByMarket($id,$periode)
    {
        $request                = $this->client->get($this->url.'prices/markets/'.$id.'/'.$periode);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getPriceByCommodities($commodity_id,$periode)
    {
        $request                = $this->client->get($this->url.'prices/price-detail/'.$commodity_id.'/'.$periode);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function delete($id)
    {
        $request                = $this->client->delete($this->url.'prices/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function create($request)
    {

        $req = $this->client->post($this->url.'prices', [
            'multipart' => [
                [
                    'name'     => 'market_id',
                    'contents' => $request->market_id,
                ],

                [
                    'name'     => 'commodity_id',
                    'contents' => $request->commodity_id,
                ],
                [
                    'name'     => 'price',
                    'contents' => $request->price
                ]

            ]
        ]);

        $response = json_decode($req->getBody()->getContents());
        return $response;
    }

    public function update($request,$id)
    {
        $req = $this->client->post($this->url.'prices/'.$id, [
            'multipart' => [
                [
                    'name'     => 'market_id',
                    'contents' => $request->market_id,
                ],

                [
                    'name'     => 'commodity_id',
                    'contents' => $request->commodity_id,
                ],
                [
                    'name'     => 'price',
                    'contents' => $request->price
                ]
            ]
        ]);

        $response               = json_decode($req->getBody()->getContents());
        return $response;
    }
}
