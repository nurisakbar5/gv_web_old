<?php

namespace App\Repositories\WebService;


class CommodityRepository
{
    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function getAllCommodity($start=null,$limit=null)
    {

        if($start===null)
        {
            $request                = $this->client->get($this->url.'commodities');
        }else
        {
            $request                = $this->client->get($this->url.'commodities?limit='.$limit.'&start='.$start);
        }
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getCommodityById($id)
    {
        $request                = $this->client->get($this->url.'commodities/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function delete($id)
    {
        $request                = $this->client->delete($this->url.'commodities/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function create($request)
    {
        ini_set('max_execution_time', 2180);
        
        $file       =   $request->file('image'); 
        $file_path  =   $file->getPathName();
        $fileName   =   $file->getClientOriginalName();

        $req = $this->client->post($this->url.'commodities', [
            'multipart' => [
                [
                    'name'     => 'image',
                    'contents' => fopen($file_path, 'r'),
                    'filename'=> $fileName
                ],
                [
                    'name'     => 'name',
                    'contents' => $request->name
                ],
                [
                    'name'     => 'category_id',
                    'contents' => $request->category_id
                ]
            ]
        ]);

        $response = json_decode($req->getBody()->getContents());
        return $response;
    }

    public function update($request,$id)
    {
        if ($request->hasFile('image')) {
            // update with image
            $file       =   $request->file('image'); 
            $file_path  =   $file->getPathName();
            $fileName   =   $file->getClientOriginalName();

            $req = $this->client->post($this->url.'commodities/'.$id, [
                'multipart' => [
                    [
                        'name'     => 'image',
                        'contents' => fopen($file_path, 'r'),
                        'filename'=> $fileName
                    ],
                    [
                        'name'     => 'name',
                        'contents' => $request->name
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ]
                ]
            ]);
        }else{
            $req = $this->client->post($this->url.'commodities/'.$id, [
                'multipart' => [
                    [
                        'name'     => 'name',
                        'contents' => $request->name
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ]
                ]
            ]);
        }

        $response               = json_decode($req->getBody()->getContents());
        return $response;
    }
}
