<?php

namespace App\Repositories\WebService;

class UserPembelianRepository
{

    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function getUserPembelianByToken($token)
    {
        $request                = $this->client->get($this->url.'users/'.$token.'/purchases');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    /*
    public function getUserDetailPembelian($id)
    {
        $request                = $this->client->get($this->url.'order/purchasedetail/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    ]*/
}
