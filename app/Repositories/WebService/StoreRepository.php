<?php

namespace App\Repositories\WebService;


class StoreRepository
{
    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function getStoreInfo($id_user)
    {
        $request                = $this->client->get($this->url.'users/'.$id_user.'/stores');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function storeWithProduct($slug)
    {
        $request                = $this->client->get($this->url.'page/android/store/'.$slug);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function create($request)
    {
        $request = $this->client->post($this->url.'stores',[
            'multipart' => [
                [
                    'name'      => 'token',
                    'contents'  => $request->token
                ],
                [
                    'name'      => 'name',
                    'contents'  => $request->name
                ],
                [
                    'name'      => 'description',
                    'contents'  => $request->description
                ],
                [
                    'name'      => 'subdistrict_id',
                    'contents'  => $request->subdistrict_id
                ],
            ]
        ]);
        $response = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function update($request, $idOrSlug)
    {
        $request = $this->client->put($this->url.'stores/'.$idOrSlug,[
            'form_params' => [
                'description'        => $request['description'],
            ]
        ]);
        $response = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function updateSubdistrict($request, $idOrSlug)
    {
        $request = $this->client->put($this->url.'stores/update-sub-district/'.$idOrSlug,[
            'form_params' => [
                'subdistrict_id' => $request['subdistrict_id']
            ]
        ]);
        $response = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function storeCouriers($idOrSlug)
    {
        $request                = $this->client->get($this->url.'stores/'.$idOrSlug.'/courier');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function changeActiveCourier($id)
    {
        $request = $this->client->put($this->url.'stores/courier',[
            'form_params' => [
                'id' => $id,
            ]
        ]);
        $response = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function couriers()
    {
        $request                = $this->client->get($this->url.'couriers');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function addCourier($request)
    {
        $request = $this->client->post($this->url.'stores/courier',[
            'multipart' => [
                [
                    'name'      => 'courier_code',
                    'contents'  => $request->courier_code
                ],
                [
                    'name'      => 'store_id',
                    'contents'  => $request->store_id
                ]
            ]
        ]);
        $response = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function searcProduct($id, $keyword)
    {
        $request                = $this->client->get($this->url.'stores/'.$id.'/products?keyword='.$keyword);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }
}
