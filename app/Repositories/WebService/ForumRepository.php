<?php

namespace App\Repositories\WebService;


class ForumRepository
{
    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function getAllForum($offset=null,$limit=null)
    {

        if($offset===null)
        {
            $request = $this->client->get($this->url.'forums');
        }else
        {
            $request = $this->client->get($this->url.'forums?offset='.$offset.'&limit='.$limit);
        }
        $response = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getForumById($id)
    {
        $request                = $this->client->get($this->url.'forums/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getForumBySlug($slug)
    {
        $request                = $this->client->get($this->url.'forums/'.$slug);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getForumReplyBySlug($slug)
    {
        $request                = $this->client->get($this->url.'forums/reply/'.$slug);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function addReply($request)
    {
        $request = $this->client->post($this->url.'forums/reply', [
            'multipart' => [
                [
                    'name'     => 'forum_id',
                    'contents' => $request->forum_id
                ],
                [
                    'name'     => 'token',
                    'contents' => $request->token
                ],
                [
                    'name'     => 'reply',
                    'contents' => $request->reply
                ]

            ]
        ]);

        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function delete($id)
    {
        $request                = $this->client->delete($this->url.'forums/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function create($request)
    {
        if ($request->hasFile('image_1') && !$request->hasFile('image_2') && !$request->hasFile('image_3')) {
            $file = $request->file('image_1');
            $file_path = $file->getPathName();
            $fileName = $file->getClientOriginalName();

            $req = $this->client->post($this->url.'forums', [
                'multipart' => [
                    [
                        'name'      => 'image_1',
                        'contents'  => fopen($file_path, 'r'),
                        'filename'  => $fileName
                    ],
                    [
                        'name'     => 'title',
                        'contents' => $request->title
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ],
                ]
            ]);
        }else if($request->hasFile('image_1') && $request->hasFile('image_2') && !$request->hasFile('image_3')){
            $file = $request->file('image_1');
            $file_path = $file->getPathName();
            $fileName = $file->getClientOriginalName();

            $file2 = $request->file('image_2');
            $file_path2 = $file2->getPathName();
            $fileName2 = $file2->getClientOriginalName();

            $req = $this->client->post($this->url.'forums', [
                'multipart' => [
                    [
                        'name'      => 'image_1',
                        'contents'  => fopen($file_path, 'r'),
                        'filename'  => $fileName
                    ],
                    [
                        'name'      => 'image_2',
                        'contents'  => fopen($file_path2, 'r'),
                        'filename'  => $fileName2
                    ],
                    [
                        'name'     => 'title',
                        'contents' => $request->title
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ],
                ]
            ]);
        }else if($request->hasFile('image_1') && $request->hasFile('image_2') && $request->hasFile('image_3')){
            $file = $request->file('image_1');
            $file_path = $file->getPathName();
            $fileName = $file->getClientOriginalName();

            $file2 = $request->file('image_2');
            $file_path2 = $file2->getPathName();
            $fileName2 = $file2->getClientOriginalName();

            $file3 = $request->file('image_3');
            $file_path3 = $file3->getPathName();
            $fileName3 = $file3->getClientOriginalName();

            $req = $this->client->post($this->url.'forums', [
                'multipart' => [
                    [
                        'name'      => 'image_1',
                        'contents'  => fopen($file_path, 'r'),
                        'filename'  => $fileName
                    ],
                    [
                        'name'      => 'image_2',
                        'contents'  => fopen($file_path2, 'r'),
                        'filename'  => $fileName2
                    ],
                    [
                        'name'      => 'image_3',
                        'contents'  => fopen($file_path3, 'r'),
                        'filename'  => $fileName3
                    ],
                    [
                        'name'     => 'title',
                        'contents' => $request->title
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ],
                ]
            ]);
        }else if($request->hasFile('image_2') && !$request->hasFile('image_1') && !$request->hasFile('image_3')){
            $file2 = $request->file('image_2');
            $file_path2 = $file2->getPathName();
            $fileName2 = $file2->getClientOriginalName();

            $req = $this->client->post($this->url.'forums', [
                'multipart' => [
                    [
                        'name'      => 'image_2',
                        'contents'  => fopen($file_path2, 'r'),
                        'filename'  => $fileName2
                    ],
                    [
                        'name'     => 'title',
                        'contents' => $request->title
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ],
                ]
            ]);
        }else if($request->hasFile('image_2') && $request->hasFile('image_3') && !$request->hasFile('image_1')){
            $file2 = $request->file('image_2');
            $file_path2 = $file2->getPathName();
            $fileName2 = $file2->getClientOriginalName();

            $file3 = $request->file('image_3');
            $file_path3 = $file3->getPathName();
            $fileName3 = $file3->getClientOriginalName();

            $req = $this->client->post($this->url.'forums', [
                'multipart' => [
                    [
                        'name'      => 'image_2',
                        'contents'  => fopen($file_path2, 'r'),
                        'filename'  => $fileName2
                    ],
                    [
                        'name'      => 'image_3',
                        'contents'  => fopen($file_path3, 'r'),
                        'filename'  => $fileName3
                    ],
                    [
                        'name'     => 'title',
                        'contents' => $request->title
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ],
                ]
            ]);
        }else if($request->hasFile('image_3') && $request->hasFile('image_1') && !$request->hasFile('image_2')){
            $file3 = $request->file('image_3');
            $file_path3 = $file3->getPathName();
            $fileName3 = $file3->getClientOriginalName();

            $file = $request->file('image_1');
            $file_path = $file->getPathName();
            $fileName = $file->getClientOriginalName();

            $req = $this->client->post($this->url.'forums', [
                'multipart' => [
                    [
                        'name'      => 'image_3',
                        'contents'  => fopen($file_path3, 'r'),
                        'filename'  => $fileName3
                    ],
                    [
                        'name'      => 'image_1',
                        'contents'  => fopen($file_path, 'r'),
                        'filename'  => $fileName
                    ],
                    [
                        'name'     => 'title',
                        'contents' => $request->title
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ],
                ]
            ]);
        }else if($request->hasFile('image_2') && !$request->hasFile('image_1') && !$request->hasFile('image_3')){
            $file3 = $request->file('image_3');
            $file_path3 = $file3->getPathName();
            $fileName3 = $file3->getClientOriginalName();

            $file = $request->file('image_1');
            $file_path = $file->getPathName();
            $fileName = $file->getClientOriginalName();

            $req = $this->client->post($this->url.'forums', [
                'multipart' => [
                    [
                        'name'      => 'image_3',
                        'contents'  => fopen($file_path3, 'r'),
                        'filename'  => $fileName3
                    ],
                    [
                        'name'      => 'image_1',
                        'contents'  => fopen($file_path, 'r'),
                        'filename'  => $fileName
                    ],
                    [
                        'name'     => 'title',
                        'contents' => $request->title
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ],
                ]
            ]);
        }else if($request->hasFile('image_3') && !$request->hasFile('image_1') && !$request->hasFile('image_2')){
            $file3 = $request->file('image_3');
            $file_path3 = $file3->getPathName();
            $fileName3 = $file3->getClientOriginalName();

            $req = $this->client->post($this->url.'forums', [
                'multipart' => [
                    [
                        'name'      => 'image_3',
                        'contents'  => fopen($file_path3, 'r'),
                        'filename'  => $fileName3
                    ],
                    [
                        'name'     => 'title',
                        'contents' => $request->title
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ],
                ]
            ]);
        }else{
            $req = $this->client->post($this->url.'forums', [
                'multipart' => [
                    [
                        'name'     => 'title',
                        'contents' => $request->title
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ],
                ]
            ]);
        }

        $response = json_decode($req->getBody()->getContents());
        return $response;
    }

    public function update($request,$id)
    {
        $req = $this->client->post($this->url.'forums/'.$id, [
            'multipart' => [
                [
                    'name'     => 'title',
                    'contents' => $request->title
                ],
                [
                    'name'     => 'body',
                    'contents' => $request->tags
                ]
            ]
        ]);
        $response               = json_decode($req->getBody()->getContents());
        return $response;
    }

    public function getForumSearch($keyword)
    {
        $request                = $this->client->get($this->url.'forums?keyword='.$keyword.'&offset=0&limit=9');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getForumByCategory($slug, $offset=null, $limit=null)
    {
        if($offset===null)
        {
            $request = $this->client->get($this->url.'forums/category/'.$slug);
        }else
        {
            $request = $this->client->get($this->url."forums/category/$slug".'?offset='.$offset.'&limit='.$limit);
        }
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getForumRelatedBySlug($slug)
    {
        $request                = $this->client->get($this->url.'forums/'.$slug.'/related?limit=3');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getLatestForum()
    {
        $request                = $this->client->get($this->url.'forums/latest?limit=5');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }
}
