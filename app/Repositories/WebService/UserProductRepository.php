<?php

namespace App\Repositories\WebService;


class UserProductRepository
{
    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function getAllProduct($start=null,$limit=null)
    {

        if($start===null)
        {
            $request                = $this->client->get($this->url.'product');
        }else
        {
            $request                = $this->client->get($this->url.'product?limit='.$limit.'&start='.$start);
        }
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getProductByCategory($categoryId,$start=null,$limit=null)
    {
        if($start===null)
        {
            $request                = $this->client->get($this->url."product/category/$categoryId");
        }else
        {
            $request                = $this->client->get($this->url."product/category/$categoryId/$start/$limit");
        }
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getProductById($id)
    {
        $request                = $this->client->get($this->url.'products/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getCommendProduct($slug)
    {
        $request                = $this->client->get($this->url.'products/'.$slug.'/comments');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getRelatedProduct($slug)
    {
        $request                = $this->client->get($this->url.'products/'.$slug.'/related');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getProductBySlug($slug)
    {
        $request                = $this->client->get($this->url.'products/'.$slug);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function delete($slug)
    {
        $request                = $this->client->delete($this->url.'products/'.$slug);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function addProductToUser($request)
    {
        if ($request->hasFile('image_4')) {
            $file       =   $request->file('image_1');
            $file_path  =   $file->getPathName();
            $fileName   =   $file->getClientOriginalName();

            $file2       =   $request->file('image_2');
            $file_path2  =   $file2->getPathName();
            $fileName2   =   $file2->getClientOriginalName();

            $file3       =   $request->file('image_3');
            $file_path3  =   $file3->getPathName();
            $fileName3   =   $file3->getClientOriginalName();

            $file4       =   $request->file('image_4');
            $file_path4  =   $file4->getPathName();
            $fileName4   =   $file4->getClientOriginalName();

            $request = $this->client->post($this->url.'products', [
                'multipart' => [
                    [
                        'name'     => 'image_1',
                        'contents' => fopen($file_path, 'r'),
                        'filename' => $fileName
                    ],
                    [
                        'name'     => 'image_2',
                        'contents' => fopen($file_path2, 'r'),
                        'filename' => $fileName2
                    ],
                    [
                        'name'     => 'image_3',
                        'contents' => fopen($file_path3, 'r'),
                        'filename' => $fileName3
                    ],
                    [
                        'name'     => 'image_4',
                        'contents' => fopen($file_path4, 'r'),
                        'filename' => $fileName4
                    ],
                    [
                        'name'     => 'name',
                        'contents' => $request->name
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'price',
                        'contents' => $request->price
                    ],
                    [
                        'name'     => 'stock',
                        'contents' => $request->stock
                    ],
                    [
                        'name'     => 'weight',
                        'contents' => $request->weight
                    ],
                    [
                        'name'     => 'unit_id',
                        'contents' => $request->unit_id
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ],
                    [
                        'name'     => 'etalase_id',
                        'contents' => $request->etalase_id
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ]
                ]
            ]);
        } else {
            $file       =   $request->file('image_1');
            $file_path  =   $file->getPathName();
            $fileName   =   $file->getClientOriginalName();

            $file2       =   $request->file('image_2');
            $file_path2  =   $file2->getPathName();
            $fileName2   =   $file2->getClientOriginalName();

            $file3       =   $request->file('image_3');
            $file_path3  =   $file3->getPathName();
            $fileName3   =   $file3->getClientOriginalName();

            $request = $this->client->post($this->url.'products', [
                'multipart' => [
                    [
                        'name'     => 'image_1',
                        'contents' => fopen($file_path, 'r'),
                        'filename' => $fileName
                    ],
                    [
                        'name'     => 'image_2',
                        'contents' => fopen($file_path2, 'r'),
                        'filename' => $fileName2
                    ],
                    [
                        'name'     => 'image_3',
                        'contents' => fopen($file_path3, 'r'),
                        'filename' => $fileName3
                    ],
                    [
                        'name'     => 'name',
                        'contents' => $request->name
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'price',
                        'contents' => $request->price
                    ],
                    [
                        'name'     => 'stock',
                        'contents' => $request->stock
                    ],
                    [
                        'name'     => 'weight',
                        'contents' => $request->weight
                    ],
                    [
                        'name'     => 'unit_id',
                        'contents' => $request->unit_id
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ],
                    [
                        'name'     => 'etalase_id',
                        'contents' => $request->etalase_id
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ]
                ]
            ]);
        }


        $response = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function update($request,$id)
    {
        if ($request->hasFile('image_1')) {
            $file       =   $request->file('image_1');
            $file_path  =   $file->getPathName();
            $fileName   =   $file->getClientOriginalName();

            $req = $this->client->post($this->url.'products/'.$id, [
                'multipart' => [
                    [
                        'name'     => 'image_1',
                        'contents' => fopen($file_path, 'r'),
                        'filename'=> $fileName
                    ],
                    [
                        'name'     => 'name',
                        'contents' => $request->name
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->description
                    ],
                    [
                        'name'     => 'price',
                        'contents' => $request->price
                    ],
                    [
                        'name'     => 'stock',
                        'contents' => $request->stock
                    ],
                    [
                        'name'     => 'weight',
                        'contents' => $request->weight
                    ],
                    [
                        'name'     => 'unit_id',
                        'contents' => $request->unit_id
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ],
                    [
                        'name'     => 'etalase_id',
                        'contents' => $request->etalase_id
                    ],
                    [
                        'name'     => 'token',
                        'contents' => $request->token
                    ]
                ]
            ]);
        }
    if ($request->hasFile('image_2')) {
        $file2       =   $request->file('image_2');
        $file_path2  =   $file2->getPathName();
        $fileName2   =   $file2->getClientOriginalName();

        $req = $this->client->post($this->url.'products/'.$id, [
            'multipart' => [
                [
                    'name'     => 'image_2',
                    'contents' => fopen($file_path2, 'r'),
                    'filename' => $fileName2
                ],
                [
                    'name'     => 'name',
                    'contents' => $request->name
                ],
                [
                    'name'     => 'description',
                    'contents' => $request->description
                ],
                [
                    'name'     => 'price',
                    'contents' => $request->price
                ],
                [
                    'name'     => 'stock',
                    'contents' => $request->stock
                ],
                [
                    'name'     => 'weight',
                    'contents' => $request->weight
                ],
                [
                    'name'     => 'unit_id',
                    'contents' => $request->unit_id
                ],
                [
                    'name'     => 'category_id',
                    'contents' => $request->category_id
                ],
                [
                    'name'     => 'etalase_id',
                    'contents' => $request->etalase_id
                ],
                [
                    'name'     => 'token',
                    'contents' => $request->token
                ]
            ]
        ]);
    }
    if($request->hasFile('image_3')){
        $file3       =   $request->file('image_3');
        $file_path3  =   $file3->getPathName();
        $fileName3   =   $file3->getClientOriginalName();

        $req = $this->client->post($this->url.'products/'.$id, [
            'multipart' => [
                [
                    'name'     => 'image_3',
                    'contents' => fopen($file_path3, 'r'),
                    'filename' => $fileName3
                ],
                [
                    'name'     => 'name',
                    'contents' => $request->name
                ],
                [
                    'name'     => 'description',
                    'contents' => $request->description
                ],
                [
                    'name'     => 'price',
                    'contents' => $request->price
                ],
                [
                    'name'     => 'stock',
                    'contents' => $request->stock
                ],
                [
                    'name'     => 'weight',
                    'contents' => $request->weight
                ],
                [
                    'name'     => 'unit_id',
                    'contents' => $request->unit_id
                ],
                [
                    'name'     => 'category_id',
                    'contents' => $request->category_id
                ],
                [
                    'name'     => 'etalase_id',
                    'contents' => $request->etalase_id
                ],
                [
                    'name'     => 'token',
                    'contents' => $request->token
                ]
            ]
        ]);
    }
    if($request->hasFile('image_4')){
        $file4       =   $request->file('image_4');
        $file_path4  =   $file4->getPathName();
        $fileName4   =   $file4->getClientOriginalName();

        $req = $this->client->post($this->url.'products/'.$id, [
            'multipart' => [
                [
                    'name'     => 'image_4',
                    'contents' => fopen($file_path4, 'r'),
                    'filename' => $fileName4
                ],
                [
                    'name'     => 'name',
                    'contents' => $request->name
                ],
                [
                    'name'     => 'description',
                    'contents' => $request->description
                ],
                [
                    'name'     => 'price',
                    'contents' => $request->price
                ],
                [
                    'name'     => 'stock',
                    'contents' => $request->stock
                ],
                [
                    'name'     => 'weight',
                    'contents' => $request->weight
                ],
                [
                    'name'     => 'unit_id',
                    'contents' => $request->unit_id
                ],
                [
                    'name'     => 'category_id',
                    'contents' => $request->category_id
                ],
                [
                    'name'     => 'etalase_id',
                    'contents' => $request->etalase_id
                ],
                [
                    'name'     => 'token',
                    'contents' => $request->token
                ]
            ]
        ]);
    }
    if(!$request->hasFile('image_1') && !$request->hasFile('image_2') && !$request->hasFile('image_3') && !$request->hasFile('image_4')){
        $req = $this->client->post($this->url.'products/'.$id, [
            'multipart' => [
                [
                    'name'     => 'name',
                    'contents' => $request->name
                ],
                [
                    'name'     => 'description',
                    'contents' => $request->description
                ],
                [
                    'name'     => 'price',
                    'contents' => $request->price
                ],
                [
                    'name'     => 'stock',
                    'contents' => $request->stock
                ],
                [
                    'name'     => 'weight',
                    'contents' => $request->weight
                ],
                [
                    'name'     => 'unit_id',
                    'contents' => $request->unit_id
                ],
                [
                    'name'     => 'category_id',
                    'contents' => $request->category_id
                ],
                [
                    'name'     => 'etalase_id',
                    'contents' => $request->etalase_id
                ],
                [
                    'name'     => 'token',
                    'contents' => $request->token
                ]
            ]
        ]);
    }

        $response               = json_decode($req->getBody()->getContents());
        return $response;
    }

    public function getAllUnit()
    {
        $request                = $this->client->get($this->url.'units');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }
}
