<?php

namespace App\Repositories\WebService;


class CourierRepository
{
    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client([
            'headers' => [
                'userkey' => env("USERKEY"),
                'passkey' => env("PASSKEY"),
                'key'     => env("KEY")
                ]
            ]);
    }

    public function getAllProvince(){
        $request                = $this->client->get($this->url.'raja-ongkir/provinces');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getCity($city_id, $province_id){
        $request                = $this->client->get($this->url.'raja-ongkir/cities/'.$city_id.'/'.$province_id);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function searchCity($keyword)
    {
        $request                = $this->client->get($this->url.'raja-ongkir/cities/search/'.$keyword);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function searchSubDistrict($subdistrictname)
    {
        $request                = $this->client->get($this->url.'raja-ongkir/subdistrict/search/'.$subdistrictname);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function cost($request)
    {
        $request = $this->client->post($this->url.'raja-ongkir/cost', [
            'form_params' => [
                'origin'      => $request['origin'],
                'destination' => $request['destination'],
                'weight'      => $request['weight'],
                'courier'     => $request['courier']
            ]
        ]);

        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }
}
