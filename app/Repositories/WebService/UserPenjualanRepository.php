<?php

namespace App\Repositories\WebService;

class UserPenjualanRepository
{

    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function getUserPenjualanByToken($token)
    {
        $request                = $this->client->get($this->url.'users/'.$token.'/sales');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    // public function getUserDetailPenjualan($id)
    // {
    //     $request                = $this->client->get($this->url.'orders/sellingdetail/'.$id);
    //     $response               = json_decode($request->getBody()->getContents());
    //     return $response->data;
    // }
}
