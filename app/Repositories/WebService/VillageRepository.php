<?php

namespace App\Repositories\WebService;


class VillageRepository
{
    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function getAllVillage($start = null, $limit = null)
    {
        if ($start === null)
        {
            $request                = $this->client->get($this->url.'village');
        } else
        {
            $request                = $this->client->get($this->url.'village?limit='.$limit.'&start='.$start);
        }
        $response                   = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getVillageBySlug($slug)
    {
        $request                = $this->client->get($this->url.'village/'.$slug);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }
}
