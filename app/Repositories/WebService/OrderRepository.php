<?php

namespace App\Repositories\WebService;


class OrderRepository
{
    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function getAllItem($token)
    {
        $request                = $this->client->get($this->url.'carts/products/'.$token);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function addProductToCart($request)
    {
        $request = $this->client->post($this->url.'carts/add-product', [
            'multipart' => [
                [
                    'name'     => 'token',
                    'contents' => $request->token
                ],
                [
                    'name'     => 'product_id',
                    'contents' => $request->product_id
                ],
                [
                    'name'     => 'qty',
                    'contents' => $request->qty
                ]
            ]
        ]);

        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function deleteProductInCart($id)
    {
        $request                = $this->client->delete($this->url.'carts/delete-product/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function updateProductQtyInCart($request)
    {
        $request = $this->client->put($this->url.'carts/update-qty', [
            'form_params' => [
                'id'        => $request['id'],
                'qty'       => $request['qty'],
                'token'     => $request['token']
            ]
        ]);

        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function addProductToCheckout($request)
    {
        if(isset($request['note']))
        {
            $note = $request['note'];
        }else
        {
            $note = "";
        }

        $request = $this->client->post($this->url.'carts/checkout',[
            'form_params' => [
                'token'             =>  $request['token'],
                'couriers'           =>  $request['courier'],
                'stores'            =>  $request['store'],
                'note'              =>  $note,
                'address_id'        =>  $request['address'],
                'payment_type_id'   =>  $request['payment_type_id'],
                'courier_services'   =>  $request['courier_service']
            ]
        ]);

        // $request = $this->client->post($this->url.'carts/checkout', [
        //     'multipart' => [
        //         [
        //             'name'     => 'token',
        //             'contents' => $request['token']
        //         ],
        //         [
        //             'name'     => 'note',
        //             'contents' => $note
        //         ],
        //         [
        //             'name'     => 'address_id',
        //             'contents' => $request['address']
        //         ],
        //         [
        //             'name'     => 'payment_type_id',
        //             'contents' => $request['payment_type_id']
        //         ],
        //         [
        //             'name'     => 'courier',
        //             'contents' => $request['courier']
        //         ],
        //         // [
        //         //     'name'     => 'shipping_charges',
        //         //     'contents' => $request['shipping_charges']
        //         // ],
        //         [
        //             'name'     => 'courier_service',
        //             'contents' => $request['courier_service']
        //         ]
        //     ]
        // ]);

        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getTransactionDetail($id)
    {
        $request                = $this->client->get($this->url.'transactions/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function confirmPayment($request)
    {
        ini_set('max_execution_time', 2180);

        $file       =   $request->file('image_confirm');
        $file_path  =   $file->getPathName();
        $fileName   =   $file->getClientOriginalName();

        $req = $this->client->post($this->url.'carts/confirm-payment', [
            'multipart' => [
                [
                    'name'     => 'image_confirm',
                    'contents' => fopen($file_path, 'r'),
                    'filename'=> $fileName
                ],
                [
                    'name'     => 'user_id',
                    'contents' => $request->user_id
                ],
                [
                    'name'     => 'no_invoice',
                    'contents' => $request->no_invoice
                ]
            ]
        ]);

        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }
}
