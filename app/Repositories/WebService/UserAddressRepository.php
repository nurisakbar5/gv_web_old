<?php

namespace App\Repositories\WebService;

class UserAddressRepository
{

    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function create($request)
    {
        $request = $this->client->post($this->url.'addresses', [
            'multipart' => [
                [
                    'name'     => 'token',
                    'contents' => $request->token
                ],
                [
                    'name'     => 'village_id',
                    'contents' => $request->village_id
                ],
                [
                    'name'     => 'street',
                    'contents' => $request->street
                ],
                [
                    'name'     => 'phone',
                    'contents' => $request->phone
                ],
                [
                    'name'     => 'name',
                    'contents' => $request->name
                ],
                [
                    'name'     => 'receiver_name',
                    'contents' => $request->receiver_name
                ]
            ]
        ]);

        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function show($id)
    {
        $request                = $this->client->get($this->url.'addresses/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function delete($id)
    {
        $request                = $this->client->delete($this->url.'addresses/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function update($request, $id)
    {
        $request = $this->client->post($this->url.'addresses/'.$id, [
            'multipart' => [
                [
                    'name'     => 'village_id',
                    'contents' => $request->village_id
                ],
                [
                    'name'     => 'street',
                    'contents' => $request->street
                ],
                [
                    'name'     => 'phone',
                    'contents' => $request->phone
                ],
                [
                    'name'     => 'name',
                    'contents' => $request->name
                ],
                [
                    'name'     => 'receiver_name',
                    'contents' => $request->receiver_name
                ]
            ]
        ]);

        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }


}
