<?php 

namespace App\Repositories\WebService;

class BannerRepository
{
	protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function getAllBanner()
    {
        $request                = $this->client->get($this->url.'banners/');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getBanner($slug)
    {
        $request                = $this->client->get($this->url.'banners/'.$slug);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function create($request)
    {
        ini_set('max_execution_time', 2180);
        
        $image_web                  =   $request->file('image_web'); 
        $file_path_image_web        =   $image_web->getPathName();
        $fileName_image_web         =   $image_web->getClientOriginalName();

        $image_mobile              =   $request->file('image_mobile'); 
        $file_path_image_mobile    =   $image_mobile->getPathName();
        $fileName_image_mobile     =   $image_mobile->getClientOriginalName();

        $req = $this->client->post($this->url.'banners', [
            'multipart' => [
                [
                    'name'     => 'image_web',
                    'contents' => fopen($file_path_image_web, 'r'),
                    'filename'=> $fileName_image_web
                ],
                [
                    'name'     => 'image_mobile',
                    'contents' => fopen($file_path_image_mobile, 'r'),
                    'filename'=> $fileName_image_mobile
                ],
                [
                    'name'     => 'name',
                    'contents' => $request->name
                ],
                [
                    'name'     => 'description',
                    'contents' => $request->description
                ],
                [
                    'name'     => 'publish',
                    'contents' => $request->publish
                ]
            ]
        ]);
        $response = json_decode($req->getBody()->getContents());
        return $response;
    }

    public function update($request,$id)
    {
        ini_set('max_execution_time', 2180);
        $content =[];

        if ($request->hasFile('image_web')) {
            $image_web              =   $request->file('image_web'); 
            $file_path_image_web    =   $image_web->getPathName();
            $fileName_image_web     =   $image_web->getClientOriginalName();

            array_push($content,['name'=>'image_web','contents'=>fopen($file_path_image_web, 'r'),'filename'=>$fileName_image_web]);
        }

        if ($request->hasFile('image_mobile')) {
            $image_mobile              =   $request->file('image_mobile'); 
            $file_path_image_mobile    =   $image_mobile->getPathName();
            $fileName_image_mobile     =   $image_mobile->getClientOriginalName();

            array_push($content,['name'=>'image_mobile','contents'=>fopen($file_path_image_mobile, 'r'),'filename'=>$fileName_image_mobile]);
        }

        array_push($content,   [
            'name'     => 'name',
            'contents' => $request->name
        ],
        [
            'name'     => 'description',
            'contents' => $request->description
        ],
        [
            'name'     => 'publish',
            'contents' => $request->publish
        ]);

        //dd($content);
        

        $req = $this->client->post($this->url.'banners/edit/'.$id, [
            'multipart' => $content
        ]);

        $response = json_decode($req->getBody()->getContents());
        return $response;
    }

    public function delete($id)
    {
        $request                = $this->client->delete($this->url.'banners/delete/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }
}