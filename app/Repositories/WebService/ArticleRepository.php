<?php

namespace App\Repositories\WebService;


class ArticleRepository
{
    protected $url;
    protected $client;

    public function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
    }

    public function getAllArticle($start=null,$limit=null)
    {

        if($start===null)
        {
            $request                = $this->client->get($this->url.'articles');
        }else
        {
            $request                = $this->client->get($this->url.'articles?limit='.$limit.'&start='.$start);
        }
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    // mendapatkan data artikel berdasarkan slug kategory
    public function getArticleByCategory($categorySlug,$start=null,$limit=null)
    {
        if($start===null)
        {
            $request = $this->client->get($this->url."articles/category/$categorySlug");
        }else
        {
            $request = $this->client->get($this->url."articles/category/$categorySlug".'?offset='.$start.'&limit='.$limit);
        }
        $response = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getArticleById($id)
    {
        $request                = $this->client->get($this->url.'articles/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getArticleBySlug($slug)
    {
        $request                = $this->client->get($this->url.'articles/'.$slug);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function getArticleCommentBySlug($slug)
    {
        $request                = $this->client->get($this->url.'articles/'.$slug.'/comments');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getArticleRelatedBySlug($slug, $limit)
    {
        $request                = $this->client->get($this->url.'articles/'.$slug.'/related?limit='.$limit);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getArticlePopular($start=null,$limit=null)
    {
        $request                = $this->client->get($this->url.'articles/popular?start='.$start.'&limit='.$limit);
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getArticleRelated()
    {
        $request                = $this->client->get($this->url.'articles/related/3/8?limit=3&start=0');
        $response               = json_decode($request->getBody()->getContents());
        return $response->data;
    }

    public function getArticleSearchByKeyword($keyword)
    {
        $request                = $this->client->get($this->url.'articles?keyword='.$keyword.'&start=0&limit=9');
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }


    public function postCommentArticle($request)
    {
        $request = $this->client->post($this->url.'articles/comments', [
            'multipart' => [
                [
                    'name'     => 'token',
                    'contents' => $request->token
                ],
                [
                    'name'     => 'comment',
                    'contents' => $request->comment
                ],
                [
                    'name'     => 'id',
                    'contents' => $request->id
                ]
            ]
        ]);

        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function delete($id)
    {
        $request                = $this->client->delete($this->url.'articles/'.$id);
        $response               = json_decode($request->getBody()->getContents());
        return $response;
    }

    public function create($request)
    {
        ini_set('max_execution_time', 2180);

        $file       =   $request->file('image');
        $file_path  =   $file->getPathName();
        $fileName   =   $file->getClientOriginalName();

        $req = $this->client->post($this->url.'articles', [
            'multipart' => [
                [
                    'name'     => 'image',
                    'contents' => fopen($file_path, 'r'),
                    'filename'=> $fileName
                ],
                [
                    'name'     => 'title',
                    'contents' => $request->title
                ],
                [
                    'name'     => 'tags',
                    'contents' => $request->tags
                ],
                [
                    'name'     => 'article',
                    'contents' => $request->article
                ],
                [
                    'name'     => 'category_id',
                    'contents' => $request->category_id
                ]
            ]
        ]);

        $response = json_decode($req->getBody()->getContents());
        return $response;
    }

    public function update($request,$id)
    {
        if ($request->hasFile('image')) {
            // update with image
            $file       =   $request->file('image');
            $file_path  =   $file->getPathName();
            $fileName   =   $file->getClientOriginalName();

            $req = $this->client->post($this->url.'articles/'.$id, [
                'multipart' => [
                    [
                        'name'     => 'image',
                        'contents' => fopen($file_path, 'r'),
                        'filename'=> $fileName
                    ],
                    [
                        'name'     => 'title',
                        'contents' => $request->title
                    ],
                    [
                        'name'     => 'article',
                        'contents' => $request->article
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ]
                    ]
            ]);
        }else{
            $req = $this->client->post($this->url.'articles/'.$id, [
                'multipart' => [
                    [
                        'name'     => 'title',
                        'contents' => $request->title
                    ],
                    [
                        'name'     => 'article',
                        'contents' => $request->article
                    ],
                    [
                        'name'     => 'tags',
                        'contents' => $request->tags
                    ],
                    [
                        'name'     => 'category_id',
                        'contents' => $request->category_id
                    ]
                    ]
            ]);
        }

        $response               = json_decode($req->getBody()->getContents());
        return $response;
    }
}
