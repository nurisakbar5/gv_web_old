<?php
namespace App\Helpers;

class Iklan{
	public static function getInfoPagesId($id)
	{
		$url =  env("APP_URL");
		$client = new \GuzzleHttp\Client(['headers' => ['userkey' => env("USERKEY"),'passkey'=>env("PASSKEY")]]);
		$request = $client->get($url.'infopages/helper/'.$id);
        $response = $request->getBody()->getContents();

		return $response;
	}
}
?>
