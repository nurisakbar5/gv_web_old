<?php

# Imports the Google Cloud client library
use Google\Cloud\Translate\TranslateClient;

class Translate
{
    static function translateThis($text, $target)
    {
        # Your Google Cloud Platform project ID
        $projectId = 'translate-global-village';

        # Instantiates a client
        $translate = new TranslateClient([
            'projectId' => $projectId,
            'key' => 'AIzaSyDxhgZqhd3KqJuvKbrbKWRIMfz5bvULLjM'
        ]);

        # The target language
        //$target = 'en'

        # Translates some text into English
        $translation = $translate->translate($text, [
            'target' => $target
        ]);

        return $translation['text'];
    }
}
