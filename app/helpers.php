<?php

function redirectIfNotLogin()
{
    if(session('user_id')==null)
    {
        return redirect('admin/login')->with('message','Anda Harus Login Dulu');
    }
}

function is_logged_in()
{
	return session()->get('name') ? true : false;
}

// Truncate string
function str_trc($str)
{
	if(strlen($str) > 40)
	{
		return substr($str, 0, 40) . ' ...';
	}

	return $str;
}

function per_page($type)
{
	return [
		'article' => 6,
		'video' => 9,
		'product' => 6,
		'harvest' => 6,
		'forum' => 5,
	][$type];
}

function price($number)
{
	return number_format($number, 0);
}