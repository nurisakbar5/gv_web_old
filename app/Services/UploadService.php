<?php

namespace App\Services;

class UploadService
{
    protected $url;
    protected $client;

    protected $category;

    function __construct()
    {
        $this->url =  env("API_URL");
        $this->client = new \GuzzleHttp\Client();
    }


    function image($request)
    {
        
    }



    function video($request,$folder,$fileName=null)
    {
        $this->validate($request, ['video' => 'mimetypes:video/mp4,video/mpeg,video/quicktime']);
        
        $file       =   $request->file('video'); 
        $file_path  =   $file->getPathName();
        $file_name  =   $file->getClientOriginalName();

        $req = $this->client->post($this->url.'/upload', [
            'multipart' => [
                [
                    'name'     => 'video',
                    'contents' => fopen($file_path, 'r')
                ],
                [
                    'name'     => 'file_name',
                    'contents' => $file_name
                ],
                [
                    'name'     => 'folder_destination',
                    'contents' => $folder
                ]
            ]
        ]);

        $response = json_decode($req->getBody()->getContents());
    }
}
