<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\WebService\CategoryRepository;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // https://bit.ly/2GrTNIC
        if ($this->app->environment() == 'local') {
            $this->app->register('Kurt\Repoist\RepoistServiceProvider');
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(CategoryRepository $categoryRepo)
    {
        // Blades
        Blade::include('components.pagination');

        $data['categoryProduct'] = $categoryRepo->getCategoryByEntity('product');
        $data['categoryHarvest'] = $categoryRepo->getCategoryByEntity('harvest');
        $data['categoryVideo'] = $categoryRepo->getCategoryByEntity('video');
        $data['categoryArticle'] = $categoryRepo->getCategoryByEntity('article');
        view()->share($data);
    }
}
