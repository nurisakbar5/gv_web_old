<div class="col-lg-3">
    <h2 class="section-title-sm mb-2">Pencarian Produk</h2>
    {{ Form::open(['url'=>'/store/public/hasil-pencarian-produk.html'])}}
    <div class="form-group">
        <div class="input-group">
            <input type="hidden" name="id" id="id" value="{{$result->store->data->id}}">
            <input type="text" name="keyword" class="form-control" placeholder="Cari di sini ...">
            <div class="input-group-append">
                <button type="submit" class="btn btn-cta btn-normal">Cari</button>
            </div>
        </div>
    </div>
    </form>

    <a href="{{url('store/'.$result->store->data->slug)}}.html"><h2 class="section-title-sm mb-2 mt-5">Semua Etalase ({{count($etalases->data)}})</h2></a>
    <ul class="links">
        @foreach ($etalases->data as $item)
            <li><a href="{{url('store/product-by-etalase/'.$item->slug)}}.html" class="nav-link{{ request()->is('store/seller/etalase/'.$item->id.'.html') ? ' active' : '' }}">{{$item->name}}</a></li>
        @endforeach
    </ul>
    <div class="w-100 bg-primary rounded mt-5" style="height: 200px;background-image: url('https://via.placeholder.com/350x200?text=Ads+Here');background-position: center;"></div>
</div>
