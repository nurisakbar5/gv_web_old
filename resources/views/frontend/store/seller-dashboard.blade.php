@extends('frontend.layout')
@section('title')
Seller Dashboard
@endsection

@section('content')
<section class="section">
    <div class="container">
        @include('alert')
        <div class="row">
            <div class="col-lg-3">
                @include('frontend.store.seller-left-content')
            </div>
            <div class="col-lg-9">
                Home
            </div>
        </div>
    </div>
</section>
@endsection
