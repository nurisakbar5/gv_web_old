@extends('frontend.layout')
@section('title')
Seller Location
@endsection
@push('css')
<link href="{{ asset('template/front/css/select2.min.css') }}" rel="stylesheet" />
@endpush
@section('content')
<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                @include('frontend.store.seller-left-content')
            </div>
            <div class="col-lg-9">
                <div class="card card-shadow card-primary">
                    <nav class="nav nav-tabs pb-0 d-flex align-items-center" style="margin-left: 10px; margin-right: 10px;">
                        <div class="d-flex">
                            <a href="/store/setting/info.html" class="nav-link">Informasi Toko</a>
                            <a href="/store/setting/location.html" class="nav-link active">Lokasi Toko</a>
                            <a href="/store/setting/courier.html" class="nav-link">Jasa Pengiriman</a>
                        </div>
                    </nav>
                    <div class="card-body">
                        <div class="loader"></div>
                        <h4 class="section-title-sm mb-0">Lokasi Toko</h4>
                        <p class="text-muted mb-5">Anda dapat mengubah lokasi asal toko Anda.</p>
                        {{-- <a href="" class="btn btn-cta btn-normal">Tambah Lokasi</a>
                        <hr> --}}
                        <div class="row info">
                            <div class="col-lg-4">
                                 <b>Alamat:</b> <br>
                                Jl. Alamat Lokasi Toko
                                {{-- Nama Lokasi Toko <br>
                                <a href="#">Ubah</a> &nbsp; <a href="#">Hapus</a> --}}
                            </div>
                            <div class="col-lg-6">
                                <b>Administratif:</b><br>
                                {{$store->data->subdistrict->subdistrict_name}},
                                {{$store->data->subdistrict->type}} {{$store->data->subdistrict->city}},
                                {{$store->data->subdistrict->province}}
                                {{-- &nbsp; <a href="#">Hapus</a> --}}
                            </div>
                            <div class="col-lg-2">
                                <b>Opsi:</b><br>
                                <a id="tampil" class="btn btn-cta btn-sm mt-1" href="javascript:;" onclick="editSubDistrictForm();">Ubah</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- MODAL EDIT ASAL LOKASI --}}
<div class="modal fade" id="editSubdistrictModal">
    <div class="modal-dialog">
        <div class="modal-content">
                <form id="frmEditSubdistrict">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Asal Lokasi Toko</h5>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group mb-0">
                        <label for="lokasi">Pilih Asal Lokasi Toko</label><br>
                        <select name="subdistrict_id" id="subdistrict_id" class="cari form-control mb-3" required></select>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="id_store" name="id_store">
                    <button type="button" class="btn btn-normal btn-link" data-dismiss="modal">Cancel</button>
                    <button type="button" id="edit" class="go btn btn-cta btn-normal">
                        Simpan Perubahan
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- END MODAL EDIT ASAL LOKASI --}}
@endsection
@push('scripts')
<script src="{{ asset('template/front/js/select2.min.js') }}"></script>
<script>
function editSubDistrictForm(){
    $.ajax({
        type: "GET",
        url: "/store/location",
        beforeSend: function(){
            $('.loader').css("visibility", "visible");
            $('.ge').css("visibility", "hidden");
        },
        success: function (data) {
            console.log(data);
            $('.loader').css("visibility", "hidden");
            $("#editSubdistrictModal").modal('show');
            $("#frmEditSubdistrict select[name=subdistrict_id]").val(data.detail.subdistrict.subdistrict_id),
            $("#frmEditSubdistrict input[name=id_store]").val(data.detail.id);
        },
        error: function(data){
        console.log(data);
        }
    });
}
$('.cari').select2({
    width: '100%',
    placeholder: 'Pilih Berdasarkan Kecamatan',
    ajax: {
        url: '/subdistrict-raja-ongkir',
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
            return {
                results:  $.map(data, function (item) {
                    return {
                        text: item.subdistrict_name +', '+ item.type +' '+ item.city +', '+ item.province,
                        id: item.subdistrict_id
                    }
                })
            };
        },
        cache: true
    }
});
</script>
<script>
$(document).ready(function(){
    $("#edit").click(function(){
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "PUT",
            url: "/store/update-sub-district/" + $("#frmEditSubdistrict input[name=id_store]").val(),
            data:{
                subdistrict_id: $("#frmEditSubdistrict select[name=subdistrict_id]").val(),
            },
            dataType: "json",
            beforeSend: function(){
                $('#edit').addClass('btn-progress');
            },
            success: function (response) {
                $('#frmEditSubdistrict').trigger("reset");
                $('#frmEditSubdistrict .close').click();
                window.location.reload();
            }
        });

        return false;
    });
});
</script>
@endpush
