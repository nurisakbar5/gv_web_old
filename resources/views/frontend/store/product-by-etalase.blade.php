@extends('frontend.layout')
@section('title')
Product by Etalase
@endsection

@section('content')
<section class="section">
    <div class="container">
        <div class="card card-shadow card-primary mb-5">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="d-flex">
                            <div class="rounded no-shrink" style="background-image:url();background-color: #eee;width:80px;height:80px;background-size:cover;background-position:center"></div>
                            <div class="ml-4">
                                <div class="section-title-md mb-0">{{ $result->store->data->name }}</div>
                                <div class="text-muted mb-0 d-flex text-sm">
                                    <div class="mr-4 d-flex align-items-center">
                                        <svg class="fill-current text-warning" width="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g data-name="Layer 2"><g data-name="star"><rect width="24" height="24" transform="rotate(90 12 12)" opacity="0"/><path d="M17.56 21a1 1 0 0 1-.46-.11L12 18.22l-5.1 2.67a1 1 0 0 1-1.45-1.06l1-5.63-4.12-4a1 1 0 0 1-.25-1 1 1 0 0 1 .81-.68l5.7-.83 2.51-5.13a1 1 0 0 1 1.8 0l2.54 5.12 5.7.83a1 1 0 0 1 .81.68 1 1 0 0 1-.25 1l-4.12 4 1 5.63a1 1 0 0 1-.4 1 1 1 0 0 1-.62.18z"/></g></g></svg>
                                        <svg class="fill-current text-warning" width="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g data-name="Layer 2"><g data-name="star"><rect width="24" height="24" transform="rotate(90 12 12)" opacity="0"/><path d="M17.56 21a1 1 0 0 1-.46-.11L12 18.22l-5.1 2.67a1 1 0 0 1-1.45-1.06l1-5.63-4.12-4a1 1 0 0 1-.25-1 1 1 0 0 1 .81-.68l5.7-.83 2.51-5.13a1 1 0 0 1 1.8 0l2.54 5.12 5.7.83a1 1 0 0 1 .81.68 1 1 0 0 1-.25 1l-4.12 4 1 5.63a1 1 0 0 1-.4 1 1 1 0 0 1-.62.18z"/></g></g></svg>
                                        <svg class="fill-current text-warning" width="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g data-name="Layer 2"><g data-name="star"><rect width="24" height="24" transform="rotate(90 12 12)" opacity="0"/><path d="M17.56 21a1 1 0 0 1-.46-.11L12 18.22l-5.1 2.67a1 1 0 0 1-1.45-1.06l1-5.63-4.12-4a1 1 0 0 1-.25-1 1 1 0 0 1 .81-.68l5.7-.83 2.51-5.13a1 1 0 0 1 1.8 0l2.54 5.12 5.7.83a1 1 0 0 1 .81.68 1 1 0 0 1-.25 1l-4.12 4 1 5.63a1 1 0 0 1-.4 1 1 1 0 0 1-.62.18z"/></g></g></svg>
                                        <svg class="fill-current text-warning" width="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g data-name="Layer 2"><g data-name="star"><rect width="24" height="24" transform="rotate(90 12 12)" opacity="0"/><path d="M17.56 21a1 1 0 0 1-.46-.11L12 18.22l-5.1 2.67a1 1 0 0 1-1.45-1.06l1-5.63-4.12-4a1 1 0 0 1-.25-1 1 1 0 0 1 .81-.68l5.7-.83 2.51-5.13a1 1 0 0 1 1.8 0l2.54 5.12 5.7.83a1 1 0 0 1 .81.68 1 1 0 0 1-.25 1l-4.12 4 1 5.63a1 1 0 0 1-.4 1 1 1 0 0 1-.62.18zM12 16.1a.92.92 0 0 1 .46.11l3.77 2-.72-4.21a1 1 0 0 1 .29-.89l3-2.93-4.2-.62a1 1 0 0 1-.71-.56L12 5.25 10.11 9a1 1 0 0 1-.75.54l-4.2.62 3 2.93a1 1 0 0 1 .29.89l-.72 4.16 3.77-2a.92.92 0 0 1 .5-.04z"/></g></g></svg>
                                        <svg class="fill-current text-warning" width="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g data-name="Layer 2"><g data-name="star"><rect width="24" height="24" transform="rotate(90 12 12)" opacity="0"/><path d="M17.56 21a1 1 0 0 1-.46-.11L12 18.22l-5.1 2.67a1 1 0 0 1-1.45-1.06l1-5.63-4.12-4a1 1 0 0 1-.25-1 1 1 0 0 1 .81-.68l5.7-.83 2.51-5.13a1 1 0 0 1 1.8 0l2.54 5.12 5.7.83a1 1 0 0 1 .81.68 1 1 0 0 1-.25 1l-4.12 4 1 5.63a1 1 0 0 1-.4 1 1 1 0 0 1-.62.18zM12 16.1a.92.92 0 0 1 .46.11l3.77 2-.72-4.21a1 1 0 0 1 .29-.89l3-2.93-4.2-.62a1 1 0 0 1-.71-.56L12 5.25 10.11 9a1 1 0 0 1-.75.54l-4.2.62 3 2.93a1 1 0 0 1 .29.89l-.72 4.16 3.77-2a.92.92 0 0 1 .5-.04z"/></g></g></svg>
                                        <div class="ml-2"><a href="">86 Ulasan</a></div>
                                    </div>
                                    <div class="d-flex">
                                        <svg class="mr-1" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10 1.66666C8.24338 1.66657 6.55772 2.35979 5.30937 3.59565C4.06103 4.83151 3.3509 6.51013 3.33334 8.26666C3.33334 12.8333 9.20834 17.9167 9.45834 18.1333C9.60928 18.2624 9.80137 18.3334 10 18.3334C10.1986 18.3334 10.3907 18.2624 10.5417 18.1333C10.8333 17.9167 16.6667 12.8333 16.6667 8.26666C16.6491 6.51013 15.939 4.83151 14.6906 3.59565C13.4423 2.35979 11.7566 1.66657 10 1.66666ZM10 16.375C8.60834 15.05 5 11.375 5 8.26666C5 6.94057 5.52679 5.6688 6.46447 4.73112C7.40215 3.79344 8.67392 3.26666 10 3.26666C11.3261 3.26666 12.5979 3.79344 13.5355 4.73112C14.4732 5.6688 15 6.94057 15 8.26666C15 11.35 11.3917 15.05 10 16.375Z" fill="#666666"/>
                                            <path d="M10 5C9.42313 5 8.85923 5.17106 8.37958 5.49155C7.89994 5.81203 7.5261 6.26756 7.30535 6.80051C7.08459 7.33346 7.02683 7.9199 7.13937 8.48568C7.25191 9.05146 7.5297 9.57116 7.9376 9.97906C8.34551 10.387 8.86521 10.6648 9.43098 10.7773C9.99676 10.8898 10.5832 10.8321 11.1162 10.6113C11.6491 10.3906 12.1046 10.0167 12.4251 9.53708C12.7456 9.05744 12.9167 8.49353 12.9167 7.91667C12.9167 7.14312 12.6094 6.40125 12.0624 5.85427C11.5154 5.30729 10.7735 5 10 5ZM10 9.16667C9.75277 9.16667 9.5111 9.09336 9.30553 8.956C9.09997 8.81865 8.93976 8.62343 8.84515 8.39502C8.75054 8.16661 8.72578 7.91528 8.77402 7.6728C8.82225 7.43033 8.9413 7.2076 9.11611 7.03278C9.29093 6.85797 9.51366 6.73892 9.75613 6.69068C9.99861 6.64245 10.2499 6.66721 10.4784 6.76182C10.7068 6.85643 10.902 7.01664 11.0393 7.2222C11.1767 7.42777 11.25 7.66944 11.25 7.91667C11.25 8.24819 11.1183 8.56613 10.8839 8.80055C10.6495 9.03497 10.3315 9.16667 10 9.16667Z" fill="#666666"/>
                                        </svg>
                                        {{ ucwords(strtolower($result->store->data->subdistrict->subdistrict_name)) }}, {{ ucwords(strtolower($result->store->data->subdistrict->type)) }} {{ ucwords(strtolower($result->store->data->subdistrict->city)) }}
                                    </div>
                                </div>

                                <div class="mt-4">
                                    <a href="" class="btn btn-cta btn-sm mr-1">Chat Penjual</a>
                                    <a href="" class="btn btn-outline-cta btn-sm">Follow</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 d-flex align-items-center">
                        <div class="divider-v ml-0" style="margin-right: 60px;height: 80px; background-color: #eee;"></div>
                        <div class="row w-100">
                            <div class="col">
                                <p class="text-primary stat-value">200</p>
                                <h6 class="stat-name">Total Produk</h6>
                            </div>
                            <div class="col">
                                <p class="text-primary stat-value">100</p>
                                <h6 class="stat-name">Penjualan</h6>
                            </div>
                            <div class="col">
                                <p class="text-primary stat-value">89</p>
                                <h6 class="stat-name">Pelanggan</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <nav class="nav nav-tabs bordered pb-0 d-flex align-items-center">
                <div class="d-flex">
                    <a href="{{ url('user-produk') }}.html" class="nav-link active">Produk</a>
                    <a href="{{ url('user-panen') }}.html" class="nav-link">Panen</a>
                    <a href="{{ url('user-lahan') }}.html" class="nav-link">Lahan</a>
                </div>
                <div class="ml-auto mr-3 d-flex align-items-center">
                    <div class="text-muted">Bagikan: </div>
                    <ul class="social-links sm mb-0 pl-0 ml-2">
                        <li><a class="social-links-item social-links-item-facebook" href="#"><svg xmlns="http://www.w3.org/2000/svg" class="social-links-icon" viewBox="0 0 24 24">
                            <g data-name="Layer 2">
                                <g data-name="facebook">
                                    <rect width="24" height="24" transform="rotate(180 12 12)" opacity="0"></rect>
                                    <path d="M17 3.5a.5.5 0 0 0-.5-.5H14a4.77 4.77 0 0 0-5 4.5v2.7H6.5a.5.5 0 0 0-.5.5v2.6a.5.5 0 0 0 .5.5H9v6.7a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-6.7h2.62a.5.5 0 0 0 .49-.37l.72-2.6a.5.5 0 0 0-.48-.63H13V7.5a1 1 0 0 1 1-.9h2.5a.5.5 0 0 0 .5-.5z"></path>
                                </g>
                            </g>
                        </svg></a></li>
                        <li><a class="social-links-item social-links-item-twitter" href="#"><svg class="social-links-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                            <g data-name="Layer 2">
                                <g data-name="twitter">
                                    <polyline points="0 0 24 0 24 24 0 24" opacity="0"></polyline>
                                    <path d="M8.08 20A11.07 11.07 0 0 0 19.52 9 8.09 8.09 0 0 0 21 6.16a.44.44 0 0 0-.62-.51 1.88 1.88 0 0 1-2.16-.38 3.89 3.89 0 0 0-5.58-.17A4.13 4.13 0 0 0 11.49 9C8.14 9.2 5.84 7.61 4 5.43a.43.43 0 0 0-.75.24 9.68 9.68 0 0 0 4.6 10.05A6.73 6.73 0 0 1 3.38 18a.45.45 0 0 0-.14.84A11 11 0 0 0 8.08 20"></path>
                                </g>
                            </g>
                        </svg></a></li>
                        <li><a class="social-links-item social-links-item-whatsapp" href="#"><svg class="social-links-icon" role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <title>WhatsApp icon</title>
                            <path d="M17.498 14.382c-.301-.15-1.767-.867-2.04-.966-.273-.101-.473-.15-.673.15-.197.295-.771.964-.944 1.162-.175.195-.349.21-.646.075-.3-.15-1.263-.465-2.403-1.485-.888-.795-1.484-1.77-1.66-2.07-.174-.3-.019-.465.13-.615.136-.135.301-.345.451-.523.146-.181.194-.301.297-.496.1-.21.049-.375-.025-.524-.075-.15-.672-1.62-.922-2.206-.24-.584-.487-.51-.672-.51-.172-.015-.371-.015-.571-.015-.2 0-.523.074-.797.359-.273.3-1.045 1.02-1.045 2.475s1.07 2.865 1.219 3.075c.149.195 2.105 3.195 5.1 4.485.714.3 1.27.48 1.704.629.714.227 1.365.195 1.88.121.574-.091 1.767-.721 2.016-1.426.255-.705.255-1.29.18-1.425-.074-.135-.27-.21-.57-.345m-5.446 7.443h-.016c-1.77 0-3.524-.48-5.055-1.38l-.36-.214-3.75.975 1.005-3.645-.239-.375c-.99-1.576-1.516-3.391-1.516-5.26 0-5.445 4.455-9.885 9.942-9.885 2.654 0 5.145 1.035 7.021 2.91 1.875 1.859 2.909 4.35 2.909 6.99-.004 5.444-4.46 9.885-9.935 9.885M20.52 3.449C18.24 1.245 15.24 0 12.045 0 5.463 0 .104 5.334.101 11.893c0 2.096.549 4.14 1.595 5.945L0 24l6.335-1.652c1.746.943 3.71 1.444 5.71 1.447h.006c6.585 0 11.946-5.336 11.949-11.896 0-3.176-1.24-6.165-3.495-8.411"></path>
                        </svg></a></li>
                        <li><a class="social-links-item social-links-item-telegram" href="#"><svg class="social-links-icon" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                            <title>Telegram icon</title>
                            <path d="M23.91 3.79L20.3 20.84c-.25 1.21-.98 1.5-2 .94l-5.5-4.07-2.66 2.57c-.3.3-.55.56-1.1.56-.72 0-.6-.27-.84-.95L6.3 13.7l-5.45-1.7c-1.18-.35-1.19-1.16.26-1.75l21.26-8.2c.97-.43 1.9.24 1.53 1.73z"></path>
                        </svg></a></li>
                    </ul>
                </div>
            </nav>
        </div>

        <div class="row gutter-md">
            @include('frontend.store.left-content')
            <div class="col-lg-9">
                <div class="d-flex align-items-center mb-2">
                    <h2 class="section-title-md">Produk</h2>
                    <div class="dropdown ml-auto">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown">Urutkan Berdasarkan</a>
                        <ul class="dropdown-menu">
                            <li><a href="" class="dropdown-item">Terbaru</a></li>
                            <li><a href="" class="dropdown-item">Terlama</a></li>
                            <li><a href="" class="dropdown-item">Termahal</a></li>
                            <li><a href="" class="dropdown-item">Termurah</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    @foreach($products->data as $product)
                        <div class="col-lg-4 mb-5">
                            <div class="card card-shadow ">
                                <div class="card-image" style="background-image:url({{ $product->image_url }});background-position:center;background-size:cover;height:180"></div>
                                <div class="card-body">
                                    <h6 class="card-title"><a href="{{ url('produk', $product->slug) }}.html">{{ $product->name }}</a></h6>
                                    <div class="card-price">Rp. {{ $product->price_idr }}</div>
                                    <div class="card-author">
                                        <div class="card-author-image" style="background-image:url({{ $product->store->image }});background-position:center;background-size:cover"></div>
                                        <div class="card-author-detail">
                                            <div class="card-author-name">{{ $product->store->name }}</div>
                                            <div class="card-author-desc">
                                                    {{ $product->store->region->subdistrict_name }},
                                                    {{ $product->store->region->type }}
                                                    {{ $product->store->region->city }},
                                                    {{ $product->store->region->province }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
