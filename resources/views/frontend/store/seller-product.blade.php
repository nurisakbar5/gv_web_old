@extends('frontend.layout')
@section('title')
Seller Products
@endsection

@section('content')
<section class="section">
    <div class="container">
        @include('alert')
        <div class="row">
            <div class="col-lg-3">
                @include('frontend.store.seller-left-content')
                <br>
                <div class="card card-shadow card-primary">
                    <ul class="nav nav-pills bordered flex-column">
                        <li><a href="{{ url('store/setting/'.$store->data->slug.'/products') }}.html" class="nav-link">Semua Etalase ({{count($etalases->data)}})</a></li>
                        @foreach ($etalases->data as $item)
                        <li><a href="{{url('store/seller/etalase/'.$item->slug)}}.html" class="nav-link{{ request()->is('store/seller/etalase/'.$item->id.'.html') ? ' active' : '' }}">{{$item->name}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="col-lg-6">
                    <div class="form-group">
                        {{ Form::open(['url'=>'/store/hasil-pencarian-produk.html'])}}
                        <div class="input-group">
                            <input type="hidden" name="id" id="id" value="{{$result->store->data->id}}">
                            <input type="text" name="keyword" class="form-control" placeholder="Cari di sini ...">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-cta btn-normal">Cari</button>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>

                <div class="card card-shadow card-primary">
                    <div class="card-body">
                        <div class="row">
                            <span id="go"></span>
                                @foreach($result->products->data as $product)
                                <div class="col-lg-4 mb-5">
                                    <div class="card card-shadow ">
                                        <div class="card-image" style="background-image:url({{ $product->image_url }});background-position:center;background-size:cover;height:180"></div>
                                        <div class="card-body">
                                            <h6 class="card-title"><a href="{{ url('produk', $product->slug) }}.html">{{ $product->name }}</a></h6>
                                            <div class="card-price">Rp. {{ $product->price_idr }}</div>
                                            <div class="card-author">
                                                <div class="card-author-image" style="background-image:url({{ $product->store->image }});background-position:center;background-size:cover"></div>
                                                <div class="card-author-detail">
                                                    <div class="card-author-name">{{ $product->store->name }}</div>
                                                    <div class="card-author-desc">{{ $product->store->region->type }}
                                                            {{ $product->store->region->city }},
                                                            {{ $product->store->region->province }}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
<script>
$(document).ready(function(){
    $('.etalpro').change(function(id){
        $.ajax({
            type: "GET",
            url: "/produk/etalase/"+id,
            data:{
                id: $(this).val(),
            }
        });
    })
});
</script>
@endpush
