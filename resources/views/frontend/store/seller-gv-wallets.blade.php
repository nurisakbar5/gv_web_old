@extends('frontend.layout')
@section('title')
Seller Gv Wallets
@endsection
@push('css')
<link href="{{ asset('template/front/css/select2.min.css') }}" rel="stylesheet" />
@endpush
@section('content')
<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
            @include('frontend.store.seller-left-content')
        </div>
            <div class="col-lg-9">
                @include('alert')
                <div class="loader"></div>
                <div class="card card-shadow card-primary">
                    <ul class="nav nav-tabs pb-0 d-flex align-items-center" id="myTab" role="tablist" style="margin-left: 10px; margin-right: 10px;">
                        <li class="nav-item">
                            <a class="nav-link active" id="rekening-tab" data-toggle="tab" href="#rekening" role="tab" aria-controls="rekening" aria-selected="true">Rekening Saya</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="tambah-rekening-tab" data-toggle="tab" href="#tambah-rekening" role="tab" aria-controls="tambah-rekening" aria-selected="false">Tambah Rekening</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="histori-tab" data-toggle="tab" href="#histori" role="tab" aria-controls="histori" aria-selected="false">Histori Transaksi</a>
                        </li>
                    </ul>

                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="rekening" role="tabpanel" aria-labelledby="rekening-tab">
                                @foreach ($accounts->data as $account)
                                <div class="row">
                                    <div class="col-lg-8">
                                        {{$account->bank->name}} | {{$account->account_number}}
                                    </div>
                                    <div class="col-lg-4">
                                    <a id="tampil" onclick="editAccountForm('{{$account->id}}')">Ubah</a> | <a onclick="deleteTaskForm('{{$account->id}}')">Hapus</a>
                                    </div>
                                </div>
                                <h5><b>{{$account->account_name}}</b></h5>
                                <hr>
                                @endforeach
                            </div>
                            <div class="tab-pane fade" id="tambah-rekening" role="tabpanel" aria-labelledby="tambah-rekening-tab">
                                <form action="/addAccount" method="post">
                                    @csrf
                                    <input type="hidden" name="store_id" id="store_id" value="{{ $store->data->id }}">
                                    <div class="form-group">
                                        <label for="bank">Pilih Bank</label>
                                        <select name="bank_id" id="bank_id" class="form-control">
                                            @foreach ($banks as $bank)
                                            <option value="{{$bank->id}}">{{$bank->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Nama Pemiliki Rekening</label>
                                        <input type="text" name="account_name" id="account_name" class="form-control" placeholder="Contoh: Fulan">
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Nomor Pemiliki Rekening</label>
                                        <input type="number" name="account_number" id="account_number" class="form-control" placeholder="Contoh: 123929939">
                                    </div>
                                    <button type="submit" class="btn btn-cta btn-normal">Tambah</button>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="histori" role="tabpanel" aria-labelledby="histori-tab">
                                3
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- MODAL EDIT REKENING --}}
<div class="modal fade" id="editRekeningModal">
    <div class="modal-dialog">
        <div class="modal-content">
                <form id="frmEditRekening">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Rekening</h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">x</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="bank">Pilih Bank</label>
                        <select name="bank_id" id="bank_id" class="form-control">
                            @foreach ($banks as $bank)
                            <option value="{{$bank->id}}">{{$bank->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Nama Pemiliki Rekening</label>
                        <input type="text" name="account_name" id="account_name" class="form-control" placeholder="Contoh: Fulan">
                    </div>
                    <div class="form-group">
                        <label for="name">Nomor Pemiliki Rekening</label>
                        <input type="number" name="account_number" id="account_number" class="form-control" placeholder="Contoh: 123929939">
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="id" name="id">
                    <input type="button" value="Cancel" class="btn btn-normal btn-warning" data-dismiss="modal">
                    <button type="button" id="edit" class="btn btn-cta btn-normal">
                        Edit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- END MODAL EDIT REKENING --}}
{{-- MODAL HAPUS REKENING --}}
<div class="modal fade" id="deleteRekeningModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="frmDeleteRekening">
                    <div class="modal-header">
                        <h4 class="modal-title" id="delete-title" name="title">Hapus Rekening</h4>
                        <button type="button" aria-hidden="true" class="close" data-dismiss="modal">
                            x
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Anda Yakin Ingin Menghapus Rekening Ini</p>
                        <p class="text-warning">
                            <small>Tindakan ini tidak bisa dibatalkan</small>
                        </p>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" id="id" name="id" value="0">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button class="btn btn-danger" id="btn-delete" type="button">
                            Hapus Rekening
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

{{-- END MODAL HAPUS REKENING --}}
@endsection
@push('scripts')
<script>
    function editAccountForm(account_id){
        $.ajax({
            type: "GET",
            url: "/bankaccounts/" + account_id,
            beforeSend: function(){
                $('.loader').css("visibility", "visible");
                $('.ge').css("visibility", "hidden");
            },
            success: function (data) {
                console.log(data);
                $("#ad-error-bag").hide();
                $('.loader').css("visibility", "hidden");
                $("#editRekeningModal").modal('show');
                $("#frmEditRekening select[name=bank_id]").val(data.detail.bank.id),
                $("#frmEditRekening input[name=account_name]").val(data.detail.account_name);
                $("#frmEditRekening input[name=account_number]").val(data.detail.account_number);
                $("#frmEditRekening input[name=id]").val(data.detail.id);
            },
            error: function(data){
            console.log(data);
            }
        });
    }
    function deleteTaskForm(account_id){
    $.ajax({
        type: 'GET',
        url: '/bankaccounts/' + account_id,
        beforeSend: function(){
            $('.loader').css("visibility", "visible");
            $('.ge').css("visibility", "hidden");
        },
        success: function(data){
            $('.loader').css("visibility", "hidden");
            $("#frmDeleteRekening #delete-title").html("Hapus Rekening (" + data.detail.account_name + ")?");
            $("#frmDeleteRekening input[name=id]").val(data.detail.id);
            $("#deleteRekeningModal").modal('show');
        },
        error: function(data){
            console.log(data);
        }
    });
}
</script>
<script>
    $(document).ready(function(){
        $("#edit").click(function(){
            $.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "PUT",
                url: "/bankaccounts/update/" + $("#frmEditRekening input[name=id]").val(),
                data:{
                        bank_id: $("#frmEditRekening select[name=bank_id]").val(),
                        account_name: $("#frmEditRekening input[name=account_name]").val(),
                        account_number: $("#frmEditRekening input[name=account_number]").val(),
                },
                dataType: "json",
                beforeSend: function(){
                    // $('.go').css("visibility", "hidden");
                    $('#edit').addClass('btn-progress');
                },
                success: function (response) {
                    $('#frmEditRekening').trigger("reset");
                    $('#frmEditRekening .close').click();
                    window.location.reload();
                }
            });
        });

        $("#btn-delete").click(function(){
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'DELETE',
            url: '/bankaccounts/delete/' + $("#frmDeleteRekening input[name=id]").val(),
            dataType: 'json',
            beforeSend: function(){
                // $('.go').css("visibility", "hidden");
                $('#btn-delete').addClass('btn-progress');
            },
            success: function(data){
                $("#frmDeleteRekening .close").click();
                window.location.reload();
            },
            error: function(data){
                console.log(data);
            }
        });
    });
    });
</script>
@endpush
