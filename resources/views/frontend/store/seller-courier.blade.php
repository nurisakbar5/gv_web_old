@extends('frontend.layout')
@section('title')
Seller Courier
@endsection

@section('content')
<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                @include('frontend.store.seller-left-content')
            </div>
            <div class="col-lg-9">
                @include('alert')
                <div class="card card-shadow card-primary">
                    <nav class="nav nav-tabs pb-0 d-flex align-items-center" style="margin-left: 10px; margin-right: 10px;">
                        <div class="d-flex">
                            <a href="/store/setting/info.html" class="nav-link">Informasi Toko</a>
                            <a href="/store/setting/location.html" class="nav-link">Lokasi Toko</a>
                            <a href="/store/setting/courier.html" class="nav-link active">Jasa Pengiriman</a>
                        </div>
                    </nav>
                    <div class="card-body">
                        <h5 class="section-title-sm">Asal Pengiriman</h5>
                        <div class="row">
                            <div class="col-md-3">
                                <b>Kabupaten/Kota:</b><br>
                                {{$store->data->subdistrict->type}} {{$store->data->subdistrict->city}}
                            </div>
                            <div class="col-md-3">
                                <b>Kecamatan:</b><br>
                                {{$store->data->subdistrict->subdistrict_name}}
                            </div>
                            <div class="col-md-3">
                                <b>Provinsi:</b><br>
                                {{$store->data->subdistrict->province}}
                            </div>
                        </div>
                        <hr class="mt-5 mb-4">
                        <div class="d-flex align-items-center">
                            <h5 class="section-title-sm">Jasa Pengiriman</h5>
                            <a id="tampil" href="javascript:;" onclick="event.preventDefault();addTaskForm();" class="ml-auto btn btn-sm btn-primary">Tambah Jasa Pengiriman</a>
                        </div>
                        <div class="row go mb-4" style="display: none;">
                            <div class="col-lg-5">
                                <form id="frmAddCour">
                                    <input type="hidden" name="store_id" id="store_id" value="{{$store->data->id}}">
                                    <label>Tambah Kurir</label><br>
                                    <select class="form-control go" name="courier_code" id="courier_code">
                                        <option value="">Pilih Kurir</option>
                                        @foreach ($cours as $cour)
                                        <option value="{{$cour->courier_code}}">{{ $cour->courier_name }}</option>
                                        @endforeach
                                    </select>
                                </form>
                            </div>
                        </div>
                        <div class="loader"></div>
                        <div class="row">
                            @foreach ($couriers->data as $item)
                            <div class="col-lg-4 mb-4 {{ ($item->active!="y" && $item->active!="n") ? "hidden" : "" }}">
                                <label class="imagecheck w-100 h">
                                    <input class="imagecheck-input" type="checkbox" onclick='activated("{{ $item->id }}")' name="id" value="{{ $item->id }}" {{ ($item->active=="y")? "checked" : "" }}>
                                    <figure class="imagecheck-figure p-2" style="background-image: url({{ $item->logo }});height: 90px;background-size: 55%;background-position: center;background-repeat: no-repeat;"></figure>
                                    <div class="text-center mt-2 font-weight-bold">
                                        {{ ucwords(strtolower($item->courier_name)) }}
                                    </div>
                                </label>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
<script>
    function activated(id)
    {
        $.ajax({
            url: "/changeActiveCourier",
            data:'id='+id,
            type: 'GET',
            beforeSend: function(){
                $('.loader').css("visibility", "visible");
                $('.info').css("visibility", "hidden");
            },
            success: function(res) {
                // alert('Berhasil Memperbaharui Informasi Pengiriman');
                console.log(res);
            },
            complete: function(){
                $('.loader').css("visibility", "hidden");
                $('.info').css("visibility", "visible");
            }
        });
    }
    function addTaskForm(){
        $(document).ready(function(){
            $('.go').slideDown();
        });
    }

    $(document).ready(function(){
        $('.go').change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/addCourier",
                data:{
                    store_id: $("#frmAddCour input[name=store_id]").val(),
                    courier_code: $("#frmAddCour select[name=courier_code]").val(),
                },
                type: 'POST',
                beforeSend: function(){
                    $('.loader').css("visibility", "visible");
                    $('.go').css("visibility", "hidden");
                },
                success: function(res) {
                    //$("#alamat_pengiriman").html(res);
                    console.log(res);
                    // $(".h").html(res);
                },
                complete: function(){
                    $('.loader').css("visibility", "visible");
                    $('.info').css("visibility", "hidden");
                    $(document).ajaxStop(function(){
                        window.location.reload();
                    });
                }
            });
        })
    });
</script>
@endpush
