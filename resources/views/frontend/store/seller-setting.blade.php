@extends('frontend.layout')
@section('title')
Seller Informasi Toko
@endsection

@section('content')
<section class="section">
    <div class="container">
        @include('alert')
        <div class="row">
            <div class="col-lg-3">
                @include('frontend.store.seller-left-content')
            </div>
            <div class="col-lg-9">
                <div class="card card-shadow card-primary">
                    <nav class="nav nav-tabs pb-0 d-flex align-items-center" style="margin-left: 10px; margin-right: 10px;">
                        <div class="d-flex">
                            <a href="/store/setting/info.html" class="nav-link active">Informasi Toko</a>
                            <a href="/store/setting/location.html" class="nav-link">Lokasi Toko</a>
                            <a href="/store/setting/courier.html" class="nav-link">Jasa Pengiriman</a>
                        </div>
                    </nav>
                    <div class="card-body">
                        <h4 class="section-title-sm mb-0">Informasi Toko</h4>
                        <p class="text-muted mb-5">Ubah deskripsi toko Anda. Anda tidak dapat mengubah nama toko.</p>

                        {{ Form::model($store,['url'=>'store/'.$store->data->slug])}}
                        @method('PUT')
                            <div class="row">
                                <div class="col-lg-2">
                                    Nama Toko
                                </div>
                                <div class="col-lg-10">
                                    <input type="text" name="name" id="name" class="form-control" value="{{$store->data->name}}" disabled>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-2">
                                    Deskripsi Toko
                                </div>
                                <div class="col-lg-10">
                                    <textarea name="description" id="description" class="form-control">{{$store->data->description}}</textarea>
                                    <br>
                                    <button type="submit" class="btn btn-cta btn-normal">Simpan</button>
                                </div>
                            </div>
                        {{ Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
