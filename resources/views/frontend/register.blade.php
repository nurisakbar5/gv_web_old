@extends('frontend.layout')

@section('title','Pendaftaran Akun baru')

@section('content')
    <div id="__next">
	<section class="section">
        <div class="container">
        	{{-- @if(session('message')!='')
				<div class="alert alert-primary" role="alert">
					<b>Message : </b>{{ session('message')}}
				</div>
				@endif --}}

        	<div class="row justify-content-center">
        		<div class="col-lg-5">
					@include('alert')
					@include('validation')
					<div class="card fat card-primary">
						<div class="card-body">
							<h1 class="card-title">Pendaftaran Akun Baru</h1>
							{{ Form::open(['url'=>'/goRegister','class'=>'needs-validation','novalidate'=>''])}}

								<div class="form-group">
									<label for="name">Nama Lengkap</label>
									<input value="{{ old('name') }}" id="name" type="text" class="form-control{{ $errors->first('name') ? ' is-invalid' : '' }}" name="name" value="" required autofocus>
									<div class="invalid-feedback">
										{{ $errors->first('name') }}
									</div>
								</div>

								<div class="form-group">
									<label for="email">Alamat Email</label>
									<input value="{{ old('email') }}" id="email" type="email" class="form-control{{ $errors->first('email') ? ' is-invalid' : '' }}" name="email" value="" required autofocus>
									<div class="invalid-feedback">
										{{ $errors->first('email') }}
									</div>
								</div>

								<div class="form-group">
									<label for="password">Password</label>
									<input id="password" type="password" class="form-control{{ $errors->first('password') ? ' is-invalid' : '' }}" name="password" required data-eye>
								    <div class="invalid-feedback">
										{{ $errors->first('password') }}
									</div>
								</div>

								<div class="form-group">
									<div class="custom-checkbox custom-control">
										<input type="checkbox" name="agree" id="agree" class="custom-control-input{{ $errors->first('agree') ? ' is-invalid' : '' }}"{{ old('agree') ? ' checked' : '' }}>
										<label for="agree" class="custom-control-label">I agree to the <a href="#">Terms and Conditions</a></label>
										<div class="invalid-feedback">
											Anda harus menyutujui syarat dan ketentuan
										</div>
									</div>
								</div>

								<div class="form-group m-0">
									<button type="submit" class="btn btn-cta btn-block">
										Daftar Sekarang
									</button>
								</div>
								<div class="mt-4 text-center">
									Sudah Punya Akun? <a href="/login.html">Login</a>
								</div>
							</form>
						</div>
					</div>
        		</div>
        	</div>
        </div>
    </section>
    </div>

@endsection
