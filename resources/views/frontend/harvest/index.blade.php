@extends('frontend.layout')
@section('title','Jual Beli Hasil Panen')
@section('content')

<section class="section">
    <div class="container">
        <div class="row">
            @include('frontend.harvest.leftcontent')

            <div class="col-lg-9">
                <div class="d-flex align-items-center">
                    <h2 class="section-title-md">Jual Beli Hasil Panen</h2>
                    <div class="dropdown ml-auto">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown">Urutkan Berdasarkan</a>
                        <ul class="dropdown-menu">
                            <li><a href="" class="dropdown-item">Terbaru</a></li>
                            <li><a href="" class="dropdown-item">Terlama</a></li>
                        </ul>
                    </div>
                </div>

                <div class="row mt-4">

                @foreach($harvestByCategory as $ct)
                    {{-- <p> {{ $ct->name}}</p> --}}
                    @foreach($ct->harvests->data as $harvest)
                    <div class="col-lg-4 mb-5">
                        <div class="card card-shadow ">
                            <div class="card-image" style="background-image:url({{ $harvest->front_images[0] }});background-position:center;background-size:cover;height:150px">
                            </div>
                            <div class="card-body">
                                <h6 class="card-title mb-2"><a href="panen/{{ $harvest->slug }}.html">{{ $harvest->title }}</a></h6>
                                <p class="d-flex align-items-center time">
                                    <svg class="mr-2" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M17.5 5.83333C17.5079 5.77527 17.5079 5.7164 17.5 5.65833C17.4928 5.60941 17.4787 5.56173 17.4583 5.51667C17.4363 5.47594 17.4112 5.43694 17.3833 5.4C17.3516 5.34729 17.3123 5.29956 17.2667 5.25833L17.1667 5.2C17.1186 5.16413 17.0651 5.13601 17.0083 5.11667H16.8417C16.7909 5.0675 16.7316 5.02796 16.6667 5H12.5C12.279 5 12.067 5.0878 11.9107 5.24408C11.7545 5.40036 11.6667 5.61232 11.6667 5.83333C11.6667 6.05435 11.7545 6.26631 11.9107 6.42259C12.067 6.57887 12.279 6.66667 12.5 6.66667H14.8583L11.525 10.5917L7.925 8.45C7.75449 8.34858 7.55338 8.31133 7.35786 8.34493C7.16233 8.37854 6.9852 8.4808 6.85834 8.63333L2.69167 13.6333C2.6215 13.7175 2.56863 13.8148 2.5361 13.9194C2.50356 14.0241 2.49199 14.1342 2.50205 14.2433C2.51212 14.3525 2.54361 14.4585 2.59474 14.5555C2.64587 14.6525 2.71562 14.7384 2.8 14.8083C2.94993 14.9326 3.13863 15.0004 3.33334 15C3.45576 15.0002 3.57673 14.9734 3.68763 14.9216C3.79853 14.8697 3.89665 14.7941 3.975 14.7L7.68334 10.25L11.2417 12.3833C11.4104 12.4834 11.6091 12.5208 11.8027 12.4888C11.9963 12.4567 12.1724 12.3574 12.3 12.2083L15.8333 8.08333V10C15.8333 10.221 15.9211 10.433 16.0774 10.5893C16.2337 10.7455 16.4457 10.8333 16.6667 10.8333C16.8877 10.8333 17.0996 10.7455 17.2559 10.5893C17.4122 10.433 17.5 10.221 17.5 10V5.83333Z" fill="#666666"/>
                                    </svg>
                                    {{ $harvest->category->name }}
                                </p>
                                <div class="card-author">
                                    <div class="card-author-image" style="background-image:url({{ $harvest->user->photo }});background-position:center;background-size:cover"></div>
                                    <div class="card-author-detail">
                                        <div class="card-author-name">{{ $harvest->user->name }}</div>
                                        <div class="card-author-desc">{{ $harvest->region->regency }}, {{ $harvest->region->province }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
