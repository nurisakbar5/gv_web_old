<div class="col-lg-3">
    <h2 class="section-title-sm mb-3">Pencarian Panen</h2>
    {{ Form::open(['url'=>'/panen/pencarian-hasil-panen.html'])}}
    <div class="form-group">
        <div class="input-group">
            <input type="text" name="keyword" class="form-control" placeholder="Cari di sini ...">
            <div class="input-group-append">
                <button type="submit" class="btn btn-primary btn-normal">Cari</button>
            </div>
        </div>
    </div>
    </form>

    <h2 class="section-title-sm mb-3">Kategori Panen</h2>
    <ul class="links">

        @foreach($categories as $category)
        <li>
            <a href="/panen/kategori/{{ $category->slug }}.html">{{ $category->name }}</a>
        </li>
        @endforeach
    </ul>
</div>
