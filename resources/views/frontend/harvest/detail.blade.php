@extends('frontend.layout')
@section('seo')
{!! SEO::generate(true) !!}
@endsection
@section('content')
<section class="section">
    <div class="container">
        <div class="row gutter-md">
            <div class="col-lg-3">
                <div class="sticky-outer-wrapper">
                    <div class="carousel-preview chocolat-parent">
                        @foreach ($harvest->front_images as $image)
                        <a href="{{ $image }}" class="chocolat-image">
                            <div class="rounded-lg" style="background-image:url({{$image}});width:100%;height:300px;background-size:cover;background-position:center"></div>
                        </a>
                        @endforeach
                    </div>
                    <div class="d-flex mt-n3 preview-thumbs">
                        @foreach ($harvest->front_images as $image)
                        <div class="preview-thumb no-shink" data-index="0">
                            <div style="border-radius: 3px;background-image:url({{$image}});width:50px;height:50px;background-size:cover;background-position:center"></div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="/panen.html">Harvest</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $harvest->title }}</li>
                    </ol>
                </nav>
                <h1 class="page-title">{{ $harvest->title }}</h1>
                <p class="d-flex time text-muted">
                    <svg class="mr-1" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M10 1.66666C8.24338 1.66657 6.55772 2.35979 5.30937 3.59565C4.06103 4.83151 3.3509 6.51013 3.33334 8.26666C3.33334 12.8333 9.20834 17.9167 9.45834 18.1333C9.60928 18.2624 9.80137 18.3334 10 18.3334C10.1986 18.3334 10.3907 18.2624 10.5417 18.1333C10.8333 17.9167 16.6667 12.8333 16.6667 8.26666C16.6491 6.51013 15.939 4.83151 14.6906 3.59565C13.4423 2.35979 11.7566 1.66657 10 1.66666ZM10 16.375C8.60834 15.05 5 11.375 5 8.26666C5 6.94057 5.52679 5.6688 6.46447 4.73112C7.40215 3.79344 8.67392 3.26666 10 3.26666C11.3261 3.26666 12.5979 3.79344 13.5355 4.73112C14.4732 5.6688 15 6.94057 15 8.26666C15 11.35 11.3917 15.05 10 16.375Z" fill="#666666"/>
                        <path d="M10 5C9.42313 5 8.85923 5.17106 8.37958 5.49155C7.89994 5.81203 7.5261 6.26756 7.30535 6.80051C7.08459 7.33346 7.02683 7.9199 7.13937 8.48568C7.25191 9.05146 7.5297 9.57116 7.9376 9.97906C8.34551 10.387 8.86521 10.6648 9.43098 10.7773C9.99676 10.8898 10.5832 10.8321 11.1162 10.6113C11.6491 10.3906 12.1046 10.0167 12.4251 9.53708C12.7456 9.05744 12.9167 8.49353 12.9167 7.91667C12.9167 7.14312 12.6094 6.40125 12.0624 5.85427C11.5154 5.30729 10.7735 5 10 5ZM10 9.16667C9.75277 9.16667 9.5111 9.09336 9.30553 8.956C9.09997 8.81865 8.93976 8.62343 8.84515 8.39502C8.75054 8.16661 8.72578 7.91528 8.77402 7.6728C8.82225 7.43033 8.9413 7.2076 9.11611 7.03278C9.29093 6.85797 9.51366 6.73892 9.75613 6.69068C9.99861 6.64245 10.2499 6.66721 10.4784 6.76182C10.7068 6.85643 10.902 7.01664 11.0393 7.2222C11.1767 7.42777 11.25 7.66944 11.25 7.91667C11.25 8.24819 11.1183 8.56613 10.8839 8.80055C10.6495 9.03497 10.3315 9.16667 10 9.16667Z" fill="#666666"/>
                    </svg>
                    @if($harvest->region!=null)
                    {{ ucwords(strtolower($harvest->region->vilage . ',' .  $harvest->region->district . ',' . $harvest->region->regency . ',' . $harvest->region->province)) }}
                    @endif
                </p>
                <ul class="page-specs">
                    <li>
                        <div class="page-spec-name">Added at</div>
                        <div class="page-spec-value">{{ $harvest->publish_date }}</div>
                    </li>
                    {{-- <li>
                        <div class="page-spec-name">Panen</div>
                        <div class="page-spec-value">{{ $harvest->unit->id }} {{ $harvest->unit->name }}</div>
                    </li> --}}
                    <li>
                        <div class="page-spec-name">Luas Tanah</div>
                        <div class="page-spec-value">
                            @if(isset($harvest->land->large))
                            {{ $harvest->land->large }} {{ $harvest->land->unit_area }}<sup>2</sup>
                            @endif
                        </div>
                    </li>
                    <li>
                        <div class="page-spec-name">Perkiraan Pendapatan</div>
                        <div class="page-spec-value">Rp {{ $harvest->estimated_income }}/{{ $harvest->unit->name }}</div>
                    </li>
                </ul>
                <ul class="nav nav-tabs mt-4">
                    <li class="nav-item"><a href="#tab-desc" data-toggle="tab" class="nav-link active">Deskripsi</a></li>
                    <li class="nav-item"><a href="#tab-seller" data-toggle="tab" class="nav-link ">Diunggah Oleh</a></li>
                    <li class="nav-item"><a href="#tab-disc" data-toggle="tab" class="nav-link ">Komentar</a></li>
                </ul>
                <div class="tab-content mt-3">
                    <div id="tab-desc" class="tab-pane desc active">
                        <p>{!! $harvest->description !!}</p>
                    </div>
                    <div id="tab-seller" class="tab-pane">
                        <h4 class="mb-3 page-block-title text-primary">Diunggah Oleh</h4>
                        <div class="d-flex">
                            <div class="rounded shadow" style="background-image:url({{ $harvest->user->photo }});width:100px;height:100px;background-size:cover;background-position:center"></div>
                            <div class="ml-3">
                                <h6 class="mb-1 mt-2">{{ $harvest->user->name }}</h6>
                                <p class="text-muted mb-0">
                                    Kabupaten Bandung Barat
                                </p>
                                <p class="text-muted mt-2 mb-0">Member Since September 2019</p>
                            </div>
                        </div>
                        <ul class="icon-list mt-5">
                            <li><svg class="the-icon" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M19 4H5C4.20435 4 3.44129 4.31607 2.87868 4.87868C2.31607 5.44129 2 6.20435 2 7V17C2 17.7956 2.31607 18.5587 2.87868 19.1213C3.44129 19.6839 4.20435 20 5 20H19C19.7956 20 20.5587 19.6839 21.1213 19.1213C21.6839 18.5587 22 17.7956 22 17V7C22 6.20435 21.6839 5.44129 21.1213 4.87868C20.5587 4.31607 19.7956 4 19 4ZM18.33 6L12 10.75L5.67 6H18.33ZM19 18H5C4.73478 18 4.48043 17.8946 4.29289 17.7071C4.10536 17.5196 4 17.2652 4 17V7.25L11.4 12.8C11.5731 12.9298 11.7836 13 12 13C12.2164 13 12.4269 12.9298 12.6 12.8L20 7.25V17C20 17.2652 19.8946 17.5196 19.7071 17.7071C19.5196 17.8946 19.2652 18 19 18Z" fill="#666666"></path>
                            </svg>
                            <div class="the-label">nam@mail.com</div>
                        </li>
                        <li><svg class="the-icon" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M17.4 22C13.3173 21.9947 9.40331 20.3705 6.5164 17.4836C3.62949 14.5967 2.00529 10.6827 2 6.59999C2 5.38 2.48464 4.20997 3.34731 3.3473C4.20998 2.48463 5.38 1.99999 6.6 1.99999C6.85834 1.99802 7.11625 2.02147 7.37 2.06999C7.61531 2.10629 7.85647 2.16658 8.09 2.24999C8.25425 2.30762 8.40061 2.40713 8.51461 2.53866C8.62861 2.6702 8.7063 2.82922 8.74 2.99999L10.11 8.99999C10.1469 9.16286 10.1425 9.33237 10.0971 9.49308C10.0516 9.65378 9.96671 9.80055 9.85 9.91999C9.72 10.06 9.71 10.07 8.48 10.71C9.46499 12.8708 11.1932 14.6062 13.35 15.6C14 14.36 14.01 14.35 14.15 14.22C14.2694 14.1033 14.4162 14.0183 14.5769 13.9729C14.7376 13.9275 14.9071 13.9231 15.07 13.96L21.07 15.33C21.2353 15.3683 21.3881 15.4481 21.5141 15.5618C21.64 15.6756 21.735 15.8195 21.79 15.98C21.8744 16.2174 21.938 16.4616 21.98 16.71C22.0202 16.9613 22.0403 17.2155 22.04 17.47C22.0216 18.6848 21.5233 19.8429 20.654 20.6916C19.7847 21.5404 18.6149 22.0107 17.4 22ZM6.6 3.99999C5.91125 4.00263 5.25146 4.2774 4.76443 4.76442C4.27741 5.25145 4.00263 5.91124 4 6.59999C4.00265 10.1531 5.41528 13.5599 7.92769 16.0723C10.4401 18.5847 13.8469 19.9973 17.4 20C18.0888 19.9974 18.7485 19.7226 19.2356 19.2356C19.7226 18.7485 19.9974 18.0887 20 17.4V17.07L15.36 16L15.07 16.55C14.62 17.42 14.29 18.05 13.45 17.71C11.7929 17.1169 10.2887 16.162 9.04673 14.9149C7.80477 13.6677 6.85622 12.1596 6.27 10.5C5.91 9.71999 6.59 9.35999 7.45 8.90999L8 8.63999L6.93 3.99999H6.6Z" fill="#666666"></path>
                        </svg>
                        <div class="the-label">0181882981</div>
                    </li>
                    <li><svg class="the-icon" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M12 2C10.0222 2 8.08879 2.58649 6.44429 3.6853C4.7998 4.78412 3.51808 6.3459 2.7612 8.17317C2.00433 10.0004 1.80629 12.0111 2.19214 13.9509C2.578 15.8907 3.5304 17.6725 4.92893 19.0711C6.32745 20.4696 8.10928 21.422 10.0491 21.8079C11.9889 22.1937 13.9996 21.9957 15.8268 21.2388C17.6541 20.4819 19.2159 19.2002 20.3147 17.5557C21.4135 15.9112 22 13.9778 22 12C22 10.6868 21.7413 9.38642 21.2388 8.17317C20.7362 6.95991 19.9996 5.85752 19.0711 4.92893C18.1425 4.00035 17.0401 3.26375 15.8268 2.7612C14.6136 2.25866 13.3132 2 12 2ZM12 20C10.4177 20 8.87103 19.5308 7.55543 18.6518C6.23984 17.7727 5.21446 16.5233 4.60896 15.0615C4.00346 13.5997 3.84503 11.9911 4.15371 10.4393C4.4624 8.88743 5.22432 7.46197 6.34314 6.34315C7.46196 5.22433 8.88743 4.4624 10.4393 4.15372C11.9911 3.84504 13.5997 4.00346 15.0615 4.60896C16.5233 5.21447 17.7727 6.23984 18.6518 7.55544C19.5308 8.87103 20 10.4177 20 12C20 14.1217 19.1571 16.1566 17.6569 17.6569C16.1566 19.1571 14.1217 20 12 20Z" fill="#666666"></path>
                        <path d="M16 11H13V8C13 7.73478 12.8946 7.48043 12.7071 7.29289C12.5196 7.10536 12.2652 7 12 7C11.7348 7 11.4804 7.10536 11.2929 7.29289C11.1054 7.48043 11 7.73478 11 8V12C11 12.2652 11.1054 12.5196 11.2929 12.7071C11.4804 12.8946 11.7348 13 12 13H16C16.2652 13 16.5196 12.8946 16.7071 12.7071C16.8946 12.5196 17 12.2652 17 12C17 11.7348 16.8946 11.4804 16.7071 11.2929C16.5196 11.1054 16.2652 11 16 11Z" fill="#666666"></path>
                    </svg>
                    <div class="the-label">Online 3 Hours Ago</div>
                </li>
            </ul>
        </div>
        <div id="tab-disc" class="tab-pane">
            @if(session('message')!='')
            <br>
            <div class="alert alert-primary" role="alert">
                <b>Message : </b>{{ session('message')}}
            </div>
            @endif
            <form action="/postCommentHarvest/{{ $harvest->id }}" method="POST" class="mt-4 mb-4">
                @csrf
                <div class="form-group"><textarea class="form-control" placeholder="Tulis tanggapan di sini ..." style="height:100px" name="comment" id="comment" required=""></textarea></div>
                <input type="hidden" name="id" value="{{ $harvest->id }}">
                <input type="hidden" name="token" value="{{ session()->get('token') }}">
                <div class="text-right"><button class="btn btn-cta mt-2">Kirim</button></div>
            </form>
            <h4 class="page-block-title text-primary mb-3">{{ count($comments) }} Komentar Dalam Produk Ini</h4>
            <div class="list-border">
                            <!--
                                ==================================
                                HARVEST COMMENT
                                ==================================
                            -->
                            @foreach($comments as $harvestComment)
                            <div class="d-flex">

                                <div class="rounded shadow" style="background-image:url({{ $harvestComment->photo }});width:60px;height:60px;background-size:cover;background-position:center"></div>
                                <div class="ml-3 mt-2">
                                    <h6 class="mb-1">{{ $harvestComment->name }}</h6>
                                    <p class="mb-0 text-muted">{{ $harvestComment->created_at }}</p>
                                    <blockquote class="mt-3">{{ $harvestComment->comment }}</blockquote>
                                </div>
                            </div>
                            @endforeach
                            <!--
                                ==================================
                                END HARVEST COMMENT
                                ==================================
                            -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="card card-primary card-shadow">
                    <div class="card-body">
                        <h4 class="card-title mb-4">Info Penguggah</h4>

                        <div class="d-flex">
                            <div class="rounded shadow no-shrink" style="background-image:url({{ $harvest->user->photo }});width:50px;height:50px;background-size:cover;background-position:center"></div>
                            <div class="ml-3">
                                <h6 class="mb-1 font-weight-bold">{{ $harvest->user->name }}</h6>
                                <p class="text-primary mt-2 mb-0">Member Since September 2018</p>
                            </div>
                        </div>
                        <ul class="icon-list bordered mt-5">
                            <li class="px-2"><svg class="the-icon" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M19 4H5C4.20435 4 3.44129 4.31607 2.87868 4.87868C2.31607 5.44129 2 6.20435 2 7V17C2 17.7956 2.31607 18.5587 2.87868 19.1213C3.44129 19.6839 4.20435 20 5 20H19C19.7956 20 20.5587 19.6839 21.1213 19.1213C21.6839 18.5587 22 17.7956 22 17V7C22 6.20435 21.6839 5.44129 21.1213 4.87868C20.5587 4.31607 19.7956 4 19 4ZM18.33 6L12 10.75L5.67 6H18.33ZM19 18H5C4.73478 18 4.48043 17.8946 4.29289 17.7071C4.10536 17.5196 4 17.2652 4 17V7.25L11.4 12.8C11.5731 12.9298 11.7836 13 12 13C12.2164 13 12.4269 12.9298 12.6 12.8L20 7.25V17C20 17.2652 19.8946 17.5196 19.7071 17.7071C19.5196 17.8946 19.2652 18 19 18Z" fill="#666666"></path>
                                </svg>
                                <div class="the-label">ahmad@albar.com</div>
                            </li>
                            <li class="px-2">
                                <svg class="the-icon" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M10 1.66666C8.24338 1.66657 6.55772 2.35979 5.30937 3.59565C4.06103 4.83151 3.3509 6.51013 3.33334 8.26666C3.33334 12.8333 9.20834 17.9167 9.45834 18.1333C9.60928 18.2624 9.80137 18.3334 10 18.3334C10.1986 18.3334 10.3907 18.2624 10.5417 18.1333C10.8333 17.9167 16.6667 12.8333 16.6667 8.26666C16.6491 6.51013 15.939 4.83151 14.6906 3.59565C13.4423 2.35979 11.7566 1.66657 10 1.66666ZM10 16.375C8.60834 15.05 5 11.375 5 8.26666C5 6.94057 5.52679 5.6688 6.46447 4.73112C7.40215 3.79344 8.67392 3.26666 10 3.26666C11.3261 3.26666 12.5979 3.79344 13.5355 4.73112C14.4732 5.6688 15 6.94057 15 8.26666C15 11.35 11.3917 15.05 10 16.375Z" fill="#666666"/>
                                    <path d="M10 5C9.42313 5 8.85923 5.17106 8.37958 5.49155C7.89994 5.81203 7.5261 6.26756 7.30535 6.80051C7.08459 7.33346 7.02683 7.9199 7.13937 8.48568C7.25191 9.05146 7.5297 9.57116 7.9376 9.97906C8.34551 10.387 8.86521 10.6648 9.43098 10.7773C9.99676 10.8898 10.5832 10.8321 11.1162 10.6113C11.6491 10.3906 12.1046 10.0167 12.4251 9.53708C12.7456 9.05744 12.9167 8.49353 12.9167 7.91667C12.9167 7.14312 12.6094 6.40125 12.0624 5.85427C11.5154 5.30729 10.7735 5 10 5ZM10 9.16667C9.75277 9.16667 9.5111 9.09336 9.30553 8.956C9.09997 8.81865 8.93976 8.62343 8.84515 8.39502C8.75054 8.16661 8.72578 7.91528 8.77402 7.6728C8.82225 7.43033 8.9413 7.2076 9.11611 7.03278C9.29093 6.85797 9.51366 6.73892 9.75613 6.69068C9.99861 6.64245 10.2499 6.66721 10.4784 6.76182C10.7068 6.85643 10.902 7.01664 11.0393 7.2222C11.1767 7.42777 11.25 7.66944 11.25 7.91667C11.25 8.24819 11.1183 8.56613 10.8839 8.80055C10.6495 9.03497 10.3315 9.16667 10 9.16667Z" fill="#666666"/>
                                </svg>
                                <div class="the-label">{{ $harvest->user->region->regency_name ?? '-' }}</div>
                            </li>
                            <li class="px-2"><svg class="the-icon" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M17.4 22C13.3173 21.9947 9.40331 20.3705 6.5164 17.4836C3.62949 14.5967 2.00529 10.6827 2 6.59999C2 5.38 2.48464 4.20997 3.34731 3.3473C4.20998 2.48463 5.38 1.99999 6.6 1.99999C6.85834 1.99802 7.11625 2.02147 7.37 2.06999C7.61531 2.10629 7.85647 2.16658 8.09 2.24999C8.25425 2.30762 8.40061 2.40713 8.51461 2.53866C8.62861 2.6702 8.7063 2.82922 8.74 2.99999L10.11 8.99999C10.1469 9.16286 10.1425 9.33237 10.0971 9.49308C10.0516 9.65378 9.96671 9.80055 9.85 9.91999C9.72 10.06 9.71 10.07 8.48 10.71C9.46499 12.8708 11.1932 14.6062 13.35 15.6C14 14.36 14.01 14.35 14.15 14.22C14.2694 14.1033 14.4162 14.0183 14.5769 13.9729C14.7376 13.9275 14.9071 13.9231 15.07 13.96L21.07 15.33C21.2353 15.3683 21.3881 15.4481 21.5141 15.5618C21.64 15.6756 21.735 15.8195 21.79 15.98C21.8744 16.2174 21.938 16.4616 21.98 16.71C22.0202 16.9613 22.0403 17.2155 22.04 17.47C22.0216 18.6848 21.5233 19.8429 20.654 20.6916C19.7847 21.5404 18.6149 22.0107 17.4 22ZM6.6 3.99999C5.91125 4.00263 5.25146 4.2774 4.76443 4.76442C4.27741 5.25145 4.00263 5.91124 4 6.59999C4.00265 10.1531 5.41528 13.5599 7.92769 16.0723C10.4401 18.5847 13.8469 19.9973 17.4 20C18.0888 19.9974 18.7485 19.7226 19.2356 19.2356C19.7226 18.7485 19.9974 18.0887 20 17.4V17.07L15.36 16L15.07 16.55C14.62 17.42 14.29 18.05 13.45 17.71C11.7929 17.1169 10.2887 16.162 9.04673 14.9149C7.80477 13.6677 6.85622 12.1596 6.27 10.5C5.91 9.71999 6.59 9.35999 7.45 8.90999L8 8.63999L6.93 3.99999H6.6Z" fill="#666666"></path>
                                </svg>
                                <div class="the-label">+62128742983</div>
                            </li>
                            <li class="px-2"><svg class="the-icon" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M12 2C10.0222 2 8.08879 2.58649 6.44429 3.6853C4.7998 4.78412 3.51808 6.3459 2.7612 8.17317C2.00433 10.0004 1.80629 12.0111 2.19214 13.9509C2.578 15.8907 3.5304 17.6725 4.92893 19.0711C6.32745 20.4696 8.10928 21.422 10.0491 21.8079C11.9889 22.1937 13.9996 21.9957 15.8268 21.2388C17.6541 20.4819 19.2159 19.2002 20.3147 17.5557C21.4135 15.9112 22 13.9778 22 12C22 10.6868 21.7413 9.38642 21.2388 8.17317C20.7362 6.95991 19.9996 5.85752 19.0711 4.92893C18.1425 4.00035 17.0401 3.26375 15.8268 2.7612C14.6136 2.25866 13.3132 2 12 2ZM12 20C10.4177 20 8.87103 19.5308 7.55543 18.6518C6.23984 17.7727 5.21446 16.5233 4.60896 15.0615C4.00346 13.5997 3.84503 11.9911 4.15371 10.4393C4.4624 8.88743 5.22432 7.46197 6.34314 6.34315C7.46196 5.22433 8.88743 4.4624 10.4393 4.15372C11.9911 3.84504 13.5997 4.00346 15.0615 4.60896C16.5233 5.21447 17.7727 6.23984 18.6518 7.55544C19.5308 8.87103 20 10.4177 20 12C20 14.1217 19.1571 16.1566 17.6569 17.6569C16.1566 19.1571 14.1217 20 12 20Z" fill="#666666"></path>
                                <path d="M16 11H13V8C13 7.73478 12.8946 7.48043 12.7071 7.29289C12.5196 7.10536 12.2652 7 12 7C11.7348 7 11.4804 7.10536 11.2929 7.29289C11.1054 7.48043 11 7.73478 11 8V12C11 12.2652 11.1054 12.5196 11.2929 12.7071C11.4804 12.8946 11.7348 13 12 13H16C16.2652 13 16.5196 12.8946 16.7071 12.7071C16.8946 12.5196 17 12.2652 17 12C17 11.7348 16.8946 11.4804 16.7071 11.2929C16.5196 11.1054 16.2652 11 16 11Z" fill="#666666"></path>
                                </svg>
                                <div class="the-label">Login 3 jam yang lalu</div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <h2 class="section-title-md mt-5">Panen Lainnya</h2>
        <div class="row mt-4">
            <!--
                ==================================
                RELATED HARVEST
                ==================================
            -->
            @foreach($relateds as $related)
            <div class="col-lg-3">
                <div class="card card-shadow ">
                    <div class="card-image" style="background-image:url({{ $related->back_images[0]->image_1 }});background-position:center;background-size:cover;height:150px;">
                    </div>
                    <div class="card-body">
                        <h6 class="card-title mb-2"><a href="{{ url('panen', $related->slug)}}.html">{{ $related->title }}</a></h6>
                        <p class="d-flex align-items-center time">
                            <div class="card-category"><a href="#">{{ $related->category->name }}</a>
                            </div>
                        </p>
                        <div class="card-author">
                            <div class="card-author-image" style="background-image:url({{ $related->user->photo }});background-position:center;background-size:cover"></div>
                            <div class="card-author-detail">
                                <div class="card-author-name">{{ $related->user->name }}</div>
                                <div class="card-author-desc">
                                    {{-- @if($related->region!=null)
                                    {{ $related->user->region->regency_name }}, {{ $related->user->region->province_name }}
                                    @endif --}}
                                    Kabupaten Bandung Barat
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <!--
                ==================================
                END RELATED HARVEST
                ==================================
            -->
        </div>
    </div>
</section>
@endsection

@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chocolat/0.4.21/css/chocolat.min.css">
@endpush

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/chocolat/0.4.21/js/jquery.chocolat.min.js"></script>
<script>
    $('.chocolat-parent').Chocolat();
    $(".carousel-preview").slick({
        slidesToShow: 1,
        slidesToScroll: 1
    });

    $(".carousel-preview").on('afterChange', function(slick, {currentSlide}) {
        $(".preview-thumbs .preview-thumb").removeClass('active');
        $(".preview-thumbs .preview-thumb").eq(currentSlide).addClass('active');
    });

    $(".preview-thumbs .preview-thumb").click(function(){
        const index = $(this).index();
        $(".carousel-preview").slick('slickGoTo', index);
    })
</script>
@endpush
