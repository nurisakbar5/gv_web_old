@extends('frontend.layout')

@section('title') Login @endsection

@section('content')
	<div id="__next">
		<section class="section">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-5">
						@if(session('message')!='')
						<div class="alert alert-danger" role="alert">
							{{ session('message')}}
						</div>
						@endif
						<div class="card fat card-primary">
							<div class="card-body">
								<h1 class="card-title">Login</h1>
								{{ Form::open(['url'=>'/goLogin','class'=>'needs-validation','novalidate'=>''])}}
								
								{{Form::hidden('next', request()->next)}}
								<div class="form-group">
									<label for="email">E-Mail Address</label>
									<input id="email" type="email" class="form-control{{ $errors->first('email') ? ' is-invalid' : '' }}" name="email" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										{{ $errors->first('email') }}
									</div>
								</div>

								<div class="form-group">
									<label for="password" class="d-flex">Password
										<a href="forgot.html" class="ml-auto">
											Forgot Password?
										</a>
									</label>
									<input id="password" type="password" class="form-control{{ $errors->first('password') ? ' is-invalid' : '' }}" name="password" tabindex="2" required data-eye>
									<div class="invalid-feedback">
										{{ $errors->first('password') }}
									</div>
								</div>

								<div class="form-group">
									<div class="custom-checkbox custom-control">
										<input type="checkbox" name="remember" id="remember" class="custom-control-input">
										<label for="remember" class="custom-control-label">Remember Me</label>
									</div>
								</div>

								<div class="form-group m-0">
									<button type="submit" class="btn btn-cta btn-block">
										Login
									</button>
								</div>
								<div class="mt-4 text-center">
									Tidak punya akun? <a href="/register.html">Buat Sekarang</a>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection