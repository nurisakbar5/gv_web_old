@extends('frontend.layout') 
@section('title', 'Jelajahi Potensi Desa Indonesia') 
@section('content')    
<div class="card-image-featured need-overlay-gradient" style="background-image:url(https://images.unsplash.com/photo-1523760957528-55d1d540360d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80)">
    <div class="need-overlay-layer d-flex px-5 py-4 flex-column justify-content-end h-100">
        <h1 class="page-title text-white">Desa Cilame</h1>
        <p class="d-flex align-items-center text-white text-opacity">
            <svg class="mr-1" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M10 1.66666C8.24338 1.66657 6.55772 2.35979 5.30937 3.59565C4.06103 4.83151 3.3509 6.51013 3.33334 8.26666C3.33334 12.8333 9.20834 17.9167 9.45834 18.1333C9.60928 18.2624 9.80137 18.3334 10 18.3334C10.1986 18.3334 10.3907 18.2624 10.5417 18.1333C10.8333 17.9167 16.6667 12.8333 16.6667 8.26666C16.6491 6.51013 15.939 4.83151 14.6906 3.59565C13.4423 2.35979 11.7566 1.66657 10 1.66666ZM10 16.375C8.60834 15.05 5 11.375 5 8.26666C5 6.94057 5.52679 5.6688 6.46447 4.73112C7.40215 3.79344 8.67392 3.26666 10 3.26666C11.3261 3.26666 12.5979 3.79344 13.5355 4.73112C14.4732 5.6688 15 6.94057 15 8.26666C15 11.35 11.3917 15.05 10 16.375Z" fill="#666666"/>
            <path d="M10 5C9.42313 5 8.85923 5.17106 8.37958 5.49155C7.89994 5.81203 7.5261 6.26756 7.30535 6.80051C7.08459 7.33346 7.02683 7.9199 7.13937 8.48568C7.25191 9.05146 7.5297 9.57116 7.9376 9.97906C8.34551 10.387 8.86521 10.6648 9.43098 10.7773C9.99676 10.8898 10.5832 10.8321 11.1162 10.6113C11.6491 10.3906 12.1046 10.0167 12.4251 9.53708C12.7456 9.05744 12.9167 8.49353 12.9167 7.91667C12.9167 7.14312 12.6094 6.40125 12.0624 5.85427C11.5154 5.30729 10.7735 5 10 5ZM10 9.16667C9.75277 9.16667 9.5111 9.09336 9.30553 8.956C9.09997 8.81865 8.93976 8.62343 8.84515 8.39502C8.75054 8.16661 8.72578 7.91528 8.77402 7.6728C8.82225 7.43033 8.9413 7.2076 9.11611 7.03278C9.29093 6.85797 9.51366 6.73892 9.75613 6.69068C9.99861 6.64245 10.2499 6.66721 10.4784 6.76182C10.7068 6.85643 10.902 7.01664 11.0393 7.2222C11.1767 7.42777 11.25 7.66944 11.25 7.91667C11.25 8.24819 11.1183 8.56613 10.8839 8.80055C10.6495 9.03497 10.3315 9.16667 10 9.16667Z" fill="#666666"/>
            </svg>
            Ngamprah, Bandung Barat
        </p>
    </div>
</div>
<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <h2 class="section-title-md">Info Desa</h2>
                <div class="desc">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
                </div>

                <h2 class="section-title-md mt-5">Galeri</h2>
                <div class="d-flex" id="gallery">
                    <a href="https://images.unsplash.com/photo-1508325739122-c57a76313bf4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1190&q=80" class="preview-thumb" data-index="1">
                        <div class="rounded-lg" style="background-image:url(https://images.unsplash.com/photo-1508325739122-c57a76313bf4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1190&q=80);width:70px;height:70px;background-size:cover;background-position:center"></div>
                    </a>
                    <a href="https://images.unsplash.com/photo-1434393493133-00c371247786?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80" class="preview-thumb" data-index="2">
                        <div class="rounded-lg" style="background-image:url(https://images.unsplash.com/photo-1434393493133-00c371247786?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80);width:70px;height:70px;background-size:cover;background-position:center"></div>
                    </a>
                    <a href="https://images.unsplash.com/photo-1444214058525-761aeb793113?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1189&q=80" class="preview-thumb" data-index="1">
                        <div class="rounded-lg" style="background-image:url(https://images.unsplash.com/photo-1444214058525-761aeb793113?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1189&q=80);width:70px;height:70px;background-size:cover;background-position:center"></div>
                    </a>
                    <a href="https://images.unsplash.com/photo-1473951574080-01fe45ec8643?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1079&q=80" class="preview-thumb" data-index="1">
                        <div class="rounded-lg" style="background-image:url(https://images.unsplash.com/photo-1473951574080-01fe45ec8643?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1079&q=80);width:70px;height:70px;background-size:cover;background-position:center"></div>
                    </a>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card card-shadow p-2 card-texture">
                            <div class="card-body p-4 text-center">
                                <svg class="card-icon" width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g opacity="0.9">
                                <path d="M24.9998 4.16663C20.6082 4.16641 16.3941 5.89946 13.2732 8.98911C10.1524 12.0788 8.37705 16.2753 8.33313 20.6666C8.33313 32.0833 23.0206 44.7916 23.6456 45.3333C24.023 45.6561 24.5032 45.8334 24.9998 45.8334C25.4964 45.8334 25.9766 45.6561 26.354 45.3333C27.0831 44.7916 41.6665 32.0833 41.6665 20.6666C41.6225 16.2753 39.8472 12.0788 36.7264 8.98911C33.6055 5.89946 29.3913 4.16641 24.9998 4.16663ZM24.9998 40.9375C21.5206 37.625 12.4998 28.4375 12.4998 20.6666C12.4998 17.3514 13.8168 14.172 16.161 11.8278C18.5052 9.48359 21.6846 8.16663 24.9998 8.16663C28.315 8.16663 31.4944 9.48359 33.8386 11.8278C36.1828 14.172 37.4998 17.3514 37.4998 20.6666C37.4998 28.375 28.479 37.625 24.9998 40.9375Z" fill="#5FAD40"/>
                                <path d="M24.9998 12.5C23.5576 12.5 22.1479 12.9276 20.9488 13.7289C19.7497 14.5301 18.8151 15.6689 18.2632 17.0013C17.7113 18.3336 17.5669 19.7998 17.8482 21.2142C18.1296 22.6286 18.8241 23.9279 19.8438 24.9477C20.8636 25.9674 22.1628 26.6619 23.5773 26.9432C24.9917 27.2246 26.4578 27.0802 27.7902 26.5283C29.1226 25.9764 30.2614 25.0418 31.0626 23.8427C31.8638 22.6436 32.2915 21.2338 32.2915 19.7917C32.2915 17.8578 31.5232 16.0031 30.1558 14.6357C28.7883 13.2682 26.9337 12.5 24.9998 12.5ZM24.9998 22.9167C24.3817 22.9167 23.7776 22.7334 23.2636 22.39C22.7497 22.0466 22.3492 21.5586 22.1127 20.9876C21.8762 20.4165 21.8143 19.7882 21.9348 19.182C22.0554 18.5758 22.3531 18.019 22.7901 17.582C23.2271 17.1449 23.784 16.8473 24.3901 16.7267C24.9963 16.6061 25.6247 16.668 26.1957 16.9045C26.7667 17.1411 27.2548 17.5416 27.5981 18.0555C27.9415 18.5694 28.1248 19.1736 28.1248 19.7917C28.1248 20.6205 27.7956 21.4153 27.2095 22.0014C26.6235 22.5874 25.8286 22.9167 24.9998 22.9167Z" fill="#5FAD40"/>
                                </g>
                                </svg>

                                <h5 class="mt-3 font-weight-bold">Info Lokasi</h5>					
                                <p class="text-muted">
                                    Kecamatan Ciwidey, Kabupaten Bandung, Provinsi Jawa Barat
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card card-shadow p-2 card-texture">
                            <div class="card-body p-4 text-center">
                                <svg class="card-icon" width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M36.25 45.8333C27.7444 45.8223 19.5903 42.4386 13.5759 36.4242C7.56145 30.4098 4.17771 22.2557 4.16669 13.75C4.16669 11.2084 5.17636 8.7708 6.97358 6.97357C8.7708 5.17635 11.2084 4.16668 13.75 4.16668C14.2882 4.16258 14.8255 4.21143 15.3542 4.31251C15.8653 4.38814 16.3677 4.51374 16.8542 4.68751C17.1964 4.80758 17.5013 5.01488 17.7388 5.28891C17.9763 5.56295 18.1381 5.89424 18.2084 6.25001L21.0625 18.75C21.1395 19.0893 21.1302 19.4425 21.0356 19.7773C20.941 20.1121 20.764 20.4178 20.5209 20.6667C20.25 20.9583 20.2292 20.9792 17.6667 22.3125C19.7188 26.8143 23.3192 30.4295 27.8125 32.5C29.1667 29.9167 29.1875 29.8958 29.4792 29.625C29.728 29.3819 30.0338 29.2049 30.3686 29.1103C30.7034 29.0157 31.0566 29.0064 31.3959 29.0833L43.8959 31.9375C44.2402 32.0174 44.5586 32.1836 44.821 32.4205C45.0834 32.6575 45.2813 32.9572 45.3959 33.2917C45.5717 33.7862 45.7041 34.295 45.7917 34.8125C45.8755 35.3361 45.9173 35.8656 45.9167 36.3958C45.8783 38.9266 44.8402 41.3395 43.0291 43.1076C41.218 44.8758 38.781 45.8556 36.25 45.8333V45.8333ZM13.75 8.33335C12.3151 8.33883 10.9406 8.91128 9.92592 9.92591C8.91129 10.9405 8.33884 12.3151 8.33335 13.75C8.33887 21.1523 11.2819 28.2498 16.516 33.484C21.7502 38.7182 28.8478 41.6612 36.25 41.6667C37.6849 41.6612 39.0595 41.0887 40.0741 40.0741C41.0888 39.0595 41.6612 37.6849 41.6667 36.25V35.5625L32 33.3333L31.3959 34.4792C30.4584 36.2917 29.7709 37.6042 28.0209 36.8958C24.5685 35.6601 21.4348 33.6709 18.8474 31.0727C16.26 28.4744 14.2838 25.3325 13.0625 21.875C12.3125 20.25 13.7292 19.5 15.5209 18.5625L16.6667 18L14.4375 8.33335H13.75Z" fill="#6BB54E"/>
                                </svg>

                                <h5 class="mt-3 font-weight-bold">Info Kontak</h5>					
                                <p class="text-muted">
                                    022-12341234<br>
                                    023-124123 <br>
                                    john@doe.com
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 mt-4">
                        <div class="card card-shadow p-2 card-texture">
                            <div class="card-body p-4 text-center">
                                <svg class="card-icon" width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M42.5208 12.2708L34.1875 8.52081H32.4792L25 11.875L17.5208 8.52081H17.4167L17.1667 8.33331H15.9167L7.58333 12.0833C7.16768 12.2426 6.81397 12.5308 6.57403 12.9057C6.33408 13.2806 6.22053 13.7225 6.24999 14.1666V39.5833C6.25058 39.9319 6.33861 40.2748 6.50604 40.5805C6.67347 40.8862 6.91494 41.1451 7.20833 41.3333C7.54365 41.5499 7.93414 41.6656 8.33333 41.6666C8.62811 41.6653 8.91924 41.6014 9.18749 41.4791L16.6667 38.125L24.1458 41.4791H24.25C24.4848 41.5889 24.7408 41.6458 25 41.6458C25.2592 41.6458 25.5152 41.5889 25.75 41.4791H25.8542L33.3333 38.125L40.8125 41.4791C41.0807 41.6014 41.3719 41.6653 41.6667 41.6666C42.0659 41.6656 42.4563 41.5499 42.7917 41.3333C43.0851 41.1451 43.3265 40.8862 43.4939 40.5805C43.6614 40.2748 43.7494 39.9319 43.75 39.5833V14.1666C43.7492 13.7659 43.6328 13.3739 43.4147 13.0376C43.1967 12.7014 42.8863 12.4351 42.5208 12.2708ZM10.4167 15.5L14.5833 13.6458V34.4791L10.4167 36.3333V15.5ZM18.75 13.6458L22.9167 15.5V36.3333L18.75 34.4791V13.6458ZM27.0833 15.5L31.25 13.6458V34.4791L27.0833 36.3333V15.5ZM39.5833 36.3333L35.4167 34.4791V13.6458L39.5833 15.5V36.3333Z" fill="#5FAD40"/>
                                </svg>

                                <h5 class="mt-3 font-weight-bold">Luas Wilayah</h5>
                                <h4 class="text-primary font-weight-bold">809.736</h4>
                                <p>
                                    Hektar
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 mt-4">
                        <div class="card card-shadow p-2 card-texture">
                            <div class="card-body p-4 text-center">
                                <svg class="card-icon" width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M18.75 22.9167C20.3982 22.9167 22.0094 22.4279 23.3798 21.5122C24.7502 20.5966 25.8183 19.2951 26.449 17.7724C27.0797 16.2496 27.2448 14.5741 26.9232 12.9576C26.6017 11.3411 25.808 9.85622 24.6426 8.69078C23.4771 7.52534 21.9923 6.73167 20.3758 6.41013C18.7593 6.08858 17.0837 6.25361 15.561 6.88434C14.0383 7.51507 12.7368 8.58317 11.8211 9.95358C10.9054 11.324 10.4167 12.9352 10.4167 14.5833C10.4167 16.7935 11.2947 18.9131 12.8575 20.4759C14.4203 22.0387 16.5399 22.9167 18.75 22.9167ZM18.75 10.4167C19.5741 10.4167 20.3797 10.661 21.0649 11.1189C21.7501 11.5767 22.2842 12.2275 22.5995 12.9888C22.9149 13.7502 22.9974 14.588 22.8366 15.3962C22.6759 16.2045 22.279 16.9469 21.6963 17.5296C21.1136 18.1123 20.3712 18.5092 19.5629 18.6699C18.7546 18.8307 17.9169 18.7482 17.1555 18.4328C16.3941 18.1175 15.7434 17.5834 15.2856 16.8982C14.8277 16.213 14.5834 15.4074 14.5834 14.5833C14.5834 13.4783 15.0223 12.4185 15.8037 11.6371C16.5851 10.8557 17.645 10.4167 18.75 10.4167Z" fill="#5FAD40"/>
                                <path d="M35.4167 27.0833C36.6528 27.0833 37.8612 26.7168 38.889 26.03C39.9168 25.3432 40.7179 24.3671 41.1909 23.2251C41.664 22.083 41.7878 20.8264 41.5466 19.614C41.3054 18.4016 40.7102 17.288 39.8361 16.4139C38.962 15.5398 37.8484 14.9446 36.636 14.7034C35.4236 14.4623 34.167 14.586 33.0249 15.0591C31.8829 15.5321 30.9068 16.3332 30.22 17.361C29.5332 18.3888 29.1667 19.5972 29.1667 20.8333C29.1667 22.4909 29.8252 24.0806 30.9973 25.2527C32.1694 26.4248 33.7591 27.0833 35.4167 27.0833ZM35.4167 18.75C35.8287 18.75 36.2315 18.8722 36.5741 19.1011C36.9167 19.33 37.1838 19.6554 37.3414 20.0361C37.4991 20.4167 37.5404 20.8356 37.46 21.2398C37.3796 21.6439 37.1812 22.0151 36.8898 22.3065C36.5985 22.5978 36.2273 22.7962 35.8231 22.8766C35.419 22.957 35.0001 22.9157 34.6194 22.7581C34.2388 22.6004 33.9134 22.3334 33.6845 21.9908C33.4555 21.6482 33.3334 21.2454 33.3334 20.8333C33.3334 20.2808 33.5528 19.7509 33.9435 19.3602C34.3342 18.9695 34.8642 18.75 35.4167 18.75Z" fill="#5FAD40"/>
                                <path d="M35.4167 29.1666C33.108 29.1692 30.8656 29.9387 29.0417 31.3541C27.0011 29.3215 24.4043 27.9387 21.5788 27.38C18.7533 26.8213 15.8257 27.1118 13.1651 28.2148C10.5045 29.3179 8.23005 31.184 6.62865 33.578C5.02725 35.9719 4.17058 38.7865 4.16669 41.6666C4.16669 42.2192 4.38618 42.7491 4.77688 43.1398C5.16758 43.5305 5.69749 43.75 6.25002 43.75C6.80255 43.75 7.33246 43.5305 7.72316 43.1398C8.11386 42.7491 8.33335 42.2192 8.33335 41.6666C8.33335 38.904 9.43082 36.2544 11.3843 34.3009C13.3378 32.3474 15.9873 31.25 18.75 31.25C21.5127 31.25 24.1622 32.3474 26.1157 34.3009C28.0692 36.2544 29.1667 38.904 29.1667 41.6666C29.1667 42.2192 29.3862 42.7491 29.7769 43.1398C30.1676 43.5305 30.6975 43.75 31.25 43.75C31.8026 43.75 32.3325 43.5305 32.7232 43.1398C33.1139 42.7491 33.3334 42.2192 33.3334 41.6666C33.3383 39.2259 32.7217 36.824 31.5417 34.6875C32.4624 33.9599 33.5693 33.5063 34.7357 33.3784C35.9021 33.2506 37.081 33.4537 38.1374 33.9645C39.1939 34.4753 40.0851 35.2732 40.7093 36.2669C41.3334 37.2606 41.6652 38.4099 41.6667 39.5833C41.6667 40.1358 41.8862 40.6657 42.2769 41.0564C42.6676 41.4471 43.1975 41.6666 43.75 41.6666C44.3026 41.6666 44.8325 41.4471 45.2232 41.0564C45.6139 40.6657 45.8334 40.1358 45.8334 39.5833C45.8334 36.8206 44.7359 34.1711 42.7824 32.2176C40.8289 30.2641 38.1794 29.1666 35.4167 29.1666Z" fill="#5FAD40"/>
                                </svg>

                                <h5 class="mt-3 font-weight-bold">Penduduk</h5>
                                <h4 class="text-primary font-weight-bold">15.328</h4>
                                <p>
                                    Jiwa
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <h2 class="section-title-md mt-5">Potensi Pertanian</h2>
        <div class="row panen-carousel mt-4">
            <div class="col-lg-12">
                <div class="card card-shadow">
                    <div class="card-image" style="background-image:url(https://images.unsplash.com/photo-1543362906-acfc16c67564?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=701&q=80);background-position:center;background-size:cover;height:250px"></div>
                    <div class="card-body text-center">
                        <h6 class="card-title mb-0">Sayuran Hijau</h6>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card card-shadow">
                    <div class="card-image" style="background-image:url(https://images.unsplash.com/photo-1554579384-2d90b36072eb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=633&q=80);background-position:center;background-size:cover;height:250px"></div>
                    <div class="card-body text-center">
                        <h6 class="card-title mb-0">Strawberry</h6>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card card-shadow">
                    <div class="card-image" style="background-image:url(https://images.unsplash.com/photo-1487943155999-d5534d14a384?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1051&q=80);background-position:center;background-size:cover;height:250px"></div>
                    <div class="card-body text-center">
                        <h6 class="card-title mb-0">Padi</h6>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card card-shadow">
                    <div class="card-image" style="background-image:url(https://images.unsplash.com/photo-1523191742994-272374196a36?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=633&q=80);background-position:center;background-size:cover;height:250px"></div>
                    <div class="card-body text-center">
                        <h6 class="card-title mb-0">Jeruk</h6>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card card-shadow">
                    <div class="card-image" style="background-image:url(https://images.unsplash.com/photo-1487943155999-d5534d14a384?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1051&q=80);background-position:center;background-size:cover;height:250px"></div>
                    <div class="card-body text-center">
                        <h6 class="card-title mb-0">Padi</h6>
                    </div>
                </div>
            </div>
        </div>

        <h2 class="section-title-md mt-5">Potensi Peternakan</h2>
        <div class="row panen-carousel mt-4">
            <div class="col-lg-12">
                <div class="card card-shadow">
                    <div class="card-image" style="background-image:url(https://images.unsplash.com/photo-1545407263-7ff5aa2ad921?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=675&q=80);background-position:center;background-size:cover;height:250px"></div>
                    <div class="card-body text-center">
                        <h6 class="card-title mb-0">Sapi</h6>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card card-shadow">
                    <div class="card-image" style="background-image:url(https://images.unsplash.com/photo-1548550023-2bdb3c5beed7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80);background-position:center;background-size:cover;height:250px"></div>
                    <div class="card-body text-center">
                        <h6 class="card-title mb-0">Ayam</h6>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card card-shadow">
                    <div class="card-image" style="background-image:url(https://images.unsplash.com/photo-1456434893711-6a909e9cd81d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1051&q=80);background-position:center;background-size:cover;height:250px"></div>
                    <div class="card-body text-center">
                        <h6 class="card-title mb-0">Domba</h6>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card card-shadow">
                    <div class="card-image" style="background-image:url(https://images.unsplash.com/photo-1549476986-950ff4fa6ad2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80);background-position:center;background-size:cover;height:250px"></div>
                    <div class="card-body text-center">
                        <h6 class="card-title mb-0">Bebek</h6>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card card-shadow">
                    <div class="card-image" style="background-image:url(https://images.unsplash.com/photo-1456434893711-6a909e9cd81d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1051&q=80);background-position:center;background-size:cover;height:250px"></div>
                    <div class="card-body text-center">
                        <h6 class="card-title mb-0">Domba</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script src="{{ asset('template/front/js/lightgallery.min.js') }}"></script>
<script>
$(document).ready(function() {
    $(".panen-carousel").slick({
        slidesToShow: 4,
        slidesToScroll: 1
    });

    lightGallery(document.getElementById('gallery'));
});
</script>
@endpush