@extends('frontend.layout')
@section('title', 'Desa')
@section('content')
<section class="section">
    <div class="container">
        <div class="row mt-4">
            <div class="col-lg-8">
            <h2 class="section-title-md mb-4">Info Desa</h2>
                <div class="row">
                    @foreach($villages as $village)
                    <div class="col-lg-4">
                        <div class="card card-shadow ">
                            <div class="card-image" style="background-image:url(https://images.unsplash.com/photo-1523760957528-55d1d540360d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80);background-position:center;background-size:cover;height:150px">
                            </div>
                            <div class="card-body">
                                <h6 class="card-title mb-2"><a href="{{ url('desa/detail') }}.html"> {{ $village->village_name}}</a></h6>
                                <p class="d-flex align-items-center time">
                                    <svg class="mr-1" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M10 1.66666C8.24338 1.66657 6.55772 2.35979 5.30937 3.59565C4.06103 4.83151 3.3509 6.51013 3.33334 8.26666C3.33334 12.8333 9.20834 17.9167 9.45834 18.1333C9.60928 18.2624 9.80137 18.3334 10 18.3334C10.1986 18.3334 10.3907 18.2624 10.5417 18.1333C10.8333 17.9167 16.6667 12.8333 16.6667 8.26666C16.6491 6.51013 15.939 4.83151 14.6906 3.59565C13.4423 2.35979 11.7566 1.66657 10 1.66666ZM10 16.375C8.60834 15.05 5 11.375 5 8.26666C5 6.94057 5.52679 5.6688 6.46447 4.73112C7.40215 3.79344 8.67392 3.26666 10 3.26666C11.3261 3.26666 12.5979 3.79344 13.5355 4.73112C14.4732 5.6688 15 6.94057 15 8.26666C15 11.35 11.3917 15.05 10 16.375Z" fill="#666666"/>
                                    <path d="M10 5C9.42313 5 8.85923 5.17106 8.37958 5.49155C7.89994 5.81203 7.5261 6.26756 7.30535 6.80051C7.08459 7.33346 7.02683 7.9199 7.13937 8.48568C7.25191 9.05146 7.5297 9.57116 7.9376 9.97906C8.34551 10.387 8.86521 10.6648 9.43098 10.7773C9.99676 10.8898 10.5832 10.8321 11.1162 10.6113C11.6491 10.3906 12.1046 10.0167 12.4251 9.53708C12.7456 9.05744 12.9167 8.49353 12.9167 7.91667C12.9167 7.14312 12.6094 6.40125 12.0624 5.85427C11.5154 5.30729 10.7735 5 10 5ZM10 9.16667C9.75277 9.16667 9.5111 9.09336 9.30553 8.956C9.09997 8.81865 8.93976 8.62343 8.84515 8.39502C8.75054 8.16661 8.72578 7.91528 8.77402 7.6728C8.82225 7.43033 8.9413 7.2076 9.11611 7.03278C9.29093 6.85797 9.51366 6.73892 9.75613 6.69068C9.99861 6.64245 10.2499 6.66721 10.4784 6.76182C10.7068 6.85643 10.902 7.01664 11.0393 7.2222C11.1767 7.42777 11.25 7.66944 11.25 7.91667C11.25 8.24819 11.1183 8.56613 10.8839 8.80055C10.6495 9.03497 10.3315 9.16667 10 9.16667Z" fill="#666666"/>
                                    </svg>
                                    {{ $village->regency_name}}, {{ $village->province_name}}
                                </p>
                                <div class="text-uppercase text-sm font-weight-bold tracking mt-4 text-muted">Potensi</div>
                                <ul class="tags tags-potensi">
                                    <li><a href="#">Padi</a></li>
                                    <li><a href="#">Strawberry</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endforeach
                 </div>
            </div>
            <div class="col-lg-4">
                <h2 class="section-title-sm mb-3 mt-4">Lokasi</h2>
                <select class="form-control mb-3" style="padding: 0 !important;">
                    @foreach ($provinces->data as $province)
                    <option>{{ $province->name }}</option>
                    @endforeach
                </select>
                <select class="form-control mb-3" style="padding: 0 !important;">
                    <option>Bogor</option>
                </select>
                <select class="form-control mb-3" style="padding: 0 !important;">
                    <option>Bogor Barat</option>
                </select>
                <button class="btn btn-block btn-normal btn-cta">Filter</button>

                <hr>

                <h2 class="section-title-sm mb-3 mt-4">Artikel Populer</h2>
                <div class="list-posts mt-4">
                    @foreach ($populars as $popular)
                    <div class="post">
                        <div class="post-image" style="background-image:url({{ $popular->image_url }});width:60px;height:60px;background-size:cover;background-position:center"></div>
                        <div class="post-detail">
                            <h4 class="post-title"><a href="{{ url('artikel', $popular->slug) }}.html">{{ $popular->title }}</a></h4>
                            <div class="post-metas">
                                <div><a href="#">{{ $popular->category->name }}</a></div>
                                <div>•</div>
                                <div class="normal">{{ $popular->publish_date }}</div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>

                <hr>
                <div class="w-100 bg-primary rounded mt-4" style="height: 200px;background-image: url('https://via.placeholder.com/350x200?text=Ads+Here');background-position: center;"></div>

            </div>
        </div>
    </div>
</section>
@endsection
