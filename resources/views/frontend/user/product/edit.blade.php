@extends('frontend.layout')
@section('title', 'Edit Produk')
@section('content')
    <section class="section">
        <div class="container">
            <h1 class="section-title-md mb-4">Edit Produk</h1>
            {{ Form::model($products,['url'=>'edit-produk/'.$products->slug,'files'=>true])}}
                {{-- @include('validation') --}}
                @include('frontend.user.product.form')
            {{ Form::close()}}
        </div>
    </section>
@endsection