@extends('frontend.layout')
@section('title', 'Tambah Produk')

@section('content')
    <section class="section">
        <div class="container">
            <h1 class="section-title-md mb-4">Tambah Produk</h1>
            {{ Form::open(['url'=>'addProductToUser','files'=>true])}}
                {{-- @include('validation') --}}
                @include('frontend.user.product.form')
            {{ Form::close()}}
        </div>
    </section>
    <div class="modal fade" id="addEtalaseModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="frmAddEtalase">
                        <div class="modal-header">
                            <h4 class="modal-title">Tambah Etalase Baru</h4>
                            <button aria-hidden="true" class="close" data-dismiss="modal" type="button">x</button>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-danger" id="ad-error-bag">
                                <ul id="ad-task-errors"></ul>
                            </div>
                            <div class="form-group">
                                <label for="etalase">Nama Etalase</label>
                                <input type="text" name="store_id" id="store_id" value="{{ $profile->data->store_id }}">
                                <input type="text" class="form-control" id="name" name="name">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="button" value="Cancel" class="btn btn-warning" data-dismiss="modal">
                            <button type="submit" class="btn btn-info" id="btn-add" value="Add">Tambah Etalase</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@endsection
@push('scripts')
<script>
        $('select[name=etalase_id]').change(function(){
            if ($(this).val() == '')
            {
                $("#ad-error-bag").hide();
                $("#addEtalaseModal").modal('show');
            }
        });
        </script>
        <script>
        $(document).ready(function(){
            $("#btn-add").change(function(){
                $.ajaxSetup({
                    headers:{
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: '/etalaseAjax',
                    data:{
                        store_id: $("#frmAddEtalase input[name=store_id]").val(),
                        name: $("#frmAddEtalase input[name=name]").val(),
                    },
                    dataType: 'json',
                    success: function(data){
                        var newThing = document.getElementById("name");
                        var newValue = $('option', this).length;
                        $('#frmAddEtalase').trigger("reset");
                        $("#frmAddEtalase .close").click();
                        $("#addEtalaseModal").modal('hide');
                        window.location.reload();
                    },
                    error: function (data) {
                        var errors = data.responseText;
                        $('#ad-task-errors').html('');
                        $.each(errors.messages, function(key, value){
                            $('#ad-task-errors').append('<li>' + value + '</li>');
                        });
                        $('#ad-error-bag').show();
                    }
                });
            });
        });
        </script>
@endpush
