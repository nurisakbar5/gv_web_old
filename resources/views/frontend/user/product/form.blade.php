<?php
$btnLabel = Request::segment(2) == 'tambah-produk.html' ? 'Tambah Produk' : 'Update';
?>

<input type="hidden" name="token" value="{{ session()->get('token') }}">

<div class="card card-shadow card-primary">
    <div class="card-body">
        <h4 class="section-title-sm mb-0">Informasi Umum</h4>
        <p class="text-muted mb-5">Seperti nama produk, kategori dan harga.</p>

        <div class="form-group row {{ $errors->first('name') ? "is-invalid": ""}}">
            <label class="col-lg-3 {{ $errors->first('name') ? "text-danger": ""}}">Nama Produk</label>
            <div class="col-lg-9">
                {{ Form::text('name',null,['class'=>'form-control ' . ($errors->first('name') ? 'is-invalid': ''), 'placeholder'=>'Nama Produk'])}}
                <span class="text-danger">
                    {{ $errors->first('name') }}
                </span>
            </div>
        </div>

        <div class="form-group row {{ $errors->first('category_id') ? "is-invalid": ""}}">
            <label class="col-lg-3 {{ $errors->first('category_id') ? "text-danger": ""}}">Kategori Produk</label>
            <div class="col-lg-9">
                <select style="padding: 0 !important;" name="category_id" class="form-control {{ $errors->first('category_id') ? 'is-invalid': '' }}">
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}"
                        @if(isset($products))
                        {{ $category->id == $products->category->id ? 'selected' : '' }}
                        @endif>{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <span class="text-danger">
                {{ $errors->first('category_id') }}
            </span>
        </div>

        <div class="form-group row {{ $errors->first('etalase_id') ? "is-invalid": ""}}">
                <label class="col-lg-3 {{ $errors->first('etalase_id') ? "text-danger": ""}}">Pilih Etalase</label>
                <div class="col-lg-9">
                    <select style="padding: 0 !important;" name="etalase_id" class="etalase form-control {{ $errors->first('etalase_id') ? 'is-invalid': '' }}">
                        @foreach($etalases->data as $item)
                            <option value="{{ $item->id }}"
                            @if(isset($products))
                            {{ $item->id == $products->etalase->id ? 'selected' : '' }}
                            @endif>{{ $item->name }}</option>
                        @endforeach
                        {{-- <option value="">Tambah Etalase</option> --}}
                    </select>
                </div>
                <span class="text-danger">
                    {{ $errors->first('etalase_id') }}
                </span>
            </div>

        <div class="form-group row {{ $errors->first('price') ? "is-invalid": ""}}">
            <label class="col-lg-3 {{ $errors->first('price') ? "text-danger": ""}}">Harga</label>
            <div class="col-lg-4">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Rp.</span>
                    </div>
                    {{ Form::number('price',null,['class'=>'form-control ' . ($errors->first('price') ? 'is-invalid': ''),'placeholder'=>'Mis. 100000'])}}
                </div>
                <span class="text-danger">
                    {{ $errors->first('price') }}
                </span>
            </div>
        </div>

        <div class="form-group row {{ $errors->first('weight') ? "is-invalid": ""}}">
            <label class="col-lg-3 {{ $errors->first('weight') ? "text-danger": ""}}">Berat</label>
            <div class="col-lg-4">
                <div class="input-group">
                 {{ Form::number('weight',null,['class'=>'form-control ' . ($errors->first('weight') ? 'is-invalid': ''),'placeholder'=>'Mis. 10'])}}
                    <div class="input-group-append">
                        <select class="input-group-text" name="unit_id">
                            @foreach($units as $unit)
                            <option value="{{ $unit->id }}"
                                @if(isset($products))
                                {{ $unit->id == $products->unit->id ? 'selected' : '' }}
                                @endif
                                >{{ $unit->name }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <span class="text-danger">
                    {{ $errors->first('weight') }}
                </span>
            </div>
        </div>

        <div class="form-group row {{ $errors->first('stock') ? "is-invalid": ""}}">
            <label class="col-lg-3 {{ $errors->first('stock') ? "text-danger": ""}}">Stok</label>
            <div class="col-lg-4">
                {{ Form::number('stock',null,['class'=>'form-control ' . ($errors->first('stock') ? 'is-invalid': ''),'placeholder'=>'Mis. 150'])}}
                <span class="text-danger">
                    {{ $errors->first('stock') }}
                </span>
            </div>
        </div>
    </div>
</div>

<div class="card card-shadow card-primary mt-5">
    <div class="card-body">
        <h4 class="section-title-sm mb-0">Deskripsi Produk</h4>
        <p class="text-muted mb-5">Jelaskan secara rinci produk yang hendak Anda jual.</p>

        <div class="form-group row">
            <label class="col-lg-3">Keterangan produk</label>
            <div class="col-lg-9">
                {{ Form::textarea('description',null,['class'=>'form-control description'])}}
                <div class=" {{ $errors->first('description') ? "alert alert-danger": ""}}">
                    {{ $errors->first('description') }}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card card-shadow card-primary mt-5">
    <div class="card-body">
        <h4 class="section-title-sm mb-0">Sertakan Gambar</h4>
        <p class="text-muted mb-5">Sertakan empat gambar terkait produk Anda.</p>

        <div class="row">
            @foreach([1,2,3,4] as $i => $r)
            <div class="col-lg-3">
                <label id="preview-image-{{$r}}" for="input-image-{{$r}}" style="display:flex;justify-content: center;align-items: center;font-size: 16px;width: 100%;height: 230px;background-color: #f9f9f9;border: 2px dashed #ddd;border-radius: 3px;cursor: pointer;{{ isset($products->front_images[$i]) ? 'background-image: url('. $products->front_images[$i] .')' : ''}}">
                    @if ($r==1)
                        Pilih Gambar
                        @if ( Request::segment(2) == 'tambah-produk.html')
                            (Wajib)
                        @else

                        @endif
                    @elseif($r==2)
                        Pilih Gambar
                        @if ( Request::segment(2) == 'tambah-produk.html')
                            (Wajib)
                        @else

                        @endif
                    @elseif($r==3)
                        Pilih Gambar
                        @if ( Request::segment(2) == 'tambah-produk.html')
                            (Wajib)
                        @else

                        @endif
                    @else
                        Pilih Gambar
                        @if ( Request::segment(2) == 'tambah-produk.html')
                            (Tidak Wajib)
                        @else

                        @endif
                    @endif
                </label>
                <input type="file" name="image_{{$r}}" id="input-image-{{$r}}" class="d-none image-upload" data-preview="#preview-image-{{$r}}">

                <span class="text-danger">
                    {{ $errors->first('image_' . $r) }}
                </span>
            </div>
            @endforeach
        </div>
    </div>
</div>

{{-- <div class="row">
    <div class="col-lg-4">
        <h4 class="section-title-sm">Sertakan Gambar</h4>
        <p class="text-muted">Sertakan tiga gambar terkait produk Anda.</p>
    </div>
    <div class="col-lg-8">
        <div class="form-group">
            <div class="col-sm-12">
                <div class="custom-file-container" data-upload-id="myUniqueUploadId">
                    <label class="{{ $errors->first('image_1') ? "text-danger": ""}}">Gambar Produk (1) <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">&times;</a></label>
                    <br>
                    <span class="text-danger">
                            {{ $errors->first('image_1') }}
                        </span>
                    <label class="custom-file-container__custom-file" >
                        <input type="file" name='image_1' class="custom-file-container__custom-file__custom-file-input {{ $errors->first('image_1') ? 'is-invalid': '' }}" accept="*" aria-label="Choose File">
                        <input type="hidden" name="MAX_FILE_SIZE" value="10485760" />
                        <span class="custom-file-container__custom-file__custom-file-control"></span>
                    </label>
                    <div class="custom-file-container__image-preview"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4"></div>
    <div class="col-lg-8">
        <div class="form-group">
            <div class="col-sm-12">
                <div class="custom-file-container" data-upload-id="myUniqueUploadId2">
                    <label class="{{ $errors->first('image_2') ? "text-danger": ""}}">Gambar Produk (2) <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">&times;</a></label>
                    <br>
                    <span class="text-danger">
                            {{ $errors->first('image_2') }}
                        </span>
                    <label class="custom-file-container__custom-file" >
                        <input type="file" name='image_2' class="custom-file-container__custom-file__custom-file-input {{ $errors->first('image_2') ? 'is-invalid': '' }}" accept="*" aria-label="Choose File">
                        <input type="hidden" name="MAX_FILE_SIZE" value="10485760" />
                        <span class="custom-file-container__custom-file__custom-file-control"></span>
                    </label>
                    <div class="custom-file-container__image-preview"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4"></div>
    <div class="col-lg-8">
        <div class="form-group">
            <div class="col-sm-12">
                <div class="custom-file-container" data-upload-id="myUniqueUploadId3">
                    <label class="{{ $errors->first('image_3') ? "text-danger": ""}}">Gambar Produk (3) <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">&times;</a></label>
                    <br>
                    <span class="text-danger">
                            {{ $errors->first('image_3') }}
                        </span>
                    <label class="custom-file-container__custom-file" >
                        <input type="file" name='image_3' class="custom-file-container__custom-file__custom-file-input {{ $errors->first('image_3') ? 'is-invalid': '' }}" accept="*" aria-label="Choose File">
                        <input type="hidden" name="MAX_FILE_SIZE" value="10485760" />
                        <span class="custom-file-container__custom-file__custom-file-control"></span>
                    </label>
                    <div class="custom-file-container__image-preview"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4"></div>
    <div class="col-lg-8">
        <div class="form-group">
            <div class="col-sm-12">
                <div class="custom-file-container" data-upload-id="myUniqueUploadId4">
                    <label class="{{ $errors->first('image_4') ? "text-danger": ""}}">Gambar Produk (4) <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">&times;</a></label>
                    <br>
                    <span class="text-danger">
                            {{ $errors->first('image_4') }}
                        </span>
                    <label class="custom-file-container__custom-file" >
                        <input type="file" name='image_4' class="custom-file-container__custom-file__custom-file-input {{ $errors->first('image_3') ? 'is-invalid': '' }}" accept="*" aria-label="Choose File">
                        <input type="hidden" name="MAX_FILE_SIZE" value="10485760" />
                        <span class="custom-file-container__custom-file__custom-file-control"></span>
                    </label>
                    <div class="custom-file-container__image-preview"></div>
                </div>
            </div>
        </div>
    </div> --}}
    {{-- @if(Request::segment(2)!='tambah-produk.html')
    <div class="col-lg-4">
        <h4 class="section-title-sm">Old Cover</h4>
    </div>
    <div class="col-lg-8">
        <div class="form-group">
            <div class="col-sm-12">


                @foreach($products->front_images as $image)
                <img src="{{ $image }}" class="img-thumbnail" width="230px" height="300px">
                @endforeach

            </div>
        </div>
    </div>
    @endif --}}

<div class="form-group text-right mt-5">
    <button class="btn btn-cta" type="submit"><i class="fas fa-save"></i> {{ $btnLabel }}</button>
    <a href="/user-produk.html" class="btn btn-outline-secondary"><i class="fas fa-backward"></i> Back</a>
</div>



@push('scripts')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
<script src="https://unpkg.com/file-upload-with-preview@4.0.2/dist/file-upload-with-preview.min.js"></script>
<script>
    CKEDITOR.replace( 'description' );

    function readURL(input, element) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $(element).css({
            'background-image': 'url(' + e.target.result + ')',
            'background-position': 'center',
            'background-size': 'cover',
        });
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    $(".image-upload").change(function() {
        console.log('a')
      readURL(this, $(this).data('preview'));
    });
</script>
@endpush

@push('css')
<link rel="stylesheet" href="https://unpkg.com/file-upload-with-preview@4.0.2/dist/file-upload-with-preview.min.css">
@endpush
