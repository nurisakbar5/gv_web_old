<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" class="next-head">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('template/front/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/front/css/style.css?v=' . time()) }}">
    <title>GlobalVillage Indonesia | Verifikasi Akun</title>
</head>
<body>

    <div id="__next">
        <div class="hero">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-12">
                        <div class="hero-text">
                            <div class="text-badge">
                                <p style="text-align:center;">Akun Anda Telah Terverifikasi!</p>
                                <h4 class="title" style="text-align:center;">
                                    Selamat! kini akun anda telah aktif.
                                </h4>
                                <p style="text-align:center;">
                                    <a href="/login.html" target="_blank" class="btn btn-cta mt-4 hero-cta text-center">Go To Website</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('template/front/js/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('template/front/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('template/front/js/custom.js') }}"></script>
</body>
</html>
