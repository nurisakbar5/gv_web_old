@extends('frontend.layout')
@section('title', 'Edit Panen')
@section('content')
    <section class="section">
        <div class="container">
            <h1 class="section-title-md mb-4">Edit Panen</h1>
            {{ Form::open(['url'=> '/updatePanenUser/'.$panen->id,'files'=>true,'method'=>'PUT'])}}
                {{--@include('validation')--}}
                @include('frontend.user.panen.form')
            {{ Form::close() }}              
        </div>
    </section>
@endsection