<?php
    $btnLabel = Request::segment(2) == 'tambah-panen.html' ? 'Tambah Panen' : 'Update Panen';
?>

<input type="hidden" name="token" value="{{ session()->get('token') }}">

<div class="card card-shadow card-primary">
    <div class="card-body">
        <h4 class="section-title-sm mb-0">Informasi Umum</h4>
        <p class="text-muted mb-5">Seperti judul, lahan, kategori dan lainnya.</p>

        <div class="form-group row">
            <label class="col-lg-3">Lahan</label>
            <div class="col-lg-9">
                @if (isset($lands->data)==null)
                    <input type="text" class="form-control" value="Anda Belum Memiliki Lahan, Input Lahan Dahulu" disabled>
                @else
                <select name="land_id" class="form-control">
                    @foreach($lands->data as $land)
                        <option value="{{ $land->id }}"
                        @if(isset($panen))
                            {{ $land->id==$panen->land->id?'selected':'' }}
                        @endif
                        >{{ $land->name }}</option>
                    @endforeach
                @endif
                </select>
                <span class="text-danger">
                    {{ $errors->first('land_id') }}
                </span>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-lg-3">Judul</label>
            <div class="col-lg-9">
                <input type="text" class="form-control" placeholder='Judul' name="title" value="{{ $panen->title ?? '' }}" required>
                <span class="text-danger">
                    {{ $errors->first('title') }}
                </span>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-lg-3">Estimasi Pendapatan</label>

            <div class="col-lg-3">
                <div class="input-group">
                    <input type="number" class="form-control" placeholder="Mis. 10" name="estimated_income" value="{{ $panen->estimated_income ?? '' }}" required>
                    <span class="text-danger">
                        {{ $errors->first('estimated_income') }}
                    </span>
                    <div class="input-group-append">
                        <select name="unit_id" class="form-control">
                        @foreach($units->data as $unit)
                            <option value="{{ $unit->id }}"
                            @if(isset($panen))
                                {{ $unit->id==$panen->unit->id?'selected':'' }}
                            @endif
                            >{{ $unit->name }}</option>
                        @endforeach
                        </select>
                        <span class="text-danger">
                            {{ $errors->first('unit_id') }}
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-lg-3">Estimasi Tanggal Panen</label>
            <div class="col-lg-3">
                <input type="text" class="form-control datepicker" placeholder="Pilih tanggal" name="estimated_date" id="estimated_date" value="{{ $panen->estimated_date ?? '' }}" required>
                <span class="text-danger">
                    {{ $errors->first('estimated_date') }}
                </span>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-lg-3">Kategori</label>
            <div class="col-lg-5">
                <select name="category_id" class="form-control">
                @foreach($categories as $category)
                    <option value="{{ $category->id }}"
                    @if(isset($panen))
                        {{ $category->id==$panen->category->id?'selected':'' }}
                    @endif
                    >{{ $category->name }}</option>
                @endforeach
                </select>
                <span class="text-danger">
                    {{ $errors->first('category_id') }}
                </span>
            </div>
        </div>
    </div>
</div>

<div class="card card-shadow card-primary mt-5">
    <div class="card-body">
        <h4 class="section-title-sm mb-0">Deskripsi Panen</h4>
        <p class="text-muted mb-5">Jelaskan secara rinci panen yang hendak Anda publikasikan.</p>

        <div class="form-group row">
            <label class="col-lg-3">Keterangan</label>
            <!--<input type="text" name="description" class="form-control" placeholder="Deskripsi">-->
            <div class="col-lg-9">
                <textarea name="description" class="form-control" required>{{ $panen->description ?? '' }}</textarea>
            </div>
        </div>
    </div>
</div>

<div class="card card-shadow card-primary mt-5">
    <div class="card-body">
        <h4 class="section-title-sm mb-0">Sertakan Gambar</h4>
        <p class="text-muted mb-5">Sertakan empat gambar terkait panen Anda.</p>

        <div class="row">
            @foreach([1,2,3,4] as $i => $r)
            <div class="col-lg-3">
                <label id="preview-image-{{$r}}" for="input-image-{{$r}}" style="display:flex;justify-content: center;align-items: center;font-size: 16px;width: 100%;height: 230px;background-color: #f9f9f9;border: 2px dashed #ddd;border-radius: 3px;cursor: pointer;{{ isset($panen->front_images[$i]) ? 'background-image: url('. $panen->front_images[$i] .')' : ''}}">
                    @if ($r==1)
                        Pilih Gambar
                        @if ( Request::segment(2) == 'tambah-panen.html')
                            (Wajib)
                        @else

                        @endif
                    @elseif($r==2)
                        Pilih Gambar
                        @if ( Request::segment(2) == 'tambah-panen.html')
                            (Wajib)
                        @else

                        @endif
                    @else
                        Pilih Gambar
                        @if ( Request::segment(2) == 'tambah-panen.html')
                            (Tidak Wajib)
                        @else

                        @endif
                    @endif
                </label>
                <input type="file" name="image_{{$r}}" id="input-image-{{$r}}" class="d-none image-upload" data-preview="#preview-image-{{$r}}">

                <span class="text-danger">
                    {{ $errors->first('image_' . $r) }}
                </span>
            </div>
            @endforeach
        </div>
    </div>
</div>

<div class="form-group text-right mt-5">
    <button class="btn btn-cta" type="submit"><i class="fas fa-save"></i> {{ $btnLabel }}</button>
    <a href="/user-panen.html" class="btn btn-outline-secondary"><i class="fas fa-backward"></i> Back</a>
</div>

@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" href="https://unpkg.com/file-upload-with-preview@4.0.2/dist/file-upload-with-preview.min.css">
@endpush

@push('scripts')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
<script src="https://unpkg.com/file-upload-with-preview@4.0.2/dist/file-upload-with-preview.min.js"></script>
<script src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
CKEDITOR.replace( 'description' );
$(document).ready(function(){
    $('.datepicker').daterangepicker({
        singleDatePicker: true,
        autoUpdateInput: false,
        locale: {
            format: 'YYYY-MM-DD'
    }
    }, function(e, d) {
        $(this.element).val(e.format('YYYY-MM-DD'));
    });
});

    function readURL(input, element) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $(element).css({
            'background-image': 'url(' + e.target.result + ')',
            'background-position': 'center',
            'background-size': 'cover',
        });
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    $(".image-upload").change(function() {
        console.log('a')
      readURL(this, $(this).data('preview'));
    });
</script>
@endpush
