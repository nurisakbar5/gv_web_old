@extends('frontend.layout')
@section('title', 'Tambah Panen')
@section('content')
    <section class="section">
        <div class="container">
            <h1 class="section-title-md mb-4">Tambah Panen</h1>
            {{ Form::open(['url'=>'/addPanenToUser','files'=>true])}}
                {{--@include('validation')--}}
                @include('frontend.user.panen.form')
            {{ Form::close() }}              
        </div>
    </section>
@endsection