@extends('frontend.user.profile.layout')
@section('title', 'Edit Password')
@section('content2')
        @include('alert')
        @include('validation')
        <form action="/update-profile-password" method="post" accept-charset="utf-8">
            @csrf
            <input type="hidden" name="token" id="token" value="{{ session()->get('token') }}">
            <div class="card card-shadow card-primary">
                <div class="card-body">
                    <h4 class="section-title-sm mb-0">Edit Password</h4>
                    <p class="text-muted mb-5">Edit password lama Anda, tidak memerlukan login kembali.</p>

                    <div class="form-group row">
                        <label class="col-lg-3">Password Lama</label>
                        <div class="col-lg-9">
                            <input type="password" name="old_password" id="old_password" class="form-control" placeholder="Masukkan Password Lama Anda" data-eye>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3">Password Baru</label>
                        <div class="col-lg-9">
                            <input type="password" name="new_password" id="new_password" class="form-control" placeholder="Masukkan Password Baru Anda" data-eye>
                        </div>
                    </div>
                    <div class="text-right mb-0 form-group">
                        <button type="submit" class="btn btn-cta">Simpan Perubahan</button>
                    </div>
                </div>
            </div>
        </form>
@endsection
