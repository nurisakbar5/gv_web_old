@extends('frontend.layout')
@section('title','User Profile')

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('DataTables/datatables.min.css') }}">
<style>
    .image {
        position: relative;
    }

    .overlay-edit {
        transition: all .5s;
        display: flex;
        align-items: center;
        justify-content: center;
        position: absolute;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 100%;
        text-align: center;
        color: #fff;
        background-color: rgba(0,0,0,.3);
        border-radius: 50%;
        cursor: pointer;
    }
</style>
@endpush

@section('content')
<section class="section">
    <div class="container">
        @include('alert')
        <div class="row">
            <div class="col-lg-3">
                {{ Form::open(['url'=>'update-profile-image','files'=>true])}}
                <div class="card card-shadow card-primary">
                    <div class="card-body text-center">
                        <div class="rounded-circle d-inline-block image" style="background-image:url({{ $user->data->photo }});background-position:center;background-size:cover;width: 150px;height: 150px;">
                            <input type="hidden" value="{{ session()->get('token') }}" name="token">
                            {{ Form::file('image', ['style'=>'display:none;', 'id'=>'image','accept'=>'.jpg,.png'])}}
                            <label for="image" class="mb-0 overlay-edit" title="Pilih Gambar">Ubah Gambar</label>
                        </div>
                    </div>
                    <div class="px-5 pb-4 text-center">
                        <button class="btn btn-normal btn-cta btn-sm" disabled="" id="upload-btn" type="submit">Upload Foto</button>
                    </div>
                    <ul class="nav nav-pills bordered flex-column">
                        <li><a href="/user-profile.html" class="nav-link{{ request()->is('user-profile.html') ? ' active' : '' }}">
                            <svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g data-name="Layer 2"><g data-name="person"><rect width="24" height="24" opacity="0"/><path d="M12 11a4 4 0 1 0-4-4 4 4 0 0 0 4 4zm0-6a2 2 0 1 1-2 2 2 2 0 0 1 2-2z"/><path d="M12 13a7 7 0 0 0-7 7 1 1 0 0 0 2 0 5 5 0 0 1 10 0 1 1 0 0 0 2 0 7 7 0 0 0-7-7z"/></g></g></svg>
                            Profile
                        </a></li>
                        <li><a href="/show-user/{{ $user->data->id }}" class="nav-link{{ request()->is('show-user/*') ? ' active' : '' }}">
                            <svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g data-name="Layer 2"><g data-name="edit"><rect width="24" height="24" opacity="0"/><path d="M19.4 7.34L16.66 4.6A2 2 0 0 0 14 4.53l-9 9a2 2 0 0 0-.57 1.21L4 18.91a1 1 0 0 0 .29.8A1 1 0 0 0 5 20h.09l4.17-.38a2 2 0 0 0 1.21-.57l9-9a1.92 1.92 0 0 0-.07-2.71zM9.08 17.62l-3 .28.27-3L12 9.32l2.7 2.7zM16 10.68L13.32 8l1.95-2L18 8.73z"/></g></g></svg>
                            Ubah Profile
                        </a></li>
                        <li><a href="/edit-password" class="nav-link{{ request()->is('edit-password') ? ' active' : '' }}">
                            <svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g data-name="Layer 2"><g data-name="lock"><rect width="24" height="24" opacity="0"/><path d="M17 8h-1V6.11a4 4 0 1 0-8 0V8H7a3 3 0 0 0-3 3v8a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3v-8a3 3 0 0 0-3-3zm-7-1.89A2.06 2.06 0 0 1 12 4a2.06 2.06 0 0 1 2 2.11V8h-4zM18 19a1 1 0 0 1-1 1H7a1 1 0 0 1-1-1v-8a1 1 0 0 1 1-1h10a1 1 0 0 1 1 1z"/><path d="M12 12a3 3 0 1 0 3 3 3 3 0 0 0-3-3zm0 4a1 1 0 1 1 1-1 1 1 0 0 1-1 1z"/></g></g></svg>
                            Ubah Password
                        </a></li>
                        <li><a href="/edit-village" class="nav-link{{ request()->is('edit-village') ? ' active' : '' }}">
                            <svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g data-name="Layer 2"><g data-name="pin"><rect width="24" height="24" opacity="0"/><path d="M12 2a8 8 0 0 0-8 7.92c0 5.48 7.05 11.58 7.35 11.84a1 1 0 0 0 1.3 0C13 21.5 20 15.4 20 9.92A8 8 0 0 0 12 2zm0 17.65c-1.67-1.59-6-6-6-9.73a6 6 0 0 1 12 0c0 3.7-4.33 8.14-6 9.73z"/><path d="M12 6a3.5 3.5 0 1 0 3.5 3.5A3.5 3.5 0 0 0 12 6zm0 5a1.5 1.5 0 1 1 1.5-1.5A1.5 1.5 0 0 1 12 11z"/></g></g></svg>
                            Ubah Administratif
                        </a></li>
                        <li><a href="/store.html" class="nav-link{{ request()->is('store') ? ' active' : '' }}">
                            <svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g data-name="Layer 2"><g data-name="shopping-cart"><rect width="24" height="24" opacity="0"/><path d="M21.08 7a2 2 0 0 0-1.7-1H6.58L6 3.74A1 1 0 0 0 5 3H3a1 1 0 0 0 0 2h1.24L7 15.26A1 1 0 0 0 8 16h9a1 1 0 0 0 .89-.55l3.28-6.56A2 2 0 0 0 21.08 7zm-4.7 7H8.76L7.13 8h12.25z"/><circle cx="7.5" cy="19.5" r="1.5"/><circle cx="17.5" cy="19.5" r="1.5"/></g></g></svg>
                            Toko Saya
                        </a></li>
                    </ul>
                </div>
                {{ Form::close()}}
            </div>
            <div class="col-lg-9">
                @yield('content2')
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
    <script>
        $('#image').change(function() {
            let input = this;

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.image').css('background-image', 'url(' + e.target.result + ')');
                }

                reader.readAsDataURL(input.files[0]);

                $('#upload-btn').attr('disabled', false);
            }
        });
    </script>
@endpush
