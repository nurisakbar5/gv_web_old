@extends('frontend.user.profile.layout')
@section('title','Ubah Profile')
@section('content2')
        @include('alert')
        @include('validation')
        <form action="/update-profile" method="POST" accept-charset="utf-8">
            @csrf
            <input type="hidden" name="token" id="token" value="{{ session()->get('token') }}">
            <div class="card card-shadow card-primary">
                <div class="card-body">
                    <h4 class="section-title-sm mb-0">Ubah Profile</h4>
                    <p class="text-muted mb-5">Seperti nama, email, alamat dan lainnya.</p>

                    <div class="form-group row">
                        <label class="col-lg-3">Name</label>
                        <div class="col-lg-9">
                            <input type="text" name="name" id="name" class="form-control" value="{{ $profile->data->name }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3">Email</label>
                        <div class="col-lg-9">
                            <input type="email" name="email" id="email" class="form-control" value="{{ $profile->data->email }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3">No. Handphone</label>
                        <div class="col-lg-9">
                            <input type="number" name="phone" id="phone" class="form-control" value="{{ $profile->data->phone }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3">Alamat Tempat Tinggal</label>
                        <div class="col-lg-9">
                            <textarea name="address" id="address" class="form-control">{{ $profile->data->address }}</textarea>
                        </div>
                    </div>
                    <div class="text-right mb-0 form-group">
                        <button type="submit" class="btn btn-cta">Simpan Perubahan</button>
                    </div>
                </div>
            </div>
            <input type="hidden" name="village" id="village" class="form-control" value="{{ isset($profile->data->village->id) ?  $profile->data->village->id : "Anda Belum Memilih Desa"}}">

        </form>
@endsection
