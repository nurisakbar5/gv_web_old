@extends('frontend.user.profile.layout')
@section('title','Edit Desa')
@section('content2')
        @include('alert')
        @include('validation')
        <form action="/update-profile-village" method="post">
            @csrf
            <input type="hidden" name="token" id="token" value="{{ session()->get('token') }}">
            <div class="card card-shadow card-primary">
                <div class="card-body">
                    <h4 class="section-title-sm mb-0">Administratif</h4>
                    <p class="text-muted mb-5">Kami perlu mengetahui provinsi, kota, kecamatan dan desa.</p>

                    <div class="form-group row">
                        <label class="col-lg-3">Provinsi</label>
                        <div class="col-lg-9">
                            <select class="form-control mb-3 province">
                                @foreach(['Pilih Provinsi'] + $provinces->data as $province)
                                <option value="{{ $province->id ?? '' }}">{{ $province->name ?? $province }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3">Kota/Kabupaten</label>
                        <div class="col-lg-9">
                            <select name="regency" class="form-control mb-3 regency">
                                <option value="">Pilih provinsi dahulu</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-lg-3">Kecamatan</label>
                        <div class="col-lg-9">
                            <select name="district" class="form-control mb-3 district">
                                <option value="">Pilih kabupaten dahulu</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-lg-3">Desa</label>
                        <div class="col-lg-9">
                            <select name="village" class="form-control mb-3 village">
                                <option value="">Pilih kecamatan dahulu</option>
                            </select>
                        </div>
                    </div>
        
                    <div class="text-right form-group mb-0">
                        <button type="submit" class="btn btn-cta">Simpan Perubahan</button>
                    </div>
                </div>
            </div>

        </form>
@endsection
@push('scripts')
<script>
    $(document).ready(function (){
        $('.province').change(function() {
            const id = $(this).val();

            $.ajax({
                url: '/regency/' + id ,
                beforeSend: function(request) {
                    $('.regency option').remove();
                    $('.regency').append('<option value="">Memuat ...</option>');
                },
                success: function(data) {
                    let option = '<option value="{id}">{name}</option>';

                    $('.regency option').remove();
                    $('.regency').append('<option value="">Pilih Kota/Kabupaten</option>');
                    data.forEach(function(item) {
                        $('.regency').append(
                        option.replace(/{id}/g, item.id).replace(/{name}/g, item.name)
                        );
                    });
                },
                complete: function() {

                }
            });
        });

        $('.regency').change(function() {
            const id = $(this).val();

            $.ajax({
                url: '/district/' + id,
                beforeSend: function() {
                    $('.district option').remove();
                    $('.district').append('<option value="">Memuat ...</option>');
                },
                success: function(data) {
                    let option = '<option value="{id}">{name}</option>';

                    $('.district option').remove();
                    $('.district').append('<option value="">Pilih Kecamatan</option>');
                    data.forEach(function(item) {
                        $('.district').append(
                        option.replace(/{id}/g, item.id).replace(/{name}/g, item.name)
                        );
                    });
                },
                complete: function() {

                }
            });
        });

        $('.district').change(function() {
            const id = $(this).val();

            $.ajax({
                url: '/village/' + id,
                beforeSend: function() {
                    $('.village option').remove();
                    $('.village').append('<option value="">Memuat ...</option>');
                },
                success: function(data) {
                    let option = '<option value="{id}">{name}</option>';

                    $('.village option').remove();
                    $('.village').append('<option value="">Pilih Desa</option>');
                    data.forEach(function(item) {
                        $('.village').append(
                        option.replace(/{id}/g, item.village_id).replace(/{name}/g, item.village_name)
                        );
                    });
                },
                complete: function() {

                }
            });
        });
    });
</script>
@endpush
