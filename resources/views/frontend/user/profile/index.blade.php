@extends('frontend.user.profile.layout')

@section('content2')
                <div class="card card-shadow card-primary">
                    <div class="card-body">
                        <div class="d-flex align-items-center mb-4">
                            <h6 class="card-title mb-0">Biodata Diri</h6>
                            <div class="text-right ml-auto">
                                <a href="/edit-profile.html" class="btn btn-outline-primary btn-sm btn-xs">Ubah Profile</a>
                                <a href="{{ url('edit-password') }}" class="btn btn-outline-info btn-sm btn-xs">Ubah Password</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label>Nama Lengkap</label>
                            </div>
                            <div class="col-lg-8">
                                <p>: {{ $user->data->name }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label>Email</label>
                            </div>
                            <div class="col-lg-8">
                                <p>: {{ $user->data->email }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label>No. Handphone</label>
                            </div>
                            <div class="col-lg-8">
                                <p>: {{ $user->data->phone }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label>Alamat Tempat Tinggal</label>
                            </div>
                            <div class="col-lg-8">
                                <p>: {{ $user->data->address }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label>Password</label>
                            </div>
                            <div class="col-lg-8">
                                <p>: <i>&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;</i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-shadow card-primary mt-4">
                    <div class="card-body">
                        <div class="d-flex align-items-center mb-4">
                            <h6 class="card-title mb-0">Administratif</h6>
                            <div class="text-right ml-auto">
                                <a href="{{ url('/edit-village') }}" class="btn btn-outline-primary btn-sm btn-xs">Ubah Administratif</a>
                            </div>
                        </div>
                        @if (isset($user->data->region->village_id))
                        <div class="row">
                                <div class="col-lg-4">
                                    <label>Nama Provinsi</label>
                                </div>
                                <div class="col-lg-8">
                                    <p>: {{ $user->data->region->province_name }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <label>Nama Kota/Kab.</label>
                                </div>
                                <div class="col-lg-8">
                                    <p>: {{ $user->data->region->regency_name }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <label>Kecamatan</label>
                                </div>
                                <div class="col-lg-8">
                                    <p>: {{ $user->data->region->district_name }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <label>Desa</label>
                                </div>
                                <div class="col-lg-8">
                                    <p>: {{ $user->data->region->village_name }}</p>
                                </div>
                            </div>
                        @else
                        Anda Belum Memilih Asal Daerah Anda, Silahkan klik tombol 'UBAH ADMINISTRATIF'
                        @endif
                    </div>
                </div>
                <div class="card card-shadow card-primary mt-4">
                    <div class="card-body">
                        <div class="d-flex align-items-center mb-4">
                            <h6 class="card-title mb-0">Daftar Alamat</h6>
                            <div class="ml-auto">
                                <a href="{{ url('create-address') }}" class="btn btn-outline-primary btn-sm btn-xs" title="Tambah Alamat">Tambah Alamat</a>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Penerima</th>
                                    <th width="200">Alamat Pengiriman</th>
                                    <th>Daerah Pengiriman</th>
                                    <th width="150"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($addresses->data as $address)
                                <tr>
                                    <td>
                                        <b>{{ $address->receiver_name }}</b>
                                        <br>
                                        {{ $address->phone }}
                                    </td>
                                    <td>
                                        <b>{{ $address->name }}</b>
                                        <br>
                                        {{ $address->street }}
                                    </td>
                                    <td>
                                        @if (isset($address->region->village_id))
                                            {{ $address->region->village_name }},
                                            {{ $address->region->district_name }},
                                            {{ $address->region->regency_name }},
                                            {{ $address->region->province_name }}
                                        @else
                                            Belum Ada Daerah Pengiriman
                                        @endif

                                    </td>
                                    <td>
                                        <a href="{{ url('show-address/'.$address->id) }}" class="btn btn-outline-secondary btn-sm btn-block mb-2" title="Ubah Alamat">Ubah</a>
                                        {{ Form::open(['url'=>'delete-address/'.$address->id,'method'=>'delete', 'onsubmit'=>"return confirm('Yakin Ingin Menhapus Alamat Ini?')"])}}
                                        <button type="submit" class="btn btn-danger btn-sm btn-block" title="Hapus Alamat">Hapus</button>
                                        {{ Form::close()}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
@stop
