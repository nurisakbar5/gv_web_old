@extends('frontend.layout')
@section('title')Tambah Alamat Baru @endsection
@section('content')
<section class="section">
    <div class="container">
        <h1 class="section-title-md mb-4">Tambah Alamat Baru</h1>
        <div class="col-lg-12">
            @include('alert')
            @include('validation')
        </div>
        <form action="{{ url('store-address') }}" method="post">
            @csrf
            <div class="row mt-4">
                <div class="col-lg-4">
                    <h4 class="section-title-sm">Informasi Umum</h4>
                    <p class="text-muted">Seperti nama alamat, alamat lengkap dan lainnya.</p>
                </div>
                <div class="col-lg-8">
                    <input type="hidden" name="token" id="token" value="{{ session()->get('token') }}">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Nama Alamat</label>
                            <input type="text" name="name" id="name" class="form-control" placeholder="Contoh: Alamat Rumah">
                        </div>
                        <div class="form-group">
                            <label>Nama Penerima</label>
                            <input type="text" name="receiver_name" id="receiver_name" class="form-control" placeholder="Contoh: Agus Kurama">
                        </div>
                        <div class="form-group">
                            <label>No Handphone</label>
                            <input type="number" name="phone" id="phone" class="form-control" placeholder="Contoh: 0812627xxx">
                        </div>
                        <div class="form-group">
                            <label>Alamat Lengkap</label>
                            <input type="text" name="street" id="street" class="form-control" placeholder="Contoh: Jl. Sultan Alimuddin Blok A No 108">
                        </div>
                    </div>
                </div>
            </div>
            <hr class="mt-4 mb-5">
            <div class="row mt-4">
                <div class="col-lg-4">
                    <h4 class="section-title-sm">Administratif</h4>
                    <p class="text-muted">Kami perlu mengetahui provinsi, kota, kecamatan dan desa.</p>
                </div>
                <div class="col-lg-8">
                    <input type="hidden" name="token" id="token" value="{{ session()->get('token') }}">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <select class="form-control mb-3 province">
                                    <option value="" class="disabled">Pilih Provinsi</option>
                                    @foreach($provinces->data as $province)
                                    <option value="{{ $province->id ?? '' }}">{{ $province->name ?? $province }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <select name="regency" class="form-control mb-3 regency">
                                    <option value="">Pilih provinsi dahulu</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <select name="district" class="form-control mb-3 district">
                                    <option value="">Pilih kabupaten dahulu</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <select name="village_id" class="form-control mb-3 village">
                                    <option value="">Pilih kecamatan dahulu</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <hr class="mt-4 mb-5">
            <div class="text-right">
                <button type="submit" class="btn btn-cta">Simpan Perubahan</button>
                <a href="{{ url('user-profile.html') }}" class="btn btn-outline-secondary">Kembali</a>
            </div>
        </form>
    </div>
</section>
@endsection
@push('scripts')
<script>
    $(document).ready(function (){
        $('.province').change(function() {
            const id = $(this).val();

            $.ajax({
                url: '/regency/' + id ,
                beforeSend: function(request) {
                    $('.regency option').remove();
                    $('.regency').append('<option value="">Memuat ...</option>');
                },
                success: function(data) {
                    let option = '<option value="{id}">{name}</option>';

                    $('.regency option').remove();
                    $('.regency').append('<option value="">Pilih Kota/Kabupaten</option>');
                    data.forEach(function(item) {
                        $('.regency').append(
                        option.replace(/{id}/g, item.id).replace(/{name}/g, item.name)
                        );
                    });
                },
                complete: function() {

                }
            });
        });

        $('.regency').change(function() {
            const id = $(this).val();

            $.ajax({
                url: '/district/' + id,
                beforeSend: function() {
                    $('.district option').remove();
                    $('.district').append('<option value="">Memuat ...</option>');
                },
                success: function(data) {
                    let option = '<option value="{id}">{name}</option>';

                    $('.district option').remove();
                    $('.district').append('<option value="">Pilih Kecamatan</option>');
                    data.forEach(function(item) {
                        $('.district').append(
                        option.replace(/{id}/g, item.id).replace(/{name}/g, item.name)
                        );
                    });
                },
                complete: function() {

                }
            });
        });

        $('.district').change(function() {
            const id = $(this).val();

            $.ajax({
                url: '/village/' + id,
                beforeSend: function() {
                    $('.village option').remove();
                    $('.village').append('<option value="">Memuat ...</option>');
                },
                success: function(data) {
                    let option = '<option value="{id}">{name}</option>';

                    $('.village option').remove();
                    $('.village').append('<option value="">Pilih Desa</option>');
                    data.forEach(function(item) {
                        $('.village').append(
                        option.replace(/{id}/g, item.village_id).replace(/{name}/g, item.village_name)
                        );
                    });
                },
                complete: function() {

                }
            });
        });
    });
</script>
@endpush
