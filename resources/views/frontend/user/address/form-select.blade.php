<form action="{{ url('update-address/'.$address->data->id) }}" method="post">
            @csrf
            {{method_field('PUT')}}
            <div class="row mt-4">
                <div class="col-lg-4">
                    <h4 class="section-title-sm">Informasi Umum</h4>
                    <p class="text-muted">Seperti nama alamat, alamat lengkap dan lainnya.</p>
                </div>
                <div class="col-lg-8">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Nama Alamat</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ $address->data->name }}" placeholder="Contoh: Alamat Rumah">
                        </div>
                        <div class="form-group">
                            <label>Nama Penerima</label>
                        <input type="text" value="{{ $address->data->receiver_name }}" name="receiver_name" id="receiver_name" class="form-control" placeholder="Contoh: Agus Kurama">
                        </div>
                        <div class="form-group">
                            <label>No Handphone</label>
                            <input type="number" value="{{ $address->data->phone }}" name="phone" id="phone" class="form-control" placeholder="Contoh: 0812627xxx">
                        </div>
                        <div class="form-group">
                            <label>Alamat Lengkap</label>
                            <input type="text" name="street" value="{{ $address->data->street }}" id="street" class="form-control" placeholder="Contoh: Jl. Sultan Alimuddin Blok A No 108">
                        </div>
                    </div>
                </div>
            </div>
            <hr class="mt-4 mb-5">
            <div class="row mt-4">
                <div class="col-lg-4">
                    <h4 class="section-title-sm">Administratif</h4>
                    <p class="text-muted">Kami perlu mengetahui provinsi, kota, kecamatan dan desa.</p>
                </div>
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <select class="form-control mb-3 province">
                                    <option value="" class="disabled">Pilih Provinsi</option>
                                    @foreach($provinces->data as $province)
                                    <option value="{{ $province->id ?? '' }}">
                                        {{ $province->name ?? $province }}
                                    </option>
                                    @endforeach
                                </select>
                                {{-- <select class="form-control mb-3 province">
                                    @foreach(['Pilih Provinsi'] + $provinces->data as $province)
                                    <option value="{{ $province->id ?? '' }}"
                                        @if (isset($address))
                                            {{ $province->id == $address->data->region->province_id ? 'selected' : '' }}
                                        @endif>{{ $province->name ?? $province }}</option>
                                    @endforeach
                                </select> --}}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <select name="regency" class="form-control mb-3 regency">
                                    <option value="">Pilih provinsi dahulu</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <select name="district" class="form-control mb-3 district">
                                    <option value="">Pilih kabupaten dahulu</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <select name="village_id" class="form-control mb-3 village">
                                    <option value="">Pilih kecamatan dahulu</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <hr class="mt-4 mb-5">
            <div class="text-right">
                <button type="submit" class="btn btn-cta">Simpan Perubahan</button>
                <a href="{{ url('user-profile.html') }}" class="btn btn-outline-secondary">Kembali</a>
            </div>
        </form>
