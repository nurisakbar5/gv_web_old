@extends('frontend.layout')
@section('title')Edit Alamat @endsection
@section('content')
<section class="section">
    <div class="container">
        <h1 class="section-title-md mb-4">Edit Alamat</h1>
        <div class="col-lg-12">
            @include('alert')
            @include('validation')
        </div>
        @include('frontend.user.address.form-select')
    </div>
</section>
@endsection
@push('scripts')
<script>
    $(document).ready(function (){
        $('.province').change(function() {
            const id = $(this).val();

            $.ajax({
                url: '/regency/' + id ,
                beforeSend: function(request) {
                    $('.regency option').remove();
                    $('.regency').append('<option value="">Memuat ...</option>');
                },
                success: function(data) {
                    let option = '<option value="{id}">{name}</option>';

                    $('.regency option').remove();
                    $('.regency').append('<option value="">Pilih Kota/Kabupaten</option>');
                    data.forEach(function(item) {
                        $('.regency').append(
                        option.replace(/{id}/g, item.id).replace(/{name}/g, item.name)
                        );
                    });
                },
                complete: function() {

                }
            });
        });

        $('.regency').change(function() {
            const id = $(this).val();

            $.ajax({
                url: '/district/' + id,
                beforeSend: function() {
                    $('.district option').remove();
                    $('.district').append('<option value="">Memuat ...</option>');
                },
                success: function(data) {
                    let option = '<option value="{id}">{name}</option>';

                    $('.district option').remove();
                    $('.district').append('<option value="">Pilih Kecamatan</option>');
                    data.forEach(function(item) {
                        $('.district').append(
                        option.replace(/{id}/g, item.id).replace(/{name}/g, item.name)
                        );
                    });
                },
                complete: function() {

                }
            });
        });

        $('.district').change(function() {
            const id = $(this).val();

            $.ajax({
                url: '/village/' + id,
                beforeSend: function() {
                    $('.village option').remove();
                    $('.village').append('<option value="">Memuat ...</option>');
                },
                success: function(data) {
                    let option = '<option value="{id}">{name}</option>';

                    $('.village option').remove();
                    $('.village').append('<option value="">Pilih Desa</option>');
                    data.forEach(function(item) {
                        $('.village').append(
                        option.replace(/{id}/g, item.village_id).replace(/{name}/g, item.village_name)
                        );
                    });
                },
                complete: function() {

                }
            });
        });
    });
</script>
@endpush
