@extends('frontend.layout')

@section('title', 'Detail Pembelian')

@section('content')
<section class="section">
	<div class="container">
		<div class="row gutter-lg">
			<div class="col-lg-4">
				<div class="card card-shadow" style="border:none;">
					<div class="card-body text-center">
						<div class="rounded-circle d-inline-block" style="background-image:url({{{ isset($profile->data->photo) ? $profile->data->photo : 'http://ws2.globalvillage.co.id/img_no_images.png' }}});background-position:center;background-size:cover;width: 150px;height: 150px;"></div>
					</div>
					<div class="d-flex">
						<ul class="icon-list bordered mt-2 mx-auto justify-content-center">
							<li><svg class="the-icon" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M19 4H5C4.20435 4 3.44129 4.31607 2.87868 4.87868C2.31607 5.44129 2 6.20435 2 7V17C2 17.7956 2.31607 18.5587 2.87868 19.1213C3.44129 19.6839 4.20435 20 5 20H19C19.7956 20 20.5587 19.6839 21.1213 19.1213C21.6839 18.5587 22 17.7956 22 17V7C22 6.20435 21.6839 5.44129 21.1213 4.87868C20.5587 4.31607 19.7956 4 19 4ZM18.33 6L12 10.75L5.67 6H18.33ZM19 18H5C4.73478 18 4.48043 17.8946 4.29289 17.7071C4.10536 17.5196 4 17.2652 4 17V7.25L11.4 12.8C11.5731 12.9298 11.7836 13 12 13C12.2164 13 12.4269 12.9298 12.6 12.8L20 7.25V17C20 17.2652 19.8946 17.5196 19.7071 17.7071C19.5196 17.8946 19.2652 18 19 18Z" fill="#666666"></path>
							</svg>
							<div class="the-label">{{ $profile->data->email }}</div>
						</li>
						<li><svg class="the-icon" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M17.4 22C13.3173 21.9947 9.40331 20.3705 6.5164 17.4836C3.62949 14.5967 2.00529 10.6827 2 6.59999C2 5.38 2.48464 4.20997 3.34731 3.3473C4.20998 2.48463 5.38 1.99999 6.6 1.99999C6.85834 1.99802 7.11625 2.02147 7.37 2.06999C7.61531 2.10629 7.85647 2.16658 8.09 2.24999C8.25425 2.30762 8.40061 2.40713 8.51461 2.53866C8.62861 2.6702 8.7063 2.82922 8.74 2.99999L10.11 8.99999C10.1469 9.16286 10.1425 9.33237 10.0971 9.49308C10.0516 9.65378 9.96671 9.80055 9.85 9.91999C9.72 10.06 9.71 10.07 8.48 10.71C9.46499 12.8708 11.1932 14.6062 13.35 15.6C14 14.36 14.01 14.35 14.15 14.22C14.2694 14.1033 14.4162 14.0183 14.5769 13.9729C14.7376 13.9275 14.9071 13.9231 15.07 13.96L21.07 15.33C21.2353 15.3683 21.3881 15.4481 21.5141 15.5618C21.64 15.6756 21.735 15.8195 21.79 15.98C21.8744 16.2174 21.938 16.4616 21.98 16.71C22.0202 16.9613 22.0403 17.2155 22.04 17.47C22.0216 18.6848 21.5233 19.8429 20.654 20.6916C19.7847 21.5404 18.6149 22.0107 17.4 22ZM6.6 3.99999C5.91125 4.00263 5.25146 4.2774 4.76443 4.76442C4.27741 5.25145 4.00263 5.91124 4 6.59999C4.00265 10.1531 5.41528 13.5599 7.92769 16.0723C10.4401 18.5847 13.8469 19.9973 17.4 20C18.0888 19.9974 18.7485 19.7226 19.2356 19.2356C19.7226 18.7485 19.9974 18.0887 20 17.4V17.07L15.36 16L15.07 16.55C14.62 17.42 14.29 18.05 13.45 17.71C11.7929 17.1169 10.2887 16.162 9.04673 14.9149C7.80477 13.6677 6.85622 12.1596 6.27 10.5C5.91 9.71999 6.59 9.35999 7.45 8.90999L8 8.63999L6.93 3.99999H6.6Z" fill="#666666"></path>
						</svg>
						<div class="the-label">{{ $profile->data->email }}</div>
					</li>
					<li><svg class="the-icon" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M12 2C10.0222 2 8.08879 2.58649 6.44429 3.6853C4.7998 4.78412 3.51808 6.3459 2.7612 8.17317C2.00433 10.0004 1.80629 12.0111 2.19214 13.9509C2.578 15.8907 3.5304 17.6725 4.92893 19.0711C6.32745 20.4696 8.10928 21.422 10.0491 21.8079C11.9889 22.1937 13.9996 21.9957 15.8268 21.2388C17.6541 20.4819 19.2159 19.2002 20.3147 17.5557C21.4135 15.9112 22 13.9778 22 12C22 10.6868 21.7413 9.38642 21.2388 8.17317C20.7362 6.95991 19.9996 5.85752 19.0711 4.92893C18.1425 4.00035 17.0401 3.26375 15.8268 2.7612C14.6136 2.25866 13.3132 2 12 2ZM12 20C10.4177 20 8.87103 19.5308 7.55543 18.6518C6.23984 17.7727 5.21446 16.5233 4.60896 15.0615C4.00346 13.5997 3.84503 11.9911 4.15371 10.4393C4.4624 8.88743 5.22432 7.46197 6.34314 6.34315C7.46196 5.22433 8.88743 4.4624 10.4393 4.15372C11.9911 3.84504 13.5997 4.00346 15.0615 4.60896C16.5233 5.21447 17.7727 6.23984 18.6518 7.55544C19.5308 8.87103 20 10.4177 20 12C20 14.1217 19.1571 16.1566 17.6569 17.6569C16.1566 19.1571 14.1217 20 12 20Z" fill="#666666"></path>
						<path d="M16 11H13V8C13 7.73478 12.8946 7.48043 12.7071 7.29289C12.5196 7.10536 12.2652 7 12 7C11.7348 7 11.4804 7.10536 11.2929 7.29289C11.1054 7.48043 11 7.73478 11 8V12C11 12.2652 11.1054 12.5196 11.2929 12.7071C11.4804 12.8946 11.7348 13 12 13H16C16.2652 13 16.5196 12.8946 16.7071 12.7071C16.8946 12.5196 17 12.2652 17 12C17 11.7348 16.8946 11.4804 16.7071 11.2929C16.5196 11.1054 16.2652 11 16 11Z" fill="#666666"></path>
					</svg>
					<div class="the-label">Login 3 jam yang lalu</div>
				</li>
			</ul>
		</div>

		<div class="text-center pb-4">
			<div class="d-inline-flex mb-2 mt-3">
				<svg class="fill-current text-warning" width="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g data-name="Layer 2"><g data-name="star"><rect width="24" height="24" transform="rotate(90 12 12)" opacity="0"/><path d="M17.56 21a1 1 0 0 1-.46-.11L12 18.22l-5.1 2.67a1 1 0 0 1-1.45-1.06l1-5.63-4.12-4a1 1 0 0 1-.25-1 1 1 0 0 1 .81-.68l5.7-.83 2.51-5.13a1 1 0 0 1 1.8 0l2.54 5.12 5.7.83a1 1 0 0 1 .81.68 1 1 0 0 1-.25 1l-4.12 4 1 5.63a1 1 0 0 1-.4 1 1 1 0 0 1-.62.18z"/></g></g></svg>
				<svg class="fill-current text-warning" width="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g data-name="Layer 2"><g data-name="star"><rect width="24" height="24" transform="rotate(90 12 12)" opacity="0"/><path d="M17.56 21a1 1 0 0 1-.46-.11L12 18.22l-5.1 2.67a1 1 0 0 1-1.45-1.06l1-5.63-4.12-4a1 1 0 0 1-.25-1 1 1 0 0 1 .81-.68l5.7-.83 2.51-5.13a1 1 0 0 1 1.8 0l2.54 5.12 5.7.83a1 1 0 0 1 .81.68 1 1 0 0 1-.25 1l-4.12 4 1 5.63a1 1 0 0 1-.4 1 1 1 0 0 1-.62.18z"/></g></g></svg>
				<svg class="fill-current text-warning" width="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g data-name="Layer 2"><g data-name="star"><rect width="24" height="24" transform="rotate(90 12 12)" opacity="0"/><path d="M17.56 21a1 1 0 0 1-.46-.11L12 18.22l-5.1 2.67a1 1 0 0 1-1.45-1.06l1-5.63-4.12-4a1 1 0 0 1-.25-1 1 1 0 0 1 .81-.68l5.7-.83 2.51-5.13a1 1 0 0 1 1.8 0l2.54 5.12 5.7.83a1 1 0 0 1 .81.68 1 1 0 0 1-.25 1l-4.12 4 1 5.63a1 1 0 0 1-.4 1 1 1 0 0 1-.62.18z"/></g></g></svg>
				<svg class="fill-current text-warning" width="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g data-name="Layer 2"><g data-name="star"><rect width="24" height="24" transform="rotate(90 12 12)" opacity="0"/><path d="M17.56 21a1 1 0 0 1-.46-.11L12 18.22l-5.1 2.67a1 1 0 0 1-1.45-1.06l1-5.63-4.12-4a1 1 0 0 1-.25-1 1 1 0 0 1 .81-.68l5.7-.83 2.51-5.13a1 1 0 0 1 1.8 0l2.54 5.12 5.7.83a1 1 0 0 1 .81.68 1 1 0 0 1-.25 1l-4.12 4 1 5.63a1 1 0 0 1-.4 1 1 1 0 0 1-.62.18zM12 16.1a.92.92 0 0 1 .46.11l3.77 2-.72-4.21a1 1 0 0 1 .29-.89l3-2.93-4.2-.62a1 1 0 0 1-.71-.56L12 5.25 10.11 9a1 1 0 0 1-.75.54l-4.2.62 3 2.93a1 1 0 0 1 .29.89l-.72 4.16 3.77-2a.92.92 0 0 1 .5-.04z"/></g></g></svg>
				<svg class="fill-current text-warning" width="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g data-name="Layer 2"><g data-name="star"><rect width="24" height="24" transform="rotate(90 12 12)" opacity="0"/><path d="M17.56 21a1 1 0 0 1-.46-.11L12 18.22l-5.1 2.67a1 1 0 0 1-1.45-1.06l1-5.63-4.12-4a1 1 0 0 1-.25-1 1 1 0 0 1 .81-.68l5.7-.83 2.51-5.13a1 1 0 0 1 1.8 0l2.54 5.12 5.7.83a1 1 0 0 1 .81.68 1 1 0 0 1-.25 1l-4.12 4 1 5.63a1 1 0 0 1-.4 1 1 1 0 0 1-.62.18zM12 16.1a.92.92 0 0 1 .46.11l3.77 2-.72-4.21a1 1 0 0 1 .29-.89l3-2.93-4.2-.62a1 1 0 0 1-.71-.56L12 5.25 10.11 9a1 1 0 0 1-.75.54l-4.2.62 3 2.93a1 1 0 0 1 .29.89l-.72 4.16 3.77-2a.92.92 0 0 1 .5-.04z"/></g></g></svg>
			</div>
			<div>(4/10)</div>
		</div>
	</div>
</div>
<div class="col-lg-8">
	<div class="d-flex align-items-center">
		<h2 class="font-weight-bold">{{ $profile->data->name }}</h2>
		<a href="#" class="btn btn-normal btn-outline-secondary ml-4">
			Ubah Informasi
		</a>
		<div class="dropdown">
			<a href="#" class="btn btn-normal btn-outline-cta ml-2 dropdown-toggle" data-toggle="dropdown">
				Tambah
			</a>
			<ul class="dropdown-menu">
				<li><a href="{{ url('user/tambah-produk.html') }}" class="dropdown-item">Produk</a></li>
				<li><a href="{{ url('user/tambah-panen.html') }}" class="dropdown-item">Panen</a></li>
				<li><a href="{{ url('user/tambah-lahan.html') }}" class="dropdown-item">Lahan</a></li>
			</ul>
		</div>
	</div>
	<p class="d-flex align-items-center time">
		<svg class="mr-1" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path d="M10 1.66666C8.24338 1.66657 6.55772 2.35979 5.30937 3.59565C4.06103 4.83151 3.3509 6.51013 3.33334 8.26666C3.33334 12.8333 9.20834 17.9167 9.45834 18.1333C9.60928 18.2624 9.80137 18.3334 10 18.3334C10.1986 18.3334 10.3907 18.2624 10.5417 18.1333C10.8333 17.9167 16.6667 12.8333 16.6667 8.26666C16.6491 6.51013 15.939 4.83151 14.6906 3.59565C13.4423 2.35979 11.7566 1.66657 10 1.66666ZM10 16.375C8.60834 15.05 5 11.375 5 8.26666C5 6.94057 5.52679 5.6688 6.46447 4.73112C7.40215 3.79344 8.67392 3.26666 10 3.26666C11.3261 3.26666 12.5979 3.79344 13.5355 4.73112C14.4732 5.6688 15 6.94057 15 8.26666C15 11.35 11.3917 15.05 10 16.375Z" fill="#666666"/>
			<path d="M10 5C9.42313 5 8.85923 5.17106 8.37958 5.49155C7.89994 5.81203 7.5261 6.26756 7.30535 6.80051C7.08459 7.33346 7.02683 7.9199 7.13937 8.48568C7.25191 9.05146 7.5297 9.57116 7.9376 9.97906C8.34551 10.387 8.86521 10.6648 9.43098 10.7773C9.99676 10.8898 10.5832 10.8321 11.1162 10.6113C11.6491 10.3906 12.1046 10.0167 12.4251 9.53708C12.7456 9.05744 12.9167 8.49353 12.9167 7.91667C12.9167 7.14312 12.6094 6.40125 12.0624 5.85427C11.5154 5.30729 10.7735 5 10 5ZM10 9.16667C9.75277 9.16667 9.5111 9.09336 9.30553 8.956C9.09997 8.81865 8.93976 8.62343 8.84515 8.39502C8.75054 8.16661 8.72578 7.91528 8.77402 7.6728C8.82225 7.43033 8.9413 7.2076 9.11611 7.03278C9.29093 6.85797 9.51366 6.73892 9.75613 6.69068C9.99861 6.64245 10.2499 6.66721 10.4784 6.76182C10.7068 6.85643 10.902 7.01664 11.0393 7.2222C11.1767 7.42777 11.25 7.66944 11.25 7.91667C11.25 8.24819 11.1183 8.56613 10.8839 8.80055C10.6495 9.03497 10.3315 9.16667 10 9.16667Z" fill="#666666"/>
		</svg>
		{{ $profile->data->address }}
	</p>

	<nav class="nav nav-tabs pb-0">
		<li><a href="{{ url('user-produk') }}.html" class="nav-link">Produk</a></li>
		<li><a href="{{ url('user-panen') }}.html" class="nav-link">Panen</a></li>
		<li><a href="{{ url('user-penjualan') }}.html" class="nav-link">Penjualan</a></li>
		<li><a href="{{ url('user-pembelian') }}.html" class="nav-link active">Pembelian</a></li>
		<li><a href="{{ url('user-lahan') }}.html" class="nav-link">Lahan</a></li>
	</nav>

	<div class="d-flex align-items-center mt-4 mb-4">
		<h5 class="section-title-sm">Invoice: {{ $detail->data->order_number }}</h5>
		<a href="{{ url('user-pembelian.html') }}" class="btn btn-outline-secondary btn-sm ml-2 mb-2">Kembali</a>
	</div>

	<h6 class="font-weight-bold">Rincian Produk</h6>
	<hr class="mb-4">
	<?php
	$total = 0;
	?>
	@foreach($detail->data->purchase_item as $data)
	<div class="d-flex mb-4 align-items-center">
		<img width="100" class="rounded" src="{{ $data->image }}">
		<div class="ml-3">
			<h6 class="mb-0">{{ $data->name }}</h6>
		</div>
		<div class="mx-5">
			<div class="text-muted">Harga</div>
			<div class="font-weight-bold">Rp. {{ price($data->price) }}</div>
		</div>
		<div class="mx-5">
			<div class="text-muted">Qty</div>
			<div class="font-weight-bold">{{ $data->qty }}</div>
		</div>
		<div class="mx-5">
			<div class="text-muted">Subtotal</div>
			<div class="font-weight-bold">Rp. {{ price($data->price) }}</div>
		</div>
	</div>
	<hr class="my-4">
	<?php
	$total = $total+($data->qty*$data->price);
	?>
	@endforeach

	<div class="row">
		<div class="col-lg-6 desc">
			<h6 class="font-weight-bold">Pengiriman</h6>
			<p>
				<b>{{ $detail->data->address_delivery->receiver_name }}</b><br>
				{{ $detail->data->address_delivery->phone }}<br>
				{{ $detail->data->address_delivery->street }},<br>
				{{ $detail->data->address_delivery->village_name }},
				{{ $detail->data->address_delivery->district_name }},
				{{ $detail->data->address_delivery->regency_name }},
                {{ $detail->data->address_delivery->province_name }},
                {{ $detail->data->address_delivery->post_code }}
			</p>
		</div>
		<div class="col-lg-6">
			<div class="total">
				<div class="total-row">
					<div class="total-name">Subtotal</div>
					<div class="total-value">Rp. {{ $detail->data->total_pay }}</div>
				</div>
				<?php
				$ppn = $total*0.1;
				?>
				<div class="total-row">
					<div class="total-name">PPN</div>
					<div class="total-value">Rp. {{ price($ppn)}}</div>
				</div>
				<div class="total-row">
					<div class="total-name">Ongkir</div>
					<div class="total-value">Rp. 0</div>
				</div>
				<div class="total-row">
					<div class="total-name">Total</div>
					<div class="total-value">Rp. {{ price($total+ $ppn) }}</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</section>
@endsection
