@extends('frontend.layout')
@section('title', 'Tambah Lahan')
@section('content')
    <section class="section">
        <div class="container">
            <h1 class="section-title-md mb-4">Tambah Lahan</h1>
            {{ Form::open(['url' => '/addLahanToUser', 'files' => true]) }}
                {{--@include('validation') --}}
                @include('frontend.user.lahan.form')
            {{ Form::close() }}
        </div>
    </section>
@endsection

