@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" href="https://unpkg.com/file-upload-with-preview@4.0.2/dist/file-upload-with-preview.min.css">
@endpush

<?php
    $btnLabel = Request::segment(2) == 'tambah-lahan.html' ? 'Tambah Lahan' : 'Update Lahan';
?>

<input type="hidden" name="token" value="{{ session()->get('token') }}">

<div class="card card-shadow card-primary">
    <div class="card-body">
        <h4 class="section-title-sm mb-0">Informasi Umum</h4>
        <p class="text-muted mb-5">Seperti nama, luas.</p>

        <div class="form-group row {{ $errors->first('name') ? "is-invalid": ""}}">
            <label class="col-lg-3 {{ $errors->first('name') ? "text-danger": ""}}">Nama Lahan</label>
            <div class="col-lg-9">
                <input type="text" class="form-control" placeholder="Nama Lahan" name="name" value="{{ $land->data->name ?? '' }}" required>
                <span class="text-danger">
                    {{ $errors->first('name') }}
                </span>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-lg-3">Luas</label>
            <div class="col-lg-3">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Mis. 10" name="large" value="{{ $land->data->large ?? '' }}"  required>
                    <span class="text-danger">
                        {{ $errors->first('large') }}
                    </span>
                    <div class="input-group-append">
                        <select name="unit_area" class="form-control">
                        <option value="M">M</option>
                        <option value="KM">KM</option>
                        {{-- @foreach($units->data as $unit)
                            <option value="{{ $unit->name }}"
                            @if(isset($land))
                                {{ $unit->name == $land->data->unit_area ? 'selected' : '' }}
                            @endif
                            >{{ $unit->name }}</option>
                        @endforeach --}}
                        </select>
                        <span class="text-danger">
                            {{ $errors->first('unit_area') }}
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card card-shadow card-primary mt-5">
    <div class="card-body">
        <h4 class="section-title-sm mb-0">Administratif</h4>
        <p class="text-muted mb-5">Seperti alamat, provinsi, kota/kabupaten dan kecamatan.</p>

        <div class="form-group row {{ $errors->first('street') ? "is-invalid": ""}}">
            <label class="col-lg-3 {{ $errors->first('street') ? "text-danger": ""}}">Alamat Lahan</label>
            <div class="col-lg-9">
                <input type="text" class="form-control" placeholder="Alamat Lahan" name="street" value="{{ $land->data->street ?? '' }}" required>
                <span class="text-danger">
                    {{ $errors->first('street') }}
                </span>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-lg-3">Provinsi</label>
            <div class="col-lg-4">
                <select class="form-control mb-3 province" required>
                    <option value="">Pilih Provinsi</option>
                    @foreach($provinces->data as $province)
                        <option value="{{ $province->id ?? '' }}">{{ $province->name ?? $province }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-lg-3">Kota/Kabupaten</label>
            <div class="col-lg-4">
                <select name="regency" class="form-control mb-3 regency" required>
                    <option value="">Pilih provinsi dahulu</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-lg-3">Kecamatan</label>
            <div class="col-lg-4">
                <select name="district" class="form-control mb-3 district" required>
                    <option value="">Pilih kabupaten dahulu</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-lg-3">Desa</label>
            <div class="col-lg-4">
                <select name="village_id" class="form-control mb-3 village" required>
                    <option value="">Pilih kecamatan dahulu</option>
                </select>
                <span class="text-danger">
                    {{ $errors->first('village_id') }}
                </span>
            </div>
        </div>
    </div>
</div>

<div class="card card-shadow card-primary mt-5">
    <div class="card-body">
        <h4 class="section-title-sm mb-0">Deskripsi Lahan</h4>
        <p class="text-muted mb-5">Jelaskan secara rinci lahan yang hendak Anda publikasikan.</p>

        <div class="form-group row">
            <label class="col-lg-3">Keterangan</label>
            <div class="col-lg-9">
                <textarea name="description" class="form-control" required>{{ $land->data->description ?? ''}}</textarea>
                <br>
                <div class=" {{ $errors->first('description') ? "alert alert-danger": ""}}">
                    {{ $errors->first('description') }}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card card-shadow card-primary mt-5">
    <div class="card-body">
        <h4 class="section-title-sm mb-0">Sertakan Gambar</h4>
        <p class="text-muted mb-5">Sertakan empat gambar terkait produk Anda.</p>

        <div class="row">
            @foreach([1,2,3,4] as $i => $r)
            <div class="col-lg-3">
                <label id="preview-image-{{$r}}" for="input-image-{{$r}}" style="display:flex;justify-content: center;align-items: center;font-size: 16px;width: 100%;height: 230px;background-color: #f9f9f9;border: 2px dashed #ddd;border-radius: 3px;cursor: pointer;{{ isset($land->data->front_images[$i]) ? 'background-image: url('. $land->data->front_images[$i] .')' : ''}}">
                    @if ($r==1)
                        Pilih Gambar
                        @if ( Request::segment(2) == 'tambah-lahan.html')
                            (Wajib)
                        @else

                        @endif
                    @elseif($r==2)
                        Pilih Gambar
                        @if ( Request::segment(2) == 'tambah-lahan.html')
                            (Wajib)
                        @else

                        @endif
                    @elseif($r==3)
                        Pilih Gambar
                        @if ( Request::segment(2) == 'tambah-lahan.html')
                            (Wajib)
                        @else

                        @endif
                    @elseif($r==4)
                        Pilih Gambar
                        @if ( Request::segment(2) == 'tambah-lahan.html')
                            (Wajib)
                        @else

                        @endif
                    @else
                        Pilih Gambar
                        @if ( Request::segment(2) == 'tambah-lahan.html')
                            (Tidak Wajib)
                        @else

                        @endif
                    @endif
                </label>
                <input type="file" name="image_{{$r}}" id="input-image-{{$r}}" class="d-none image-upload" data-preview="#preview-image-{{$r}}">

                <span class="text-danger">
                    {{ $errors->first('image_' . $r) }}
                </span>
            </div>
            @endforeach
        </div>
    </div>
</div>

<div class="form-group text-right mt-5">
    <button class="btn btn-cta" type="submit"><i class="fas fa-save"></i> {{ $btnLabel }}</button>
    <a href="/user-lahan.html" class="btn btn-outline-secondary"><i class="fas fa-backward"></i> Back</a>
</div>

@push('scripts')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
<script src="https://unpkg.com/file-upload-with-preview@4.0.2/dist/file-upload-with-preview.min.js"></script>
<script src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script>
$(document).ready( function() {
    CKEDITOR.replace( 'description' );

    $('.province').change(function() {
		const id = $(this).val();

		$.ajax({
			url: '/regency/' + id ,
			beforeSend: function(request) {
				$('.regency option').remove();
				$('.regency').append('<option value="">Memuat ...</option>');
			},
			success: function(data) {
				let option = '<option value="{id}">{name}</option>';

				$('.regency option').remove();
				$('.regency').append('<option value="">Pilih Kota/Kabupaten</option>');
				data.forEach(function(item) {
					$('.regency').append(
						option.replace(/{id}/g, item.id).replace(/{name}/g, item.name)
					);
				});
			},
			complete: function() {

			}
		});
	});

    $('.regency').change(function() {
		const id = $(this).val();

		$.ajax({
			url: '/district/' + id,
			beforeSend: function() {
				$('.district option').remove();
				$('.district').append('<option value="">Memuat ...</option>');
			},
			success: function(data) {
				let option = '<option value="{id}">{name}</option>';

				$('.district option').remove();
				$('.district').append('<option value="">Pilih Kecamatan</option>');
				data.forEach(function(item) {
					$('.district').append(
						option.replace(/{id}/g, item.id).replace(/{name}/g, item.name)
					);
				});
			},
			complete: function() {

			}
		});
	});

    $('.district').change(function() {
		const id = $(this).val();

		$.ajax({
			url: '/village/' + id,
			beforeSend: function() {
				$('.village option').remove();
				$('.village').append('<option value="">Memuat ...</option>');
			},
			success: function(data) {
				let option = '<option value="{id}">{name}</option>';

				$('.village option').remove();
				$('.village').append('<option value="">Pilih Desa</option>');
				data.forEach(function(item) {
					$('.village').append(
						option.replace(/{id}/g, item.village_id).replace(/{name}/g, item.village_name)
					);
				});
			},
			complete: function() {

			}
		});
	});
});


    function readURL(input, element) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $(element).css({
            'background-image': 'url(' + e.target.result + ')',
            'background-position': 'center',
            'background-size': 'cover',
        });
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    $(".image-upload").change(function() {
        console.log('a')
      readURL(this, $(this).data('preview'));
    });
</script>
@endpush
