@extends('frontend.layout')
@section('title', 'Edit Lahan')
@section('content')
    <section class="section">
        <div class="container">
            <h1 class="section-title-md mb-4">Edit Lahan</h1>
            {{ Form::open(['url'=>'/updateLahanUser/'.$land->data->id,'files'=>true,'method'=>'put'])}}
                {{--@include('validation')--}}
                @include('frontend.user.lahan.form')
            {{ Form::close() }}             
        </div>
    </section>
@endsection