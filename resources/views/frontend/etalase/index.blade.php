@extends('frontend.layout')
@section('title')
Seller Etalase
@endsection

@section('content')
<section class="section">
    <div class="container">
        @include('alert')
        <div class="row">
            <div class="col-lg-3">
                @include('frontend.store.seller-left-content')
            </div>
            <div class="col-lg-9">
                <div class="card card-shadow card-primary">
                    <ul class="nav nav-tabs pb-0 d-flex align-items-center" id="myTab" role="tablist" style="margin-left: 10px; margin-right: 10px;">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Etalase Saya</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Tambah Etalase</a>
                        </li>
                    </ul>
                    <div class="card-body">
                        <h4 class="section-title-sm mb-0">Etalase</h4>
                        <p class="text-muted mb-5">Kelola etalase yang Anda miliki di halaman ini. Anda dapat menambah, mengubah dan menghapus etalase.</p>

                        <div class="tab-content">
                            <div class="tab-pane active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <div class="card-group-x"> 
                                    @foreach ($etalases->data as $etalase)
                                    <div class="card card-no-affect">
                                        {{ Form::model($etalase,['url'=>'etalase/'.$etalase->id,'files'=>true])}}
                                        @method('PUT')
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col">
                                                    <input class="form-control" type="text" name="name" id="name" value="{{ $etalase->name }}">
                                                </div>
                                                <div class="col-auto">
                                                    <button type="submit" class="btn btn-primary">Ubah</button>
                                                </div>
                                                <div class="col-auto">
                                                    <button type="button" class="btn btn-danger" onclick="$('#delete-{{$etalase->id}}').submit();">Hapus</button>
                                                </div>
                                            </div>
                                        </div>
                                        {{ Form::close()}}
                                        {{ Form::open(['url'=>'etalase/'.$etalase->slug,'method'=>'delete', 'onsubmit'=>"return confirm('Yakin Ingin Menghapus Etalase Ini?')", 'id' => 'delete-' . $etalase->id])}}
                                        {{ Form::close()}}
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <form action="/etalase" method="post">
                                    @csrf
                                    <input type="hidden" name="store_id" id="store_id" value="{{ $store->data->id }}">
                                    <div class="form-group">
                                        <label for="name">Nama Etalase</label>
                                        <input type="text" name="name" id="name" class="form-control" placeholder="Contoh: Buku Cerita, Buku Sains">
                                    </div>
                                    <button type="submit" class="btn btn-cta btn-normal">Tambah</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
