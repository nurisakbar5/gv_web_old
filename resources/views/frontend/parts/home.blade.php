<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>School @yield('title')</title>
    <link rel="stylesheet" href="{{ asset('template/front/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/front/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/front/css/owl.theme.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/front/css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('template/front/css/carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('template/front/css/calendar.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link href="https://raw.githubusercontent.com/daneden/animate.css/master/animate.css" rel="stylesheet">
    @yield('css')
</head>
<body>
    {{-- NAV --}}
    <nav class="navbar fixed-top navbar-expand-md flex-nowrap navbar-new-top">
        <div class="row py-0">
            <!--Breaking box-->
            <div class="col-1 col-md-3 col-lg-2 py-1 pr-md-0 mb-md-1">
                <div class="d-inline-block d-md-block text-white text-center breaking-caret py-1 px-2">
                    <span class="fas fa-phone" style="color:black"></span>
                    <span class="d-none d-md-inline-block" style="color:black">Our Contact</span>
                </div>
            </div>
            <!--Breaking content-->
            <div class="col-11 col-md-9 col-lg-10 pl-1 pl-md-2">
                <div class="breaking-box pt-2 pb-1">
                    <!--marque-->
                    <marquee behavior="scroll" direction="left" onmouseover="this.stop();" onmouseleave="this.start();">
                        <a class="h6 font-weight-light" href="#"><span class="position-relative mx-2 badge badge-primary rounded-0">Email</span> email@sch.id</a>
                        <a class="h6 font-weight-light" href="#"><span class="position-relative mx-2 badge badge-primary rounded-0">Address</span> Jl. Sultan Alimuddin No. 27, Makmur Jaya, Kongbeng, Kutai Timur, Kalimantan Timur 75655</a>
                        <a class="h6 font-weight-light" href=""><span class="position-relative mx-2 badge badge-primary rounded-0">Phone</span> 08125081822</a>
                        <a class="h6 font-weight-light" href=""><span class="position-relative mx-2 badge badge-primary rounded-0">Sosial Media</span></a> <a href="#">Facebook</a> &nbsp; <a href="#">Twitter</a> &nbsp; <a href="#">Instagram</a>
                    </marquee>
                </div>
            </div>
        </div>
    </nav>
    <nav class="navbar fixed-top navbar-expand-md navbar-new-bottom">
        <div class="navbar-collapse collapse pt-2 pt-md-0" id="navbar2">
            <ul class="navbar-nav w-100 px-3">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown
                    </a>
                    <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link">Link</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link">Link</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link">Link</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link">Link</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto px-4" style="width:35%">
                <form class="form-inline">
                    @csrf
                    <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                </form>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Profile </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-cyan" aria-labelledby="navbarDropdownMenuLink-4">
                        <a class="dropdown-item" href="#">My account</a>
                        <a class="dropdown-item" href="#">Log out</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    {{-- END NAV --}}

    @include('frontend.parts.carousel')
    @include('frontend.parts.announcement')
    @include('frontend.parts.general-news')
    @include('frontend.parts.student-news')
    @include('frontend.parts.academic-calendar')

    <script src="{{ asset('template/front/js/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('template/front/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('template/front/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('template/front/js/custom.js') }}"></script>
    <script src="{{ asset('template/front/js/calendar.js') }}"></script>
    <script>
         $('#myCarousel').carousel({
            interval: 3000,
        });
    </script>
    @stack('js')
</body>
</html>
