<div class="container">
        <h3 class="h3">Berita Umum</h3>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div id="news-slider4" class="owl-carousel">
                    <div class="post-slide4">
                        <ul class="post-info">
                            <li><i class="fa fa-tag"></i><a href="#">java</a>,<a href="#">php</a></li>
                            <li><i class="fa fa-calendar"></i><a href="#">June 10, 2016</a></li>
                            <li><i class="fa fa-comment"></i><a href="#">1</a></li>
                        </ul>
                        <div class="post-img">
                            <img src="http://bestjquery.com/tutorial/news-slider/demo31/images/img-1.jpg" alt="">
                            <a href="#" class="read">read more</a>
                        </div>
                        <div class="post-content">
                            <h3 class="post-title">Latest News Post</h3>
                            <p class="post-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore ducimus est, excepturi nam odit possimus? Accusantium, aut beatae commodi dolore dolores esse fugit id illum ipsam nemo nesciunt obcaecati officiis praesentium provident quasi quis quo repellat sapiente sequi temporibus voluptates.</p>
                        </div>
                    </div>

                    <div class="post-slide4">
                        <ul class="post-info">
                            <li><i class="fa fa-tag"></i><a href="#">java</a>,<a href="#">php</a></li>
                            <li><i class="fa fa-calendar"></i><a href="#">June 12, 2016</a></li>
                            <li><i class="fa fa-comment"></i><a href="#">3</a></li>
                        </ul>
                        <div class="post-img">
                            <img src="http://bestjquery.com/tutorial/news-slider/demo31/images/img-2.jpg" alt="">
                            <a href="#" class="read">read more</a>
                        </div>
                        <div class="post-content">
                            <h3 class="post-title">Latest News Post</h3>
                            <p class="post-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore ducimus est, excepturi nam odit possimus? Accusantium, aut beatae commodi dolore dolores esse fugit id illum ipsam nemo nesciunt obcaecati officiis praesentium provident quasi quis quo repellat sapiente sequi temporibus voluptates.</p>
                        </div>
                    </div>

                    <div class="post-slide4">
                        <ul class="post-info">
                            <li><i class="fa fa-tag"></i><a href="#">java</a>,<a href="#">php</a></li>
                            <li><i class="fa fa-calendar"></i><a href="#">June 12, 2016</a></li>
                            <li><i class="fa fa-comment"></i><a href="#">3</a></li>
                        </ul>
                        <div class="post-img">
                            <img src="http://bestjquery.com/tutorial/news-slider/demo31/images/img-3.jpg" alt="">
                            <a href="#" class="read">read more</a>
                        </div>
                        <div class="post-content">
                            <h3 class="post-title">Latest News Post</h3>
                            <p class="post-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore ducimus est, excepturi nam odit possimus? Accusantium, aut beatae commodi dolore dolores esse fugit id illum ipsam nemo nesciunt obcaecati officiis praesentium provident quasi quis quo repellat sapiente sequi temporibus voluptates.</p>
                        </div>
                    </div>

                    <div class="post-slide4">
                        <ul class="post-info">
                            <li><i class="fa fa-tag"></i><a href="#">java</a>,<a href="#">php</a></li>
                            <li><i class="fa fa-calendar"></i><a href="#">June 12, 2016</a></li>
                            <li><i class="fa fa-comment"></i><a href="#">3</a></li>
                        </ul>
                        <div class="post-img">
                            <img src="http://bestjquery.com/tutorial/news-slider/demo31/images/img-4.jpg" alt="">
                            <a href="#" class="read">read more</a>
                        </div>
                        <div class="post-content">
                            <h3 class="post-title">Latest News Post</h3>
                            <p class="post-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore ducimus est, excepturi nam odit possimus? Accusantium, aut beatae commodi dolore dolores esse fugit id illum ipsam nemo nesciunt obcaecati officiis praesentium provident quasi quis quo repellat sapiente sequi temporibus voluptates.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
</div>
