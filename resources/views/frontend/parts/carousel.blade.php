<div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <div class="mask flex-center">
              <div class="container">
                <div class="row align-items-center">
                  <div class="col-md-7 col-12 order-md-1 order-2">
                    <h4>Jelang semester genap <br>
                        FKTI Unmul gelar rapat persiapan</h4>
                    <p>FKTI Unmul – Waktu perkuliahan semester genap 2018/2019 yang semakin dekat, dimanfaatkan Civitas adakemika FKTI Unmul untuk melakukan rapat persiapan guna memberikan pelayanan terbaik terhadap mahasiswa.</p>
                    <a href="#">READ MORE</a> </div>
                    <div class="col-md-5 col-12 order-md-2 order-1"><img src="{{ asset('test.jpg') }}" class="mx-auto" alt="slide"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <div class="mask flex-center">
              <div class="container">
                <div class="row align-items-center">
                  <div class="col-md-7 col-12 order-md-1 order-2">
                    <h4>Rangkaian Orientasi Mahasiswa (ROM) <br>
                        Fakultas Ilmu Komputer Dan Teknologi Informasi 2019.</h4>
                    <p>FKTI Unmul – Dekan Fakultas Ilmu Komputer & Teknologi Informasi Dr. Nataniel Dengen, M.Si membuka pelaksaan Rangkaian Orientasi Mahasiswa (ROM) Fakultas Ilmu Komputer Dan Teknologi Informasi (FKTI) dan dihadiri pula oleh Dr. H. Fahrul Agus, M.T & Dyna Marisa Khairina, S.Kom., M.Kom selaku Wakil Dekan I dan II FKTI Unmul.</p>
                    <a href="#">READ MORE</a> </div>
                  <div class="col-md-5 col-12 order-md-2 order-1">
                      <img src="{{ asset('test2.jpg') }}" class="mx-auto" alt="slide">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <div class="mask flex-center">
              <div class="container">
                <div class="row align-items-center">
                  <div class="col-md-7 col-12 order-md-1 order-2">
                    <h4>Kominfo Gandeng <br>
                        Enam Perguruan Tinggi di Kalimantan</h4>
                    <p>Samarinda, Kominfo - Salah satu perubahan besar dalam Revolusi Industri 4.0 di bidang teknologi informasi dan komunikasi adalah transformasi dengan kehadiran perangkat dan keahlian big data, internet of things, artificial intelligence, machine learning, cloud computing, dan beragam teknologi interaktif terbaru.</p>
                    <a href="#">READ MORE</a> </div>
                  <div class="col-md-5 col-12 order-md-2 order-1"><img src="{{ asset('test2.jpg') }}" class="mx-auto" alt="slide"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
