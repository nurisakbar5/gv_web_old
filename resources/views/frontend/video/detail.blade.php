@extends('frontend.layout')
@section('seo')
{!! SEO::generate(true) !!}
@endsection
@section('content')
<section class="section">
    <div class="container">
        <div class="row align-items-start">
            <div class="col-lg-7">
                {{-- <div id="player" data-plyr-provider="youtube" data-plyr-embed-id="https://www.youtube.com/watch?v=6ROwVrF0Ceg"></div> --}}
                <video width="620" height="580" controls>
                    <source src="{{ $video->video_file }}" type="video/mp4">
                        <source src="{{ $video->video_file }}" type="video/ogg">
                          Your browser does not support the video tag.
                      </video>
                      <h2 class="section-title-md mt-5">Video Lainnya</h2>
                      <div class="row cards-transparent mt-4 more-videos">
                        @foreach($related as $related_video)
                        <div class="col-lg-12">
                            <div class="card card-shadow">
                                <div class="card-image" style="background-image:url({{ $related_video->img_thumbnail }});background-position:center;background-size:cover;height:150px">
                                    <div class="card-duration">03:50</div>
                                </div>
                                <div class="card-body">
                                    <div class="card-category"><a href="/">{{ $related_video->category->name }}</a></div>
                                    <h6 class="card-title"><a href="{{ url('video/'.$related_video->slug) }}.html">{{ $related_video->title }}</a></h6>
                                    <p class="time">{{ $related_video->view }} Views</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-lg-5">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mt-4">
                            <li class="breadcrumb-item"><a href="/">Home</a></li>
                            <li class="breadcrumb-item"><a href="/video.html">Videos</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $video->title }}</li>
                        </ol>
                    </nav>
                    <h1 class="page-title">{{ $video->title }}</h1>
                    <p class="time text-gray">{{ $video->view }} Views</p>
                    <ul class="nav nav-tabs mt-4">
                        <li class="nav-item"><a href="#tab-desc" data-toggle="tab" class="nav-link active">Deskripsi</a></li>
                        <li class="nav-item"><a href="#tab-disc" data-toggle="tab" class="nav-link ">Diskusi</a></li>
                        <li class="nav-item"><a href="#tab-uploader" data-toggle="tab" class="nav-link ">Diunggah Oleh</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-desc" class="tab-pane active">
                            <p class="time text-gray mt-3">Published on {{ $video->publish_date }}</p>
                            <div class="desc">
                                <p>{!! $video->description !!}</p>
                            </div>
                        </div>
                        <div id="tab-uploader" class="tab-pane">
                            <h4 class="mb-3 mt-3 page-block-title text-primary">Info Pengunggah</h4>
                            <div class="d-flex">
                                <div class="rounded shadow" style="background-image:url(https://images.unsplash.com/photo-1553798194-cc0213ae7f99?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=100&amp;h=100&amp;q=80);width:100px;height:100px;background-size:cover;background-position:center"></div>
                                <div class="ml-3">
                                    <h6 class="mb-1 mt-2">Ahmad Albar</h6>
                                    <p class="text-muted mb-0">Jawa Barat</p>
                                    <p class="text-muted mt-2 mb-0">Member Since September 2018</p>
                                </div>
                            </div>
                            <ul class="icon-list mt-5">
                                <li><svg class="the-icon" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M19 4H5C4.20435 4 3.44129 4.31607 2.87868 4.87868C2.31607 5.44129 2 6.20435 2 7V17C2 17.7956 2.31607 18.5587 2.87868 19.1213C3.44129 19.6839 4.20435 20 5 20H19C19.7956 20 20.5587 19.6839 21.1213 19.1213C21.6839 18.5587 22 17.7956 22 17V7C22 6.20435 21.6839 5.44129 21.1213 4.87868C20.5587 4.31607 19.7956 4 19 4ZM18.33 6L12 10.75L5.67 6H18.33ZM19 18H5C4.73478 18 4.48043 17.8946 4.29289 17.7071C4.10536 17.5196 4 17.2652 4 17V7.25L11.4 12.8C11.5731 12.9298 11.7836 13 12 13C12.2164 13 12.4269 12.9298 12.6 12.8L20 7.25V17C20 17.2652 19.8946 17.5196 19.7071 17.7071C19.5196 17.8946 19.2652 18 19 18Z" fill="#666666"></path>
                                </svg>
                                <div class="the-label">ahmad@albar.com</div>
                            </li>
                            <li><svg class="the-icon" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M17.4 22C13.3173 21.9947 9.40331 20.3705 6.5164 17.4836C3.62949 14.5967 2.00529 10.6827 2 6.59999C2 5.38 2.48464 4.20997 3.34731 3.3473C4.20998 2.48463 5.38 1.99999 6.6 1.99999C6.85834 1.99802 7.11625 2.02147 7.37 2.06999C7.61531 2.10629 7.85647 2.16658 8.09 2.24999C8.25425 2.30762 8.40061 2.40713 8.51461 2.53866C8.62861 2.6702 8.7063 2.82922 8.74 2.99999L10.11 8.99999C10.1469 9.16286 10.1425 9.33237 10.0971 9.49308C10.0516 9.65378 9.96671 9.80055 9.85 9.91999C9.72 10.06 9.71 10.07 8.48 10.71C9.46499 12.8708 11.1932 14.6062 13.35 15.6C14 14.36 14.01 14.35 14.15 14.22C14.2694 14.1033 14.4162 14.0183 14.5769 13.9729C14.7376 13.9275 14.9071 13.9231 15.07 13.96L21.07 15.33C21.2353 15.3683 21.3881 15.4481 21.5141 15.5618C21.64 15.6756 21.735 15.8195 21.79 15.98C21.8744 16.2174 21.938 16.4616 21.98 16.71C22.0202 16.9613 22.0403 17.2155 22.04 17.47C22.0216 18.6848 21.5233 19.8429 20.654 20.6916C19.7847 21.5404 18.6149 22.0107 17.4 22ZM6.6 3.99999C5.91125 4.00263 5.25146 4.2774 4.76443 4.76442C4.27741 5.25145 4.00263 5.91124 4 6.59999C4.00265 10.1531 5.41528 13.5599 7.92769 16.0723C10.4401 18.5847 13.8469 19.9973 17.4 20C18.0888 19.9974 18.7485 19.7226 19.2356 19.2356C19.7226 18.7485 19.9974 18.0887 20 17.4V17.07L15.36 16L15.07 16.55C14.62 17.42 14.29 18.05 13.45 17.71C11.7929 17.1169 10.2887 16.162 9.04673 14.9149C7.80477 13.6677 6.85622 12.1596 6.27 10.5C5.91 9.71999 6.59 9.35999 7.45 8.90999L8 8.63999L6.93 3.99999H6.6Z" fill="#666666"></path>
                            </svg>
                            <div class="the-label">+62128742983</div>
                        </li>
                        <li><svg class="the-icon" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12 2C10.0222 2 8.08879 2.58649 6.44429 3.6853C4.7998 4.78412 3.51808 6.3459 2.7612 8.17317C2.00433 10.0004 1.80629 12.0111 2.19214 13.9509C2.578 15.8907 3.5304 17.6725 4.92893 19.0711C6.32745 20.4696 8.10928 21.422 10.0491 21.8079C11.9889 22.1937 13.9996 21.9957 15.8268 21.2388C17.6541 20.4819 19.2159 19.2002 20.3147 17.5557C21.4135 15.9112 22 13.9778 22 12C22 10.6868 21.7413 9.38642 21.2388 8.17317C20.7362 6.95991 19.9996 5.85752 19.0711 4.92893C18.1425 4.00035 17.0401 3.26375 15.8268 2.7612C14.6136 2.25866 13.3132 2 12 2ZM12 20C10.4177 20 8.87103 19.5308 7.55543 18.6518C6.23984 17.7727 5.21446 16.5233 4.60896 15.0615C4.00346 13.5997 3.84503 11.9911 4.15371 10.4393C4.4624 8.88743 5.22432 7.46197 6.34314 6.34315C7.46196 5.22433 8.88743 4.4624 10.4393 4.15372C11.9911 3.84504 13.5997 4.00346 15.0615 4.60896C16.5233 5.21447 17.7727 6.23984 18.6518 7.55544C19.5308 8.87103 20 10.4177 20 12C20 14.1217 19.1571 16.1566 17.6569 17.6569C16.1566 19.1571 14.1217 20 12 20Z" fill="#666666"></path>
                            <path d="M16 11H13V8C13 7.73478 12.8946 7.48043 12.7071 7.29289C12.5196 7.10536 12.2652 7 12 7C11.7348 7 11.4804 7.10536 11.2929 7.29289C11.1054 7.48043 11 7.73478 11 8V12C11 12.2652 11.1054 12.5196 11.2929 12.7071C11.4804 12.8946 11.7348 13 12 13H16C16.2652 13 16.5196 12.8946 16.7071 12.7071C16.8946 12.5196 17 12.2652 17 12C17 11.7348 16.8946 11.4804 16.7071 11.2929C16.5196 11.1054 16.2652 11 16 11Z" fill="#666666"></path>
                        </svg>
                        <div class="the-label">Login 3 jam yang lalu</div>
                    </li>
                </ul>
            </div>
            <div id="tab-disc" class="tab-pane">
                <br>
                @if(session()->get('token'))
                    @if(session('message')!='')
                    <div class="alert alert-primary" role="alert">
                        <b>Message : </b>{{ session('message')}}
                    </div>
                    @endif
                    <form action="/postCommentVideo/{{ $video->id }}" method="POST" class="mt-4 mb-4">
                        @csrf
                        <div class="form-group"><textarea class="form-control" placeholder="Tulis tanggapan di sini ..." style="height:100px" name="comment" id="comment" required=""></textarea></div>
                        <input type="hidden" name="id" value="{{ $video->id }}">
                        <input type="hidden" name="token" value="{{ session()->get('token') }}">
                        <div class="text-right"><button class="btn btn-cta mt-2">Kirim</button></div>
                    </form>
                @else
                <div class="card mb-4">
                    <div class="card-body">
                        <a href="{{ url('login') }}.html">Silahkan login untuk berkomentar</a>
                    </div>
                </div>
                @endif
                <h4 class="page-block-title text-primary mb-3">{{ count($comment) }} Diskusi Dalam Produk Ini</h4>
                @foreach($comment as $dataa)
                <div class="list-border">
                    <div class="d-flex">
                        <div class="rounded shadow no-shrink" style="background-image:url({{ $dataa->photo }});width:60px;height:60px;background-size:cover;background-position:center">
                        </div>
                        <div class="ml-3 mt-2 desc">
                            <h6 class="mb-1">{{ $dataa->name }}</h6>
                            <p class="mb-0 text-gray">{{ $dataa->created_at }}</p>
                            <p class="mt-3 mb-4">{{ $dataa->comment }}</p><a href="#">Reply</a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
</div>
</section>
@endsection

@push('scripts')
<script src="{{ asset('template/front/js/plyr.min.js') }}"></script>
<script>
    $(".more-videos").slick({
        slidesToShow: 2,
        slidesToScroll: 1
    });

    const player = new Plyr('#player');
</script>

@endpush
