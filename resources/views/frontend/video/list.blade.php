@extends('frontend.layout')
@section('title', $videos->category_name)
@section('content')
<section class="section">
    <div class="container">
        <div class="row">

            <div class="col-lg-3">
                <h2 class="section-title-sm mb-3">Pencarian Video</h2>
                <form action="/pencarian-produk.html" method="get">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" name="keyword" class="form-control" placeholder="Cari di sini ...">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-cta btn-normal">Cari</button>
                            </div>
                        </div>
                    </div>
                </form>
                <hr>
                <h2 class="section-title-sm mb-3">Kategori Video</h2>
                <ul class="cat-links">
                    @foreach($categories as $category)
                        <li><a href="/video/kategori/{{ $category->slug }}.html">{{ $category->name }}</a></li>
                    @endforeach
                </ul>
                <hr>

                <div class="w-100 bg-primary rounded mt-5" style="height: 200px;background-image: url('https://via.placeholder.com/350x200?text=Ads+Here');background-position: center;"></div>
            </div>

            <div class="col-lg-9">
                <div class="mb-4 d-flex align-items-center">
                    <h2 class="section-title-md">Video Dengan Kategori <i>{{ $videos->category_name }}</i></h2>
                    <div class="dropdown ml-auto">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown">Urutkan Berdasarkan</a>
                        <ul class="dropdown-menu">
                            <li><a href="" class="dropdown-item">Terbaru</a></li>
                            <li><a href="" class="dropdown-item">Terlama</a></li>
                            <li><a href="" class="dropdown-item">Termahal</a></li>
                            <li><a href="" class="dropdown-item">Termurah</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row cards-transparent mt-4 mb-4">
                    @foreach($videos->data as $video)
                    <div class="col-lg-4">
                        <div class="card card-shadow ">
                            <div class="card-image" style="background-image:url({{ $video->img_thumbnail}});background-position:center;background-size:cover;height:150px">
                                <div class="card-duration">03:50</div>
                            </div>
                            <div class="card-body">
                                <div class="card-category"><a href="{{ url('video/kategori/'.$video->category->slug) }}.html">{{ $video->category->name }}</a></div>
                                <h6 class="card-title"><a href="{{ url('video/'.$video->slug) }}.html">{{ $video->title}}</a></h6>
                                <p class="time">{{ $video->view}} Views</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>

                @pagination
            </div>
        </div>
    </div>
</section>

@endsection
