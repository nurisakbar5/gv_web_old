@extends('frontend.layout') 
@section('title','Petani TV') 
@section('content')
    <section class="section">
        <div class="container mx-auto">
            <div class="row cards-transparent mt-4">
                <div class="col-lg-8">
                <h2 class="section-title-sm mb-3 mt-4">Hasil Pencarian Video</h2>
                    <div class="row">
                    @if(!empty($videoSearch))
                        @foreach($videoSearch as $video)                        
                        <div class="col-lg-4">
                            <div class="card card-shadow ">
                                <div class="card-image" style="background-image:url({{ $video->img_thumbnail}});background-position:center;background-size:cover;height:150px">
                                    <div class="card-duration">03:50</div>
                                </div>
                                <div class="card-body">
                                    <div class="card-category"><a href="video/kategori/{{ $video->category->name }}.html">{{ $video->category->name }}</a></div>
                                    <h6 class="card-title"><a href="{{ url('video/'.$video->slug) }}.html">{{ $video->title}}</a></h6>
                                    <p class="time">{{ $video->view}} Views</p>
                                </div>
                            </div>
                        </div>
                        @endforeach 
                    @else
                        <h5 class="ml-3"><i>Maaf, video tidak ditemukan.</i></h5>
                    @endif
                    </div>
                </div>
                
                @include('frontend.video.rightcontent')
                
            </div>
        </div>
    </section>
@endsection

@push('scripts')
<script>
$(document).ready(function() {
    $(".carousel").slick({
        slidesToShow: 1,
        slidesToScroll: 1
    });
});
</script>
@endpush