<div class="col-lg-4">
    <h2 class="section-title-sm mb-3">Pencarian Video</h2>
    {{ Form::open(['url'=>'/video/pencarian-hasil-video.html'])}}
    <div class="form-group">
        <div class="input-group">
            <input type="text" name="keyword" class="form-control" placeholder="Cari di sini ...">
            <div class="input-group-append">
                <button type="submit" class="btn btn-primary btn-normal">Cari</button>
            </div>
        </div>
    </div>
    </form>
    <hr>

        <!-- Video Kategori -->
        <h2 class="section-title-sm mb-3 mt-4">Kategori Video</h2>
        <ul class="cat-links">
            @foreach ($video_category as $category)
            <li><a href="/video/kategori/{{ $category->slug }}.html">{{ $category->name }}</a></li>
            @endforeach
        </ul>
        
       

    <hr>

    <!-- Artikel Populer -->
    <h2 class="section-title-sm mb-3 mt-4">Artikel Populer</h2>
    <div class="list-posts mt-4">
        @foreach ($populars as $popular)
        <div class="post">
            <div class="post-image" style="background-image:url({{ $popular->image_url }});width:60px;height:60px;background-size:cover;background-position:center"></div>
            <div class="post-detail">
                <h4 class="post-title"><a href="{{ url('artikel', $popular->slug) }}.html">{{ $popular->title }}</a></h4>
                <div class="post-metas">
                    <div><a href="artikel/kategori/{{ $popular->category->name }}.html">{{ $popular->category->name }}</a></div>
                    <div>•</div>
                    <div class="normal">{{ $popular->publish_date }}</div>
                </div>
            </div>
        </div>
        @endforeach
    </div>

    <hr>


    
    <!-- Iklan -->
    <div class="mt-4">
        @foreach ($iklans as $iklan)
            <p><a href="{{ $iklan->link }}"><img src="{{ $iklan->image }}" width="300" /></a></p>
        @endforeach
    </div>
</div>