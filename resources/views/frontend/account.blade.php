@extends('frontend.layout')
@section('title', 'Checkout')
@section('content')
<section class="section">
    <div class="container">
        @include('frontend.cart.top')
        @include('alert')
        <div class="row mt-5 gutter-lg">
            <div class="col-lg-6 desc">
                {{ Form::open(['url'=>'checkout'])}}

                <h2 class="section-title-sm mb-4">Informasi Pengiriman</h2>
                <div class="form-group">
                    <div class="row">
                        <div class="form-group col-lg-6">
                            <label>Pilih Alamat Pengiriman</label>
                            <select onchange="showDeliveryDestination()" id="deliveryDestination" class="form-control" name="address" style="padding: 0 !important;">
                                @foreach($userDeliveryDestantionList->data as $DeliveryDestantion)
                                <option value="{{ $DeliveryDestantion->id}}">{{ $DeliveryDestantion->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-lg-6">
                            <a href="" class="btn btn-primary mt-4">Tambah Alamat</a>
                        </div>
                    </div>
                </div>

                <div id="alamat_pengiriman">
                </div>


                {{--  Menampilkan Daftar Produk Yang Dibeli  --}}
                <hr>
                <h2 class="section-title-sm bordered d-flex">Order Summary</h2>
                @foreach($getItems as $store)
                <div class="row">
                    <div class="col-md-6">
                            <input type="hidden" name="store[]" value="{{ $store->id }}">
                            <b>{{ $store->name}}</b><br>
                            <p>{{ $store->subdistrict_name}}, {{ $store->city}}, {{ $store->province}}</p>
                    </div>

                    <div class="col-md-6">
                            <select name="courier[]" class="form-control" onchange="pilihService('{{ $store->id}}',{{ $store->subdistrict_id}})"   id="kurir-{{ $store->id}}">
                                    <option value="0">--Pilih Kurir--</option>
                                    @foreach($store->courier as $courier_active)
                                    <option value="{{ $courier_active->courier_code }}">{{ $courier_active->courier_code }}</option>
                                    @endforeach
                                </select> 
                                <br>
                                <div id="ongkir-{{ $store->id}}">
                                </div>
                    </div>
                </div>
                <?php $berat = 0; ?>
                @foreach($store->product_in_cart as $item)

                    <?php
                    $total = $item->qty*$item->price;
                    $berat = $item->qty*($item->weight*$item->weight_unit_in_gram);
                    ?>
                    <div class="d-flex mb-4 align-items-center">
                        <img width="100" class="rounded" src="{{ $item->image }}">
                        <div class="ml-3">
                            <h6 class="mb-0">{{ $item->name }}</h6>
                            <ul class="d-flex list-unstyled mx-n2 mt-2 mb-0">
                                <li class="mx-2">Qty: <span id="qty">{{ $item->qty }}</span></li>
                                <li class="mx-2">Rp. {{ $item->price }} | {{ $item->weight*$item->weight_unit_in_gram }}</li>
                                {{-- <span id="subtotal"></span> --}}
                            </ul>
                        </div>
                    </div>
                    @endforeach
                    <input type="hidden" name="berat[]" id="berat-{{ $store->id }}" value="{{ $berat }}">
                    <input type="text" name="note[]" placeholder="Catatan Untuk Pembeli" class="form-control">
                    <hr class="my-4">
                @endforeach
                <hr>
                
                <input type="hidden" name="token" value="{{ session()->get('token') }}">
                <hr class="mb-5 mt-5">
                <h2 class="section-title-sm mb-4">Pilih Metode Pembayaran</h2>
                <div class="row">
                    @foreach($paymentChannels->data as $paymentChannel)
                    <div class="col-lg-4">
                        <label class="imagecheck w-100">
                            <input class="imagecheck-input" type="radio" name="payment_type_id" value="{{ $paymentChannel->bankCode.'-'.$paymentChannel->productCode}}" required>
                            <figure class="imagecheck-figure p-2" style="background-image: url(https://ws.globalvillage.id/logobank/{{ $paymentChannel->bankCode }}.png);height: 80px;background-size: 100%;background-position: center;background-repeat: no-repeat;">
                            </figure>
                            <div class="text-center mt-2 font-weight-bold">
                                {{ ucwords(strtolower($paymentChannel->productName)) }}
                            </div>
                        </label>
                    </div>
                    @endforeach
                </div>
                <hr class="my-4">

                <div class="row">
                    <div class="col-sm-6">
                        <a href="{{ url('cart') }}.html" class="btn btn-outline-cta btn-block">
                            &lsaquo; Perbarui Cart
                        </a>
                    </div>
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-cta btn-block">Proses Checkout</button>
                    </div>
                </div>
            </div>
            {{ Form::close()}}

            <div class="col-lg-6">
                

                <hr class="mt-4 mb-0">
                <div class="total">
                    <div class="total-row">
                        <div class="total-name">Subtotal</div>
                        <div class="total-value">Rp. <span id="ssubtotal"></span></div>
                    </div>
                    <div class="total-row">
                        <div class="total-name">PPN</div>
                        <div class="total-value">Rp. 1000</div>
                        {{-- <span id="ppn"></span> --}}
                    </div>
                    <div class="total-row">
                        <div class="total-name">Ongkir</div>
                        <div class="total-value">Rp. 1000</div>
                        {{-- <span id="ongkir"></span> --}}
                    </div>
                    <div class="total-row">
                        <div class="total-name">Total</div>
                        <div class="total-value">Rp. 1000</div>
                        {{-- <span id="total"></span> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script>
    function showDeliveryDestination()
    {
        var id  = $("#deliveryDestination").val();

        $.ajax({
            url: "/order/address",
            data:'id='+id,
            type: 'GET',
            success: function(res) {
                $("#alamat_pengiriman").html(res);
                //console.log(res);
                //alert(res);
            }
        });
    }
</script>


<script>

    function pilihService(store_id,destination)
    {
        var origin      = $("#address_id").val();
        console.log(origin);
        console.log(destination);
        var weight      = $("#berat-"+store_id).val();
        var courier     = $("#kurir-"+store_id).val();
        $.ajax({
            url: "/rajaongkir/cost",
            data: {
                "_token": "{{ csrf_token() }}",
                "id": 'sasas',
                "destination":destination,
                "store_id":store_id,
                "origin":origin,
                "weight":weight,
                "courier":courier
                },
            type: 'POST',
            success: function(res) {
                $("#ongkir-"+store_id).html(res);
                //console.log(res);
                //alert(res);
            }
        });
    }


    $(document).ready(function() {

        showDeliveryDestination();

        $("#subtotal").text(sessionStorage.subtotal);

        $("#ssubtotal").text(sessionStorage.subtotal);

        $("#ppn").text(sessionStorage.ppn);

        $("#ongkir").text(sessionStorage.ongkir);

        $("#total").text(sessionStorage.total);
    });
</script>
@endpush
