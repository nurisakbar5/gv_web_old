@extends('frontend.layout')
@section('title', 'Keranjang Belanja')
@push('css')
<link href="{{ asset('template/front/css/select2.min.css') }}" rel="stylesheet" />
@endpush
@section('content')
<section class="section">
    <div class="container">
        @include('frontend.cart.top')
        <div class="row mt-5">
        	<div class="col-lg-8">
		        @if(isset($getItems))
		        <h2 class="section-title-sm bordered mb-4">Keranjang</h2>
		        @foreach($getItems as $items)
		        <div class="card mb-5">
		        	<div class="card-body">
				        <div class="d-flex align-items-start mb-2">
		                	<svg width="30" class="fill-current text-primary mr-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 616 512"><path fill="currentColor" d="M602 118.6L537.1 15C531.3 5.7 521 0 510 0H106C95 0 84.7 5.7 78.9 15L14 118.6c-29.6 47.2-10 110.6 38 130.8v227.4c0 19.4 14.3 35.2 32 35.2h448c17.7 0 32-15.8 32-35.2V249.4c48-20.2 67.6-83.6 38-130.8zm-70 358.2c0 2-.8 3.1-.2 3.2l-446.6.3c-.3-.2-1.2-1.3-1.2-3.5V352h448zM84 320v-64h2.5c29.6 0 55.8-13 73.8-33.1 18 20.1 44.3 33.1 73.8 33.1 29.6 0 55.8-13 73.8-33.1 18 20.1 44.3 33.1 73.8 33.1 29.6 0 55.8-13 73.8-33.1 18.1 20.1 44.3 33.1 73.9 33.1h2.5v64zm494.2-126.5c-7.8 16.6-22.1 27.5-39.3 29.8-3.1.4-6.2.6-9.4.6-19.3 0-37-8-50-22.5L455.7 175l-23.8 26.6c-13 14.5-30.7 22.5-50 22.5s-37-8-50-22.5L308 175l-23.8 26.6c-13 14.5-30.7 22.5-50 22.5s-37-8-50-22.5L160.3 175l-23.8 26.6c-13 14.5-30.7 22.5-50 22.5-3.2 0-6.3-.2-9.4-.6-17.2-2.3-31.5-13.2-39.3-29.8-8.7-18.6-7.5-40.8 3.3-57.9L106 32h404l64.9 103.6c10.8 17.2 12 39.3 3.3 57.9z"></path></svg>
		                	<div>
			                	<h4 class="section-title-sm mb-1 mt-0 no-lh">{{ $items->name }}</h4>
			                	<p class="text-muted">{{$items->subdistrict_name}}, {{$items->city}}, {{$items->province}}</p>
			                </div>
				        </div>
		                @foreach($items->product_in_cart as $i => $item)
		                <div id='item' class="row mt-4">
		                	<div class="col-auto">
		                		<div style="width: 60px;height: 60px;background-image: url({{ $item->image }});" class="img-bg rounded"></div>
		                	</div>
		                    <div class="col-lg-5">
		                    	<h6 class="card-title clamp" title="{{ $item->name }}">{{ $item->name }}</h6>
		                    	Rp. <span id="qty_price-{{ $item->id }}">{{ $item->price }}</span>
		                    </div>
		                    <div class="col-auto">
		                    	<input id="qty_input-{{ $item->id }}" type="number" value="{{ $item->qty }}" class="form-control form-control-sm" min="1" style="width: 80px;">
		                    </div>
		                    <div class="col text-right">Rp. <span class="subtotal" id="qty_subtotal-{{ $item->id }}">{{ $item->price }}</span></div>
		                    <div class="col-auto">
		                        {{ Form::open(['url'=>'deleteProductInCart/'.$item->id,'method'=>'delete','onsubmit'=>"return confirm('Yakin Ingin Menghapus Belanja Ini?')"])}}
		                        <input type="hidden" id="id" name="id" value="{{ $item->id }}">
		                        <button type="submit" class="btn btn-sm btn-danger">&times;</button>
		                        {{ Form::close()}}
		                    </div>
		                </div>
		                @if($i != count($items->product_in_cart)-1)
		                <hr class="mt-4">
		                @endif
		                @endforeach
		        	</div>
		        </div>
		        @endforeach
		        @endif
        	</div>
        	<div class="col-lg-4">
        		<div class="card card-primary card-shadow">
        			<div class="card-body">
		        		<h2 class="section-title-sm bordered">Ringkasan</h2>
			            <div class="total-row">
			                <div class="total-name">Total</div>
			                <div class="total-value">Rp. <span id="subtotal"></span></div>
			            </div>
		                <a href="{{ url('account') }}.html" id="proses_checkout" class="btn btn-cta mt-4 btn-block">
		                    Bayar Semua
		                </a>
        			</div>
        		</div>
        	</div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script src="{{ asset('template/front/js/select2.min.js') }}"></script>
<script>
    $(document).ready( function() {

        // Load Cart Total Pertama Kali
        $('input[id=id]').each( function() {
            var subtotal = 0;
            $('.subtotal').each(function() {
                subtotal += parseFloat($(this).text());
            });
            $("#subtotal").text(subtotal);

            var ppn = parseInt($('#ppn').text());
            var ongkir = parseInt($('#ongkir').text());
            var total = subtotal + ppn + ongkir;
            $("#total").text(total);

            sessionStorage.setItem("subtotal", subtotal);
            sessionStorage.setItem("ppn", ppn);
            sessionStorage.setItem("ongkir", ongkir);
            sessionStorage.setItem("total", total);
        });

        function qtyInput(id_item) {
            var qtyInput = parseInt($("#qty_input-" + id_item).val());
            return qtyInput;
        }

        function qtyPrice(id_item) {
            var qtyPrice = parseInt($("#qty_price-" + id_item).text());
            return qtyPrice;
        }

        function qtySubtotal(id_item) {
            var qtySubtotal = parseInt($("#qty_subtotal-" + id_item).text());
            return qtySubtotal;
        }

        // updateProductQty Setiap Ada Perubahan
        $('input[type=number]').on('change keyup', function() {
            $('input[id=id]').each( function() {
                var id = $(this).val();

                var price = qtyPrice(id)
                var input = qtyInput(id);
                var subtotal = price * input;
                $("span[id=qty_subtotal-" + id + "]").text(subtotal);

                var subtotal = 0;
                $('.subtotal').each(function() {
                    subtotal += parseFloat($(this).text());
                });
                $("#subtotal").text(subtotal);

                var subtotal = parseInt($("#subtotal").text());
                var ppn = parseInt($('#ppn').text());
                var ongkir = parseInt($('#ongkir').text());
                var total = subtotal + ppn + ongkir;
                $("#total").text(total);

                sessionStorage.setItem("subtotal", subtotal);
                sessionStorage.setItem("ppn", ppn);
                sessionStorage.setItem("ongkir", ongkir);
                sessionStorage.setItem("total", total);

                // AJAX updateProductQty
                $.ajax({
                    url: "updateProductQtyInCart",
                    type: "POST",
                    data: {
                        "_token" : "{{ csrf_token() }}",
                        //"_method" : "PUT",
                        "id" : id,
                        "qty" : qtyInput(id),
                        "token" : "{{ session()->get('token') }}"
                    },
                    success: function(response) {
                        console.log("success : ", response.message);
                        //console.log(response);
                    },
                    error: function(response) {
                        console.log("error : ", response.message);
                    }
                });
            });
        });
    });
</script>
@endpush
