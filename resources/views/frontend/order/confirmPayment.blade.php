@extends('frontend.layout')
@section('title', 'Konfirmasi Pembayaran')
@push('css')
<link rel="stylesheet" href="https://unpkg.com/file-upload-with-preview@4.0.2/dist/file-upload-with-preview.min.css">
@endpush
@section('content')
<section class="section">
    <div class="container">
        @include('alert')

        <h1 class="section-title-md mb-4">Konfirmasi Pembayaran</h1>
        <br>
        <form method="post" action="/confirmPayment" files="true">
        @csrf
            <input type="hidden" name="user_id" value="{{ session()->get('token') }}"/>

            <div class="row">
                <div class="col-lg-4">
                    <h4 class="section-title-sm">No. Invoice</h4>
                    <p class="text-muted">Masukan nomor pemesanan.</p>
                </div>
                <div class="col-lg-8">
                    <div class="form-group {{ $errors->first('no_invoice') ? "is-invalid": ""}}">
                        <label class="{{ $errors->first('no_invoice') ? "text-danger": ""}}">No. Invoice</label>
                        <input type="text" class="form-control" placeholder="No. Invoice" name="no_invoice" required>
                        <span class="text-danger">
                            {{ $errors->first('no_invoice') }}
                        </span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4">
                    <h4 class="section-title-sm">Bukti Pembayaran</h4>
                    <p class="text-muted">Sertakan gambar terkait pembayaran Anda.</p>
                </div>
                <div class="col-lg-8">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-9">
                                <div class="custom-file-container" data-upload-id="myUniqueUploadId-1">
                                    <label>Bukti Pembayaran <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">&times;</a></label>
                                    <span class="text-danger">
                                        {{ $errors->first('image_confirm') }}
                                    </span>
                                    <label class="custom-file-container__custom-file" >
                                        <input type="file" name='image_confirm' class="custom-file-container__custom-file__custom-file-input" accept="*" aria-label="Choose File">
                                        <input type="hidden" name="MAX_FILE_SIZE" value="10485760" />
                                        <span class="custom-file-container__custom-file__custom-file-control"></span>
                                    </label>
                                    <div class="custom-file-container__image-preview"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group text-right">
                <button class="btn btn-cta" type="submit"><i class="fas fa-save"></i> Konfirmasi</button>
            </div>
            
        </form>
    </div>
</section>
@endsection

@push('scripts')
<script src="https://unpkg.com/file-upload-with-preview@4.0.2/dist/file-upload-with-preview.min.js"></script>

<script>
$(document).ready( function() {
    var upload = new FileUploadWithPreview('myUniqueUploadId-1');
});
</script>
@endpush