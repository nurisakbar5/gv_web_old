@extends('frontend.layout')
@section('title','Jual Beli Produk')
@section('content')
<section class="section">
    <div class="container">
        <div class="row">

            @include('frontend.products.leftcontent')

            <div class="col-lg-9">
                <div class="d-flex align-items-center">
                    <h2 class="section-title-md">Hasil Pencarian Produk</h2>
                    <div class="dropdown ml-auto">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown">Urutkan Berdasarkan</a>
                        <ul class="dropdown-menu">
                            <li><a href="" class="dropdown-item">Terbaru</a></li>
                            <li><a href="" class="dropdown-item">Terlama</a></li>
                            <li><a href="" class="dropdown-item">Termahal</a></li>
                            <li><a href="" class="dropdown-item">Termurah</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row mt-4">
                    <!--
                        ==================================
                        PRODUCT
                        ==================================
                    -->
                    @if (!empty($productSearch))
                    @foreach($productSearch as $product)
                    <div class="col-lg-4 mb-5">
                        <div class="card card-shadow ">
                            <div class="card-image" style="background-image:url({{ $product->image_url }});background-position:center;background-size:cover;height:180"></div>
                            <div class="card-body">
                                <h6 class="card-title"><a href="{{ url('produk', $product->slug) }}.html">{{ $product->name }}</a></h6>
                                <div class="card-price">Rp. {{ $product->price_idr }}</div>
                                <div class="card-author">
                                    <div class="card-author-image" style="background-image:url({{ $product->store->image }});background-position:center;background-size:cover"></div>
                                    <div class="card-author-detail">
                                        <div class="card-author-name">{{ $product->store->name }}</div>
                                        <div class="card-author-desc">
                                            {{ $product->store->region->type }}
                                            {{ $product->store->region->city }},
                                            {{ $product->store->region->province }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @else
                        <h5 class="section-title-md" style="margin-left:10px;"><i>Maaf, produk yang anda cari tidak ditemukan.</i></h5>
                    @endif
                    <!--
                        ==================================
                        END PRODUCT
                        ==================================
                    -->
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
