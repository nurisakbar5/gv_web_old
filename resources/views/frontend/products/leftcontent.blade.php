@push('css')
<link href="{{ asset('template/front/css/select2.min.css') }}" rel="stylesheet" />
<style>
option.disabled {
  pointer-events: none;
  cursor: default;
}
</style>
@endpush
<div class="col-lg-3">
    <h2 class="section-title-sm mb-2">Pencarian Produk</h2>
    {{ Form::open(['url'=>'/produk/pencarian-hasil-produk.html'])}}
    <div class="form-group">
        <div class="input-group">
            <input type="text" name="keyword" class="form-control" placeholder="Cari di sini ...">
            <div class="input-group-append">
                <button type="submit" class="btn btn-cta btn-normal">Cari</button>
            </div>
        </div>
    </div>
    </form>

    <h2 class="section-title-sm mb-2 mt-5">Lokasi</h2>
    <form>
        @if(request()->regency)
            <input type="hidden" name="regency" value="{{ request()->regency }}">
        @endif
        <select name="regency" id="regency" class="cari form-control mb-3" required></select>
        <br><br>
        <button class="btn btn-block btn-normal btn-cta">Filter</button>
    </form>

    <h2 class="section-title-sm mb-2 mt-5">Kategori Produk</h2>
    <ul class="links">
        @foreach($categories as $category)
            <li>
                <a href="/produk/kategori/{{ $category->slug }}.html">{{ $category->name }}</a>
            </li>
        @endforeach
    </ul>

    <div class="w-100 bg-primary rounded mt-5" style="height: 200px;background-image: url('https://via.placeholder.com/350x200?text=Ads+Here');background-position: center;"></div>
</div>

@push('scripts')
<script src="{{ asset('template/front/js/select2.min.js') }}"></script>
<script>
$('.cari').select2({
    placeholder: 'Pilih Berdasarkan Kabupaten/Kota',
    ajax: {
        url: '/searchCity',
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
            return {
                results:  $.map(data, function (item) {
                    return {
                        text: item.type +' '+ item.name +', '+ item.province_name,
                        id: item.id
                    }
                })
            };
        },
        cache: true
    }
});
</script>
@endpush
