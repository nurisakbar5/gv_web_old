@extends('frontend.layout')
@section('title','Halaman Jual Beli')
@section('content')
<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="card card-bg card-featured" style="background-image:url(https://images.unsplash.com/photo-1489450278009-822e9be04dff?ixlib=rb-1.2.1&amp;auto=format&amp;fit=crop&amp;w=752&amp;q=80)">
                    <div class="card-body">
                        <h4 class="mb-3">Lihat Semua Produk Sayur-Sayuran</h4><a class="btn btn-cta btn-wide">Lihat Produk</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card card-bg card-featured" style="background-image:url(https://images.unsplash.com/photo-1488459716781-31db52582fe9?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=750&amp;q=80)">
                    <div class="card-body">
                        <h4 class="mb-3">Lihat Semua Produk Buah-Buahan</h4><a class="btn btn-cta btn-wide">Lihat Produk</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-4">
                <h2 class="section-title-md">Terbaru</h2>
            </div>
            <div class="col-lg-8 text-right">
                <div class="btn-group"><button class="btn btn-normal btn-primary">Penjualan</button><button class="btn btn-normal btn-outline-primary">Permintaan</button><button class="btn btn-normal btn-outline-primary">Kerjasama</button></div>
            </div>
        </div>
        <div class="row mt-4">
            @foreach($latest as $product)
            @if($product->user->id == session()->get('id'))
                @continue
            @endif
            <div class="col-lg-3">
                <div class="card card-shadow ">
                    <div class="card-image" style="background-image:url({{ $product->image_url}});background-position:center;background-size:cover;height:180"></div>
                    <div class="card-body">
                        <h6 class="card-title"><a href="produk/{{ $product->slug}}.html">{{ $product->name}}</a></h6>
                        <div class="card-price">Rp.{{ $product->price_idr}}</div>
                        <div class="card-author">
                            <div class="card-author-image" style="background-image:url({{ $product->store->image}});background-position:center;background-size:cover"></div>
                            <div class="card-author-detail">
                                <div class="card-author-name">{{ $product->store->name }}</div>
                                <div class="card-author-desc">{{ $product->store->region->subdistrict_name }},
                                        {{ $product->store->region->type }}
                                        {{ $product->store->region->city }},
                                        {{ $product->store->region->province }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="text-center mt-5"><a class="btn btn-cta" href="/produk.html">Lihat Semua</a></div>
    </div>
</section>
@foreach($productGroupByCategories as $category)
<section class="section">
    <div class="container">
        <div class="card bg-image card-block card-block-p0 need-overlay" style="background-image:url({{ $category->image_web}})">
            <div class="row align-items-center need-overlay-layer">
                <div class="col-lg-4">
                    <div class="card-body text-white">
                        <h2>{{ $category->name}}</h2>
                        <p>Lihat semua produk {{ $category->name}} yang tersedia di toko kami.</p><a class="btn btn-cta btn-wide mt-3" href="/produk/kategori/{{ $category->slug}}.html">Lihat Semua</a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="card-block-slider store-carousel no-overlap">
                        @foreach($category->products->data as $product)

                        <div>
                            <div class="card card-shadow ">
                                <div class="card-image" style="background-image:url({{ $product->image_url }});background-position:center;background-size:cover;height:180"></div>
                                <div class="card-body">
                                    <h6 class="card-title"><a href="jual-beli/{{ $product->slug }}.html">{{ $product->name }}</a></h6>
                                    <div class="card-price">Rp.{{ $product->price_idr }}</div>
                                    <div class="card-author">
                                        <div class="card-author-image" style="background-image:url({{ $product->store->image }});background-position:center;background-size:cover"></div>
                                        <div class="card-author-detail">
                                            <div class="card-author-name">{{ $product->store->name }}</div>
                                            <div class="card-author-desc"> {{ $product->store->region->type }}
                                                    {{ $product->store->region->city }},
                                                    {{ $product->store->region->province }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endforeach

@endsection

@push('scripts')
<script>
$(document).ready( function() {
    $(".store-carousel").slick({
        slidesToShow: 2,
        slidesToScroll: 1
    });
});
</script>
@endpush
