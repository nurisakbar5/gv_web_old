@extends('frontend.layout')

@section('title') Lupa Password @endsection

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css')}}">
@endpush

@section('content')
<section class="section">
        <div class="container">
        	@if(session('message')!='')
				<div class="alert alert-primary" role="alert">
					<b>Pesan : </b>{{ session('message')}}
				</div>
				@endif
        	<div class="row justify-content-center">
        		<div class="col-lg-5">
					<div class="card fat">
						<div class="card-body">
							<h1 class="card-title">Forgot Password</h1>
							{{ Form::open(['url'=>'/goForgot','class'=>'needs-validation','novalidate'=>''])}}
								<div class="form-group">
									<label for="email">E-Mail Address</label>
									<input id="email" type="email" class="form-control" name="email" value="" required autofocus>
									<div class="has-warning">
										{{ $errors->first('email') }}
									</div>
									<p class="text-muted mt-3 mb-0">By clicking "Reset Password" we will send a password reset link</p>
								</div>

								<div class="form-group">
									<button type="submit" class="btn btn-cta btn-block">
										Reset
									</button>
								</div>
								<div class="mt-4 text-center">
									Remember your password? <a href="/login.html">Login</a>
								</div>
							</form>
						</div>
					</div>
        		</div>
        	</div>
        </div>
    </section>
@endsection