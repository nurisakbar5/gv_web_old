@if (Request::segment(1) == 'artikel.html')
{{ Form::open(['url'=>'/artikel/pencarian-hasil-artikel.html', 'class' => 'form-inline search-form mr-2'])}}
<div class="input-group"><input type="text" name="keyword" class="form-control search-control" placeholder="Cari Artikel di sini ..." />
</div>
{{ Form::close() }}
@elseif(Request::segment(1) == 'video.html')
{{ Form::open(['url'=>'/video/pencarian-hasil-video.html', 'class' => 'form-inline search-form mr-2'])}}
<div class="input-group"><input type="text" name="keyword" class="form-control search-control" placeholder="Cari Video di sini ..." />
</div>
{{ Form::close() }}
@elseif(request()->is('artikel/pencarian-hasil-artikel.html'))
{{ Form::open(['url'=>'/artikel/pencarian-hasil-artikel.html', 'class' => 'form-inline search-form mr-2'])}}
<div class="input-group"><input type="text" name="keyword" class="form-control search-control" placeholder="Cari Artikel di sini ..." />
</div>
{{ Form::close() }}
@elseif(request()->is('video/pencarian-hasil-video.html'))
{{ Form::open(['url'=>'/video/pencarian-hasil-video.html', 'class' => 'form-inline search-form mr-2'])}}
<div class="input-group"><input type="text" name="keyword" class="form-control search-control" placeholder="Cari Video di sini ..." />
</div>
{{ Form::close() }}
@elseif(Request::segment(1) == 'jual-beli.html')
{{ Form::open(['url'=>'/produk/pencarian-hasil-produk.html', 'class' => 'form-inline search-form mr-2'])}}
<div class="input-group"><input type="text" name="keyword" class="form-control search-control" placeholder="Cari Produk di sini ..." />
</div>
{{ Form::close() }}
@elseif(request()->is('produk/pencarian-hasil-produk.html'))
{{ Form::open(['url'=>'/produk/pencarian-hasil-produk.html', 'class' => 'form-inline search-form mr-2'])}}
<div class="input-group"><input type="text" name="keyword" class="form-control search-control" placeholder="Cari Produk di sini ..." />
</div>
{{ Form::close() }}
@elseif(Request::segment(1) == 'produk.html')
{{ Form::open(['url'=>'/produk/pencarian-hasil-produk.html', 'class' => 'form-inline search-form mr-2'])}}
<div class="input-group"><input type="text" name="keyword" class="form-control search-control" placeholder="Cari Produk di sini ..." />
</div>
{{ Form::close() }}
@elseif(Request::segment(1) == 'panen.html')
{{ Form::open(['url'=>'/panen/pencarian-hasil-panen.html', 'class' => 'form-inline search-form mr-2'])}}
<div class="input-group"><input type="text" name="keyword" class="form-control search-control" placeholder="Cari Hasil Panen di sini ..." />
</div>
{{ Form::close() }}
@elseif(request()->is('panen/pencarian-hasil-panen.html'))
{{ Form::open(['url'=>'/panen/pencarian-hasil-panen.html', 'class' => 'form-inline search-form mr-2'])}}
<div class="input-group"><input type="text" name="keyword" class="form-control search-control" placeholder="Cari Hasil Panen di sini ..." />
</div>
{{ Form::close() }}
@elseif(Request::segment(1) == 'desa.html')
{{ Form::open(['url'=>'/panen/pencarian-hasil-panen.html', 'class' => 'form-inline search-form mr-2'])}}
<div class="input-group"><input type="text" name="keyword" class="form-control search-control" placeholder="Cari Hasil Panen di sini ..." />
</div>
{{ Form::close() }}
@elseif(Request::segment(1) == 'forum.html')
{{ Form::open(['url'=>'/forum/pencarian-hasil-forum.html', 'class' => 'form-inline search-form mr-2'])}}
<div class="input-group"><input type="text" name="keyword" class="form-control search-control" placeholder="Cari Forum di sini ..." />
</div>
{{ Form::close() }}
@elseif(request()->is('forum/pencarian-hasil-forum.html'))
{{ Form::open(['url'=>'/forum/pencarian-hasil-forum.html', 'class' => 'form-inline search-form mr-2'])}}
<div class="input-group"><input type="text" name="keyword" class="form-control search-control" placeholder="Cari Forum di sini ..." />
</div>
{{ Form::close() }}
@else
{{ Form::open(['url'=>'/artikel/pencarian-hasil-artikel.html', 'class' => 'form-inline search-form mr-2'])}}
<div class="input-group"><input type="text" name="keyword" class="form-control search-control" placeholder="Cari di sini ..." />
</div>
{{ Form::close() }}
@endif
