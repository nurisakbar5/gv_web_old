@extends('frontend.layout') 
@section('title', 'Baca Artikel Pengetahuan') 
@section('content')
<section class="section-pad">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="carousel-slide" style="background-image:url({{ $populars[0]->image_url }});height:400px">
                    <div class="carousel-slide-content">
                        <h3><a class="text-white" href="{{ url('artikel/' . $populars[0]->slug . '.html') }}">{{ $populars[0]->title }}</a></h3>
                        <p class="text-uppercase text-opacity">{{ $populars[0]->publish_date }}</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <h2 class="section-title-sm">Artikel Populer</h2>
                <div class="list-posts mt-2">
                    @foreach (array_slice($populars, 1, 3) as $popular)
                    <div class="post align-items-start mb-3">
                        <div class="post-image" style="background-image:url({{ $popular->image_url }});width:80px;height:80px;background-size:cover;background-position:center"></div>
                        <div class="post-detail">
                            <h4 class="post-title"><a href="{{ url('artikel', $popular->slug) }}.html">{{ $popular->title }}</a></h4>
                            <div class="post-metas">
                                <div><a href="{{ url('artikel/kategori/'.$popular->category->slug) }}.html">{{ $popular->category->name }}</a></div>
                            </div>
                            <p class="time mt-1">Dipublikasikan Pada {{ $popular->publish_date}}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section-pad pt-0">
    <div class="container">
        <h2 class="section-title-md">Artikel Terbaru</h2>
        <div class="row mt-3">
            <div class="col-lg-8">
                <div class="row cards-transparent">
                    @foreach(array_slice($latests, 0, 2) as $latest)
                    <div class="col-lg-6">
                        <div class="card card-shadow ">
                            <div class="card-image" style="background-image:url({{ $latest->image_url }});background-position:center;background-size:cover;height:180"></div>
                            <div class="card-body">
                                <div class="card-category"><a href="{{ url('artikel/kategori/'.$latest->category->slug) }}.html">{{ $latest->category->name }}</a></div>
                                <h6 class="card-title"><a href="artikel/{{ $latest->slug}}.html">
                                {{ $latest->title }}</a></h6>
                                <p class="time">Ditulis Oleh Nuris Akbar Pada {{ $latest->publish_date}}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card card-fat border-transparent">
                    <div class="card-body bg-gray text-center">
                        <svg style="width:80px;fill:var(--primary)" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                            <g data-name="Layer 2">
                                <g data-name="email">
                                    <rect width="24" height="24" opacity="0"></rect>
                                    <path d="M19 4H5a3 3 0 0 0-3 3v10a3 3 0 0 0 3 3h14a3 3 0 0 0 3-3V7a3 3 0 0 0-3-3zm-.67 2L12 10.75 5.67 6zM19 18H5a1 1 0 0 1-1-1V7.25l7.4 5.55a1 1 0 0 0 .6.2 1 1 0 0 0 .6-.2L20 7.25V17a1 1 0 0 1-1 1z"></path>
                                </g>
                            </g>
                        </svg>
                        <h6 class="pt-3 pb-2">Daftarkan email Anda &amp; jadilah yang pertama tahu!</h6>
                        <div class="input-group">
                            <input class="form-control form-control-bg" placeholder="Email Anda" style="background-image:url(&quot;data:image/svg+xml,%3Csvg xmlns=&#x27;http://www.w3.org/2000/svg&#x27; viewBox=&#x27;0 0 24 24&#x27;%3E%3Cg data-name=&#x27;Layer 2&#x27;%3E%3Cg data-name=&#x27;email&#x27;%3E%3Crect width=&#x27;24&#x27; height=&#x27;24&#x27; opacity=&#x27;0&#x27;/%3E%3Cpath d=&#x27;M19 4H5a3 3 0 0 0-3 3v10a3 3 0 0 0 3 3h14a3 3 0 0 0 3-3V7a3 3 0 0 0-3-3zm-.67 2L12 10.75 5.67 6zM19 18H5a1 1 0 0 1-1-1V7.25l7.4 5.55a1 1 0 0 0 .6.2 1 1 0 0 0 .6-.2L20 7.25V17a1 1 0 0 1-1 1z&#x27;/%3E%3C/g%3E%3C/g%3E%3C/svg%3E&quot;)" />
                            <div class="input-group-append">
                                <button class="btn btn-cta btn-normal">Kirim</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section-pad pt-0">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <h2 class="section-title-md">Lebih Banyak Artikel</h2>
                <div class="mt-3 cards-transparent list-border">
                    @foreach ($latests as $related)
                    <div class="card card-shadow card-row">
                        <div class="card-image" style="background-image:url({{ $related->image_url }});background-position:center;background-size:cover;height:180"></div>
                        <div class="card-body py-0">
                            <div class="card-category"><a href="{{ url('artikel/kategori/'.$related->category->slug) }}.html">{{ $related->category->name }}</a></div>
                            <h6 class="card-title"><a href="{{ url('artikel', $related->slug)}}.html">{{ $related->title }}</a></h6>
                            <p class="time text-muted mb-0">Ditulis Oleh Nuris Akbar Pada {{ $related->publish_date }}</p>
                            <div class="desc mt-n4">
                                <p class="mt-0">{!! substr($related->article,0,160)!!}</p>
                            </div>
                            <div class="text-right"><a href="{{ url('artikel', $related->slug)}}.html">Selengkapnya ›</a></div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-lg-4">
                <h2 class="section-title-sm">Paling Banyak Dibaca</h2>
                <div class="list-posts mt-4">
                    @foreach ($paling as $p)
                    <div class="post align-items-start">
                        <div class="post-image" style="background-image:url({{ $p->image_url }});width:80px;height:80px;background-size:cover;background-position:center"></div>
                        <div class="post-detail">
                            <h4 class="post-title"><a href="artikel/{{ $p->slug}}.html">{{ $p->title}}</a></h4>
                            <div class="post-metas">
                                <div><a href="{{ url('artikel/kategori/'.$p->category->slug) }}.html">{{ $p->category->name}}</a></div>
                            </div>
                            <p class="time mt-1">Dipublikasikan Pada {{ $p->publish_date}}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endsection 