@extends('frontend.layout')
@section('seo')
{!! SEO::generate(true) !!}
@endsection
@section('content')
    <section class="section mt-0">
        <div class="card-image-featured need-overlay" style="background-image:url({{ $article->image_url }})">
            <div class="need-overlay-layer d-flex flex-column align-items-center justify-content-center h-100">
                <div class="card-category"><a href="{{ (url('artikel/kategori/'.$article->category->slug)) }}.html">{{ $article->category->name }}</a></div>
                <h1 class="page-title text-white">{{ $article->title }}</h1>
                <p class="text-uppercase text-opacity text-white">{{ $article->publish_date }}</p>
            </div>
        </div>
    </div>
</section>
<section class="section">
    <div class="container">
        <div class="row gutter-lg">
            <div class="col-lg-8 desc desc-article">
                {!! $article->article !!}

                <div class="d-flex align-items-center">
                    <div>
                        <div class="text-uppercase mr-3 text-primary">Share</div>
                        <ul class="social-links pl-0 mt-2">
                            <li><a class="social-links-item social-links-item-facebook" href="#"><svg xmlns="http://www.w3.org/2000/svg" class="social-links-icon" viewBox="0 0 24 24">
                                <g data-name="Layer 2">
                                    <g data-name="facebook">
                                        <rect width="24" height="24" transform="rotate(180 12 12)" opacity="0"></rect>
                                        <path d="M17 3.5a.5.5 0 0 0-.5-.5H14a4.77 4.77 0 0 0-5 4.5v2.7H6.5a.5.5 0 0 0-.5.5v2.6a.5.5 0 0 0 .5.5H9v6.7a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-6.7h2.62a.5.5 0 0 0 .49-.37l.72-2.6a.5.5 0 0 0-.48-.63H13V7.5a1 1 0 0 1 1-.9h2.5a.5.5 0 0 0 .5-.5z"></path>
                                    </g>
                                </g>
                            </svg></a></li>
                            <li><a class="social-links-item social-links-item-twitter" href="#"><svg class="social-links-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                <g data-name="Layer 2">
                                    <g data-name="twitter">
                                        <polyline points="0 0 24 0 24 24 0 24" opacity="0"></polyline>
                                        <path d="M8.08 20A11.07 11.07 0 0 0 19.52 9 8.09 8.09 0 0 0 21 6.16a.44.44 0 0 0-.62-.51 1.88 1.88 0 0 1-2.16-.38 3.89 3.89 0 0 0-5.58-.17A4.13 4.13 0 0 0 11.49 9C8.14 9.2 5.84 7.61 4 5.43a.43.43 0 0 0-.75.24 9.68 9.68 0 0 0 4.6 10.05A6.73 6.73 0 0 1 3.38 18a.45.45 0 0 0-.14.84A11 11 0 0 0 8.08 20"></path>
                                    </g>
                                </g>
                            </svg></a></li>
                            <li><a class="social-links-item social-links-item-whatsapp" href="#"><svg class="social-links-icon" role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <title>WhatsApp icon</title>
                                <path d="M17.498 14.382c-.301-.15-1.767-.867-2.04-.966-.273-.101-.473-.15-.673.15-.197.295-.771.964-.944 1.162-.175.195-.349.21-.646.075-.3-.15-1.263-.465-2.403-1.485-.888-.795-1.484-1.77-1.66-2.07-.174-.3-.019-.465.13-.615.136-.135.301-.345.451-.523.146-.181.194-.301.297-.496.1-.21.049-.375-.025-.524-.075-.15-.672-1.62-.922-2.206-.24-.584-.487-.51-.672-.51-.172-.015-.371-.015-.571-.015-.2 0-.523.074-.797.359-.273.3-1.045 1.02-1.045 2.475s1.07 2.865 1.219 3.075c.149.195 2.105 3.195 5.1 4.485.714.3 1.27.48 1.704.629.714.227 1.365.195 1.88.121.574-.091 1.767-.721 2.016-1.426.255-.705.255-1.29.18-1.425-.074-.135-.27-.21-.57-.345m-5.446 7.443h-.016c-1.77 0-3.524-.48-5.055-1.38l-.36-.214-3.75.975 1.005-3.645-.239-.375c-.99-1.576-1.516-3.391-1.516-5.26 0-5.445 4.455-9.885 9.942-9.885 2.654 0 5.145 1.035 7.021 2.91 1.875 1.859 2.909 4.35 2.909 6.99-.004 5.444-4.46 9.885-9.935 9.885M20.52 3.449C18.24 1.245 15.24 0 12.045 0 5.463 0 .104 5.334.101 11.893c0 2.096.549 4.14 1.595 5.945L0 24l6.335-1.652c1.746.943 3.71 1.444 5.71 1.447h.006c6.585 0 11.946-5.336 11.949-11.896 0-3.176-1.24-6.165-3.495-8.411"></path>
                            </svg></a></li>
                            <li><a class="social-links-item social-links-item-telegram" href="#"><svg class="social-links-icon" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                <title>Telegram icon</title>
                                <path d="M23.91 3.79L20.3 20.84c-.25 1.21-.98 1.5-2 .94l-5.5-4.07-2.66 2.57c-.3.3-.55.56-1.1.56-.72 0-.6-.27-.84-.95L6.3 13.7l-5.45-1.7c-1.18-.35-1.19-1.16.26-1.75l21.26-8.2c.97-.43 1.9.24 1.53 1.73z"></path>
                            </svg></a></li>
                        </ul>
                    </div>

                    <ul class="tags ml-auto">
                        @foreach($article->tags_array as $tag)
                        <li><a href="#">{{ $tag }}</a></li>
                        @endforeach
                    </ul>
                </div>

                <div class="bg-gray p-45 rounded mt-5">
                    <h2 class="section-title-md">Artikel Terkait</h2>
                    <div class="cards-transparent mt-4">
                        @foreach($relateds as $k => $related)
                        <div class="card card-shadow card-row{{ $k !== 2 ? ' mb-4' :'' }}">
                            <div class="card-image card-image-sm" style="background-image:url({{ $related->image_url }});background-position:center;background-size:cover;width: 100px;height: 100px;"></div>
                            <div class="card-body py-0">
                                <div class="card-category"><a href="{{ url('artikel/kategori/'.$related->category->slug) }}.html">{{ $related->tags_array[0] }}</a></div>
                                <h6 class="card-title"><a href="{{ url('artikel', $related->slug) }}.html">{{ $related->title }}</a></h6>
                                <p class="time">{{ $related->publish_date }}</p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

                <h2 class="section-title-md mt-5">Tulis Tanggapan</h2>

                @if (session()->get('token'))
                    @if(session('message')!='')
                    <div class="alert alert-primary" role="alert">
                        <b>Message : </b>{{ session('message')}}
                    </div>
                    @endif
                    <form action="/postCommentArticle/{{ $article->id }}" method="POST" class="mt-4">
                        @csrf
                        {{-- <div class="row">
                            <div class="form-group col-lg-4"><input class="form-control" placeholder="Name" /></div>
                            <div class="form-group col-lg-4"><input class="form-control" placeholder="Email" /></div>
                            <div class="form-group col-lg-4"><input class="form-control" placeholder="Website" /></div>
                        </div> --}}
                        <div class="form-group">
                            <textarea class="form-control" placeholder="Tulis tanggapan di sini ..." style="height:200px" name="comment" id="comment" required=""></textarea>
                            <input type="hidden" name="id" value="{{ $article->id }}">
                            <input type="hidden" name="token" value="{{ session()->get('token') }}">
                        </div>
                        <button class="btn btn-cta mt-2">Kirim Tanggapan</button>
                    </form>
                @else
                <div class="alert alert-info">Silahkan <a href="{{ url('login') }}.html">login</a> untuk berkomentar</div>
                @endif

                <h2 class="section-title-md mt-5">{{ count($comments) }} Orang Menanggapi</h2>
                <div class="mt-4">
                    <div class="list-border">
                        @foreach($comments as $comment)
                        <div class="d-flex">
                            <div class="rounded shadow no-shrink" style="background-image:url({{ $comment->photo }});width:60px;height:60px;background-size:cover;background-position:center"></div>
                            <div class="ml-4 mt-2 desc">
                                <h6 class="mb-1 text-primary">{{ $comment->name }} <span class="ml-2 text-muted text-sm">{{ $comment->created_at }}</span></h6>
                                <p class="mt-2 mb-3">{{ $comment->comment }}</p>
                                <a href="#">Reply</a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div id="sidebar" class="pb-5">
                    <h2 class="section-title-sm mb-2">Artikel Populer</h2>
                    <div class="list-posts">
                        @foreach ($populars as $p)
                        <div class="post">
                            <div class="post-image" style="background-image:url({{ $p->image_url }});width:60px;height:60px;background-size:cover;background-position:center"></div>
                            <div class="post-detail">
                                <h4 class="post-title"><a href="/artikel/{{ $p->slug}}.html">{{ $p->title}}</a></h4>
                                <div class="post-metas">
                                    <div><a href="{{ url('artikel/kategori/'.$p->category->slug) }}.html">{{ $p->category->name}}</a></div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <h2 class="section-title-sm mt-5">Kategori Artikel</h2>
                    <ul class="cat-links">
                        @foreach($categories as $catArticle)
                            <li><a href="/artikel/kategori/{{ $catArticle->slug}}.html">{{ $catArticle->name}}</a></li>
                        @endforeach
                    </ul>

                    <div class="mt-5 d-flex align-items-center">
                        <h2 class="section-title-sm mb-0">Produk</h2>
                        <div id="product-nav" class="btn-group ml-auto">
                            <a href="#" class="btn btn-sm btn-outline-secondary prev">&lsaquo;</a>
                            <a href="#" class="btn btn-sm btn-outline-secondary next">&rsaquo;</a>
                        </div>
                    </div>
                    <div class="row mt-2 mb-n4 carousel h-auto nav-no-overlap" data-nav="#product-nav">
                        @foreach ($products as $product)
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-image" style="background-image:url({{ $product->image_url }});background-position:center;background-size:cover;height:180"></div>
                                <div class="card-body">
                                    <h6 class="card-title"><a href="/jual-beli/{{ $product->slug}}.html">{{ $product->name}}</a></h6>
                                    <div class="card-price">Rp.{{ $product->price_idr}}</div>
                                    <div class="card-author">
                                        <div class="card-author-image no-shrink" style="background-image:url({{ $product->store->image}});background-position:center;background-size:cover"></div>
                                        <div class="card-author-detail">
                                            <div class="card-author-name">{{ $product->store->name}}</div>
                                            <div class="card-author-desc"> {{ $product->store->region->type }}
                                                    {{ $product->store->region->city }},
                                                    {{ $product->store->region->province }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <div class="mt-5 d-flex align-items-center">
                        <h2 class="section-title-sm mb-0">Video</h2>
                        <div id="video-nav" class="btn-group ml-auto">
                            <a href="#" class="btn btn-sm btn-outline-secondary prev">&lsaquo;</a>
                            <a href="#" class="btn btn-sm btn-outline-secondary next">&rsaquo;</a>
                        </div>
                    </div>
                    <div class="list-posts mt-2 cards-transparent carousel h-auto" data-nav="#video-nav">
                        @foreach (array_slice($videos->data, 0, 3) as $video)
                        <div class="card border-transparent">
                            <div class="card-image" style="background-image:url({{ $video->img_thumbnail }});background-position:center;background-size:cover;height:150px">
                                <div class="card-duration">03:50</div>
                            </div>
                            <div class="card-body">
                                <div class="card-category"><a href="{{ url('video/kategori/'.$video->category->slug) }}.html">{{ $video->category->name }}</a></div>
                                <h6 class="card-title"><a href="{{ url('video/'.$video->slug) }}.html">{{ $video->title}}</a></h6>
                                <p class="time mb-0">{{ $video->view}} Views</p>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <h2 class="section-title-sm">Dapatkan Aplikasi</h2>
                    <img src="/google-play-1.png" class="img-fluid">

                    <div class="w-100 rounded mt-5" style="height: 200px;background-position: center;">
                        <?php //Iklan::getIklan("0b53be4a-7b4e-4590-8b94-45f9aa03f7e0");?>
                        <h2 class="section-title-sm">Pertamina Mempersembahkan</h2>
                        <a href="https://www.pertamina.com/">
                        <img src="https://wsdev.globalvillage.co.id/img_iklan/2019_09_06_22_23_53_spbu_pertamina_20170815_151612.jpg" width="316">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script>
$(document).ready(function() {
    $("#sidebar").stick_in_parent({
        offset_top: 40
    });

    $(".carousel").each(function() {
        let carousel = $(this),
            nav = carousel.data('nav');

        carousel.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false
        });

        $(nav).find(".prev").click(function(e) {
            carousel.slick('prev');

            e.preventDefault();
        });

        $(nav).find(".next").click(function(e) {
            carousel.slick('next');

            e.preventDefault();
        });
    });
});
</script>
@endpush
