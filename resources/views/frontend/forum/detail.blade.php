@extends('frontend.layout')
@section('title','Forum '.$forum->title)
@section('content')
    <section class="section">
        <div class="container">
            <div class="row gutter-lg">
                <div class="col-lg-4">
                    <a href="/forum/create.html" class="btn btn-cta btn-block align-items-center mb-5">
                        <svg class="mr-1 text-white stroke-current" width="15" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polygon points="16 3 21 8 8 21 3 21 3 16 16 3"></polygon></svg>
                        Buat Pertanyaan
                    </a>

                    @include('frontend.forum.sidebar')

                    <h2 class="section-title-sm mb-2 mt-5">Kategori Forum</h2>
                    <ul class="cat-links">
                    @foreach($categories as $category)
                    <li>
                        <a href="{{ url('forum/category',$category->slug )}}.html">{{ $category->name }}</a>
                    </li>
                    @endforeach
                    </ul>
                </div>
                <div class="col-lg-8">
                    <h1 class="section-title-md">Forum</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">Home</a></li>
                            <li class="breadcrumb-item"><a href="/forum.html">Forum</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $forum->title }}</li>
                        </ol>
                    </nav>

                    {{-- @include('flash::message') --}}
                    @include('alert')

                    <div class="card card-shadow card-primary">
                        <div class="card-body">
                            <h4 class="section-title-sm mb-0">{{ $forum->title }}</h4>
                            <div class="desc">
                                {!! $forum->description !!}
                            </div>
                            @if (isset($forum->images) && count($forum->images) > 0)
                            <div class="font-weight-bold mt-3">Lampiran Gambar</div>
                            <div class="d-flex mt-2 mb-4 preview-thumbs chocolat-parent">
                                @foreach ($forum->images as $image)
                                    <a href="{{ $image }}" class="preview-thumb chocolat-image" style="width: auto;">
                                        <div class="rounded-lg" style="background-image:url({{ $image }});border:1px solid #ddd;width:50px;height:50px;background-size:cover;background-position:center"></div>
                                    </a>
                                @endforeach
                            </div>
                            @endif

                            <div class="d-flex text-muted">
                                <p class="text-info">{{ $forum->view }}x Dilihat</p>
                                &nbsp;&nbsp;&bull;&nbsp;&nbsp;
                                <p>Dibuat pada {{ $forum->created_at }}</p>
                                @if ($forum->user->id == session()->get('id'))
                                    &nbsp;&nbsp;&bull;&nbsp;&nbsp;
                                    {{ Form::open(['url'=>'forum/delete/'.$forum->slug,'method'=>'delete', 'onsubmit'=>"return confirm('Delete This Forum?')", 'id' => 'delete-form'])}}
                                        <a href="#" class="text-danger" onclick="$('#delete-form').submit();return false;" title="Hapus Forum">Hapus</a>
                                    {{ Form::close()}}
                                @endif
                            </div>

                            <ul class="tags">
                                {{-- Category seharus banyak
                                    @foreach($categories as $category)
                                        <li><a href="{{ $category->id }}">{{ $category->name }}</a></li>
                                    @endforeach --}}
                                    <li><a href="{{ url('forum/category',$category->slug )}}.html">{{ $forum->category->name }}</a></li>
                            </ul>

                            <hr class="mt-4">

                            <div class="d-flex align-items-center mt-4">
                                <div class="card-author mt-0">
                                    <div class="card-author-image" style="background-image:url({{$forum->user->photo ?? ''}});background-position:center;background-size:cover"></div>
                                    <div class="card-author-detail">
                                        <div class="card-author-name">{{ $forum->user->name }}</div>
                                        <div class="card-author-desc">
                                            @if (isset($forum->user->region->village_id))
                                                {{ $forum->user->region->regency_name }},
                                                {{ $forum->user->region->province_name }}
                                            @else
                                                Daerah Tidak Diketahui
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="ml-auto">
                                    <div class="btn-group">
                                        <a href="" class="btn btn-outline-secondary btn-sm btn-icon">
                                            <svg class="fill-current mr-2" width="16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g data-name="Layer 2"><g data-name="message-circle"><circle cx="12" cy="12" r="1"/><circle cx="16" cy="12" r="1"/><circle cx="8" cy="12" r="1"/><path d="M19.07 4.93a10 10 0 0 0-16.28 11 1.06 1.06 0 0 1 .09.64L2 20.8a1 1 0 0 0 .27.91A1 1 0 0 0 3 22h.2l4.28-.86a1.26 1.26 0 0 1 .64.09 10 10 0 0 0 11-16.28zm.83 8.36a8 8 0 0 1-11 6.08 3.26 3.26 0 0 0-1.25-.26 3.43 3.43 0 0 0-.56.05l-2.82.57.57-2.82a3.09 3.09 0 0 0-.21-1.81 8 8 0 0 1 6.08-11 8 8 0 0 1 9.19 9.19z"/><rect width="24" height="24" opacity="0"/></g></g></svg>
                                            {{ $forum->total_comment }} Komentar
                                        </a>
                                        <a href="" class="btn btn-outline-secondary btn-sm btn-icon">
                                            <svg class="stroke-current mr-2" width="16" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><circle cx="18" cy="5" r="3"></circle><circle cx="6" cy="12" r="3"></circle><circle cx="18" cy="19" r="3"></circle><line x1="8.59" y1="13.51" x2="15.42" y2="17.49"></line><line x1="15.41" y1="6.51" x2="8.59" y2="10.49"></line></svg>
                                            Bagikan
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- End of card --}}

                    <h2 class="section-title-md mb-3 mt-5">Respons</h2>
                    <div class="card-group-x card-shadow">
                        @foreach($replies as $reply)
                        <div class="card">
                            <div class="card-body">
                                <div class="card-author mt-0 mb-4">
                                    <div class="card-author-image" style="background-image:url({{ $reply->photo ?? '' }});background-position:center;background-size:cover"></div>
                                    <div class="card-author-detail">
                                        <div class="card-author-name">{{ $reply->name}}</div>
                                        <div class="card-author-desc">Bogor, Jawa Barat &nbsp;&bull;&nbsp; <span class="text-dark">{{ $reply->created_at }}</span></div>
                                    </div>
                                </div>

                                <div class="desc">
                                    <p class="mb-0">{{ $reply->reply }}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @if (session()->get('token'))
                        <div class="card">
                            <div class="card-body">
                                <div class="card-author mt-0 mb-4">
                                    <div class="card-author-image" style="background-image:url({{ session()->get('photo') ?? '' }});background-position:center;background-size:cover"></div>
                                    <div class="card-author-detail">
                                        <div class="card-author-name">{{ session()->get('name') }}</div>
                                        <div class="card-author-desc">Bandung, Jawa Barat</div>
                                    </div>
                                </div>

                                <form action="/addReply" method="POST">
                                    @csrf
                                    <input type="hidden" name="forum_id" value="{{ $forum->id }}">
                                    <input type="hidden" name="token" value="{{ session()->get('token') }}">
                                    <textarea class="form-control" name="reply" placeholder="Komentar Anda di sini ..."></textarea>
                                    <button type="submit" class="btn btn-cta mt-3">Kirim</button>
                                </form>
                            </div>
                        </div>
                        @else
                        <div class="card">
                            <div class="card-body">
                                <a href="{{ url('login') }}.html">Silahkan login untuk berkomentar</a>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chocolat/0.4.21/css/chocolat.min.css">
@endpush

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/chocolat/0.4.21/js/jquery.chocolat.min.js"></script>
<script>
    $('.chocolat-parent').Chocolat();
</script>
@endpush
