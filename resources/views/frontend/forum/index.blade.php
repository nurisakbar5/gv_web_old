@extends('frontend.layout')
@section('title','Forum')
@section('content')
    <section class="section">
        <div class="container">
            <div class="row gutter-lg">
                <div class="col-lg-4">
                    <!-- Pencarian & Kategori -->
                    @include('frontend.forum.leftcontent')
                </div>
                <div class="col-lg-8">
                    <div class="d-flex align-items-center mb-4">
                        <h1 class="section-title-md">Tanya Jawab</h1>
                        <div class="ml-auto d-flex align-items-center">
                            <div class="dropdown mr-4">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Urutkan</a>
                                <ul class="dropdown-menu">
                                    <li><a href="" class="dropdown-item">Terbaru</a></li>
                                    <li><a href="" class="dropdown-item">Terlama</a></li>
                                </ul>
                            </div>
                            <a href="/forum/create.html" class="btn btn-cta btn-normal align-items-center">
                                <svg class="mr-1 text-white stroke-current" width="15" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polygon points="16 3 21 8 8 21 3 21 3 16 16 3"></polygon></svg>
                                Buat Pertanyaan
                            </a>
                        </div>
                    </div>
                    @include('alert')
                    <div class="card-group-x card-shadow card-primary">
                        @foreach($forums->data as $forum)
                        <div class="card">
                            <div class="card-body">
                                <h4 class="section-title-sm"><a class="text-dark" href="{{ url('forum/'. $forum->slug) }}.html">{{ $forum->title }}</a></h4>

                                <div class="d-flex text-muted mt-2">
                                    <p class="text-info">{{ $forum->view }}x Dilihat</p>
                                    &nbsp;&nbsp;&bull;&nbsp;&nbsp;
                                    <p class="text-muted">Dibuat pada {{ $forum->created_at }}</p>
                                </div>

                                <ul class="tags">
                                {{-- Category seharus banyak
                                    @foreach($categories as $category)
                                        <li><a href="{{ $category->id }}">{{ $category->name }}</a></li>
                                    @endforeach --}}
                                    <li><a href="{{ url('forum/category/'.$forum->category->slug) }}.html">{{ $forum->category->name }}</a></li>
                                </ul>

                                <div class="d-flex align-items-center mt-4">
                                    <div class="card-author mt-0">
                                        <div class="card-author-image" style="background-image:url({{ $forum->user->photo }});"></div>
                                        <div class="card-author-detail">
                                            <div class="card-author-name">{{ $forum->user->name }}</div>
                                            <div class="card-author-desc">
                                                @if (isset($forum->user->region->village_id))
                                                    {{ $forum->user->region->regency_name }},
                                                    {{ $forum->user->region->province_name }}
                                                @else
                                                    Daerah Tidak Diketahui
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="btn-group">
                                            <a href="" class="btn btn-outline-secondary btn-sm btn-icon">
                                                <svg class="fill-current mr-2" width="16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g data-name="Layer 2"><g data-name="message-circle"><circle cx="12" cy="12" r="1"/><circle cx="16" cy="12" r="1"/><circle cx="8" cy="12" r="1"/><path d="M19.07 4.93a10 10 0 0 0-16.28 11 1.06 1.06 0 0 1 .09.64L2 20.8a1 1 0 0 0 .27.91A1 1 0 0 0 3 22h.2l4.28-.86a1.26 1.26 0 0 1 .64.09 10 10 0 0 0 11-16.28zm.83 8.36a8 8 0 0 1-11 6.08 3.26 3.26 0 0 0-1.25-.26 3.43 3.43 0 0 0-.56.05l-2.82.57.57-2.82a3.09 3.09 0 0 0-.21-1.81 8 8 0 0 1 6.08-11 8 8 0 0 1 9.19 9.19z"/><rect width="24" height="24" opacity="0"/></g></g></svg>
                                                {{ $forum->total_comment }} Komentar
                                            </a>
                                            <a href="" class="btn btn-outline-secondary btn-sm btn-icon">
                                                <svg class="stroke-current mr-2" width="16" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><circle cx="18" cy="5" r="3"></circle><circle cx="6" cy="12" r="3"></circle><circle cx="18" cy="19" r="3"></circle><line x1="8.59" y1="13.51" x2="15.42" y2="17.49"></line><line x1="15.41" y1="6.51" x2="8.59" y2="10.49"></line></svg>
                                                Bagikan
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <div class="mt-5 text-center">
                        @pagination
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
