<!-- Diskusi Lainnya -->

<h2 class="section-title-sm mb-2 mt-4">Diskusi Lainnya</h2>
<div class="card-group-x card-shadow card-primary">
    @foreach($forumRelated as $forum)
    <div class="card">
        <div class="card-body p-3">
            <h4 class="card-title mb-1"><a href="{{ $forum->slug }}.html">{{ $forum->title }}</a></h4>
            <p class="mb-0 text-muted">Oleh <a href="#">{{ $forum->user->name }}</a> pada <span class="text-dark">{{ $forum->created_at }}</span></p>
        </div>
    </div>
    @endforeach
</div>
