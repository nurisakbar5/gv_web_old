<h2 class="section-title-sm mb-2">Pencarian</h2>
{{ Form::open(['url'=>'/forum/pencarian-hasil-forum.html'])}}
<div class="form-group">
    <div class="input-group">
        <input type="text" name="keyword" name="" class="form-control" placeholder="Cari di sini ...">
        <div class="input-group-append">
            <button type="submit" class="btn btn-primary btn-normal">Cari</button>
        </div>
    </div>
</div>
</form>

<h2 class="section-title-sm mb-2 mt-5">Kategori Forum</h2>
<ul class="cat-links">
    @foreach($categories as $category)
    <li><a href="{{ url('forum/category',$category->slug )}}.html">{{ $category->name }}</a></li>
    @endforeach
</ul>

{{-- <h2 class="section-title-sm mb-3 mt-4">Lokasi</h2>
<select class="form-control mb-3" style="padding: 0 !important;">
    <option>Aceh</option>
    <option>Jawa Barat</option>
    <option>Jawa Timur</option>
    <option>Jawa Tengah</option>
    <option>Kalimantan Timur</option>
    <option>Sulawesi</option>
</select>
<select class="form-control mb-3" style="padding: 0 !important;">
    <option>Bogor</option>
</select>
<select class="form-control mb-3" style="padding: 0 !important;">
    <option>Bogor Barat</option>
</select>
<button class="btn btn-block btn-normal btn-cta">Filter</button>

<hr> --}}

<!-- Artikel Populer -->
<h2 class="section-title-sm mb-2 mt-5">Artikel Populer</h2>
<div class="list-posts">
    @foreach ($articlePopulars as $popular)
    <div class="post align-items-start">
        <div class="post-image" style="background-image:url({{ $popular->image_url }});width:80px;height:80px;background-size:cover;background-position:center"></div>
        <div class="post-detail">
            <h4 class="post-title"><a href="{{ url('artikel', $popular->slug) }}.html">{{ $popular->title }}</a></h4>
            <div class="post-metas">
                <div><a href="artikel/kategori/{{ $popular->category->name }}.html">{{ $popular->category->name }}</a></div>
            </div>
            <p class="time mt-1">Dipublikasikan Pada {{ $popular->publish_date}}</p>
        </div>
    </div>
    @endforeach
</div>

<!-- Video Terbaru -->
<div class="mt-5 d-flex align-items-center">
    <h2 class="section-title-sm">Video Terbaru</h2>
    <div id="video-nav" class="btn-group ml-auto">
        <a href="#" class="btn btn-sm btn-outline-secondary prev">&lsaquo;</a>
        <a href="#" class="btn btn-sm btn-outline-secondary next">&rsaquo;</a>
    </div>
</div>

<div class="list-posts cards-transparent carousel h-auto" data-nav="#video-nav">
    @foreach ($videos->data as $video)
    <div class="card border-transparent">
        <div class="card-image" style="background-image:url({{ $video->img_thumbnail }});background-position:center;background-size:cover;height:150px">
            <div class="card-duration">03:50</div>
        </div>
        <div class="card-body">
            <div class="card-category"><a href="{{ url('video/kategori/'.$video->category->slug) }}.html">{{ $video->category->name }}</a></div>
            <h6 class="card-title"><a href="{{ url('video/'.$video->slug) }}.html">{{ $video->title}}</a></h6>
            <p class="time">{{ $video->view}} Views</p>
        </div>
    </div>
    @endforeach
</div>

@push('scripts')
<script>
$(document).ready(function() {
    $("#sidebar").stick_in_parent({
        offset_top: 40
    });

    $(".carousel").each(function() {
        let carousel = $(this),
            nav = carousel.data('nav');

        carousel.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false
        });

        $(nav).find(".prev").click(function(e) {
            carousel.slick('prev');

            e.preventDefault();
        });

        $(nav).find(".next").click(function(e) {
            carousel.slick('next');

            e.preventDefault();
        });
    });
});
</script>
@endpush
