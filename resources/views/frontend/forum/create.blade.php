@extends('frontend.layout')

@section('title','Create')

@section('content')
<section class="section">
    <div class="container">
        <div class="row gutter-lg">
            <div class="col-lg-4">
                @include('frontend.forum.sidebar')
            </div>
            <div class="col-lg-8">
                <h1 class="section-title-md">Forum</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item"><a href="/forum.html">Forum</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Buat Pertanyaan</li>
                    </ol>
                </nav>

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="card card-shadow card-primary mt-4">
                    <div class="card-body">
                        <h4 class="card-title">Buat Pertanyaan</h4>
                        <p class="mb-4 text-muted">Isi formulir berikut untuk memulai membuat pertanyaan.</p>

                        <form method="post" enctype="multipart/form-data" action="/forum/store">
                            @csrf
                            <input type="hidden" name="token" value="{{ session()->get('token') }}">
                            <div class="form-group">
                                <label class="font-weight-bold">Judul</label>
                                <input type="text" name="title" class="form-control" placeholder="Contoh: Bagaimana cara untuk menanam padi?">
                            </div>
                            <div class="form-group">
                                <label class="font-weight-bold">Deskripsi Pertanyaan</label>
                                <textarea id="summernote" name="description" class="form-control" placeholder="Deskripsikan pertanyaan Anda dengan jelas"></textarea>
                            </div>
                            <div class="form-group">
                                <label class="font-weight-bold">Kategori</label>
                                <select name="category_id" class="form-control">
                                    <option value="">Pilih Kategori</option>
                                    @foreach($categories as $c)
                                    <option value="{{ $c->id }}">{{ $c->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="font-weight-bold">Sertakan Gambar <span class="font-weight-normal text-muted">(optional)</span></label>
                                <div class="row">
                                    @foreach([1,2,3] as $i => $r)
                                    <div class="col-lg-4">
                                        <label id="preview-image-{{$r}}" for="input-image-{{$r}}" style="display:flex;justify-content: center;align-items: center;font-size: 16px;width: 100%;height: 230px;background-color: #f9f9f9;border: 2px dashed #ddd;border-radius: 3px;cursor: pointer;{{ isset($products->front_images[$i]) ? 'background-image: url('. $products->front_images[$i] .')' : ''}}">
                                            Pilih Gambar
                                        </label>
                                        <input type="file" name="image_{{$r}}" id="input-image-{{$r}}" class="d-none image-upload" data-preview="#preview-image-{{$r}}">

                                        <span class="text-danger">
                                            {{ $errors->first('image_' . $r) }}
                                        </span>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group text-right">
                                <button class="btn btn-cta">
                                    Kirim Pertanyaan
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">
@endpush
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>
<script src="https://unpkg.com/file-upload-with-preview@4.0.2/dist/file-upload-with-preview.min.js"></script>
<script>
    $('#summernote').summernote({
        tabsize: 2,
        height: 250
    });

    function readURL(input, element) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $(element).css({
                    'background-image': 'url(' + e.target.result + ')',
                    'background-position': 'center',
                    'background-size': 'cover',
                });
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".image-upload").change(function() {
        console.log('a')
        readURL(this, $(this).data('preview'));
    });
</script>
@endpush

@push('css')
<link rel="stylesheet" href="https://unpkg.com/file-upload-with-preview@4.0.2/dist/file-upload-with-preview.min.css">
@endpush
