@extends('frontend.layout')

@section('title', 'Info Harga')

@section('css')
<style>
option.disabled {
  pointer-events: none;
  cursor: default;
}
</style>
@endsection

@section('content')
    <section class="section">
        <div class="container">
        	<div class="row gutter-lg">
        		<div class="col-lg-4">
	                <h2 class="section-title-sm mb-3 mt-4">Period</h2>
	                <form>
	                	@if(request()->regency)
	                	<input type="hidden" name="regency" value="{{ request()->regency }}">
	                	@endif
	                	<div class="input-group">
			                <input type="text" name="period" class="form-control datepicker" value="{{ request()->period ?? date('Y-m-d') }}">
			                <div class="input-group-append">
			                	<button class="btn btn-cta btn-normal">
			                		Filter
			                	</button>
			                </div>
	                	</div>
		            </form>

					<hr>

	                <h2 class="section-title-sm mb-3 mt-4">Lokasi</h2>
	                <form>
	                	@if(request()->period)
	                	<input type="hidden" name="period" value="{{ request()->period }}">
	                	@endif
                        <select class="form-control mb-3 province" style="padding: 0 !important;">
                            <option value="" class="disabled">Pilih Provinsi</option>
							@foreach($provinces->data as $province)
								<option value="{{ $province->id ?? '' }}"> {{ $province->name ?? $province }}</option>
							@endforeach
						</select>
		                <select name="regency" class="form-control mb-3 cities" required="" style="padding: 0 !important;">
		                    <option value="">Pilih provinsi dahulu</option>
		                </select>
		                <button class="btn btn-block btn-normal btn-cta">Filter</button>
		            </form>
        		</div>
        		<div class="col-lg-8">

					<h2 class="section-title-md mb-4">Harga Komoditas {{ $regencyDetail[0]->name}}, {{ $regencyDetail[0]->province_name}}</h2>
		        	<div>
						<div class="list-group card-shadow list-group-flush card-primary">
							@foreach($prices as $price)
							<div class="list-group-item d-flex">
								<svg class="text-success fill-current" width="30" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g data-name="Layer 2"><g data-name="trending-up"><rect width="24" height="24" transform="rotate(-90 12 12)" opacity="0"/><path d="M21 7a.78.78 0 0 0 0-.21.64.64 0 0 0-.05-.17 1.1 1.1 0 0 0-.09-.14.75.75 0 0 0-.14-.17l-.12-.07a.69.69 0 0 0-.19-.1h-.2A.7.7 0 0 0 20 6h-5a1 1 0 0 0 0 2h2.83l-4 4.71-4.32-2.57a1 1 0 0 0-1.28.22l-5 6a1 1 0 0 0 .13 1.41A1 1 0 0 0 4 18a1 1 0 0 0 .77-.36l4.45-5.34 4.27 2.56a1 1 0 0 0 1.27-.21L19 9.7V12a1 1 0 0 0 2 0V7z"/></g></g></svg>
								<div class="ml-4 w-100">
									<div class="row align-items-center">

										<div class="col-lg-3">
											<img src="{{ $price->commodity_image }}" class="img-fluid rounded">
										</div>
										<div class="col-lg-3">
											<div class="font-weight-bold text-16"> {{ $price->commodity_name}}</div>
											<div class="mt-1 text-muted">Rp. {{ $price->average_price}}</div>
										</div>

										<div class="col-lg-3">
											<span class="text-muted">{{ $price->name_regency}}</span>
										</div>
										<div class="col-lg-3">
											<span class="text-primary">{{ $price->date}}</span>
										</div>
									</div>
								</div>
							</div>
							@endforeach
						</div>
		        	</div>
        		</div>
        	</div>
        </div>
    </section>
@stop

@push('scripts')
<script src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
	$('.datepicker').daterangepicker({
		singleDatePicker: true,
		autoUpdateInput: false,
		locale: {
			format: 'YYYY-MM-DD'
		}
	}, function(e, d) {
		$(this.element).val(e.format('YYYY-MM-DD'));
	});

	$('.province').change(function() {
		const id = $(this).val();

		$.ajax({
			url: '/regency/' + id,
			beforeSend: function() {
				$('.cities option').remove();
				$('.cities').append('<option value="">Memuat ...</option>');
			},
			success: function(data) {
				let option = '<option value="{id}">{name}</option>';

				$('.cities option').remove();
				$('.cities').append('<option value="">Pilih Kota/Kabupaten</option>');
				data.forEach(function(item) {
					$('.cities').append(
						option.replace(/{id}/g, item.id).replace(/{name}/g, item.name)
					);
				});
			},
			complete: function() {

			}
		});
	});
</script>
@endpush

@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endpush
