<!DOCTYPE html>
<html>
<head>
    @yield('seo')
    <title>@yield('title')</title>
    <meta charSet="utf-8" class="next-head" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" href="{{ asset('template/front/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/front/css/slick.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/front/css/slick-theme.min.css') }}">
    <link rel="stylesheet" href="https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css">
    @stack('css')
    <link rel="stylesheet" href="{{ asset('template/front/css/style.css?v=' . time()) }}">
</head>

<body>
	<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
		@csrf
	</form>

    <div id="__next">
        {{-- {{ session('address') }} --}}
        <div class="navbar main-navbar navbar-expand-lg navbar-bg-primary">
            <div class="container-fluid"><a class="navbar-brand" href="/">Global Village</a>
                <div class="d-flex align-items-center">
                    @include('frontend.layout-search-top')
                    <div class="dropdown megamenu">
                        <a href="#" class="dropdown-toggle mr-5 nav-link" data-toggle="dropdown">Categories</a>
                        <div class="dropdown-menu bg-white">
                            <div class="container">
                                <div class="row py-4">
                                    <div class="col-lg-3">
                                        <h5 class="cat-title">Kategori Produk</h5>
                                        <ul class="cat-links">
                                            @foreach($categoryProduct as $catProduct)
                                                <li><a href="/produk/kategori/{{ $catProduct->slug}}.html">{{ $catProduct->name}}</a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="col-lg-3">
                                        <h5 class="cat-title">Kategori Panen</h5>
                                        <ul class="cat-links">
                                            @foreach($categoryHarvest as $catHarvest)
                                                <li><a href="/panen/kategori/{{ $catHarvest->slug}}.html">{{ $catHarvest->name}}</a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="col-lg-3">
                                        <h5 class="cat-title">Kategori Video</h5>
                                        <ul class="cat-links">
                                            @foreach($categoryVideo as $catVideo)
                                                <li><a href="/video/kategori/{{ $catVideo->slug}}.html">{{ $catVideo->name}}</a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="col-lg-3">
                                        <h5 class="cat-title">Kategori Berita</h5>
                                        <ul class="cat-links">
                                            @foreach($categoryArticle as $catArticle)
                                                <li><a href="/artikel/kategori/{{ $catArticle->slug}}.html">{{ $catArticle->name}}</a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <a href="/cart.html" class="btn position-relative btn-icon btn-primary btn-fat btn-round">
                        <svg class="btn-the-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g data-name="Layer 2"><g data-name="shopping-cart"><rect width="24" height="24" opacity="0"/><path d="M21.08 7a2 2 0 0 0-1.7-1H6.58L6 3.74A1 1 0 0 0 5 3H3a1 1 0 0 0 0 2h1.24L7 15.26A1 1 0 0 0 8 16h9a1 1 0 0 0 .89-.55l3.28-6.56A2 2 0 0 0 21.08 7zm-4.7 7H8.76L7.13 8h12.25z"/><circle cx="7.5" cy="19.5" r="1.5"/><circle cx="17.5" cy="19.5" r="1.5"/></g></g></svg>
                        Keranjang
                        <div class="badge badge-warning">0</div>
                    </a>

                    <div class="navbar-divider"></div>
                    @if(is_logged_in())
                    <div class="dropdown">
                        <a href="#" data-toggle="dropdown">
                            <svg width="45" height="45" viewBox="0 0 55 55" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="27.5" cy="27.5" r="27.5" fill="#DDDDDD"></circle>
                                <path d="M28 25.5833C29.1208 25.5833 30.2164 25.251 31.1483 24.6283C32.0801 24.0057 32.8065 23.1207 33.2354 22.0852C33.6643 21.0498 33.7765 19.9104 33.5578 18.8112C33.3392 17.7119 32.7995 16.7022 32.007 15.9097C31.2145 15.1172 30.2048 14.5775 29.1056 14.3589C28.0063 14.1402 26.8669 14.2525 25.8315 14.6814C24.7961 15.1102 23.911 15.8366 23.2884 16.7684C22.6657 17.7003 22.3334 18.7959 22.3334 19.9167C22.3334 21.4196 22.9304 22.8609 23.9931 23.9236C25.0558 24.9863 26.4971 25.5833 28 25.5833ZM28 17.0833C28.5604 17.0833 29.1082 17.2495 29.5742 17.5608C30.0401 17.8722 30.4033 18.3147 30.6177 18.8324C30.8321 19.3501 30.8883 19.9198 30.7789 20.4694C30.6696 21.019 30.3998 21.5239 30.0035 21.9201C29.6073 22.3164 29.1024 22.5862 28.5528 22.6956C28.0032 22.8049 27.4335 22.7488 26.9158 22.5343C26.398 22.3199 25.9555 21.9567 25.6442 21.4908C25.3329 21.0248 25.1667 20.477 25.1667 19.9167C25.1667 19.1652 25.4652 18.4446 25.9966 17.9132C26.5279 17.3818 27.2486 17.0833 28 17.0833Z" fill="#666666"></path>
                                <path d="M28 28.4167C25.37 28.4167 22.8476 29.4615 20.9879 31.3212C19.1282 33.1809 18.0834 35.7033 18.0834 38.3333C18.0834 38.7091 18.2326 39.0694 18.4983 39.3351C18.764 39.6007 19.1243 39.75 19.5 39.75C19.8758 39.75 20.2361 39.6007 20.5018 39.3351C20.7675 39.0694 20.9167 38.7091 20.9167 38.3333C20.9167 36.4547 21.663 34.653 22.9914 33.3247C24.3198 31.9963 26.1214 31.25 28 31.25C29.8787 31.25 31.6803 31.9963 33.0087 33.3247C34.3371 34.653 35.0834 36.4547 35.0834 38.3333C35.0834 38.7091 35.2326 39.0694 35.4983 39.3351C35.764 39.6007 36.1243 39.75 36.5 39.75C36.8758 39.75 37.2361 39.6007 37.5018 39.3351C37.7675 39.0694 37.9167 38.7091 37.9167 38.3333C37.9167 35.7033 36.8719 33.1809 35.0122 31.3212C33.1524 29.4615 30.6301 28.4167 28 28.4167Z" fill="#666666"></path>
                            </svg>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <h6 class="dropdown-header text-muted">Hai, {{ session('name') }}!</h6>
                            <a href="/user-profile.html" class="dropdown-item">Profile</a>
                            <a href="/user-produk.html" class="dropdown-item">Produk</a>
                            <a href="/user-panen.html" class="dropdown-item">Panen</a>
                            <a href="/user-lahan.html" class="dropdown-item">Lahan</a>
                            <a href="/user-penjualan.html" class="dropdown-item">Penjualan</a>
                            <a href="/user-pembelian.html" class="dropdown-item">Pembelian</a>
                            <a href="/konfirmasi-pembayaran.html" class="dropdown-item">Konfirmasi Pembayaran</a>
                            <div class="dropdown-divider"></div>
                            <a href="{{ route('logout') }}" class="dropdown-item text-danger" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" >{{ __('Logout') }}</a>
                        </div>
                    </div>
                    @else
                    <a href="/login.html" class="btn btn-outline-light btn-normal mr-2">Login</a>
                    <a href="/register.html" class="btn btn-cta btn-normal">Daftar</a>
                    @endif
                </div>
            </div>
        </div>
        <div class="navbar main-navbar navbar-expand-lg navbar-secondary navbar-bg-primary">
            <div class="container">
                <ul class="navbar-nav">
                    <li class="nav-item"><a class="nav-link" href="/">Home</a></li>
                    <li class="nav-item"><a class="nav-link" href="/artikel.html">Artikel</a></li>
                    <li class="nav-item"><a class="nav-link" href="/video.html">Petani TV</a></li>
                    <li class="nav-item"><a class="nav-link" href="/jual-beli.html">Jual Beli</a></li>
                    <li class="nav-item"><a class="nav-link" href="/produk.html">Produk</a></li>
                    <li class="nav-item"><a class="nav-link" href="/desa.html">Desa</a></li>
                    <li class="nav-item"><a class="nav-link" href="/panen.html">Panen</a></li>
                    <li class="nav-item"><a class="nav-link" href="/forum.html">Forum</a></li>
                    <li class="nav-item"><a class="nav-link" href="/info-harga.html">Info Harga</a></li>
                </ul>
            </div>
        </div>

        <div class="main-content">
            @if (is_logged_in())
                @if (session('address')==NULL && session('active') == 1)
                    <div class="container mt-4">
                        <div class="alert alert-primary" role="alert">
                            <b>Silahkan Lengkapi Biodata Anda</b>
                        </div>
                    </div>
                @endif

            @endif
            @yield('content')
        </div>

        @include('frontend.footer', [
            'class' => 'bordered'
        ])
    </div>

    <script src="{{ asset('template/front/js/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('template/front/js/popper.min.js') }}"></script>
    <script src="{{ asset('template/front/js//bootstrap.min.js') }}"></script>
    <script src="{{ asset('template/front/js/slick.min.js') }}"></script>
    <script src="{{ asset('template/front/js/sticky-kit.min.js') }}"></script>
    <script src="{{ asset('template/front/js/custom.js') }}"></script>

    <!--Start of Tawk.to Script-->
    {{-- <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/56e184d35b6022b35f11913e/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script> --}}
    <!--End of Tawk.to Script-->

    @stack('scripts')
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-37262615-9"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-37262615-9');
    </script>
</body>

</html>
