@extends('frontend.layout')

@section('title', $detail->data->title)

@section('content')
    <section class="section">
        <div class="container">
        	<div class="row">
        		<div class="col-lg-3">
        			<h4 class="section-title-sm">Halaman Lainnya</h4>
        			<ul class="nav nav-pills flex-column">
        				@foreach($pages as $page)
						<li><a class="nav-link{{ $page->slug == $current ? ' active' : '' }}" href="{{url('/page/' . $page->slug)}}.html">{{ $page->title }}</a></li>
						@endforeach
        			</ul>
        		</div>
        		<div class="col-lg-9">
		        	<div class="card card-shadow card-primary">
		        		<div class="card-body">
							<h1 class="page-title">{{$detail->data->title}}</h1>
							<div class="desc">
								<p>{!!$detail->data->description!!}</p>
							</div>
						</div>
					</div>
        		</div>
        	</div>
        </div>
	</section>
@endsection
