<nav aria-label="Page navigation example">
    <ul class="pagination">
        <li class="page-item">
            <a class="page-link" href="{{ request()->fullUrlWithQuery(['page' => request()->page - 1]) }}" aria-label="Next">
                <span aria-hidden="true">&lsaquo;</span>
            </a>
        </li>
        @for($i = 1; $i <= $pages; $i++)
        <li class="page-item {{ (request()->page ?? 1) == $i ? 'active' : '' }}"><a class="page-link" href="{{ request()->fullUrlWithQuery(['page' => $i]) }}">{{ $i }}</a></li>
        @endfor
        <li class="page-item">
            <a class="page-link" href="{{ request()->fullUrlWithQuery(['page' => request()->page + 1]) }}" aria-label="Next">
                <span aria-hidden="true">&rsaquo;</span>
            </a>
        </li>
    </ul>
</nav>
