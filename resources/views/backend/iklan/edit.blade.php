@extends('layouts.app')
@section('title','Edit Iklan')
@section('content')
<div class="card">
    <div class="card">
        <div class="card-header">
            <h4>@yield('title')</h4>
        </div>
        
        <div class="card-body">

      {{ Form::model($iklan,['url'=>'/admin/iklan/'.$iklan->id,'files'=>true])}}
              
      @include('validation')

      @include('backend.iklan.form')
                                
        </div>

      {{ Form::close()}}
    </div>
</div>
@endsection

