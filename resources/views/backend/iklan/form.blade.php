<?php
$btnLabel = Request::segment(3)=='create'?'Create':'Update';
?>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Nama Iklan</label>
    <div class="col-sm-9">
        {{ Form::text('title',null,['class'=>'form-control','placeholder'=>'Nama Iklan'])}}
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-3 col-form-label">Description</label>
    <div class="col-sm-9">
        {{ Form::textarea('description',null,['class'=>'form-control description'])}}
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Link</label>
    <div class="col-sm-9">
        {{ Form::text('link',null,['class'=>'form-control','placeholder'=>'Ex : https://dev.globalvillage.co.id'])}}
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-3 col-form-label">Cover</label>
    <div class="col-sm-9">
        {{ Form::file('image',null,['class'=>'form-control','placeholder'=>'Cover'])}}
    </div>
</div>

@if(Request::segment(3)!='create')
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Old Cover</label>
    <div class="col-sm-9">
        <img src="{{ $iklan->image}}" class="img-thumbnail">
    </div>
</div>
@endif


<div class="form-group row">
    <label class="col-sm-3 col-form-label"></label>
    <div class="col-sm-9">
        <button class="btn btn-primary"><i class="fas fa-save"></i> {{ $btnLabel }}</button>
        <a href="/admin/iklan" class="btn btn-danger"><i class="fas fa-backward"></i> Back</a>
    </div>
</div>

@push('scripts')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>

<script>
    CKEDITOR.replace( 'description' );
</script>
@endpush