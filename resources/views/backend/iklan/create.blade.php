@extends('layouts.app')
@section('title','Create Ads')
@section('content')
<div class="card">
    <div class="card">
        <div class="card-header">
            <h4>@yield('title')</h4>
        </div>
        
        <div class="card-body">

      {{ Form::open(['url'=>'/admin/iklan','files'=>true])}}
              
      @include('validation')

      @include('backend.iklan.form')
                                
        </div>
      {{ Form::close()}}
    </div>
</div>
@endsection

