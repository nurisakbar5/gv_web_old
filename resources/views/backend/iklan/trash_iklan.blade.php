@extends('layouts.app')
@section('title','Trashed Iklan')
@section('content')
<div class="card">
	<div class="card-header">
		<div class="col-md-6">
			<ul class="nav nav-pills card-header-pills">
				<li class="nav-item">
					<a href="/admin/iklan" class="nav-link {{ Request::path() == 'admin/iklan' ? 'active' : '' }}">Data Iklan</a>
				</li>
				<li class="nav-item">
					<a href="/admin/iklan/trashed" class="nav-link {{ Request::path() == 'admin/iklan/trashed' ? 'active' : '' }}">Trashed</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="card-body">
		@include('alert')

		<table class="table table-striped" id="tabel-data">
			<thead>
				<tr>
					<th width="10">#</th>
					<th>Image</th>
					<th>Description</th>
					<th>Link</th>
					<th>Tanggal Dibuat</th>
					<th width="30">#</th>
					<th width="30">#</th>
				</tr>
			</thead>
			<tbody>
				@foreach($trashIklan as $trash)
				<tr>
					<td>{{ $loop->iteration}}</td>
					<td><img src="{{ $trash->image }}" width="200"></td>
					<td>
						<strong>{{ $trash->title }}</strong><hr>
						<div class="slideup" data-id="{{ $trash->id }}">
							{{ substr(strip_tags($trash->description), 0, 50) }}{{ strlen(strip_tags($trash->description)) > 50 ? "..." : "" }}
						</div>
						<div class="body" style="display:none" data-id="{{ $trash->id }}">
							{!! $trash->description !!}
						</div>
					</td>
					<td>
						<a href="{{ $trash->link }}" title="{{ $trash->title }}" target="_blank">{{ $trash->link }}</a>
					</td>
					<td>{{ $trash->created_at }}</td>
					<td><a href="/admin/iklan/restore/{{ $trash->id}}" class="btn btn-info">Restore</a></td>
					<td>
						{{ Form::open(['url'=>'/admin/iklan/delPermanent/'.$trash->id,'method'=>'delete','onsubmit'=>"return confirm('Are You Sure Delete this Permanently?')"])}}
						<button type="submit" class="btn btn-danger">Delete</button>
						{{ Form::close()}}
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
</div>
@endsection
@push('scripts')

<script src="https://demo.getstisla.com/assets/modules/datatables/datatables.min.js"></script>
<script src="https://demo.getstisla.com/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script>
	$(document).ready(function(){
		$('#tabel-data').DataTable();
	});
</script>
<script>
	$(document).ready(function(){
     $('.slideup').click(function(){
        $(this).next('.body').slideDown();
        $(this).closest('.slideup').hide();
     });
     $('.body').click(function(){
         $('.body').slideUp();
         $('.slideup').show();
     });
});
</script>
@endpush
