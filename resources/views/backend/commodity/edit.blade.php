@extends('layouts.app')
@section('title','Edit Market')
@section('content')
<div class="card">
    <div class="card">
        <div class="card-header">
            <h4>@yield('title')</h4>
        </div>
        
        <div class="card-body">

      {{ Form::model($commodity,['url'=>'/admin/commodity/'.$commodity->id, 'files'=>true, 'method'=>'PUT'])}}
              
      @include('validation')

      @include('backend.commodity.form')
                                
        </div>

      {{ Form::close()}}
    </div>
</div>
@endsection

