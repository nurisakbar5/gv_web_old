@extends('layouts.app')
@section('title','Create Commodity')
@section('content')
<div class="card">
    <div class="card">
        <div class="card-header">
            <h4>@yield('title')</h4>
        </div>
        
        <div class="card-body">

      {{ Form::open(['url'=>'/admin/commodity','files'=>true])}}
              
      @include('validation')

      @include('backend.commodity.form')
                                
        </div>

      {{ Form::close()}}
    </div>
</div>
@endsection

