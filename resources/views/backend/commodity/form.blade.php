<?php
$btnLabel = Request::segment(3)=='create'?'Create':'Update';
?>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Name</label>
    <div class="col-sm-9">
        {{ Form::text('name',null,['class'=>'form-control','placeholder'=>'Name'])}}
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-3 col-form-label">Category</label>
    <div class="col-sm-9">
        <select name="category_id" class="form-control">
            @foreach($pluckCategories as $category)
                <option value="{{ $category->id }}"
                @if(isset($commodity))
                    {{ $category->id==$commodity->category->id?'selected':'' }}
                @endif
                >{{ $category->name }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-3 col-form-label">Image</label>
    <div class="col-sm-9">
        {{ Form::file('image',['class'=>'form-control'])}}
    </div>
</div>

@if(Request::segment(4)=='edit')
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Old Image</label>
    <div class="col-sm-4">
        <img src="{{ $commodity->image }}" width="300">
        <br>
        Image
    </div>
</div>
@endif

<div class="form-group row">
    <label class="col-sm-3 col-form-label"></label>
    <div class="col-sm-9">
        <button class="btn btn-primary"><i class="fas fa-save"></i> {{ $btnLabel }}</button>
        <a href="/admin/commodity" class="btn btn-danger"><i class="fas fa-backward"></i> Back</a>
    </div>
</div>