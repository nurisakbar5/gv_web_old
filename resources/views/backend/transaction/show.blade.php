@extends('layouts.app')
@section('title', $product_detail->order_number)

@section('content')
<div class="card">
	<div class="card-header">
        <div class="col-md-6">
            <h4>Detail Transaksi</h4>
        </div>
        <div class="col-md-6 text-right">
                <a href="/admin/transaction/{{ $product_detail->id}}/edit" class="btn btn-outline-warning btn-normal btn-xs" title="Edit Transaksi"><i class="fa fa-highlighter"></i></a>
                <a href="/admin/transaction/" class="btn btn-outline-primary btn-normal btn-xs" title="Kembali"><i class="fa fa-backward"></i></a>
        </div>
	</div>
	<div class="card-body">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">Invoice</label>
			<div class="col-sm-9">
				{{ $product_detail->order_number }}
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">Waktu Transaksi</label>
			<div class="col-sm-9">
				{{ $product_detail->created_at }}
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">Status Order</label>
			<div class="col-sm-9">
				@if($product_detail->order_status=="pending")
				<span class="badge btn-danger">{{ $product_detail->order_status }}</span>
				@elseif($product_detail->order_status=="waiting_confirmation")
				<span class="badge btn-warning">{{ $product_detail->order_status }}</span>
				@elseif($product_detail->order_status=="order_processed")
				<span class="badge btn-primary">{{ $product_detail->order_status }}</span>
				@elseif($product_detail->order_status=="pengiriman")
				<span class="badge btn-info">{{ $product_detail->order_status }}</span>
				@elseif($product_detail->order_status=="order_arrived")
				<span class="badge btn-success">{{ $product_detail->order_status }}</span>
				@endif
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">Nama Penjual</label>
			<div class="col-sm-9">
				{{ $product_detail->seller->name }}
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">Nama Pembeli</label>
			<div class="col-sm-9">
				{{ $product_detail->buyer->name }}
			</div>
        </div>
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">Total Pembayaran</label>
			<div class="col-sm-9">
				{{ $product_detail->total_pay }}
			</div>
		</div>
	</div>
</div>
<div class="card">
	<div class="card-header">
		<h4>Detail Alamat Pengiriman</h4>
	</div>
	<div class="card-body">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">Nama Penerima</label>
			<div class="col-sm-9">
				{{ $product_detail->address_delivery->receiver_name }}
			</div>
        </div>
        <div class="form-group row">
			<label class="col-sm-3 col-form-label">Nama Alamat</label>
			<div class="col-sm-9">
				{{ $product_detail->address_delivery->name }}
			</div>
        </div>
        <div class="form-group row">
			<label class="col-sm-3 col-form-label">No. Handphone</label>
			<div class="col-sm-9">
				{{ $product_detail->address_delivery->phone }}
			</div>
        </div>
        <div class="form-group row">
			<label class="col-sm-3 col-form-label">Nama Jalan</label>
			<div class="col-sm-9">
				{{ $product_detail->address_delivery->street }}
			</div>
        </div>
        <div class="form-group row">
			<label class="col-sm-3 col-form-label">Lokasi</label>
			<div class="col-sm-9">
                {{ $product_detail->address_delivery->village_name }},
                {{ $product_detail->address_delivery->district_name }},
                {{ $product_detail->address_delivery->regency_name }},
                {{ $product_detail->address_delivery->province_name }}
			</div>
        </div>
        <div class="form-group row">
			<label class="col-sm-3 col-form-label">Kode Pos</label>
			<div class="col-sm-9">
				{{ $product_detail->address_delivery->post_code }}
			</div>
        </div>
	</div>
</div>
<div class="card">
	<div class="card-header">
		<h4>Produk - Produk Dalam Transaksi Ini</h4>
	</div>
	<div class="card-body">
            <table class="table table-striped table-hover" id="tabel-data">
                <thead>
                    <tr>
                        <th width="10">No</th>
                        <th width="100">Foto Produk</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($product_detail->purchase_item as $data)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td><img src="{{ $data->image}}" width="200"></td>
                            <td><b>{{ $data->name }}</b><hr>
                                Harga: {{ $data->price }} | Qty: {{ $data->qty }}
                                <hr>
                                {{ $data->subtotal }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
	</div>
</div>
@endsection
@push('scripts')

<script src="https://demo.getstisla.com/assets/modules/datatables/datatables.min.js"></script>
<script src="https://demo.getstisla.com/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function(){
        $('#tabel-data').DataTable();
    });
</script>
@endpush
