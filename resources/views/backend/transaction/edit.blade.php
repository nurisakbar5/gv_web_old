@extends('layouts.app')

@section('tittle', 'Edit Transaksi')

@section('css')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.1/css/selectize.css">
@endsection

@section('content')
<div class="card">
	<div class="card-header">
		<h4>Edit Transaksi</h4>
	</div>

	<div class="card-body">
		{{ Form::model($product_edit,['url'=>'/admin/transaction/'.$product_edit->id, 'files'=>true,'method'=>'PUT']) }}

		<div class="form-group row">
			<label class="col-sm-3 col-form-label">Invoice</label>
			<div class="col-sm-9">
				{{ Form::text('order_number',null,['class'=>'form-control','placeholder'=>'Invoice', 'disabled'=>''])}}
			</div>
		</div>

		<div class="form-group row">
			<label class="col-sm-3 col-form-label">Waktu Transaksi</label>
			<div class="col-sm-9">
				{{ Form::text('created_at',null,['class'=>'form-control','disabled'=>''])}}
			</div>
		</div>

		<div class="form-group row">
			<label class="col-sm-3 col-form-label">Status Order</label>
			<div class="col-sm-9">
				<select id="select-status" name="status" placeholder="Pilih Status Order"></select>
			</div>
		</div>

		<div class="form-group row">
			<label class="col-sm-3 col-form-label">Deskripsi Status Order</label>
			<div class="col-sm-9">
				<select id="select-deskripsi" name="description" placeholder="Pilih Deskripsi Status Order"></select>
			</div>
		</div>

		<div class="form-group row">
			<label class="col-sm-3 col-form-label">Nama Penjual</label>
			<div class="col-sm-9">
                {{-- {{ Form::text('seller->name',null,['class'=>'form-control','disabled'=>''])}} --}}
                <input type="text" name="seller" class="form-control" disabled='' value="{{ $product_edit->seller->name }}">
			</div>
		</div>

		<div class="form-group row">
			<label class="col-sm-3 col-form-label">Nama Pembeli</label>
			<div class="col-sm-9">
                {{-- {{ Form::text('buyer->name',null,['class'=>'form-control','disabled'=>''])}} --}}
            <input type="text" name="buyer" class="form-control" disabled='' value="{{ $product_edit->buyer->name }}">
			</div>
		</div>

		<div class="form-group row">
			<label class="col-sm-3 col-form-label"></label>
			<div class="col-sm-9">
				<button class="btn btn-primary" title="Update Produk"><i class="fas fa-save"></i> Update</button>
				<a href="/admin/transaction" class="btn btn-danger" title="Kembali"><i class="fas fa-backward"></i> Back</a>
			</div>
		</div>
	</div>
	{{ Form::close() }}
</div>
@endsection

@push('scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.1/js/standalone/selectize.js"></script>
<script type="text/javascript">
	var timeOutA, timeOUtS;

var setOptions = function (callback)
{
  clearTimeout(timeOutA);

  options =
  [
    { name: 'pending', value: 'pending' },
    { name: 'waiting_confirmation', value: 'waiting_confirmation' },
    { name: 'order_processed', value: 'order_processed' },
    { name: 'pengiriman', value: 'pengiriman' },
    { name: 'order_arrived', value: 'order_arrived' }
  ];
  callback(options);
};

var setDescOptions = function (callback, value)
{
  clearTimeout(timeOutS);

  var options = [];
  if (value === 'pending')
  {
    options =
    [
      { name: 'menunggu pembayaran', value: 'menunggu_pembayaran' }
    ];
  }
  else if (value === 'waiting_confirmation')
  {
    options =
    [
      { name: 'menunggu konfirmasi', value: 'menunggu_konfirmasi' }
    ];
  }
  else if (value === 'order_processed')
  {
    options =
    [
      { name: 'pesanan diproses', value: 'pesanan_diproses' }
    ];
  }
  else if (value === 'pengiriman')
  {
    options =
    [
      { name: 'sedang dikirim', value: 'sedang_dikirim' }
    ];
  }
  else if (value === 'order_arrived')
  {
    options =
    [
      { name: 'pesanan tiba', value: 'pesanan_tiba' }
    ];
  }
  callback(options);
  selectSize.updatePlaceholder();
  selectSize.enable();
};

var $selectStat = $('#select-status').selectize(
{
  loadingClass: 'selectizeLoading',
  valueField: 'value',
  labelField: 'name',
  searchField: [ 'name' ],
  onChange: function (value)
  {
    if (!value.length) return;
    selectSize.disable();
    selectSize.clearOptions();
    selectSize.load(function (callback) { timeOutS = setTimeout(setDescOptions, 1000, callback, value); });
  }
});

$('#select-deskripsi').selectize(
{
  loadingClass: 'selectizeLoading',
  closeAfterSelect: true,
  valueField: 'value',
  labelField: 'name',
  searchField: [ 'name' ],
  onChange: function (value)
  {
    if (!value.length) return;
    $('#choice').text("You chose: " + value);
  }
});

var selectStat = $selectStat[0].selectize;
selectStat.load(function (callback) { timeOutA = setTimeout(setOptions, 1000, callback); });
var selectSize = $('#select-deskripsi').data('selectize');
selectSize.disable();


</script>
@endpush
