@extends('layouts.app')

@section('title', 'Data Transaction')

@section('content')
<div class="card">
	<div class="card-header">
        <h4>@yield('title')</h4>
    </div>
		<hr>
		@include('alert')
	<div class="card-body">
		<table class="table table-striped table-bordered table-hover dataTable no-footer" id="tabel-data" role="grid" aria-describedby="tabel-data_info">
			<thead>
				<tr>
					<th width="10">#</th>
					<th>Invoice</th>
					<th>Tanggal Transaksi</th>
					<th>Status Order</th>
					<th width="30">#</th>
					<th width="30">#</th>
				</tr>
			</thead>
			<tbody>
				@foreach($products as $data)
					<tr>
						<td>{{ $loop->iteration }}</td>
						<td>{{ $data->order_number }}</td>
						<td>{{ $data->created_at }}</td>
						<td>
							@if($data->order_status=="pending")
								<span class="badge btn-danger">{{ $data->order_status }}</span>
							@elseif($data->order_status=="waiting_confirmation")
								<span class="badge btn-warning">{{ $data->order_status }}</span>
							@elseif($data->order_status=="order_processed")
								<span class="badge btn-primary">{{ $data->order_status }}</span>
							@elseif($data->order_status=="pengiriman")
								<span class="badge btn-info">{{ $data->order_status }}</span>
							@elseif($data->order_status=="order_arrived")
								<span class="badge btn-success">{{ $data->order_status }}</span>
							@endif
						</td>
						<td width="1"><a href="/admin/transaction/{{ $data->id}}" class="btn btn-danger">Detail</a></td>
						<td width="1"><a href="/admin/transaction/{{ $data->id}}/edit" class="btn btn-danger">Edit</a></td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
		
	</div>
@endsection
@push('scripts')

<script src="https://demo.getstisla.com/assets/modules/datatables/datatables.min.js"></script>
<script src="https://demo.getstisla.com/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function(){
        $('#tabel-data').DataTable();
    });
</script>
@endpush