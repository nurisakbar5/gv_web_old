@extends('layouts.app')
@section('title','Edit article')
@section('content')
<div class="card">
    <div class="card">
        <div class="card-header">
            <h4>@yield('title')</h4>
        </div>
        
        <div class="card-body">

      {{ Form::model($article,['url'=>'/admin/article/'.$article->id,'files'=>true])}}
              
      @include('validation')

      @include('backend.article.form')
                                
        </div>

      {{ Form::close()}}
    </div>
</div>
@endsection

