<?php
$btnLabel = Request::segment(3)=='create'?'Create':'Update';
?>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Title</label>
    <div class="col-sm-9">
        {{ Form::text('title',null,['class'=>'form-control','placeholder'=>'Title'])}}
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-3 col-form-label">Category</label>
    <div class="col-sm-9">
        {{-- {{ Form::select('category_id',$categories,null,['class'=>'form-control'])}} --}}
        <select name="category_id" class="form-control">
            @foreach($categories as $category)
                <option value="{{ $category->id }}"
                @if(isset($article))
                    {{ $category->id==$article->category->id?'selected':'' }}
                @endif
                >{{ $category->name }}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Body</label>
    <div class="col-sm-9">
        {{ Form::textarea('article',null,['class'=>'form-control article','placeholder'=>'Article'])}}
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Tags</label>
    <div class="col-sm-9">
        {{ Form::text('tags',null,['class'=>'form-control','placeholder'=>'Pisahkan Tag Dengan Koma, Ex : Tag1,Tag2,Tag3'])}}
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-3 col-form-label">Cover</label>
    <div class="col-sm-9">
        {{ Form::file('image',null,['class'=>'form-control','placeholder'=>'Cover'])}}
    </div>
</div>

@if(Request::segment(3)!='create')
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Old Cover</label>
    <div class="col-sm-9">
        <img src="{{ $article->image_url}}" class="img-thumbnail">
    </div>
</div>
@endif


<div class="form-group row">
    <label class="col-sm-3 col-form-label"></label>
    <div class="col-sm-9">
        <button class="btn btn-primary"><i class="fas fa-save"></i> {{ $btnLabel }}</button>
        <a href="/admin/article" class="btn btn-danger"><i class="fas fa-backward"></i> Back</a>
    </div>
</div>

@push('scripts')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
{{-- <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script> --}}


<script>
    CKEDITOR.replace( 'article' );
</script>


{{-- <script>
    tinymce.init({
      selector: '.article',
      height:300,
      plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount tinymcespellchecker a11ychecker imagetools textpattern help formatpainter permanentpen pageembed tinycomments mentions linkchecker',
  toolbar: 'formatselect | bold italic strikethrough forecolor backcolor permanentpen formatpainter | link image media pageembed | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | removeformat | addcomment',

    });
    </script> --}}
@endpush