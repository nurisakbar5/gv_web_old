@extends('layouts.app')
@section('title','Data article')
@section('content')
<div class="card">
    <div class="card-header">
        <h4>@yield('title')</h4>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-3">
                <a href="/admin/article/create" class="btn btn-danger">Create New</a>
            </div>
            <div class="col-md-9">
               
            </div>
        </div>
        <hr>

        @include('alert')
    
        <table class="table table-striped" id="tabel-data">
            <thead>
                <tr>
                    <th width="10">#</th>
                    <th>Title</th>
                    <th>Publish Date</th>
                    <th>Category</th>
                    <th width="30">#</th>
                    <th width="30">#</th>
                </tr>
            </thead>
            <tbody>
                @foreach($articles as $article)
                <tr>
                    <td>{{ $loop->iteration}}</td>
                    <td>{{ $article->title}} </td>
                    <td>{{ $article->publish_date}} </td>
                    <td>{{ $article->category->name}} </td>
                    <td><a href="/admin/article/{{ $article->id}}/edit" class="btn btn-danger">Edit</a></td>
                    <td>
                        {{ Form::open(['url'=>'/admin/article/'.$article->id,'method'=>'delete'])}}
                            <button type="submit" class="btn btn-danger">Delete</button>
                        {{ Form::close()}}
                </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    </div>
</div>
@endsection
@push('scripts')

<script src="https://demo.getstisla.com/assets/modules/datatables/datatables.min.js"></script>
<script src="https://demo.getstisla.com/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function(){
        $('#tabel-data').DataTable();
    });
</script>
@endpush
