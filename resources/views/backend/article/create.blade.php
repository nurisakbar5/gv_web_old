@extends('layouts.app')
@section('title','Create Article')
@section('content')
<div class="card">
    <div class="card">
        <div class="card-header">
            <h4>@yield('title')</h4>
        </div>
        
        <div class="card-body">

      {{ Form::open(['url'=>'/admin/article','files'=>true])}}
              
      @include('validation')

      @include('backend.article.form')
                                
        </div>

      {{ Form::close()}}
    </div>
</div>
@endsection

