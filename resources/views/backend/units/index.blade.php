@extends('layouts.app')
@section('title')
	Units
@endsection
@section('content')
	<div class="card">
		<div class="card-header">
			<div class="col-md-6">
				<ul class="nav nav-pills card-header-pills">
					<li class="nav-item">
						<a href="/admin/units" class="nav-link {{ Request::path() == 'admin/units' ? 'active' : '' }}">Data Units</a>
					</li>
				</ul>
			</div>
			<div class="col-md-6 text-right">
				<a id="tampil" onclick="addNewUnits()" class="btn btn-danger">Create New</a>
			</div>
		</div>
		<div class="card-body myData">
			@include('alert')
			<table class="table table-striped" id="tabel-data">
				<thead>
					<tr>
						<th width="10">#</th>
						<th>Name</th>
						<th>Weight In Gram</th>
						<th width="30">#</th>
						<th width="30">#</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($units->data as $item)
						<tr>
							<td>{{ $loop->iteration }}</td>
							<td>{{ $item->name }}</td>
							<td>{{ $item->weight_in_gram }}</td>
							<td><a id="tampil" onclick="editUnits('{{$item->id}}')" class="btn btn-warning edith">Edit</a></td>
							<td><a id="tampil" onclick="deleteUnits('{{$item->id}}')" class="btn btn-danger hapus">Delete</a></td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="card-body create-data" style="display:none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<form id="frmAddUnits">
					<div class="modal-header">
                        <h4 class="modal-title">Add New Units</h4>
                        <button aria-hidden="true" class="close" onclick="closeCreateData()" type="button">x</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="name">Unit Name</label>
							<input type="text" name="name" id="name" class="form-control" placeholder="Mis: Kg, Ekor, Ton">
						</div>
						<div class="form-group">
							<label for="weight_in_gram">Konversikan dalam satuan Gram</label>
							<input type="text" name="weight_in_gram" id="weight_in_gram" class="form-control" placeholder="Mis: 1000, 1000000">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" id="go" class="btn btn-primary">Add New Unit</button>
					</div>
				    </form>
				</div>
			</div>
        </div>
        <div class="card-body edit-data" style="display:none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="frmEditUnits">
                        <div class="modal-header">
                            <h4 class="modal-title">Edit Units</h4>
                            <button aria-hidden="true" class="close" onclick="closeEditData()" type="button">x</button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="name">Unit Name</label>
                                <input type="text" name="name" id="name" class="form-control" placeholder="Mis: Kg, Ekor, Ton">
                            </div>
                            <div class="form-group">
                                <label for="weight_in_gram">Konversikan dalam satuan Gram</label>
                                <input type="text" name="weight_in_gram" id="weight_in_gram" class="form-control" placeholder="Mis: 1000, 1000000">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" id="id" name="id">
                            <button type="button" id="edit" class="btn btn-primary">Edit This Unit</button>
                        </div>
                    </form>
                </div>
	        </div>
        </div>
        <div class="card-body delete-data" style="display:none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="frmDeleteUnit">
                        <div class="modal-header">
                            <h4 class="modal-title" id="delete-title" name="title">Delete Unit</h4>
                            <button type="button" aria-hidden="true" class="close" onclick="closeDeleteData()">
                                x
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Anda Yakin Ingin Menghapus Unit Ini?</p>
                            <p class="text-warning">
                                <small>Tindakan ini tidak bisa dibatalkan</small>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" id="id" name="id" value="0">
                            <button class="btn btn-danger" id="delete" type="button">
                                Hapus Unit
                            </button>
                        </div>
                    </form>
                </div>
	        </div>
        </div>
	</div>
@endsection
@push('scripts')
<script src="https://demo.getstisla.com/assets/modules/datatables/datatables.min.js"></script>
<script src="https://demo.getstisla.com/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function(){
        $('#tabel-data').DataTable();
    });
</script>
<script>
   function addNewUnits()
   {
       $(document).ready(function(){
            $('.myData').hide();
            $('.create-data').slideToggle();
       });
   }
   function closeCreateData()
   {
       $(document).ready(function(){
            $('.myData').fadeIn();
            $('.create-data').fadeOut();
       });
   }
   function closeEditData()
   {
       $(document).ready(function(){
            $('.myData').fadeIn();
            $('.edit-data').fadeOut();
       });
   }
   function closeDeleteData()
   {
       $(document).ready(function(){
            $('.myData').fadeIn();
            $('.delete-data').fadeOut();
       });
   }
   function editUnits(units_id)
   {
		$.ajax({
			type: "GET",
			url: "/admin/units/" + units_id,
			// beforeSend: function(){
			// 	// $('.go').css("visibility", "hidden");
			// 	$('.edith').addClass('btn-progress');
			// },
			success: function (data) {
				console.log(data);
                $('.myData').fadeOut();
                $('.edit-data').fadeIn();
				$("#frmEditUnits input[name=name]").val(data.detail.name);
				$("#frmEditUnits input[name=weight_in_gram]").val(data.detail.weight_in_gram);
				$("#frmEditUnits input[name=id]").val(data.detail.id);
			},
			error: function(data){
			console.log(data);
			}
		});
   }
   function deleteUnits(units_id)
   {
		$.ajax({
			type: 'GET',
			url: '/admin/units/' + units_id,
			// beforeSend: function(){
			//     $('.loader').css("visibility", "visible");
			//     $('.ge').css("visibility", "hidden");
			// },
			success: function(data){
				$("#frmDeleteUnit #delete-title").html("Hapus Unit (" + data.detail.name + ")?");
                $("#frmDeleteUnit input[name=id]").val(data.detail.id);
                $('.myData').fadeOut();
				$(".delete-data").fadeIn();
			},
			error: function(data){
				console.log(data);
			}
		});
   }
   $(document).ready(function(){
		$("#go").click(function(){
			$.ajaxSetup({
				headers:{
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			$.ajax({
				type: "POST",
				url: "/admin/units/create",
				data:{
					name: $("#frmAddUnits input[name=name]").val(),
					weight_in_gram: $("#frmAddUnits input[name=weight_in_gram]").val(),
				},
				dataType: "json",
				beforeSend: function(){
					// $('.go').css("visibility", "hidden");
					$('#go').addClass('btn-progress');
				},
				success: function (response) {
					$('#frmAddUnits').trigger("reset");
					$('#frmAddUnits .close').click();
					window.location.reload();
				}
			});
		});
		$("#edit").click(function(){
			$.ajaxSetup({
				headers:{
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			$.ajax({
				type: "PUT",
				url: "/admin/units/" + $("#frmEditUnits input[name=id]").val(),
				data:{
					name: $("#frmEditUnits input[name=name]").val(),
					weight_in_gram: $("#frmEditUnits input[name=weight_in_gram]").val(),
				},
				dataType: "json",
				beforeSend: function(){
					// $('.go').css("visibility", "hidden");
					$('#edit').addClass('btn-progress');
				},
				success: function (response) {
					$('#frmEditUnits').trigger("reset");
					$('#frmEditUnits .close').click();
					window.location.reload();
				}
			});
		});
		$("#delete").click(function(){
			$.ajaxSetup({
			headers:{
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
			});
			$.ajax({
				type: 'DELETE',
				url: '/admin/units/' + $("#frmDeleteUnit input[name=id]").val(),
				dataType: 'json',
				beforeSend: function(){
					// $('.go').css("visibility", "hidden");
					$('#delete').addClass('btn-progress');
				},
				success: function(data){
					$("#frmDeleteUnit .close").click();
					window.location.reload();
				},
				error: function(data){
					console.log(data);
				}
			});
		});
   });
</script>
@endpush
