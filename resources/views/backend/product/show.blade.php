@extends('layouts.app')
@section('title', $product->name)

@section('content')
<div class="card">
	<div class="card-header">
		<h4>Detail Product</h4>
	</div>
	<div class="card-body">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">Nama Produk</label>
			<div class="col-sm-9">
				{{ $product->name }}
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">Harga</label>
			<div class="col-sm-9">
				Rp {{ $product->price_idr }}
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">Stok</label>
			<div class="col-sm-9">
				&#177; {{ $product->stock }}
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">Kategori</label>
			<div class="col-sm-9">
				{{ $product->category->name }}
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">Petani</label>
			<div class="col-sm-9">
				{{ $product->user->name }}
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">Deskripsi Produk</label>
			<div class="col-sm-9">
				{!! $product->description !!}
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">Gambar Cover</label>
			<div class="col-sm-9">
				<img src="{{ $product->image_url}}" class="img-thumbnail" height="150px" width="300px">
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">Gambar Produk</label>
			<div class="col-sm-9">
				<img src="{{ $product->back_images[0]->image_1 }}" class="img-thumbnail" height="150px" width="200px">
				<img src="{{ $product->back_images[1]->image_2 }}" class="img-thumbnail" height="150px" width="200px">
				<img src="{{ $product->back_images[2]->image_3 }}" class="img-thumbnail" height="150px" width="200px">
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">Tanggal Publish Produk</label>
			<div class="col-sm-9">
				{{ date('j F, Y', strtotime($product->publish_date)) }}
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-3 col-form-label">Lokasi Produk</label>
			<div class="col-sm-9">
				{{ $product->region->vilage }}, {{ $product->region->district }}, {{ $product->region->regency }}, {{ $product->region->province }}
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-3 col-form-label"></label>
			<div class="col-sm-9">
				<a href="/admin/product/{{ $product->id}}/edit" class="btn btn-danger" title="Edit Produk"><i class="fa fa-highlighter"></i> Edit</a>
				<a href="/admin/product/" title="Kembali" class="btn btn-warning"><i class="fa fa-backward"></i> Kembali</a>
			</div>
		</div>
	</div>
</div>
@endsection
