<?php
$btnLabel = Request::segment(3)=='create'?'Create':'Update';
?>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Nama Produk</label>
    <div class="col-sm-9">
        {{ Form::text('name',null,['class'=>'form-control','placeholder'=>'Nama Produk'])}}
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-3 col-form-label">Harga</label>
    <div class="col-sm-9">
        {{ Form::text('price_idr',null,['class'=>'form-control','placeholder'=>'Harga'])}}
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-3 col-form-label">Kategori Produk</label>
    <div class="col-sm-9">
        {{-- {{ Form::select('category_id',$categories,null,['class'=>'form-control'])}} --}}
        <select name="category_id" class="form-control">
            @foreach($categories as $category)
                <option value="{{ $category->id }}"
                @if(isset($product))
                    {{ $category->id==$product->category->id?'selected':'' }}
                @endif
                >{{ $category->name }}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Deskripsi Produk</label>
    <div class="col-sm-9">
        {{ Form::textarea('description',null,['class'=>'form-control product','placeholder'=>'product'])}}
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-3 col-form-label">Cover</label>
    <div class="col-sm-9">
        {{ Form::file('image',null,['class'=>'form-control','placeholder'=>'Cover'])}}
    </div>
</div>

@if(Request::segment(3)!='create')
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Old Cover</label>
    <div class="col-sm-9">
        <img src="{{ $product->image_url}}" class="img-thumbnail">
    </div>
</div>
@endif


<div class="form-group row">
    <label class="col-sm-3 col-form-label"></label>
    <div class="col-sm-9">
        <button class="btn btn-primary" title="Update Produk"><i class="fas fa-save"></i> {{ $btnLabel }}</button>
        <a href="/admin/product" class="btn btn-danger" title="Kembali"><i class="fas fa-backward"></i> Back</a>
    </div>
</div>

@push('scripts')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
{{-- <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script> --}}


<script>
    CKEDITOR.replace( 'description' );
</script>


{{-- <script>
    tinymce.init({
      selector: '.product',
      height:300,
      plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount tinymcespellchecker a11ychecker imagetools textpattern help formatpainter permanentpen pageembed tinycomments mentions linkchecker',
  toolbar: 'formatselect | bold italic strikethrough forecolor backcolor permanentpen formatpainter | link image media pageembed | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | removeformat | addcomment',

    });
    </script> --}}
@endpush