@extends('layouts.app')
@section('title','Edit Produk')
@section('content')
<div class="card">
    <div class="card">
        <div class="card-header">
            <h4>@yield('title')</h4>
        </div>
        
        <div class="card-body">

      {{ Form::model($product,['url'=>'/admin/product/'.$product->id,'files'=>true])}}
              
      @include('validation')

      @include('backend.product.form')
                                
        </div>

      {{ Form::close()}}
    </div>
</div>
@endsection

