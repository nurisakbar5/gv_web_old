@extends('layouts.app')
@section('title','Data Product')
@section('content')
<div class="card">
    <div class="card-header">
        <h4>@yield('title')</h4>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-3">
                <a href="/admin/product/create" class="btn btn-danger">Create New</a>
            </div>
            <div class="col-md-9">

            </div>
        </div>
        <hr>

        @include('alert')

        <table class="table table-striped" id="tabel-data">
            <thead>
                <tr>
                    <th width="10">#</th>
                    <th>Foto</th>
                    <th>Description</th>
                    <th width="30">#</th>
                    <th width="30">#</th>
                    <th width="30">#</th>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $product)
                <tr>
                    <td>{{ $loop->iteration}}</td>
                    <td><img src="{{ $product->image_url}}" width="200"></td>
                    <td>{{ $product->name }}<hr>
                        Harga  : {{ $product->price_idr}} | Category : {{ $product->category->name }} | Petani : <a href="/user/{{ $product->user->id}}">{{ $product->user->name}}</a>
                        <hr>
                        {!! $product->description !!} </td>
                    <td><a href="/admin/product/{{ $product->slug}}" class="btn btn-danger">Detail</a></td>
                    <td><a href="/admin/product/{{ $product->id}}/edit" class="btn btn-danger">Edit</a></td>
                    <td>
                        {{ Form::open(['url'=>'/admin/product/'.$product->id,'method'=>'delete'])}}
                            <button type="submit" class="btn btn-danger">Delete</button>
                        {{ Form::close()}}
                </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    </div>
@endsection
@push('scripts')

<script src="https://demo.getstisla.com/assets/modules/datatables/datatables.min.js"></script>
<script src="https://demo.getstisla.com/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function(){
        $('#tabel-data').DataTable();
    });
</script>
@endpush
