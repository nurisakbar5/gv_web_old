@extends('layouts.app')
@section('title','Create Product')
@section('content')
<div class="card">
    <div class="card">
        <div class="card-header">
            <h4>@yield('title')</h4>
        </div>
        
        <div class="card-body">

      {{ Form::open(['url'=>'/admin/product'])}}
              
      @include('validation')

      @include('backend.product.form')
                                
        </div>

      {{ Form::close()}}
    </div>
</div>
@endsection

