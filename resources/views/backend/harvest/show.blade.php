@extends('layouts.app')
@section('title','Data Panen')
@section('content')
<div class="card">
	<div class="card">
		<div class="card-header">
			<h4>@yield('title')</h4>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12 text-center">
						<h6>Detail Panen</h6>
					</div>
					<table class="table table-bordered">
						<tr>
							<th>Name</th>
							<td>{{ $harvest->data->title}}</td>
						</tr>
						<tr>
							<th>Description</th>
							<td>{!! $harvest->data->description !!}</td>
						</tr>
						<tr>
							<th>Waktu Publikasi</th>
							<td>{{ $harvest->data->publish_date }}</td>
						</tr>
						<tr>
							<th>Estimasi Waktu Panen</th>
							<td>{{ $harvest->data->estimated_date }}</td>
						</tr>
						<tr>
							<th>Estimasi Pendapatan</th>
							<td>Rp {{ $harvest->data->estimated_income }}</td>
						</tr>
						<tr>
							<th>Kategori</th>
							<td>{{ $harvest->data->category->name }}</td>
						</tr>
						<tr>
							<th>Satuan Unit</th>
							<td>{{ $harvest->data->unit->name }}</td>
						</tr>
						<tr>
							<th>Lokasi Panen</th>
							<td>{{ $harvest->data->region->vilage }}, {{ $harvest->data->region->district }}, {{ $harvest->data->region->regency }}, {{ $harvest->data->region->province }}</td>
						</tr>
						<tr>
							<th>Galeri Foto</th>
							<td>
								<div class="col-sm-12">
									@foreach($harvest->data->front_images as $images)
									<img src="{{ $images }}" class="img-thumbnail" height="150px" width="200px">
									@endforeach
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<hr>
			<div class="col-md-12 text-center">
				<h6>Detail Petani</h6>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-3">
					<img src="{{ $harvest->data->user->photo }}" class="img-thumbnail">
				</div>
				<div class="col-md-8">

					<h6>Biodata Petani</h6>

					<table class="table table-hover table-bordered">
						<tr>
							<th>Nama Lengkap</th>
							<td>{{ $harvest->data->user->name }}</td>
						</tr>
						<tr>
							<th>Alamat Lengkap</th>
							<td>
								{{-- {{ $harvest->data->user->region->village_name }},
								{{ $harvest->data->user->region->district_name }},
								{{ $harvest->data->user->region->regency_name }},
								{{ $harvest->data->user->region->province_name }} --}}
							</td>
						</tr>
					</table>
					<h6>Lahan Yang Dimiliki</h6>

					<table class="table table-hover table-bordered">
						<tr>
							<th>Nama Lahan</th>
							<td>{{ $harvest->data->land->name }}</td>
						</tr>
						<tr>
							<th>Luas Keseluruhan</th>
							<td>
								{{ $harvest->data->land->large }}
								{{ $harvest->data->land->unit_area }}<sup>2</sup>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="card-footer">
			<a href="/admin/harvest" title="Kembali" class="btn btn-info"><i class="fa fa-back"></i> Kembali</a>
		</div>
	</div>
</div>
@endsection

