@extends('layouts.app')
@section('title', 'Trashed Harvest')
@section('content')
<div class="card">
	<div class="card-header">
		<div class="col-md-6">
			<ul class="nav nav-pills card-header-pills">
				<li class="nav-item">
					<a href="/admin/harvest" class="nav-link {{ Request::path() == 'admin/harvest' ? 'active' : '' }}">Data Harvest</a>
				</li>
				<li class="nav-item">
					<a href="/admin/harvest/trashed" class="nav-link {{ Request::path() == 'admin/harvest/trashed' ? 'active' : '' }}">Trashed Harvest</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="card-body">
		@include('alert')

		<table class="table table-striped" id="tabel-data">
			<thead>
				<tr>
					<th width="10">#</th>
					<th>Image</th>
					<th>Description</th>
					<th>Estimasi Waktu Panen</th>
					<th>Estimasi Pendapatan</th>
					<th width="30">#</th>
					<th width="30">#</th>
				</tr>
			</thead>
			<tbody>
				@foreach($trashHarvest as $data)
				<tr>
					<td>{{ $loop->iteration}}</td>
					<td><img src="{{ $data->back_images[0]->image_1 }}" width="200"></td>
					<td>
						<strong>{{ $data->title }}</strong><hr>
						{{ substr(strip_tags($data->description), 0, 50) }}{{ strlen(strip_tags($data->description)) > 50 ? "..." : "" }}
					</td>
					<td>
						{{ $data->estimated_date }}
					</td>
					<td>{{ $data->estimated_income }}</td>
					<td><a href="/admin/harvest/restore/{{ $data->slug}}" class="btn btn-info">Restore</a></td>
					<td>
						{{ Form::open(['url'=>'/admin/harvest/delPermanent/'.$data->slug,'method'=>'delete','onsubmit'=>"return confirm('Are You Sure Delete this Permanently?')"])}}
						<button type="submit" class="btn btn-danger">Delete</button>
						{{ Form::close()}}
					</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
</div>
</div>
@endsection
@push('scripts')

<script src="https://demo.getstisla.com/assets/modules/datatables/datatables.min.js"></script>
<script src="https://demo.getstisla.com/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script>
	$(document).ready(function(){
		$('#tabel-data').DataTable();
	});
</script>
@endpush