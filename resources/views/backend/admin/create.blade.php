@extends('layouts.app')
@section('title','Create Admin')
@section('content')
<div class="card">
    <div class="card">
        <div class="card-header">
            <h4>@yield('title')</h4>
        </div>

        <div class="card-body">

      {{ Form::open(['url'=>'/admin/user-admin',])}}

      @include('validation')

      @include('backend.admin.form')

        </div>

      {{ Form::close()}}
    </div>
</div>
@endsection

