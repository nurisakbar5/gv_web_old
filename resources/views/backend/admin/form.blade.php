<?php
$btnLabel = Request::segment(3)=='create'?'Create':'Update';
?>

<div class="form-group row">
    <label class="col-sm-3 col-form-label">Name</label>
    <div class="col-sm-9">
        {{ Form::text('name',null,['class'=>'form-control','placeholder'=>'Nama Lengkap', ])}}
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-3 col-form-label">Email</label>
    <div class="col-sm-9">
        {{ Form::email('email',null,['class'=>'form-control','placeholder'=>'Email Anda', ])}}
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-3 col-form-label">Password</label>
    <div class="col-sm-9">
        {{-- {{ Form::password('password',null,['class'=>'form-control','placeholder'=>'Password', ])}} --}}
        <input type="password" name="password" class="form-control" placeholder="Password" id="password">
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label"></label>
    <div class="col-sm-9">
        {{-- {{ Form::password('password_confirmation',null,['class'=>'form-control','placeholder'=>'Confirm Password', ])}} --}}
        <input type="password" name="password_confirmation" class="form-control" placeholder="Konfirmasi Password" id="password_confirmation">
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-3 col-form-label"></label>
    <div class="col-sm-9">
    <button class="btn btn-primary"><i class="fas fa-save"></i> {{ $btnLabel}}</button>
        <a href="/admin/user-admin" class="btn btn-danger"><i class="fas fa-backward"></i> Back</a>
    </div>
</div>


