@extends('layouts.app')
@section('title','Edit Admin')
@section('content')
<div class="card">
    <div class="card">
        <div class="card-header">
            <h4>@yield('title')</h4>
        </div>

        <div class="card-body">

      {{ Form::model($admin,['url'=>'/admin/user-admin/'.$admin->id,'files'=>true])}}

      @include('validation')

      @include('backend.admin.form')

        </div>

      {{ Form::close()}}
    </div>
</div>
@endsection

