@extends('layouts.app')
@section('title','Trashed Admin')
@section('content')
<div class="card">
	<div class="card-header">
		<div class="col-md-6">
			<ul class="nav nav-pills card-header-pills">
				<li class="nav-item">
					<a href="/admin/user/" class="nav-link {{ Request::path() == 'admin/user' ? 'active' : '' }}">Data User</a>
				</li>
				<li class="nav-item">
					<a href="/admin/user-admin" class="nav-link {{ Request::path() == 'admin/user-admin' ? 'active' : '' }}">Data Admin</a>
				</li>
				<li class="nav-item">
					<a href="/admin/user-admin/trashed" class="nav-link {{ Request::path() == 'admin/user-admin/trashed' ? 'active' : '' }}">Trashed Admin</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="card-body">
		@include('alert')

		<table class="table table-striped" id="tabel-data">
			<thead>
				<tr>
					<th width="10">#</th>
					<th>Name</th>
					<th>Email</th>
					<th width="30">#</th>
					<th width="30">#</th>
				</tr>
			</thead>
			<tbody>
				@foreach($trashAdmin as $user)
				<tr>
					<td>{{ $loop->iteration}}</td>
					<td>{{ $user->name}} </td>
					<td>{{ $user->email }} </td>
					<td><a href="/admin/user-admin/restore/{{ $user->id}}" class="btn btn-info">Restore</a></td>
					<td>
						{{ Form::open(['url'=>'/admin/user-admin/delete-permanent/'.$user->id,'method'=>'delete','onsubmit'=>"return confirm('Delete this admin permanently?')"])}}
						<button type="submit" class="btn btn-danger">Delete Permanent</button>
						{{ Form::close()}}
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
</div>
@endsection
@push('scripts')

<script src="https://demo.getstisla.com/assets/modules/datatables/datatables.min.js"></script>
<script src="https://demo.getstisla.com/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script>
	$(document).ready(function(){
		$('#tabel-data').DataTable();
	});
</script>
@endpush
