<?php
$btnLabel = Request::segment(3)=='create'?'Create':'Update';
?>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Title</label>
    <div class="col-sm-9">
        {{ Form::text('title',null,['class'=>'form-control','placeholder'=>'Video Title'])}}
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Category</label>
    <div class="col-sm-9">
        {{ Form::textarea('description',null,['class'=>'form-control','placeholder'=>'Video Description'])}}
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-3 col-form-label">Video Category</label>
    <div class="col-sm-9">
        <select name="category_id" class="form-control">
            @foreach($categories as $category)
                <option value="{{ $category->id }}"
                    @if(isset($video))
                    {{ $category->id==$video->category->id?'selected':'' }}
                    @endif
                >{{ $category->name }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-3 col-form-label">Tags</label>
    <div class="col-sm-9">
        {{ Form::text('tags',null,['class'=>'form-control','placeholder'=>'Pisahkan Tag Dengan Koma, Ex : Tag1,Tag2,Tag3'])}}
    </div>
</div>

@if(Request::segment(3)!='create')
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Video Preview</label>
    <div class="col-sm-9">
        <video width="320" height="240" controls>
            <source src="{{ $video->video_file }}" type="video/mp4">
            <source src="{{ $video->video_file }}" type="video/ogg">
          Your browser does not support the video tag.
          </video> 
    </div>
</div>
@endif

<div class="form-group row">
    <label class="col-sm-3 col-form-label">Video Source</label>
    <div class="col-sm-4">
        {{ Form::file('video',null,['class'=>'form-control'])}}
    </div>
    {{-- <div class="col-sm-4">
        {{ Form::file('image',null,['class'=>'form-control'])}}
    </div> --}}
</div>

<div class="form-group row">
    <label class="col-sm-3 col-form-label"></label>
    <div class="col-sm-9">
        <button class="btn btn-primary"><i class="fas fa-save"></i> {{ $btnLabel}}</button>
        <a href="/admin/video" class="btn btn-danger"><i class="fas fa-backward"></i> Back</a>
    </div>
</div>

@push('scripts')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>

<script>
    CKEDITOR.replace('description');
</script>
@endpush

