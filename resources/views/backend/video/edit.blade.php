@extends('layouts.app')
@section('title','Edit video')
@section('content')
<div class="card">
    <div class="card">
        <div class="card-header">
            <h4>@yield('title')</h4>
        </div>
        
        <div class="card-body">

      {{ Form::model($video,['url'=>'/admin/video/'.$video->id,'method'=>'PUT','files'=>true])}}
              
      @include('validation')

      @include('backend.video.form')
                                
        </div>

      {{ Form::close()}}
    </div>
</div>
@endsection

