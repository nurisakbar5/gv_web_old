@extends('layouts.app')
@section('title','Create Price')
@section('content')
<div class="card">
    <div class="card">
        <div class="card-header">
            <h4>@yield('title')</h4>
        </div>
        
        <div class="card-body">

      {{ Form::open(['url'=>'/admin/price'])}}
              
      @include('validation')

      @include('backend.price.form')
                                
        </div>

      {{ Form::close()}}
    </div>
</div>
@endsection

