<?php
$btnLabel = Request::segment(3)=='create'?'Create':'Update';
?>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Price</label>
    <div class="col-sm-9">
        {{ Form::text('price',null,['class'=>'form-control','placeholder'=>'Price'])}}
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-3 col-form-label">Market</label>
    <div class="col-sm-9">
        <select name="market_id" class="form-control">
            @foreach($markets as $market)
                <option value="{{ $market->id }}"
                @if(isset($price))
                    {{ $market->id==$price->market->id_pasar?'selected':'' }}
                @endif
                >{{ $market->name }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-3 col-form-label">Commodity</label>
    <div class="col-sm-9">
        <select name="commodity_id" class="form-control">
            @foreach($commodities as $commodity)
                <option value="{{ $commodity->id }}"
                @if(isset($price))
                    {{ $commodity->id==$price->commodity->id_commodity?'selected':'' }}
                @endif
                >{{ $commodity->name }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-3 col-form-label"></label>
    <div class="col-sm-9">
        <button class="btn btn-primary"><i class="fas fa-save"></i> {{ $btnLabel }}</button>
        <a href="/admin/price" class="btn btn-danger"><i class="fas fa-backward"></i> Back</a>
    </div>
</div>