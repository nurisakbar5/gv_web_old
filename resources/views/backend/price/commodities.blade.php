@extends('layouts.app')
@section('title','Data Price')
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@stop
@section('content')
<div class="card">
    <div class="card-header">
        <h4>Daftar Harga Berdasarkan Komoditas</h4>
        <div class="dropdown ml-auto">
            <a href="" class="dropdown-toggle" data-toggle="dropdown">Tampilkan Berdasarkan</a>
            <ul class="dropdown-menu">
                <li><a href="{{ url('admin/price') }}" class="dropdown-item">Pasar</a></li>
                <li><a href="{{ url('admin/price/commodities') }}" class="dropdown-item">Komoditas</a></li>
                <li><a href="{{ url('admin/price/regencies') }}" class="dropdown-item">Kabupaten</a></li>
            </ul>
        </div>
    </div>
    <div class="card-body">
        @include('alert')
        <div class="row mb-5">
            <div class="col-lg-3">
                <div class="section-title mb-3 mt-4">Period</div>
                <form>
                    @if(request()->commodities)
                    <input type="hidden" name="commodities" value="{{ request()->commodities }}">
                    @endif
                    <div class="input-group">
                        <input type="text" name="period" class="form-control datepicker" value="{{ request()->period ?? date('Ymd') }}">
                        <div class="input-group-append">
                            <button class="btn btn-block btn-primary">
                                Filter
                            </button>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="col-lg-3">
                    <div class="section-title mb-3 mt-4">Komoditas</div>
                    <form>
                        @if(request()->period)
                        <input type="hidden" name="period" value="{{ request()->period }}">
                        @endif
                        <div class="input-group">
                            <select name="commodities" class="form-control province" style="padding: 0 !important;">
                                <option value="">Pilih Komoditas</option>
                                @foreach($commodities as $commodity)
                                <option value="{{ $commodity->id ?? '' }}"> {{ $commodity->name ?? $commodity }}</option>
                                @endforeach
                            </select>
                            <div class="input-group-append">
                                <button class="btn btn-block btn-primary btn-sm">Filter</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <table class="table table-striped" id="tabel-data">
                <thead>
                    <tr>
                        <th width="10">#</th>
                        <th>Komoditas</th>
                        <th>Tanggal</th>
                        <th>Harga</th>
                        <th>Lokasi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($prices as $price)
                    <tr>
                        <td>{{ $loop->iteration}}</td>
                        <td>{{ $price->commodity_name }} </td>
                        <td>{{ $price->date }} </td>
                        <td>{{ $price->market->price }} </td>
                        <td>{{ $price->market->market_name }}, {{ $price->name_regency }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@push('scripts')

<script src="https://demo.getstisla.com/assets/modules/datatables/datatables.min.js"></script>
<script src="https://demo.getstisla.com/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
    $(document).ready(function(){
        $('#tabel-data').DataTable();
    });
</script>
<script>
    $('.datepicker').daterangepicker({
        singleDatePicker: true,
        autoUpdateInput: false,
        locale: {
            format: 'YYYY-MM-DD'
        }
    }, function(e, d) {
        $(this.element).val(e.format('YYYY-MM-DD'));
    });
</script>
@endpush
