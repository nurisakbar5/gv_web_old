@extends('layouts.app')
@section('title','Edit Price')
@section('content')
<div class="card">
    <div class="card">
        <div class="card-header">
            <h4>@yield('title')</h4>
        </div>
        
        <div class="card-body">

      {{ Form::model($price,['url'=>'/admin/price/'.$price->id, 'method'=>'PUT'])}}
              
      @include('validation')

      @include('backend.price.form')
                                
        </div>

      {{ Form::close()}}
    </div>
</div>
@endsection

