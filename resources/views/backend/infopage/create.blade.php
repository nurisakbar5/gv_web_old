@extends('layouts.app')
@section('title','Create Info Page')
@section('content')
<div class="card">
    <div class="card">
        <div class="card-header">
            <h4>@yield('title')</h4>
        </div>

        <div class="card-body">

      {{ Form::open(['url'=>'/admin/infopage'])}}

      {{-- @include('validation') --}}

      @include('backend.infopage.form')

        </div>

      {{ Form::close()}}
    </div>
</div>
@endsection

