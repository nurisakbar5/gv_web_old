@extends('layouts.app')
@section('title','Edit Info Page')
@section('content')
<div class="card">
    <div class="card">
        <div class="card-header">
            <h4>@yield('title')</h4>
        </div>
        
        <div class="card-body">

      {{ Form::model($infopage,['url'=>'/admin/infopage/'.$infopage->id,'method'=>'PUT'])}}
              
      @include('validation')

      @include('backend.infopage.form')
                                
        </div>

      {{ Form::close()}}
    </div>
</div>
@endsection

