<?php
$btnLabel = Request::segment(3)=='create'?'Create':'Update';
?>

<div class="form-group row {{ $errors->first('title') ? "is-invalid": ""}}">
    <label class="col-sm-3 col-form-label {{ $errors->first('title') ? "text-danger": ""}}">Info Page Title</label>
    <div class="col-sm-9">
        {{ Form::text('title',null,['class'=>'form-control ' . ($errors->first('title') ? 'is-invalid': ''),'placeholder'=>'Info Page Title'])}}
        <span class="text-danger">
            {{ $errors->first('title') }}
        </span>
    </div>
</div>
<div class="form-group row {{ $errors->first('description') ? "is-invalid": ""}}">
    <label class="col-sm-3 col-form-label {{ $errors->first('description') ? "text-danger": ""}}">Description</label>
    <div class="col-sm-9">
        {{ Form::textarea('description',null,['class'=>'form-control description'])}}
        <br>
        <div class=" {{ $errors->first('description') ? "alert alert-danger": ""}}">
            {{ $errors->first('description') }}
        </div>
    </div>
</div>

<div class="form-group row {{ $errors->first('publish') ? "is-invalid": ""}}">
    <label class="col-sm-3 col-form-label {{ $errors->first('publish') ? "text-danger": ""}}">Publish</label>
    <div class="col-sm-9">
        {{ Form::select('publish',['y'=>'Yes','n'=>'No'],null,['class'=>'form-control' . ($errors->first('publish') ? 'is-invalid': '')])}}
        <span class="text-danger">
            {{ $errors->first('publish') }}
        </span>
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-3 col-form-label"></label>
    <div class="col-sm-9">
    <button class="btn btn-primary"><i class="fas fa-save"></i> {{ $btnLabel}}</button>
        <a href="/admin/infopage" class="btn btn-danger"><i class="fas fa-backward"></i> Back</a>
    </div>
</div>
@push('scripts')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>

<script>
    CKEDITOR.replace( 'description' );
</script>
@endpush
