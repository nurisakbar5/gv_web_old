@extends('layouts.app')

@section('title', 'Data Halaman Informasi')

@section('css')
<style>
a.disabled {
  pointer-events: none;
  cursor: default;
}
</style>
@endsection

@section('content')

<div class="card">
    <div class="card-header">
        <h4>@yield('title')</h4>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-3">
                <a href="/admin/infopage/create" class="btn btn-danger disabled" >Create New</a>
            </div>
            <div class="col-md-9">

            </div>
        </div>
        <hr>

        @include('alert')

        <table class="table table-striped" id="tabel-data">
            <thead>
                <tr>
                    <th width="10">#</th>
                    <th>Title</th>
                    <th>Link</th>
                    <th width="30">#</th>
                    <th width="30">#</th>
                    <th width="30">#</th>
                </tr>
            </thead>
            <tbody>
                @foreach($infopages as $infopage)
                <tr>
                    <td>{{ $loop->iteration}}</td>
                    <td>
                        <strong>{{ $infopage->title }}</strong><hr>
                        <div class="slideup" data-id="{{ $infopage->id }}">
                            {{ substr(strip_tags($infopage->description), 0, 50) }}{{ strlen(strip_tags($infopage->description)) > 50 ? "..." : "" }}
                        </div>
                        <div class="body" style="display:none" data-id="{{ $infopage->id }}">
                            {!! $infopage->description !!}
                        </div>

                    </td>
                    <td>{{ url('/page', $infopage->slug) }}.html</td>
                    <td><a href="{{ url('/page', $infopage->slug) }}.html" class="btn btn-success">Detail</a></td>
                    <td><a href="/admin/infopage/{{ $infopage->slug }}/edit" class="btn btn-danger">Edit</a></td>
                    <td>
                        {{ Form::open(['url'=>'/admin/infopage/'.$infopage->slug,'method'=>'delete', 'onsubmit'=>"return confirm('Delete This Info Page?')"])}}
                        <button type="submit" class="btn btn-danger">Delete</button>
                        {{ Form::close()}}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@push('scripts')

<script src="https://demo.getstisla.com/assets/modules/datatables/datatables.min.js"></script>
<script src="https://demo.getstisla.com/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function(){
        $('#tabel-data').DataTable();
    });
</script>
<script>
    $(document).ready(function(){
        $('.slideup').click(function(){
            $(this).next('.body').slideDown();
            $(this).closest('.slideup').hide();
        });
        $('.body').click(function(){
            $('.body').slideUp();
            $('.slideup').show();
        });
    });
</script>
@endpush
