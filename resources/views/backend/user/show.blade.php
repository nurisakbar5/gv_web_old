@extends('layouts.app')
@section('title','Biodata Petani')
@section('content')
<div class="card">
    <div class="card">
        <div class="card-header">
            <h4>@yield('title')</h4>
        </div>
        
        <div class="card-body">
          <div class="row">
            <div class="col-md-3">
                <img src="{{ $farmer->photo}}" width="250">
            </div>
            <div class="col-md-8">
              <table class="table table-bordered">
                <tr>
                  <td>Name</td>
                  <td> {{ $farmer->name}}</td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td> {{ $farmer->email}}</td>
                </tr>
                <tr>
                  <td>Phone</td>
                  <td> {{ $farmer->phone }}</td>
                </tr>
                <tr>
                  <td>Address</td>
                  <td> {{ $farmer->address }}</td>
                </tr>
              </table>
            </div>
          </div>
          
          <div class="row">
            <div class="col-md-12">
              <h6>Daftar Kepemilikan Kebun Dan Sawah</h6>
              <table class="table table-bordered">
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>Address</th>
                  <th>Description</th>
                </tr>
                {{-- @foreach($farmer->list_farm as $farm) --}}
                {{-- <tr>
                  <td>{{ $loop->iteration}}</td>
                  <td>{{ $farm->name}}</td>
                  <td>{{ $farm->address}}</td>
                  <td>{{ $farm->description}}</td>
                </tr> --}}
                {{-- @endforeach --}}
                <tr>
                  <td>Nanti</td>
                  <td>Nanti</td>
                  <td>Nanti</td>
                  <td>Nanti</td>
                </tr>
              </table>
            </div>
          </div>
        </div>
        <div class="card-footer">
            <a href="/admin/user" title="Kembali" class="btn btn-info"><i class="fa fa-back"></i> Kembali</a>
        </div>  
    </div>
</div>
@endsection

