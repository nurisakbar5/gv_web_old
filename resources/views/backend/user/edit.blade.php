@extends('layouts.app')
@section('title','Edit User')
@section('content')
<div class="card">
    <div class="card">
        <div class="card-header">
            <h4>@yield('title')</h4>
        </div>

        <div class="card-body">

      {{ Form::model($category,['url'=>'/admin/category/'.$category->id,'method'=>'PUT'])}}

      @include('validation')

      @include('backend.category.form')

        </div>

      {{ Form::close()}}
    </div>
</div>
@endsection

