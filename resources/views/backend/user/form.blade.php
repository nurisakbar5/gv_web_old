<div class="form-group row">
    <label class="col-sm-3 col-form-label">Category Name</label>
    <div class="col-sm-9">
        {{ Form::text('name',null,['class'=>'form-control','placeholder'=>'Category Name'])}}
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Category Name</label>
    <div class="col-sm-9">
        {{ Form::select('entity',$entity,null,['class'=>'form-control'])}}
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-3 col-form-label"></label>
    <div class="col-sm-9">
        <button class="btn btn-primary"><i class="fas fa-save"></i> Create</button>
        <a href="/admin/category" class="btn btn-danger"><i class="fas fa-backward"></i> Back</a>
    </div>
</div>


