@extends('layouts.app')
@section('title','Create Category')
@section('content')
<div class="card">
    <div class="card">
        <div class="card-header">
            <h4>@yield('title')</h4>
        </div>
        
        <div class="card-body">

      {{ Form::open(['url'=>'/admin/category'])}}
              
      @include('validation')

      @include('backend.category.form')
                                
        </div>

      {{ Form::close()}}
    </div>
</div>
@endsection

