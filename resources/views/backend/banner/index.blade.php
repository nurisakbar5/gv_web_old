@extends('layouts.app')

@section('title', 'Data Banner')

@section('content')

<div class="card">
	<div class="card-header">
		<h4>@yield('title')</h4>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-md-3">
				<a href="/admin/banner/create" class="btn btn-danger">Create New</a>
			</div>
			<div class="col-md-9">

			</div>
		</div>
		<hr>

		@include('alert')

		<table class="table table-striped" id="tabel-data">
			<thead>
				<tr>
					<th width="10">#</th>
					<th>Banner Web</th>
					<th>Banner Mobile</th>
					<th>Deskripsi</th>
					<th width="30">#</th>
					<th width="30">#</th>
				</tr>
			</thead>
			<tbody>
				@foreach($banners as $banner)
				<tr>
					<td>{{ $loop->iteration}}</td>
					<td><img src="{{ $banner->image_web }}" width="200"></td>
					<td><img src="{{ $banner->image_mobile }}" width="200"></td>
					<td><strong>{{ $banner->name }}</strong><hr>
						{{ $banner->description }} </td>
						<td><a href="/admin/banner/{{ $banner->slug}}/edit" class="btn btn-danger">Edit</a></td>
						<td>
							{{ Form::open(['url'=>'/admin/banner/'.$banner->id,'method'=>'delete', 'onsubmit'=>"return confirm('Delete This Banner?')"])}}
							<button type="submit" class="btn btn-danger">Delete</button>
							{{ Form::close()}}
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

@endsection

@push('scripts')

<script src="https://demo.getstisla.com/assets/modules/datatables/datatables.min.js"></script>
<script src="https://demo.getstisla.com/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function(){
        $('#tabel-data').DataTable();
    });
</script>
@endpush