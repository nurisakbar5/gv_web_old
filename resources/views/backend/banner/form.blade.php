<?php
$btnLabel = Request::segment(3)=='create'?'Create':'Update';
?>

<div class="form-group row">
    <label class="col-sm-3 col-form-label">Banner Name</label>
    <div class="col-sm-9">
        {{ Form::text('name',null,['class'=>'form-control','placeholder'=>'Banner Name'])}}
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Description</label>
    <div class="col-sm-9">
        {{ Form::text('description',null,['class'=>'form-control','placeholder'=>'Description'])}}
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-3 col-form-label">Image For Web</label>
    <div class="col-sm-9">
        {{ Form::file('image_web',['class'=>'form-control'])}}
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-3 col-form-label">Image For Mobile</label>
    <div class="col-sm-9">
        {{ Form::file('image_mobile',['class'=>'form-control'])}}
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-3 col-form-label">Publish</label>
    <div class="col-sm-9">
        {{ Form::select('publish',['y'=>'Yes','n'=>'No'],null,['class'=>'form-control'])}}
    </div>
</div>

@if(Request::segment(4)=='edit')
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Old Image</label>
    <div class="col-sm-4">
        <img src="{{ $banner->image_web }}" width="300">
        <br>
        Image Label Web
    </div>
    <div class="col-sm-4">
            <img src="{{ $banner->image_mobile }}" width="300">
            <br>
        Image Label Mobile Apps
        </div>
</div>
@endif

<div class="form-group row">
    <label class="col-sm-3 col-form-label"></label>
    <div class="col-sm-9">
    <button class="btn btn-primary"><i class="fas fa-save"></i> {{ $btnLabel}}</button>
        <a href="/admin/banner" class="btn btn-danger"><i class="fas fa-backward"></i> Back</a>
    </div>
</div>


