@extends('layouts.app')
@section('title','Edit Banner')
@section('content')
<div class="card">
    <div class="card">
        <div class="card-header">
            <h4>@yield('title')</h4>
        </div>
        
        <div class="card-body">

      {{ Form::model($banner,['url'=>'/admin/banner/'.$banner->id,'files'=>true,'method'=>'PUT'])}}
              
      @include('validation')

      @include('backend.banner.form')
                                
        </div>

      {{ Form::close()}}
    </div>
</div>
@endsection

