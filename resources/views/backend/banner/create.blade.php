@extends('layouts.app')
@section('title','Create Banner')
@section('content')
<div class="card">
    <div class="card">
        <div class="card-header">
            <h4>@yield('title')</h4>
        </div>
        
        <div class="card-body">

      {{ Form::open(['url'=>'/admin/banner','files'=>true])}}
              
      @include('validation')

      @include('backend.banner.form')
                                
        </div>

      {{ Form::close()}}
    </div>
</div>
@endsection

