@extends('layouts.app')
@section('title','Create Market')
@section('content')
<div class="card">
    <div class="card">
        <div class="card-header">
            <h4>@yield('title')</h4>
        </div>

        <div class="card-body">

      {{ Form::open(['url'=>'/admin/market'])}}

      @include('validation')

      @include('backend.market.form')

        </div>

      {{ Form::close()}}
    </div>
</div>


@push('scripts')
{{--
<script>
$(document).ready( function() {

  function sendLocation(position) {
    var lat = position.coords.latitude;
    var lon = position.coords.longitude;

    // AJAX
    $.ajax({
      url: "/sendLocation",
      type: "GET",
      data: {
        "lat" : lat,
        "lon" : lon,
      },
      success: function(response) {
        consoloe.log("success : ", response.message);
      },
      error: function(response) {
        console.log("error : ", response.message);
      }
    });
  }

  if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(sendLocation);
  } else {
    alert("Geolocation is not supported by this browser.");
  }

});
</script>
--}}
<script>
$('.province').change(function() {
        const id = $(this).val();

        $.ajax({
            url: '/regency/' + id,
            beforeSend: function() {
                $('.cities option').remove();
                $('.cities').append('<option value="">Memuat ...</option>');
            },
            success: function(data) {
                let option = '<option value="{id}">{name}</option>';

                $('.cities option').remove();
                $('.cities').append('<option value="">Pilih Kota/Kabupaten</option>');
                data.forEach(function(item) {
                    $('.cities').append(
                    option.replace(/{id}/g, item.id).replace(/{name}/g, item.name)
                    );
                });
            },
            complete: function() {

            }
        });
    });
</script>
@endpush
@endsection

