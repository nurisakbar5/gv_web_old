<?php
$btnLabel = Request::segment(3)=='create'?'Create':'Update';
?>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Nama Pasar</label>
    <div class="col-sm-9">
        {{ Form::text('name',null,['class'=>'form-control','placeholder'=>'Name'])}}
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-3 col-form-label">Lokasi</label>
    <div class="col-sm-9">
        <select class="form-control mb-3 province" style="padding: 0 !important;">
            <option value="">Pilih Provinsi</option>
            @foreach($provinces->data as $province)
            <option value="{{ $province->id ?? '' }}"> {{ $province->name ?? $province }}</option>
            @endforeach
        </select>
        <select name="regency_id" class="form-control mb-3 cities" required="" style="padding: 0 !important;">
            <option value="">Pilih provinsi dahulu</option>
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-3 col-form-label"></label>
    <div class="col-sm-9">
        <button class="btn btn-primary"><i class="fas fa-save"></i> {{ $btnLabel }}</button>
        <a href="/admin/market" class="btn btn-danger"><i class="fas fa-backward"></i> Back</a>
    </div>
</div>
